﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseSettingsVolume : MonoBehaviour
{
    public AudioSource src;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        src.volume = PlayerPrefs.GetFloat("MasterVolume", 0.7f) * PlayerPrefs.GetFloat("MusicVolume", 0.7f);
    }
}
