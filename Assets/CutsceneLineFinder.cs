﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneLineFinder : MonoBehaviour
{
    public Globals g;
    public string SearchTerm;
    [NaughtyAttributes.Button]
    public void Search()
    {
        foreach (var cs in g.All<CutsceneData>())
        {
            foreach (var line in cs.GetScript().Split(new string[] { "\n" }, StringSplitOptions.None))
            {
                if (line.Contains(SearchTerm))
                    Debug.Log(line, cs);
            }
        }
    }
}
