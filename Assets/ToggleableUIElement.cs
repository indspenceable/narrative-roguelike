﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToggleableUIElement : MonoBehaviour
{
    public Transform ActivePosition;
    public Transform InactivePosition;
    public Transform Container;
    public AnimationCurve movementCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    public float movementDuration = 0.25f;
    public bool activated { get; private set; }
    private bool animating;

    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }
    public BoolEvent onActivatedChanged;

    private void Start()
    {
        activated = false;
        animating = false;
        Container.position = InactivePosition.position;
    }

    public void Toggle()
    {
        if (animating) return;
        if (activated)
        {
            TriggerDeactivation();
        }
        else
        {
            TriggerActivation();
        }
    }

    public void SetActive(bool b)
    {
        if (activated != b) Toggle();
    }

    private void DoneAnimating()
    {
        this.animating = false;
    }

    public void TriggerActivation()
    {
        if (animating || activated) return;
        animating = true;
        activated = true;
        StartCoroutine(Util.MoveTransformToLocalPosition(Container, InactivePosition.localPosition, ActivePosition.localPosition, movementCurve, movementDuration, DoneAnimating));
        onActivatedChanged.Invoke(true);
    }
    public void TriggerDeactivation()
    {
        if (animating || !activated) return;
        animating = true;
        activated = false;
        StartCoroutine(Util.MoveTransformToLocalPosition(Container, ActivePosition.localPosition, InactivePosition.localPosition, movementCurve, movementDuration, DoneAnimating));
        onActivatedChanged.Invoke(false);
    }

}
