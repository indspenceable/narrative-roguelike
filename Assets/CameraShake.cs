﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public enum Magnitude
    {

        ZERO = 0,
        ONE = 1,
        FIVE = 2,
        ELEVEN = 3,
        CUTSCENE = 4,
        LONG_AND_LOW = 5,
    }

    private float[] translation1 = new float[]
    {
       0f, 0.1f, 0.2f, 0.5f, 0.5f, 5f
    };
    private float[] translation2 = new float[]
    {
       0f, 0.1f, 0.25f, 0.5f, 1.5f, 0.1f
    };


    public void Shake(Magnitude m)
    {
        StartCoroutine(DoShake(translation1[(int)m], translation2[(int)m]));
    }
    public IEnumerator ShakeCO(Magnitude m)
    {
        yield return StartCoroutine(DoShake(translation1[(int)m], translation2[(int)m]));
    }
    private IEnumerator DoShake(float duration, float initialMagnitude = 0.25f)
    {
        float dt = 0f;
        while (dt < duration)
        {
            yield return null;
            dt += Time.deltaTime;
            transform.localPosition = Random.insideUnitCircle * Mathf.Lerp(initialMagnitude, 0f, dt / duration);
        }
    }
    [NaughtyAttributes.Button]
    public void DoSmallShake()
    {
        Shake(Magnitude.ONE);
    }
    [NaughtyAttributes.Button]
    public void DoLargeShake()
    {
        Shake(Magnitude.ELEVEN);
    }
}
