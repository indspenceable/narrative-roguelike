﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SupportsMenu : MonoBehaviour
{
    public TMPro.TextMeshProUGUI CharacterName;
    public Button exitButton;
    public Button ButtonPrefab;
    public Globals g;
    public MetaProgression mp;
    public CutsceneMode cutsceneMode;
    public CampaignGameMode msm;
    public TransitionEffect te;
    public Transform container;
    private List<Button> SpawnedButtons = new List<Button>();
    public void Show(UnitData character, List<UnitData> team)
    {
        CharacterName.text = character.CharacterName;
        foreach (var support in g.All<SupportConversation>().Where(s => ConfirmDisplaySupport(character, s, team.Select(u => u.CharacterName).ToList()))){
            var B = Instantiate(ButtonPrefab, container);
            var btnText = B.GetComponentInChildren<TMPro.TextMeshProUGUI>();
            var requiredCharacters = support.RequiredCharacters.Where(c => character.Identifier != c).ToList();
            btnText.text = support.name;
            if (!mp.GetBool(support._HiddenIdentifier))
            {
                btnText.text += " (NEW)";
            }
            B.onClick.AddListener(() =>
            {
                mp.SetBool(support._HiddenIdentifier, true);
                cutsceneMode.ActivateWithData(te, msm.campaign, support, () => msm.ActivateWithData(te, msm.campaign));
            });
        }
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        foreach (var b in SpawnedButtons) Destroy(b.gameObject);
        SpawnedButtons.Clear();
        this.gameObject.SetActive(false);
    }

        private bool ConfirmDisplaySupport(UnitData character, SupportConversation s, List<string> currentMembers)
        {
        bool a = s.RequiredCharacters.Contains(character.CharacterName);
        bool b = s.RequiredCharacters.All(c_ => currentMembers.Contains(c_));
        bool c = mp.GetInt(mp.CharacterGroupKey(s.RequiredCharacters.ToArray())) >= s.RequiredSupportLevel;
        bool d = mp.GetBool(mp.CharacterGroupKey(character.CharacterName, "Nytmur"));
            return a && b && c && d;
        }
    }
