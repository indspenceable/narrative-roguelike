﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    public float SpinSpeed = Mathf.PI / 2f;
    public MapUnit mu;
    public SpriteRenderer sr;
    public Color Friendly;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (mu.data.Friendly && mu.data.Spent == false && !mu.data.DoesNotAct && mu.manager.IsPlayerTurn)
        {
            sr.color = Friendly;
        }
        else
            sr.color = Color.clear;
        transform.Rotate(Vector3.forward, SpinSpeed);
    }
}
