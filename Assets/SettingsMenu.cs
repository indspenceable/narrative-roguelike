﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public StatelessGameMode mainMenuMode;
    public TransitionEffect te;
    public Image i;
    public float duration = 0.25f;
    
    private void OnEnable()
    {
        i.color = Color.black;
        i.CrossFadeAlpha(0, 0.1f, true);
        i.raycastTarget = false;
    }

    public void SaveAndQuitGame()
    {
        this.mainMenuMode.ActivateWithoutData(te);
    }

    public void OnMenuActivatedChanged(bool b)
    {
        i.raycastTarget = b;
        float alpha = b ? 0.5f : 0f;
        Debug.Log("We're fading to: " + alpha);
        i.CrossFadeAlpha(alpha, duration, true);
    }
}
