﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class SnakeAI : AIStrategy
{
    private const int penalty = -999999;

    public static bool CHECK(Vector2Int c)
    {
        return (Mathf.Abs(c.x) % 2) == (Mathf.Abs(c.y) % 2);
    }

    public override float ScorePlan(MapUnit currentUnit, AIActionPlan plan)
    {
        CaveSnake snake = currentUnit as CaveSnake;

        var lastPos = currentUnit.Pos();
        var facing = snake.Facing();

        if (snake.GetSnakeData().SegmentLocations.Last() == snake.data.pos + facing)
        {
            return penalty;
        }


        int count = plan.path.Count;
        if (count > 1)
        {
            facing = plan.path[count - 1] - plan.path[count - 2];
            lastPos = plan.path[count - 1];
        }
        if (plan.skill != null)
        {
            if (facing.x > 0 && plan.target.x <= lastPos.x) return penalty;
            if (facing.x < 0 && plan.target.x >= lastPos.x) return penalty;
            if (facing.y > 0 && plan.target.y <= lastPos.y) return penalty;
            if (facing.y < 0 && plan.target.y >= lastPos.y) return penalty;
        }

        return base.ScorePlan(currentUnit, plan);
    }

    public override Dictionary<Vector2Int, List<Vector2Int>> Explore(MapUnit currentUnit, TBSGameManager manager)
    {
        return ExploreAllPathsFromSnake(currentUnit, 8);
    }

    public Dictionary<Vector2Int, List<Vector2Int>> ExploreAllPathsFromSnake(MapUnit u, int distance)
    {
        var map = u.manager.map;
        var directory = u.manager.directory;

        Queue<List<Vector2Int>> Paths = new Queue<List<Vector2Int>>();
        Paths.Enqueue(new List<Vector2Int>() { u.data.pos });

        Dictionary<Vector2Int, List<Vector2Int>> rtn = new Dictionary<Vector2Int, List<Vector2Int>>();
        rtn.Add(u.data.pos, new List<Vector2Int>() { u.data.pos });

        while (Paths.Count > 0)
        {
            var cp = Paths.Dequeue();
            var dest = cp[cp.Count - 1];
            var tile = map.TileAt(dest);
            foreach (var _neighbor in tile.Neighbors.Shuffled())
            {
                var direction = _neighbor - tile.pos;
                var neighbor = _neighbor + direction;
                if (map.TileAt(neighbor) == null) continue;

                if (!cp.Contains(neighbor) &&
                    !rtn.ContainsKey(neighbor) &&
                    distance > cp.Count - 1)
                {
                    if (u.CheckOffsets(neighbor - u.data.pos) && u.CheckOffsets(_neighbor - u.data.pos))
                    {
                        var np = cp.Append(_neighbor).Append(neighbor).ToList();

                        if (u.CanEnter(_neighbor - u.data.pos))
                            Paths.Enqueue(np);

                        rtn.Add(neighbor, np);
                    }
                }
            }
        }
        return rtn;
    }
}
