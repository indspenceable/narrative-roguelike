﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Lion/Pollen Puff")]
public class PollenPuff : AbstractSelfSkillTalent
{
    public OnHitEffect PlayerUnitsHit;
    public UnitDefinition Summonable;
    public AudioClip SFX;
    public RuntimeAnimatorController EffectRAC;

    public RuntimeAnimatorController ThornRAC;

    public override IEnumerator Use(MapUnit Me)
    {
        var PuffTarget = Me.manager.FindClosestAvailableSpotTo(Vector2Int.zero);
        yield return Me.manager.cc.FocusOn(PuffTarget.CenterOfTile());


        var puff = Me.manager.animatorPool.RequestWithAnimator(PuffTarget.CenterOfTile(), ThornRAC);

        puff.sr.sortingOrder = BaseBuilderPropPoolable.CalculateSortingOrder(puff.sr.transform);
        puff.sr.sortingLayerName = "FX";
        while (puff.anim.GetBool("Busy")) yield return null;

        yield return new WaitForSeconds(0.5f);

            // Hits all the players units for some damage
        foreach (var u in Me.manager.directory.Query(u => u.data.Friendly))
        {
            yield return AttackTalent.ExecuteAttackAll(PlayerUnitsHit, Me, u.Pos(), true);
        }

        puff.anim.SetTrigger("Done");

        for (int i = 0; i < 1; i += 1)
        {
            Vector2Int target = SelectRandomLandPoint(Me);
            yield return Me.manager.cc.FocusOn(target.CenterOfTile());
            Me.manager.directory.BuildMapUnit(Me.manager.globals.Duplicate(Summonable.Data), target, false);
            Me.manager.audioPool.PlaySimple(SFX);
            Me.manager.animatorPool.RequestWithAnimator(target.CenterOfTile(), this.EffectRAC);
            yield return new WaitForSeconds(1f);
        }

    }

    private Vector2Int SelectRandomLandPoint(MapUnit Me)
    {
        return Me.manager.map.Tiles.Where(t => t.MovementAllowed(UnitData.MobilityType.LAND)).Shuffled().First().pos;
    }
}
