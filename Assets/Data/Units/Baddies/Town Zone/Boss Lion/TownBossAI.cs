﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Lion")]
public class TownBossAI : RotateAttacksStrategy
{
    [System.Serializable]
    private class TownAIData
    {
        public int position = 0;
        public List<Vector2Int> PositionTileLocations;
        public List<Vector2Int> PositionFacing;

        internal void RecalculatePositions()
        {
            this.PositionTileLocations = new List<Vector2Int>()
            {
                MapManager.TileLocationFromTransform(GameObject.Find("BOSS NORTH").transform),
                MapManager.TileLocationFromTransform(GameObject.Find("BOSS WEST").transform),
                MapManager.TileLocationFromTransform(GameObject.Find("BOSS EAST").transform),
                MapManager.TileLocationFromTransform(GameObject.Find("BOSS SOUTH").transform),
            };
        }
    }

    public override IEnumerator SelectAndEnactPlan(MapUnit cu)
    {
        yield return cu.manager.cc.FocusOn(cu);

        // Select a position to warp to.
        var TargetPosition = UnityEngine.Random.Range(0, 4);
        TownAIData townAIData = GetData(cu);
        //Debug.Log("Target spot is : " + TargetPosition);


        if (TargetPosition != townAIData.position)
        {
            cu.CharacterAnimator.Play("WarpOut");
            yield return null;
            while (cu.CharacterAnimator.GetBool("Busy")) yield return new WaitForEndOfFrame();


            //We need to move to that spot!
            townAIData.RecalculatePositions();
            cu.MoveTo(townAIData.PositionTileLocations[TargetPosition], true);
            var facing = townAIData.PositionFacing[TargetPosition];
            cu.CharacterAnimator.SetFloat("Facing_X", facing.x);
            cu.CharacterAnimator.SetFloat("Facing_Y", facing.y);

            yield return cu.manager.cc.FocusOn(cu);
            cu.CharacterAnimator.Play("WarpIn");
            townAIData.position = TargetPosition;
            cu.data.AIDataCache["TownAIData"] = townAIData;
        }

        //yield return null;
        //if (cu != null)
        //    cu.Spent = true;
        //cu.manager.cc.ControlsDisabled = false;
        //cu.manager.InstallDefaultHandler();
        yield return base.SelectAndEnactPlan(cu);
    }

    private TownAIData GetData(MapUnit cu)
    {
        object data = new TownAIData();
        if (!cu.data.AIDataCache.TryGetValue("TownAIData", out data))
        {
            data = new TownAIData();
            TownAIData currentTownAIData = data as TownAIData;
            currentTownAIData.PositionTileLocations = new List<Vector2Int>()
            {
                MapManager.TileLocationFromTransform(GameObject.Find("BOSS NORTH").transform),
                MapManager.TileLocationFromTransform(GameObject.Find("BOSS WEST").transform),
                MapManager.TileLocationFromTransform(GameObject.Find("BOSS EAST").transform),
                MapManager.TileLocationFromTransform(GameObject.Find("BOSS SOUTH").transform),
            };
            currentTownAIData.PositionFacing = new List<Vector2Int>()
            {
                Vector2Int.down,
                Vector2Int.right,
                Vector2Int.left,
                Vector2Int.up,
            };
        }
        var aiData = data as TownAIData;
        return aiData;
    }
}
