﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Lion/Thron")]
public class LionThornWhip : AttackTalent
{
    public RuntimeAnimatorController ThornRAC;

    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        target = Me.manager.FindClosestAvailableSpotTo(target);
        yield return Me.manager.cc.FocusOn(target.CenterOfTile());
        var thorn = Me.manager.animatorPool.RequestWithAnimator(target.CenterOfTile(), ThornRAC);
        thorn.sr.sortingOrder = BaseBuilderPropPoolable.CalculateSortingOrder(thorn.sr.transform);
        thorn.sr.sortingLayerName = "FX";
        yield return null;
        while (thorn.anim.GetBool("Busy")) yield return null;
        yield return Me.BeforeUseAttack();
        yield return ExecuteAttackAll(onHit, Me, target, true);
        thorn.anim.SetTrigger("Done");
    }
}
