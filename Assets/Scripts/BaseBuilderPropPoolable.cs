﻿using System;
using System.Linq;
using UnityEngine;

public class BaseBuilderPropPoolable : AbstractPoolable
{
    public SpriteRenderer sr;
    public BaseBuilderPropPlacement backingPlacement;
    private Transform StandardParent;
    public bool RequiresRefresh = false;
    
    public BaseBuilderPropManager bbpm;

    public override void ExternalDeactivate()
    {
        base.ExternalDeactivate();
        transform.parent = StandardParent;
    }

    public override void OnActivatedByPool(AbstractObjectPool pool)
    {
        base.OnActivatedByPool(pool);
        StandardParent = this.transform.parent;
        RequiresRefresh = true;
    }

    private string SelectSortingLayer()
    {
        if (backingPlacement.propDef.layer == BaseBuilderPropDefinition.LOCATION_CATEGORY.FLOOR) return "FLOOR";
        if (backingPlacement.propDef.layer == BaseBuilderPropDefinition.LOCATION_CATEGORY.TABLE) return "OBJECT";
        if (backingPlacement.propDef.layer == BaseBuilderPropDefinition.LOCATION_CATEGORY.OBJECT) return "OBJECT";
        if (backingPlacement.propDef.layer == BaseBuilderPropDefinition.LOCATION_CATEGORY.ORNAMENT) return "ORNAMENT";
        return "ORNAMENT";
    }

    public void Update()
    {
        if (RequiresRefresh)
        {
            var table = bbpm.PoolablesAtLocation(backingPlacement.position).Find(poolable => poolable.backingPlacement.propDef.layer == BaseBuilderPropDefinition.LOCATION_CATEGORY.TABLE && poolable != this);
            int tableHeight = 0;
            if (table != null)
                tableHeight = table.backingPlacement.propDef.TableHeight;
            
            sr.transform.localPosition = backingPlacement.propDef.SpriteOffsetPositionAuto + (Vector2.up*tableHeight/16f);
            sr.sortingOrder = CalculateSortingOrder(sr.transform);
            sr.sortingLayerName = SelectSortingLayer();
            RequiresRefresh = false;
        }
    }

    public static int CalculateSortingOrder(Transform t)
    {
        return -Mathf.FloorToInt(t.position.y * 100);
    }
}