﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AssembleTeamCharacterDetailsPane : MonoBehaviour
{
    public TMPro.TextMeshProUGUI Description;
    public Image portraitDisplay;
    private UnitDefinition currentCharacter;
    public Localizer localizer;

    private UnitDefinition hovered;
    private UnitDefinition focused;
    private bool needsUpdate = false;

    public TMPro.TextMeshProUGUI CharacterName;
    public Image ComplexitySticky;
    public Image[] ComplexityStars;
    public Sprite DefaultStar;

    public AssembleTeamManager manager;


    public Sprite readyPortrait;

    public void LateUpdate()
    {
        if (!needsUpdate) return;
        if (focused != null)
        {
            SHOW(focused);
        } else if (hovered != null)
        {
            SHOW(hovered);
        } else
        {
            HIDE();
        }
        needsUpdate = false;
    }

    private void SHOW(UnitDefinition character)
    {
        currentCharacter = character;
        Description.text = localizer._($"characters.{character.Data.Identifier}.description");
        portraitDisplay.sprite = character.Data.presentation.persona.DefaultPortrait;
        portraitDisplay.SetNativeSize();

        portraitDisplay.gameObject.SetActive(true);
        Description.gameObject.SetActive(true);
        CharacterName.text = character.Data.CharacterName;
        ComplexitySticky.sprite = character.Data.presentation.NametagSticky;
        ComplexitySticky.gameObject.SetActive(true);
        for (int i = 0; i < 3; i += 1)
        {
            if (character.Data.Complexity > i)
            {
                ComplexityStars[i].sprite = character.Data.presentation.StarSprite;
            } else
            {
                ComplexityStars[i].sprite = DefaultStar;
            }
        }
    }

    //public void HIDE(UnitDefinition character)
    //{
    //    if (currentCharacter == character)
    //    {
    //        ForceHide();
    //    }
    //}
    public void HIDE()
    {
        if (manager.SelectedCharacters().Count() == 4) {
            portraitDisplay.sprite = readyPortrait;
            portraitDisplay.SetNativeSize();
            portraitDisplay.gameObject.SetActive(true);
        } else {
            portraitDisplay.gameObject.SetActive(false);
        }
        Description.gameObject.SetActive(false);
        currentCharacter = null;
        CharacterName.text = "";
        ComplexitySticky.gameObject.SetActive(false);
    }

    internal void EndFocus()
    {
        this.focused = null;
        needsUpdate = true;
    }

    internal void StartFocus(UnitDefinition character)
    {
        this.focused = character;
        needsUpdate = true;
    }

    internal void EndHover()
    {
        this.hovered = null;
        needsUpdate = true;
    }

    internal void StartHover(UnitDefinition character)
    {
        this.hovered = character;
        needsUpdate = true;
    }
}