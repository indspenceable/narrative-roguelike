﻿using static TBSGameManager;

public class NoOp : InputHandler
{
    public void Click(int mouseButton) { }

    public void Install() { }

    public void Uninstall() { }

    public void Update() {}
}
