﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static AttackTalent;

[CreateAssetMenu(menuName = "Talents/Buffs/SelfAttackAtStartOfTurn")]
public class TakeDamageAtStartOfTurn : BuffDefinition
{
    public OnHitEffect Onhit;
    public override IEnumerator StartOfTurn(IAttacker target, IAttacker Stage, Vector2Int pos, int stacks)
    {
        return ExecuteAttackAll(Onhit, Stage, pos, false);
    }
   
}
