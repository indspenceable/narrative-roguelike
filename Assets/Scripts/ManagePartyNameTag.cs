﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagePartyNameTag : MonoBehaviour
{
    public Image image;
    public TMPro.TextMeshProUGUI textDisplaySmall;
    

    public Sprite sprite
    {
        get { return image.sprite; }
        set { image.sprite = value; }
    }

    internal void SetText(string identifier, TMPro.TMP_FontAsset font, int fs)
    {
        textDisplaySmall.text = identifier;
        textDisplaySmall.font = font;
        textDisplaySmall.fontSize = fs;
    }
}
