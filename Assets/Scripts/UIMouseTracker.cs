﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIMouseTracker : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    public TBSGameManager gameManager;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
            gameManager.CurrentHandler.Click(0);
        if (eventData.button == PointerEventData.InputButton.Right)
            gameManager.CurrentHandler.Click(1);
    }
    
    public bool Hovered = false;
    public void OnPointerEnter(PointerEventData eventData)
    {
        Hovered = false;
        //gameManager.globals.SetMouseHighlight(Hovered);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Hovered = true;
        //gameManager.globals.SetMouseHighlight(Hovered);
    }

    public bool Dragging = false;
    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
            Dragging = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Dragging = false;
    }
}
