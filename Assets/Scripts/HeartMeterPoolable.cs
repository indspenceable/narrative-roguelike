﻿using UnityEngine;
using UnityEngine.UI;

public class HeartMeterPoolable : AbstractPoolable
{
    public HeartMeterPool pool;
    public Image img;
    public RectTransform rt;

    public override void ExternalDeactivate()
    {
        transform.SetParent(pool.opm.transform);
        base.ExternalDeactivate();
    }
}