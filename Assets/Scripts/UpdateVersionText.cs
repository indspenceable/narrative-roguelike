﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateVersionText : MonoBehaviour
{
    public TMPro.TextMeshProUGUI TextDisplay;
    public Globals g;

    public void Start()
    {
        TextDisplay.text = $"Version {g.VersionNumber}";
    }

    private void OnValidate()
    {
        if (TextDisplay != null && g != null)
            TextDisplay.text = $"Version {g.VersionNumber}";
    }
}
