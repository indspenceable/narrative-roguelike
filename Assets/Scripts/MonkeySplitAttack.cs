﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Lavamonkey/Split")]
public class MonkeySplitAttack : AbstractTargetedSkillTalent
{
    public List<UnitDefinition> Summonables = new List<UnitDefinition>();
    public List<BuffInstance> InitialBuffs = new List<BuffInstance>();
    public AudioPool audioPool;
    public AudioClip clip;
    public AnimatorPool animatorPool;
    public RuntimeAnimatorController effectRAC;

    public override bool TargetsFriendly => false;
    public override bool TargetsHostile => false;
    public override bool TargetsEmpty => true;
    public override bool TargetsSelf => false;

    public int clouds = 3;
    public int Radius = 7;

    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        var allSummonables = Summonables.ToList();
        List<Vector2Int> Locations = new List<Vector2Int>();
        for (int x = -Radius; x <= Radius; x += 1)
        {
            for (int y = -Radius; y <= Radius; y += 1)
            {
                var Summonable = Summonables[0];
                Vector2Int pos = target + new Vector2Int(x, y);
                var mapTile = Me.manager.map.TileAt(pos);
                if ((mapTile != null) && mapTile.MovementAllowed(Summonable.Data.Mobility) && Me.manager.directory.UnitAt(pos) == null)
                    Locations.Add(pos);
            }
        }
        int summonHP = (Me.MaxHP() - Me.data.currentDamage)/Summonables.Count;
        Locations.Shuffle();
        for (int i = 0; i < Summonables.Count; i += 1) {
            var mu = SummonSkillTalent.ExecuteSummon(Me, Locations[i], Summonables[i].Data, InitialBuffs, new List<CharacterAnimationData>());
            this.audioPool.PlaySimple(clip);
            this.animatorPool.RequestWithAnimator(target.CenterOfTile(), this.effectRAC);
            mu.data.currentDamage = mu.MaxHP() - summonHP;
            mu.data.Cooldowns.Add("Boss", 3);
            yield return null;
        }
        Me.CharacterAnimator.Play("LavaMonkeyDisappear");
        yield return new WaitUntil(() => !Me.CharacterAnimator.GetBool("Busy"));
        Me.manager.directory.RemoveUnitWithoutAnimation(Me);
    }

    public override bool ValidTargetableTileContents(MapUnit Me, Vector2Int v2i)
    {
        MapTile mapTile = Me.manager.map.TileAt(v2i);
        return base.ValidTargetableTileContents(Me, v2i) && ((mapTile != null) && mapTile.MovementAllowed(UnitData.MobilityType.LAND));
    }
}