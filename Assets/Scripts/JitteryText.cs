﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Text Effects/Jitter")]
public class JitteryText : TextEffect
{
    public float MovementDistanceScaler = 1f;
    public float StepDelay = 1f;

    private static List<KeyValuePair<Vector3, float>?> Memory = new List<KeyValuePair<Vector3, float>?>();

    internal override void Process(int vertexIndex, int characterIndex, List<Vector3> vertices)
    {
        Vector3 NewRandomValue = (Vector3)Random.insideUnitCircle * MovementDistanceScaler;

        while (Memory.Count <= characterIndex) Memory.Add(null);
        if (!Memory[characterIndex].HasValue || Time.unscaledTime - Memory[characterIndex].Value.Value > StepDelay)
            Memory[characterIndex] = new KeyValuePair<Vector3, float>(NewRandomValue, Time.unscaledTime);

        vertices[vertexIndex]     += Memory[characterIndex].Value.Key;
        vertices[vertexIndex + 2] += Memory[characterIndex].Value.Key;
        vertices[vertexIndex + 3] += Memory[characterIndex].Value.Key;
        vertices[vertexIndex + 1] += Memory[characterIndex].Value.Key;
    }
}