﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CameraControlsTBS : CameraControlsBase
{
    public TBSGameManager manager;
    public override Transform BL => GetBounds.GetBL();
    public override Transform TR => GetBounds.GetTR();
    public override IHasBounds GetBounds => manager.LevelInstance;




    public IEnumerator FocusOn(MapUnit u)
    {
        return FocusOn(u.CenterOfSprite());
    }

    public IEnumerator FocusOn(Vector3 TargetPosition)
    {
        CurrentlyFocusing = true;
        
        var distance = Vector3.Distance(CurrentPosition, TargetPosition);
        var delta = TargetPosition - CurrentPosition;
        float duration = distance / MovementSpeed;
        float dt = 0;
        var sp = CurrentPosition;
        while (dt <= duration)
        {
            yield return null;
            dt += Time.deltaTime;
            CurrentPosition = sp + delta * FocusAnimationCurve.Evaluate(dt / duration);
        }
        CurrentPosition = TargetPosition;
        CurrentlyFocusing = false;
    }
}
public abstract class CameraControlsBase : MonoBehaviour
{
    public float XBuffer = 7;
    public float YBuffer = 2;

    public abstract IHasBounds GetBounds { get; }
    public abstract Transform BL { get; }
    public abstract Transform TR { get; }

    public float MovementSpeed;

    // Start is called before the first frame update
    void Start()
    {
        CurrentPosition = transform.position;
    }

    private void OnDrawGizmos()
    {
        if (GetBounds == null) return;
        if (BL == null || TR == null) return;
        var bl = BL.transform.position + new Vector3(XBuffer, YBuffer);
        var tr = TR.transform.position - new Vector3(XBuffer, YBuffer);
        Gizmos.color = Color.green;
        Gizmos.DrawLine(bl, new Vector3(bl.x, tr.y));
        Gizmos.DrawLine(tr, new Vector3(bl.x, tr.y));
        Gizmos.DrawLine(bl, new Vector3(tr.x, bl.y));
        Gizmos.DrawLine(tr, new Vector3(tr.x, bl.y));
    }

    protected Vector3 CurrentPosition;
    protected bool CurrentlyFocusing = false;
    public AnimationCurve FocusAnimationCurve;
    public bool ControlsDisabled = false;

    // Update is called once per frame
    void LateUpdate()
    {
        //if (manager.hud.MouseInput.Dragging) {
        //    transform.position += manager.hud.DragStart +  ;
        //}
        if (!(CurrentlyFocusing || ControlsDisabled))
        {
            Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            CurrentPosition += input * Time.deltaTime * MovementSpeed;
        }
        CurrentPosition.x = Mathf.Max(BL.position.x + XBuffer, CurrentPosition.x);
        CurrentPosition.y = Mathf.Max(BL.position.y + YBuffer, CurrentPosition.y);
        CurrentPosition.x = Mathf.Min(TR.position.x - XBuffer, CurrentPosition.x);
        CurrentPosition.y = Mathf.Min(TR.position.y - YBuffer, CurrentPosition.y);
        transform.position = new Vector3(Util.SmartFloor(CurrentPosition.x, 16), Util.SmartFloor(CurrentPosition.y, 16));
    }

}
