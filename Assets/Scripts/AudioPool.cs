﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Object Pools/Audio")]
public class AudioPool : ObjectPoolBase<AudioPoolable>
{
    public float PlaySimplePct = 0.85f;
    private List<AudioClip> playedThisFrame = new List<AudioClip>();
    public SoundConfig sfx;

    public AudioPoolable Play(AudioClip clip, float v, float pitch)
    {
        if (playedThisFrame.Contains(clip)) return null;
        var rtn = Request(Vector3.zero);
 
        rtn.Play(clip, VolumeForSFX(v), pitch);
        playedThisFrame.Add(clip);
        return rtn;
    }
    public void PlaySimple(AudioClip clip)
    {
        Play(clip, PlaySimplePct, 1f);
    }
    public void PlayQuiet(AudioClip clip)
    {
        Play(clip, PlaySimplePct/3f, 1f);
    }
    public override void OnUpdate()
    {
        playedThisFrame.Clear();
    }
    public override bool RequiresUpdate()
    {
        return true;
    }

    public float VolumeForSFX(float v)
    {
        return v * PlayerPrefs.GetFloat("MasterVolume", 0.7f) * PlayerPrefs.GetFloat("SFXVolume", 1f);
    }
}