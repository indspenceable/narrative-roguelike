﻿using UnityEngine;

[CreateAssetMenu]
public class SoundConfig : ScriptableObject
{
   [Header("Music")]
    public AudioClip MenuMusic;
    public AudioClip BetweenRoomDowntime;
    public AudioClip WinMusic;
    [Header("SFX")]

    public AudioClip SelectUnit;
    public AudioClip Cancel;
    public AudioClip wake;
    public AudioClip OnDeathSFX;
    public AudioClip WaitSound;
    public AudioClip StandardBeep;
    public AudioClip Thud;
    public AudioClip PlayerTurnStart;
    public AudioClip EnemyTurnStart;
    public AudioClip OpenChestSFX;
    public AudioClip SadChestSound;
}
