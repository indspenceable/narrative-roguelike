﻿using UnityEngine;

public abstract class AbstractPoolable : MonoBehaviour
{
    protected AbstractObjectPool m_Pool;
    public virtual void OnActivatedByPool(AbstractObjectPool pool)
    {
        gameObject.SetActive(true);
        m_Pool = pool;
    }
    public virtual void ExternalDeactivate()
    {
        m_Pool.Released(this);
        if(gameObject != null)
            gameObject.SetActive(false);
    }
}
