﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class SetScaleFactorFromPPC : MonoBehaviour
{
    public PixelPerfectCamera ppc;
    public CanvasScaler scaler;
    private void Update()
    {
        //Debug.Log(this, gameObject);
        scaler.scaleFactor = ppc.pixelRatio;
    }
}
    