﻿using System.Collections;
using UnityEngine;
using static AttackTalent;

[CreateAssetMenu(menuName = "Talents/Special/Snake/Ensnared Status")]
public class Ensnared : BuffDefinition
{
    public override IEnumerator StartOfTurn(IAttacker target, IAttacker Stage, Vector2Int pos, int stacks)
    {
        // don't trigger on the last turn!
        if (target != null && stacks > 0)
        {
            target.manager.bouncyTextPool.RequestWithString(target.manager.cameraBlit.GetComponent<Camera>(), target.CenterOfSprite(), "Trapped!", Color.black, Color.white);
            target.Spent = true;
        }
        yield return null;
    }
    public override IEnumerator OnBuffExpired(MapUnit Me, IAttacker Stage)
    {
        yield return null;
        Me.MoveTo(Me.manager.FindClosestAvailableSpotTo(Me.data.pos), true);
    }
}