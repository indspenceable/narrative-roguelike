﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static AttackTalent;

[CreateAssetMenu(menuName ="Talents/Status/Stun")]
public class Stun : BuffDefinition
{
    public override IEnumerator StartOfTurn(IAttacker target, IAttacker Stage, Vector2Int pos, int stacks)
    {
        if (target != null) {
            target.manager.bouncyTextPool.RequestWithString(target.manager.cameraBlit.GetComponent<Camera>(), target.CenterOfSprite(), "Stun!", Color.black, Color.white);
            target.Spent = true;
        }
        yield return null;
    }
}
