﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageParty2SharedInventoryManager : MonoBehaviour
{
    public List<ManageParty2CharacterInventoryButton> buttons;
    public Button prev;
    public Button next;

    int currentScroll = 0;

    public Sprite[] defaultStickyNote;
    public CampaignGameMode gm;
    public ManageParty2DetailsPane details;

    internal void Setup(PartyConvoy inventory, List<UnitData> characters)
    {
        List<KeyValuePair<UnitData, Talent>> allItems = new List<KeyValuePair<UnitData, Talent>>();
        for (int i = 0; i < inventory.Count(); i += 1)
        {
            allItems.Add(new KeyValuePair<UnitData, Talent>(null, inventory.Get(i)));
        }
        foreach (var character in characters)
        {
            foreach (var item in character.inventory)
            {
                allItems.Add(new KeyValuePair<UnitData, Talent>(character, item));
            }
        }
        if (buttons.Count >= allItems.Count)
        {
            prev.interactable = false;
            next.interactable = false;
            currentScroll = 0;
            for (int i = 0; i < buttons.Count; i += 1)
            {
                if (i < allItems.Count)
                {
                    var kv = allItems[i];
                    Sprite s = (kv.Key == null ? defaultStickyNote : kv.Key.presentation.StickyNotes).Pick();
                    int count = kv.Key == null ? inventory.GetCount(kv.Value) : 1;
                    buttons[i].ShowItem(s, kv.Value, count, () =>
                    {
                        if (details.TakeItem(kv.Key, kv.Value))
                        {
                            if (kv.Key != null)
                            {
                                kv.Key.inventory.Remove(kv.Value);
                            }
                            else
                            {
                                gm.campaign.inventory.RemoveItem(kv.Value);
                            }
                            Setup(gm.campaign.inventory, gm.campaign.team);
                        }
                    });
                }
                else
                {
                    buttons[i].Hide();
                }
            }
        }
        else
        {
            prev.interactable = true;
            next.interactable = true;
            currentScroll = (currentScroll + allItems.Count) % allItems.Count;

            for (int j = 0; j < buttons.Count; j += 1)
            {
                var i = (currentScroll + j + allItems.Count) % allItems.Count;
                var kv = allItems[i];
                Sprite s = (kv.Key == null ? defaultStickyNote : kv.Key.presentation.StickyNotes).Pick();
                buttons[j].ShowItem(s, kv.Value, 1, () =>
                {
                    if (kv.Key != null)
                    {
                        kv.Key.inventory.Remove(kv.Value);
                    }
                    else
                    {
                        gm.campaign.inventory.RemoveItem(kv.Value);
                    }
                    Setup(gm.campaign.inventory, gm.campaign.team);
                });
            }
        }
    }
    public void ChangeScroll(int i)
    {
        currentScroll += i;
        Setup(gm.campaign.inventory, gm.campaign.team);
    }
}