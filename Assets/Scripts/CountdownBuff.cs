﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static AttackTalent;

[CreateAssetMenu(menuName ="Talents/Buffs/Countdown")]
public class CountdownBuff : Talent
{
    public OnHitEffect SelfDestruct;
    public OnHitEffect ExplosionDamage;

    public override IEnumerator OnBuffExpired(MapUnit Me, IAttacker Stage)
    {
        yield return ExecuteAttackAll(ExplosionDamage, Me, Me.data.pos, true);
        yield return ExecuteAttackAll(SelfDestruct, Stage, Me.data.pos, true);
        //yield return HitEverythingInRangeAttack.UseStatic(Me, Me.data.pos, ExplosionDamage, )
    }
}
