﻿
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

[CreateAssetMenu()]
public class Localizer : SerializableScriptableObject
{
    public string FileName = "strings";

    private Dictionary<string, string> loadedFile;

    public void Load()
    {
        var textContent = Resources.Load(FileName) as TextAsset;
            
        loadedFile = new Dictionary<string, string>();
        foreach (var kv in textContent.text.Split('\n').Where(line => (line.Trim() != "")).Select(line => line.Split(new char[] { ':' }, 2)))
        {
            loadedFile[kv[0]] = kv[1];
        }
    }

    public string _(string key) {
        if (loadedFile.ContainsKey(key)) return loadedFile[key];
        Debug.LogError($"Unable to localize: [{key}]");
        return $"***Key --[{key}]-- Not Found***";
    }
}