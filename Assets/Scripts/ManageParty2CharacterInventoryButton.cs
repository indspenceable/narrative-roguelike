﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageParty2CharacterInventoryButton : MonoBehaviour
{
    public float animationDuration = 0.3f;
    public float AnimationDistance = 16f;
    public Image Backing;
    public Image Icon;
    public Button b;
    public ManageParty2DetailsPane detailsPane;
    public AnimationCurve curve;
    private Action OnClick;

    Talent HoverTalent;

    internal void Hide()
    {
        gameObject.SetActive(false);
    }

    internal void ShowIntrinsic(Sprite backing, Talent t)
    {
        b.enabled = false;
        Backing.sprite = backing;
        Icon.sprite = t.sprite;
        gameObject.SetActive(true);
        HoverTalent = t;
    }

    internal void ShowItem(Sprite backing, Talent t, int count, Action OnClick)
    {
        b.enabled = true;
        Backing.sprite = backing;
        Backing.SetNativeSize();
        Icon.sprite = t.sprite;
        Icon.SetNativeSize();
        this.OnClick = OnClick;
        gameObject.SetActive(true);
        HoverTalent = t;
    }

    public void GotClicked()
    {
        OnClick();
    }

    public void HoverStart() {
        detailsPane.HoverOn(HoverTalent);
    }
    public void HoverEnd() {
        detailsPane.HoverOn(null);
    }
    public void ShowAnimation(float f)
    {
        StartCoroutine(DoAnimation(f));
        Backing.rectTransform.anchoredPosition = Vector3.up * AnimationDistance;
    }
    IEnumerator DoAnimation(float delay) {
        yield return new WaitForSeconds(delay);
        float dt = 0f;
        while (dt < animationDuration)
        {
            yield return null;
            dt += Time.deltaTime;
            Backing.rectTransform.anchoredPosition = Vector3.Lerp(Vector3.up* AnimationDistance, Vector3.zero, curve.Evaluate(dt / animationDuration));
        }
    }
}
