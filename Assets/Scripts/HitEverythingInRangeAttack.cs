﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Talents/Skills/Hit Everything In Range")]
public class HitEverythingInRangeAttack : AttackTalent {
    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        return UseStatic(Me, target, onHit, PossibleTargets(Me));
    }

    public static IEnumerator UseStatic(MapUnit Me, Vector2Int target, OnHitEffect onHit, List<Vector2Int> possibleTargets) { 
        bool first = true;
        Util.MultiCoroutineCollector cos = new Util.MultiCoroutineCollector(Me);
        foreach (var currentTarget in possibleTargets)
        {
            cos.Add(ExecuteAttack(onHit, Me, currentTarget, first));
            first = false;
        }
        yield return cos.Collect();
    }
}
