﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ManagePartyGameModeInstance : GameModeInstance
{
    public CampaignGameMode gm;
    public CampaignGameMode NextFight;
    public CutsceneMode Cutscene;

    public List<ManagePartyNameTag> characterNametags = new List<ManagePartyNameTag>();
    public TMPro.TMP_FontAsset SmallFont;
    public int smallSize;
    public int bigSize;
    public TMPro.TMP_FontAsset BigFont;


    public CharacterStatsPane characterStatsPane;
    private int CurrentCharacter = -1;
    public TransitionEffect tEffect;
    public Globals g;
    public MusicManager music;
    public LoopingBackgrounds backgroundManager;

    public List<ActivateableBehaviour> PACallbacks = new List<ActivateableBehaviour>();
    public AudioPool audioPool;

    [Header("Shared Inventory")]
    public List<IconButton> InventoryButtons;
    public Button PrevItems;
    public Button NextItems;
    public int currentInventoryOffset = 0;

    public override void PreActivate(object DeserializedData)
    {
        if (DeserializedData != null)
            gm.campaign = DeserializedData as CampaignStatus;
        //else
        foreach (var c in gm.campaign.team)
            c.currentDamage = Mathf.Min(c.currentDamage, c.MaxHP - 1);
        RefreshEverything();
        music.FadeTo(music.config.BetweenRoomDowntime);
        backgroundManager.Setup();
    }

    public override void PostActivate()
    {
        foreach (var pa in PACallbacks) pa.OnPostActivate();
    }

    public void SharedInventoryScroll(int delta)
    {
        currentInventoryOffset += delta;
        RefreshEverything();
    }

    public void RefreshEverything()
    {
        for (int i = 0; i < characterNametags.Count; i += 1)
        {
            bool selected = i == CurrentCharacter;
            characterNametags[i].sprite = selected ?
                gm.campaign.team[i].presentation.NametagLarge : 
                gm.campaign.team[i].presentation.NametagSmall;
            characterNametags[i].SetText(gm.campaign.team[i].Identifier, selected ? BigFont : SmallFont, selected ? bigSize : smallSize);
            characterNametags[i].image.SetNativeSize();
        }

        int maxOffset = Mathf.Max(0, gm.campaign.inventory.Count() - 3);
        currentInventoryOffset = Mathf.Clamp(currentInventoryOffset, 0, maxOffset);

        NextItems.interactable = currentInventoryOffset < maxOffset;
        PrevItems.interactable = currentInventoryOffset > 0;

        for (int i = 0; i < InventoryButtons.Count; i += 1)
        {
            var c = gm.campaign.inventory.Get(i+currentInventoryOffset);
            if (c != null)
            {
                InventoryButtons[i].icon.enabled = true;
                InventoryButtons[i].icon.sprite = c.sprite;
                InventoryButtons[i].button.interactable = false;
                int count = gm.campaign.inventory.GetCount(i+currentInventoryOffset);
                if (count == 1)
                {
                    InventoryButtons[i].BadgeBack.enabled = false;
                    InventoryButtons[i].Badge.text = "";
                }
                else
                {
                    InventoryButtons[i].BadgeBack.enabled = true;
                    InventoryButtons[i].Badge.text = gm.campaign.inventory.GetCount(i + currentInventoryOffset).ToString();
                }
            }
            else
            {
                InventoryButtons[i].icon.enabled = false;
                InventoryButtons[i].button.interactable = false;
                InventoryButtons[i].Badge.text = "";
                InventoryButtons[i].BadgeBack.enabled = false;
            }
        }

        if (CurrentCharacter > -1) {
            for (int i = 0; i < InventoryButtons.Count; i += 1)
            {
                var c = gm.campaign.inventory.Get(i);
                if (c != null)
                {
                    InventoryButtons[i].button.interactable = gm.campaign.team[CurrentCharacter].inventory.Count < 4;
                }
            }
        }

        g.Quicksave(gm, gm.campaign);
    }

    public void ExamineCharacter(int characterIndex)
    {
        this.audioPool.PlaySimple(audioPool.sfx.StandardBeep);
        characterStatsPane.Setup(gm.campaign.team[characterIndex]);
        CurrentCharacter = characterIndex;
        RefreshEverything();
    }
    

    public void SendItemToCharacter(int itemIndex) {
        this.audioPool.PlaySimple(audioPool.sfx.StandardBeep);
        if (itemIndex < gm.campaign.inventory.Count() &&
            CurrentCharacter != -1 &&
            gm.campaign.team[CurrentCharacter].inventory.Count < 4)
        {
            gm.campaign.team[CurrentCharacter].inventory.Add(gm.campaign.inventory.Remove(itemIndex));
            ExamineCharacter(CurrentCharacter);
        }
        RefreshEverything();
    }
    public void SendItemToConvoy(int itemIndex)
    {
        this.audioPool.PlaySimple(audioPool.sfx.StandardBeep);
        if (CurrentCharacter != -1 &&
            itemIndex <= gm.campaign.team[CurrentCharacter].inventory.Count)
        {
            gm.campaign.inventory.Add(gm.campaign.team[CurrentCharacter].inventory[itemIndex]);
            gm.campaign.team[CurrentCharacter].inventory.RemoveAt(itemIndex);
        }
        ExamineCharacter(CurrentCharacter);
        RefreshEverything();
    }

    public void NextRoom()
    {
        if (gm.campaign.currentStage.LevelsCount == gm.campaign.CurrentStageProgress+1)
            Cutscene.ActivateWithData(tEffect, gm.campaign, gm.campaign.currentStage.BossCutscene, () => NextFight.ActivateWithData(tEffect, gm.campaign));
        else
            NextFight.ActivateWithData(tEffect, gm.campaign);
    }
}