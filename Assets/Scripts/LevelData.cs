﻿using System.Collections.Generic;
using UnityEngine;

public interface IHasBounds
{
    Transform GetBL();
    Transform GetTR();
}

public class LevelData : MonoBehaviour, IHasBounds

{
    public Transform TR;

    public Transform BL;
    public List<EnemySpawnLocation> PlayerSpawnLocations;
    public List<EnemySpawnLocation> PredefinedEnemySpawns;
    public List<SpecialTileMarker> SpecialTiles;
    public AudioClip Music;
    public List<Talent> TrapTiles;
    public List<AbstractInteractable> Interactables;

    public Transform GetBL()
    {
        return BL;
    }

    public Transform GetTR()
    {
        return TR;
    }


    public void OnDrawGizmos()
    {
        var tr = TR.position;
        var bl = BL.position;
        DrawRect(tr, bl, Color.green);

        Gizmos.color = Color.blue;
        var x = 384 / 16f;
        var y = 240 / 16f;
        bl = tr - new Vector3(x, y);
        DrawRect(bl, tr, Color.blue);
    }

    private static void DrawRect(Vector3 tr, Vector3 bl, Color c)
    {
        var tl = new Vector3(tr.x, bl.y);
        var br = new Vector3(bl.x, tr.y);
        Gizmos.color = c;
        Gizmos.DrawLine(tr, tl);
        Gizmos.DrawLine(tr, br);
        Gizmos.DrawLine(br, bl);
        Gizmos.DrawLine(bl, tl);
    }
}