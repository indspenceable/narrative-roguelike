﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static AttackTalent;

public class MapUnit : MonoBehaviour, IAttacker, MapManager.IPathWalker
{
    public SpriteRenderer sr;
    public Animator DecoratorAnimator;
    public UnitData data;

    public List<AbstractSkillTalent> DefaultSkills = new List<AbstractSkillTalent>();

    public void SET_FX_LAYER_CAUSE_ANIMATOR_DOESNT_WORK()
    {
        this.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingLayerName = "FX";
    }

    public Animator CharacterAnimator;
    private PoolableParticleSystem pps;

    public AudioPool audioPool;
    public TBSGameManager _manager;
    public TBSGameManager manager => _manager;
    public List<Vector2Int> CurrentPositions => CurrentOffsets().Select(o => data.pos + o).ToList();
    public List<BuffInstance> currentBuffs
    {
        get
        {
            return data.CurrentBuffs;
        }
    }
    public Globals globals;

    [NaughtyAttributes.ReadOnly]
    public Vector2Int CurrentSize;
    private Vector2Int CalculatedSpriteOffset;
    public MeterBase HealthMeter;
    [NaughtyAttributes.ReadOnly]
    public List<string> CooldownStrings = new List<string>();
    public void LateUpdate()
    {
        CooldownStrings = data.Cooldowns.Select(kv => $"{kv.Key} - {kv.Value}").ToList();
        sr.sortingOrder = -(int)((transform.position.y*500) + CurrentSize.y);
    }

    public bool EnemiesWakeMe = true;
    public bool PCsWakeMe = true;
    public UnitStatusIndicator usi;


    public bool Spent
    {
        set
        {
            data.Spent = value;
            sr.color = value && !data.DoesNotAct ? Color.gray : Color.white;
        }
        get
        {
            return data.Spent;
        }
    }


    internal int MaxHP()
    {
        return Calculate("MaxHP", data.MaxHP);
    }

    public virtual bool CheckOffsets(Vector2Int neighbor)
    {
        return CurrentOffsets().All(_o =>
        {
            MapTile mapTile = manager.map.TileAt(neighbor + _o);
            bool blocked = mapTile != null && mapTile.Blocked(data.Mobility);
            MapUnit _u = manager.directory.UnitAt(neighbor + _o);
            return !blocked && (_u == null || (_u.data.Friendly == data.Friendly));
        });
    }
    internal bool CanEnter(Vector2Int neighbor)
    {
        return CurrentOffsets().All(_o =>
        {
            MapTile mapTile = manager.map.TileAt(neighbor + _o);
            bool blocked = mapTile != null && mapTile.Blocked(data.Mobility);
            MapUnit _u = manager.directory.UnitAt(neighbor + _o);
            return !blocked && (_u == null || _u == this);
        });
    }

    internal bool HasTemporaryStatus(Talent status)
    {
        return currentBuffs.Any(b => b.buff == status);
    }


    internal int GetStacksOfTempStatus(Talent status)
    {
        return currentBuffs.Where(b => b.buff == status).Select(b => b.stacks).Append(0).Sum();
    }


    internal int SkillUses(string identifier)
    {
        if (data.SkillUsesPerRoom.ContainsKey(identifier))
        {
            return data.SkillUsesPerRoom[identifier];
        }
        return 0;
    }
    public void MarkUse(string identifier)
    {
        data.SkillUsesPerRoom[identifier] = SkillUses(identifier) + 1;
    }


    public virtual void Setup(UnitData data, TBSGameManager manager)
    {
        this.data = data;
        if (data.presentation == null)
            Debug.LogError($"Presentation is null! {data.CharacterName}");

        if (data.presentation.AnimatorController == null)
        {
            this.CharacterAnimator.runtimeAnimatorController = null;
            sr.sprite = data.presentation.MapSprite;
        }
        else
        {
            CharacterAnimator.runtimeAnimatorController = data.presentation.AnimatorController;
            sr.sprite = null;
        }
        this._manager = manager;
        this.name = $"MapUnit ({data.CharacterName})";
        CurrentSize = Vector2Int.one;
        RefreshSize();
    }

    internal IEnumerator AnimateWalkOnPath(List<Vector2Int> path, float walkSpeed)
    {
        if (data.presentation.Teleporter)
        {
            yield return manager.map.WarpOnPath(this, path);
        }
        else
        {
            yield return manager.map.WalkOnPath(this, path, walkSpeed);
        }

    }

    public Animator GetCharacterAnimator()
    {
        return CharacterAnimator;
    }

    public virtual bool IsAt(Vector2Int pos)
    {
        return CurrentPositions.Contains(pos);
    }

    //private List<Vector2Int> SnakeOffsets = new List<Vector2Int>()
    //{
    //    new Vector2Int(-2,-1),
    //    new Vector2Int(-1,-1),
    //    new Vector2Int( 0,-1),
    //    new Vector2Int( 1,-1),
    //    new Vector2Int( 2,-1),
    //    new Vector2Int(-2, 0),
    //    new Vector2Int(-1, 0),
    //    new Vector2Int( 0, 0),
    //    new Vector2Int( 1, 0),
    //    new Vector2Int( 2, 0),
    //    new Vector2Int( 2, 0),
    //};

    public virtual List<Vector2Int> CurrentOffsets()
    {
        int xSize = Size().x;
        int ySize = Size().y;
        var rtn = new List<Vector2Int>();
        for (int x = 0; x < xSize; x += 1)
        {
            for (int y = 0; y < ySize; y += 1)
            {
                rtn.Add(new Vector2Int(x,y) + CalculatedSpriteOffset);
            }
        }
        return rtn;
    }

    internal int TakeDamage(int damage, bool Nonlethal)
    {
        int appliedDamage = Mathf.Max(damage - Armor(), 0);
        if (Nonlethal) appliedDamage = Mathf.Min(appliedDamage, MaxHP() - data.currentDamage - 1);
        data.currentDamage += appliedDamage;
        return appliedDamage;
    }

    public virtual void PostSetup()
    {
    }

    public virtual bool ChangeFacingOnAttacks()
    {
        if (data.Strategy!=null) return data.Strategy.ChangeFacingOnAttacks;
        return true;
    }

    public Vector2 CenterOfSprite()
    {
        return data.pos + (Vector2.one * Size() / 2f);
    }

    internal void ApplyStatus(BuffInstance newInstance)
    {
        //BuffInstance newInstance = globals.Duplicate(_newInstance);
        var existingStacks = currentBuffs.Find(b => b.buff == newInstance.buff);
        if (existingStacks == null)
        {
            currentBuffs.Add(globals.Duplicate(newInstance));
        }
        else
        {
            switch (newInstance.buff.StackingMethod)
            {
                case Talent.BuffStackingMethod.NO_STACKING:
                    currentBuffs.Add(globals.Duplicate(newInstance));
                    break;
                case Talent.BuffStackingMethod.EXTEND_DURATION:
                    existingStacks.stacks += newInstance.stacks;
                    break;
                case Talent.BuffStackingMethod.GREATER_DURATION:
                    existingStacks.stacks = Mathf.Max(existingStacks.stacks, newInstance.stacks);
                    break;
                case Talent.BuffStackingMethod.IGNORE_NEW:
                    break;
            }
        }
    }

    public virtual bool CanKnockBack()
    {
        return true;
    }

    public virtual void ResetAnimator()
    {
        if (this.ChangeFacingOnAttacks())
        {
            CharacterAnimator.SetFloat("Facing_X", 0);
            CharacterAnimator.SetFloat("Facing_Y", -1);
        }
        CharacterAnimator.SetBool("Moving", false);
    }

    public IEnumerator StartOfMapCo()
    {
        foreach (var t in AllTalents())
        {
            yield return t.Key.StartOfMap(this);
        }
    }

    [NaughtyAttributes.Button]
    public void RESET_COOLDOWNS_DEBUG()
    {
        data.Cooldowns.Clear();
        data.SkillUsesPerRoom.Clear();
    }



    internal IEnumerator EndOfTurnCo()
    {
        foreach (var t in AllTalents())
        {
            yield return t.Key.EndOfTurn(this, data.pos, t.Value.GetValueOrDefault(0));
        }
    }

    public IEnumerator StartOfTurnCo()
    {
        ReduceCooldowns();
        foreach (var t in AllTalents())
        {
            TooltipManager actionTitleTooltip = manager.ActionTitleTooltip;
            var sl = actionTitleTooltip.StaticLocation;
            actionTitleTooltip.StaticLocation = true;
            actionTitleTooltip.SetOverrideTarget(t.Key, true);
            var co = t.Key.StartOfTurn(this, manager.map, data.pos, t.Value.GetValueOrDefault(0));
            if (co != null) {
                yield return manager.cc.FocusOn(this);
                yield return co;
            }
            actionTitleTooltip.ClearOverrideTarget();
            actionTitleTooltip.StaticLocation = sl;
        }
        foreach (var buffData in currentBuffs)
        {
            if (buffData.buff.TicksDownOccupied)
                buffData.stacks -= 1;
        }
        yield return RemoveExpiredAndInactiveBuffs();
    }

    public void ReduceCooldowns()
    {
        foreach (var cd in data.Cooldowns.Keys.ToList())
        {
            if (data.Cooldowns[cd] > 0)
                data.Cooldowns[cd] -= 1;
        }
    }

    public IEnumerator RemoveExpiredAndInactiveBuffs()
    {
        var Expired = currentBuffs.Where(buff => buff.stacks <= 0).ToList();
        currentBuffs.RemoveAll(buff => buff.stacks <= 0);
        foreach (var e in Expired)
        {
            yield return e.buff.OnBuffExpired(this, manager.map);
        }
        RefreshSize();
    }

    public void GetHealed(int finalHeal)
    {
        data.currentDamage -= finalHeal;
        if (data.currentDamage < 0) data.currentDamage = 0;
    }


    public virtual void MoveTo(Vector2Int pos, bool RefreshDecorators)
    {
        data.pos = pos;
        transform.position = pos + (Vector2.one * Size()) / 2f;
        HealthMeter.SetContainerLocalPosition(new Vector3(0, (pos.x + pos.y) % 2 == 0 ? 1 / 8f : -1 / 8f));
        if (RefreshDecorators)
            manager.directory.mapUnits.ForEach(u => u.RefreshStatusDecorators());
    }


    public List<ISkillBase> GetSkills()
    {
        return AllTalents().Select(t => t.Key as ISkillBase).Where(t => t != null).ToList();
    }

    public IEnumerator AfterUseAttack()
    {
        foreach(var t in AllTalents())
        {
            yield return t.Key.AfterUseAttack(this);
        }
    }

    public IEnumerator BeforeUseAttack()
    {
        foreach (var t in AllTalents())
        {
            var rtn = t.Key.BeforeAttack(this);
            if (rtn != null)
                yield return rtn;
        }
    }

    internal IEnumerator OnGetHit()
    {
        WakeIfNeeded();
        foreach (var t in AllTalents()) {
            yield return t.Key.OnGetHit(this);
        }
        RefreshStatusDecorators();
    }

    public void WakeIfNeeded()
    {
        if (data.Unaware) Wake();
    }

    public virtual void OnUnitDeath()
    {
        foreach (var t in AllTalents()) t.Key.OnUnitDied(this);
    }

    public List<KeyValuePair<Talent, int?>> AllTalents()
    {
        List<Talent> talentList = new List<Talent>();
        talentList.AddRange(data.IntrinsicTalents);
        talentList.AddRange(data.inventory);
        talentList.AddRange(DefaultSkills);
        var TileBuffs = new List<Talent>();
        foreach (var t in CurrentPositions)
        {
            var tile = manager.map.TileAt(data.pos);
            if (tile != null)
                TileBuffs.AddRange(tile.AllCurrentEffects());
        }
        talentList.AddRange(TileBuffs.Distinct());
        //rtn.AddRange(currentBuffs.Select(b => b.buff));

        var rtn = talentList.Select(t => new KeyValuePair<Talent, int?>(t, null)).ToList();
        rtn.AddRange(currentBuffs.Select(b => new KeyValuePair<Talent, int?>(b.buff, b.stacks)));
        return rtn.Where(t => t.Key != null).ToList();
    }

    public int CurrentCooldown(string identifier)
    {
        if (data.Cooldowns.ContainsKey(identifier))
        {
            return data.Cooldowns[identifier];
        }
        return 0;
    }

    internal void ClearCooldowns()
    {
        data.Cooldowns.Clear();
        data.SkillUsesPerRoom.Clear();
    }

    internal void SetCooldown(string identifier, int duration)
    {
        data.Cooldowns[identifier] = duration;
        MarkUse(identifier);
    }

    public int Calculate(string name, int defaultValue)
    {
        var talents = AllTalents();
        if (!talents.Any()) return defaultValue;
        return talents.Select(t => t.Key.Modifier(name, this)).Append(defaultValue).Sum();
    }
    
    [NaughtyAttributes.Button]
    public void AddAllPossibleItems()
    {
        data.inventory.AddRange(globals.All<Talent>().Where(t => t.CanBeAwardedAsPrize));
    }


    internal bool ShouldAct()
    {
        return !(data.Spent || data.DoesNotAct || data.Unaware);
    }

    internal int Movement()
    {
        return Calculate("Movement", data.Movement);
    }

    public int SpikesAmt()
    {
        return Calculate("Spikes", 0);
    }

       

    internal List<MapUnit> UnitsThatWillWake()
    {
        // todo big monsters should listen from all tiles?
        return manager.directory.Query(u =>
            u != this &&
            MinDistanceTo(u) - this.Alertness() <= u.Noise() &&
            !u.data.Unaware &&
            (u.data.Friendly ? PCsWakeMe : EnemiesWakeMe)).ToList();

        int MinDistanceTo(MapUnit o)
        {
            return CurrentPositions.SelectMany(MyTile => o.CurrentPositions.Select(theirTile => MyTile.ManhattanDistance(theirTile))).Min();
        }
    }
    public bool ShouldWake()
    {
        return UnitsThatWillWake().Any();
    }
    public bool IsAwake()
    {
        return !data.Unaware;
    }

    internal int Armor()
    {
        return Calculate("Armor", data.Armor - (data.Unaware ? 1 : 0));
    }

    internal void Wake()
    {
        audioPool.PlaySimple(audioPool.sfx.wake);
        manager.bouncyTextPool.RequestWithString(manager.cameraBlit.GetComponent<Camera>(), CenterOfSprite(), "!!", Color.red, Color.white);
        data.Unaware = false;
        manager.directory.mapUnits.ForEach(u => u.RefreshStatusDecorators());
    }

    public int Strength()
    {
        return Calculate("Strength", data.Strength);
    }

    public int BonusHeals => Calculate("BonusHeals", 0);

    public int Noise()
    {
        return Calculate("Noise", data.BaseNoise);
    }

    public int Alertness()
    {
        return Calculate("Alertness", data.BaseAlertness);
    }

    public int Lure()
    {
        return Calculate("Lure", 0);
    }

    public Vector2Int Size()
    {
        return CurrentSize;
    }

    [NaughtyAttributes.Button]
    public void RefreshSize()
    {
        this.CurrentSize = new Vector2Int(Calculate("XSize", 1), Calculate("YSize", 1));
        this.CalculatedSpriteOffset = new Vector2Int(Calculate("XOffset", 0), Calculate("YOffset", 0));
    }
   
    internal bool Dead()
    {
        return data.currentDamage >= MaxHP();
    }


    public void RefreshStatusDecorators()
    {
        this.usi.SetBuffs(currentBuffs.Select(b => b.buff).Distinct().Where(b => b.ShownAsBuff));
        if (data.UnawareAnimatorName == "") data.UnawareAnimatorName = "Unaware";
        DecoratorAnimator.SetBool(data.UnawareAnimatorName, data.Unaware && !this.ShouldWake() && !data.DoesNotAct);
        DecoratorAnimator.SetBool("WillWake", data.Unaware && this.ShouldWake() && !data.DoesNotAct);
    }

    public Vector2Int Pos()
    {
        return data.pos;
    }

    public bool Busy()
    {
        return CharacterAnimator.GetBool("Busy");
    }

    public void DisplayAttack(Vector2 FacingDirection, string attackParameter)
    {
        if (this.ChangeFacingOnAttacks())
        {
            CharacterAnimator.SetFloat("Facing_X", FacingDirection.x);
            CharacterAnimator.SetFloat("Facing_Y", FacingDirection.y);
        }
        CharacterAnimator.SetTrigger(attackParameter);
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public bool ResetAnimatorsAtEnd()
    {
        return ChangeFacingOnAttacks();
    }

    public void OnExitTileCallback(Vector2Int v2i)
    {
        AllTalents().ForEach(t => t.Key.OnExitTile(this, v2i));
    }

    public void GoToSleep()
    {
        if (!this.ShouldWake())
        {
            data.Unaware = true;
            RefreshStatusDecorators();
        }
    }
}
