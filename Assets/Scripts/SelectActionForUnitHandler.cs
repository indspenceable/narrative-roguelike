﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SelectActionForUnitHandler : TBSGameManager.InputHandler
{
    private MapUnit actor;
    private List<Vector2Int> path;
    private Vector2Int initialPosition;
    private TBSGameManager manager;
    private TBSGameManager.InputHandler previous;

    public SelectActionForUnitHandler(MapUnit actor, List<Vector2Int> path, TBSGameManager manager, TBSGameManager.InputHandler previous)
    {
        this.actor = actor;
        this.path = path;
        this.initialPosition = path[0];
        this.manager = manager;
        this.previous = previous;
    }

    public void Click(int mouseButton)
    {
        
    }

    public void Install()
    {
        actor.HealthMeter.Displayed = true;
        var UndoButton = new MenuBase.MenuOption(
            manager.UndoIcon,
            "Cancel",
            DoUndo,
            actor.data.presentation.IconBackingSmall,
            actor.data.presentation.IconBackingSmallSelected,
            true,
            null,
            (b) => actor.manager.hoverHint.FocusMaybe(actor.manager.CancelTalent, b)
            );
        var MyPos = actor.data.pos;
        List<Vector2Int> adjacents = new List<Vector2Int>(){
            MyPos,
            MyPos + Vector2Int.left,
            MyPos + Vector2Int.right,
            MyPos + Vector2Int.up,
            MyPos + Vector2Int.down
        };

        var SkillList = actor.GetSkills().Select(CreateMenuOption).Reverse().Append(UndoButton);
        foreach(var a in adjacents)
        {
            var i = manager.interactables.AvailableInteractionsAt(a);
            if (i != null)
            {
                // make an option for interacting with it.
                var mo = new MenuBase.MenuOption(
                    i.InteractionSprite(),
                    i.GetMenuText(),
                    () => { i.OnUseInteractable(actor, this); },
                    actor.data.presentation.IconBackingSmall,
                    actor.data.presentation.IconBackingSmallSelected,
                    true,
                    null, (b) => actor.manager.hoverHint.FocusMaybe(null, false)
                );
                var startingCount = SkillList.Count();
                SkillList = SkillList.Append(mo);
                if (startingCount == SkillList.Count())
                {
                    //???
                    Debug.LogError("Append did nothing...");
                }
            }
        }


        manager.menu.Setup(actor.transform.position, SkillList.ToArray());
    }
    private MenuBase.MenuOption CreateMenuOption(ISkillBase skill)
    {
        string badge = null;
        int usesPerRoom = skill.UsesPerRoom();
        if (usesPerRoom > 0)
        {
            var usesSoFar = actor.SkillUses(skill.CooldownIdentifier());
            if (usesSoFar >= usesPerRoom)
            {
                badge = "x";
            }
            else
            {
                badge = new string('|', usesPerRoom - usesSoFar);
            }
        }
        
        return new MenuBase.MenuOption(
            skill.Icon(),
            ButtonName(actor, skill),
            () =>
            {
                manager.CurrentHandler = skill.UseSkillHandler(actor, this);
            },
            actor.data.presentation.IconBackingSmall,
            actor.data.presentation.IconBackingSmallSelected,
            actor.CurrentCooldown(skill.CooldownIdentifier()) == 0 && skill.Available(actor, path),
            badge,
            (b) => actor.manager.hoverHint.FocusMaybe(skill as Talent, b)
        );
    }

    private static string ButtonName(MapUnit actor, ISkillBase skill)
    {
        if (actor.CurrentCooldown(skill.CooldownIdentifier())>0)
        {
            return $"{skill.SkillName()} | {actor.CurrentCooldown(skill.CooldownIdentifier())}";
        }
        return skill.SkillName();
    }

    public void Uninstall()
    {
        actor.HealthMeter.Displayed = false;
        actor.globals.SetMouseHighlight(false);
        manager.menu.Close();
        actor.manager.hoverHint.FocusMaybe(null, false);
    }
    
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1))
        {
            DoUndo();
        }
    }

    public void DoUndo()
    {
        actor.MoveTo(initialPosition, true);
        manager.CurrentHandler = previous;
        manager.audioPool.PlaySimple(manager.soundConfig.Cancel);
    }
}