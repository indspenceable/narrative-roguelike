﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Text Effects/Wavy")]
public class WavyText : TextEffect
{
    public float MovementDistanceScaler = 1f;
    public float LetterScaler = 1f;
    public float TimeScaler = 10f;
    public AnimationCurve LetterMovementCurve = AnimationCurve.Linear(0, 0, 1, 1);

    internal override void Process(int vertexIndex, int characterIndex, List<Vector3> vertices)
    {

        var t = characterIndex * LetterScaler + Time.unscaledTime * TimeScaler;
        vertices[vertexIndex]     += Vector3.up * LetterMovementCurve.Evaluate(t - (int)t) * MovementDistanceScaler;
        vertices[vertexIndex + 2] += Vector3.up * LetterMovementCurve.Evaluate(t - (int)t) * MovementDistanceScaler;
        vertices[vertexIndex + 3] += Vector3.up * LetterMovementCurve.Evaluate(t - (int)t) * MovementDistanceScaler;
        vertices[vertexIndex + 1] += Vector3.up * LetterMovementCurve.Evaluate(t - (int)t) * MovementDistanceScaler;
    }
}
