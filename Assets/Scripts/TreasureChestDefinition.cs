﻿using UnityEngine;

[CreateAssetMenu(menuName = "TreasureChestDefinition")]
public class TreasureChestDefinition : SerializableScriptableObject {
    public RuntimeAnimatorController rac;
    [NaughtyAttributes.ShowAssetPreview]
    public Sprite UseIcon;
    public AnimationCurve Curve;
}
