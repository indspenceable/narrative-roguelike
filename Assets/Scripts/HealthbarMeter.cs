﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthbarMeter : MeterBase
{
    public MapUnit u;

    private int MaxValue = 12;

    [SerializeField]
    //[NaughtyAttributes.ReadOnly]
    private int TargetValue = 0;
    [SerializeField]
    [NaughtyAttributes.ReadOnly]
    private float currentDisplayedvalue = 0f;
    private bool _CurrentlyDisplayed = false;

    public float TimeToFill = 1f;
    private float _vel = 0f;

    public Image im;

    private void Update()
    {
        if (Displayed || Engaged || Input.GetKey(KeyCode.LeftAlt))
        {
            ForceUpdate();
        }
        else
        {
            _CurrentlyDisplayed = false;
            im.enabled = false;
        }
    }

    private void ForceUpdate()
    {
        MaxValue = u.MaxHP();
        TargetValue = u.MaxHP() - u.data.currentDamage;
        if (!_CurrentlyDisplayed)
        {
            currentDisplayedvalue = Mathf.Max(TargetValue, 0);
            _vel = 0f;
        }
        _CurrentlyDisplayed = true;

        if (Mathf.Abs(currentDisplayedvalue - TargetValue) > 0.2f)
        {
            currentDisplayedvalue = Mathf.SmoothDamp(currentDisplayedvalue, TargetValue, ref _vel, TimeToFill);
        }
        else
        {
            currentDisplayedvalue = TargetValue;
        }

        im.sprite = DisplayPct(currentDisplayedvalue);
        im.enabled = true;
    }

    [NaughtyAttributes.ReorderableList]
    public List<Sprite> fillImages;
    private Sprite DisplayPct(float currentDisplayedvalue)
    {
        float pct = (currentDisplayedvalue / u.MaxHP());
        int Splits = fillImages.Count;

        for (int a = 0; a < Splits; a+=1)
        {
            float maxPCT = (a) / (float)(Splits-1);
            if (pct < maxPCT) return fillImages[a];
        }
        return fillImages[fillImages.Count - 1];
    }

    public override void SetEngagedAndUpdate()
    {
        Engaged = true;
        ForceUpdate();
    }

    public override void SetContainerLocalPosition(Vector3 vector3)
    {
        //im.transform.localPosition = vector3;
    }
}
