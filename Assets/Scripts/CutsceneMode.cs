﻿using UnityEngine;

[CreateAssetMenu(menuName = "GameModes/Cutscene")]
public class CutsceneMode : CampaignGameMode {
    public System.Action cb;
    public AbstractCutsceneData cd;

    public void ActivateWithData(TransitionEffect tEffect, CampaignStatus campaign, AbstractCutsceneData cd, System.Action cb)
    {
        this.cd = cd;
        this.cb = cb;
        ActivateWithData(tEffect, campaign);
    }
}