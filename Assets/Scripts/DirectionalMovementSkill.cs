﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

public class DirectionalMovementSkill : AbstractDirectionalSkillTalent
{
    public OnHitEffect onHit;
    public BouncyTextPool bouncyText;
    public AudioClip onUseAudio;

    public override bool CanTargetDirection(MapUnit Me, Vector2Int direction)
    {
        var ct = Me.manager.map.TileAt(Me.data.pos + direction);
        return (ct != null && !ct.Blocked(Me.data.Mobility)) ;
    }

    public override bool TargetsEmpty => true;

    public override bool TargetsFriendly => false;

    public override bool TargetsHostile => true;

    public override bool TargetsSelf => false;

    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        var CurrentPosition = Me.data.pos;
        var direction = target - Me.data.pos;

        Me.CharacterAnimator.SetFloat("Facing_X", direction.x);
        Me.CharacterAnimator.SetFloat("Facing_Y", direction.y);
        Me.CharacterAnimator.SetBool("Moving", true);

        Me.globals.All<AudioPool>().First().PlaySimple(onUseAudio);

        for (int i = 0; i < 99; i += 1) {
            var ct = Me.manager.map.TileAt(CurrentPosition + direction);
            if (ct != null &&
                !ct.Blocked(Me.data.Mobility) &&
                Me.manager.directory.UnitAt(ct.pos) == null)
            {
                CurrentPosition = ct.pos;
            }
        }
        yield return Util.MoveUnitToPosition(Me, CurrentPosition, AnimationCurve.Linear(0,0,1,1), 0.5f);

        Me.ResetAnimator();

        Vector2Int finalTarget = Me.data.pos + direction;
        if (Me.manager.directory.UnitAt(finalTarget))
            yield return AttackTalent.ExecuteAttackAll(onHit, Me, finalTarget, true);
    }
}