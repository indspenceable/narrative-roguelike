﻿using static TBSGameManager;

public class ShowBestiary : InputHandler
{
    public string name;
    public string entry;
    TBSGameManager manager;
    InputHandler prev;

    public ShowBestiary(string name, string entry, TBSGameManager manager, InputHandler prev)
    {
        this.name = name;
        this.entry = entry;
        this.manager = manager;
        this.prev = prev;
    }

    public void Click(int mouseButton)
    {
        manager.CurrentHandler = prev;
    }

    public void Install()
    {
        manager.detailPane.Show(name, entry);
    }

    public void Uninstall()
    {
        manager.detailPane.Hide();
    }

    public void Update()
    {
    }
}
