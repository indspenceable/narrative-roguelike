﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterBlurb : MonoBehaviour
{
    public Image image;
    public TMPro.TextMeshProUGUI NameDisplay;
    public TMPro.TextMeshProUGUI HPDisplay;
    public Color HealthyColor = new Color(41, 160, 61);
    public Color MediumHealthyColor = new Color(255, 196, 0);
    public Color UnhealthyColor = new Color(235, 77, 111);

    public List<Image> Inventory;
    internal void Setup(UnitData data)
    {
        image.sprite = data.presentation.MapSprite;
        NameDisplay.text = data.CharacterName;
        SetHPText(data);

        for (int i = 0; i < 4; i += 1)
        {
            if (data.inventory.Count > i)
            {
                Inventory[i].enabled = true;
                Inventory[i].sprite = data.inventory[i].sprite;
            }
            else
            {
                Inventory[i].enabled = false;
            }
        }
    }

    private void SetHPText(UnitData data)
    {
        // TODO maxHP might be displayed wrong here.
        var f = 1f - (data.currentDamage/data.MaxHP);
        HPDisplay.text = data.FormatUnalteredHP();
        HPDisplay.color = f > 0.66f ? HealthyColor : f > 0.33f ? MediumHealthyColor : UnhealthyColor;
    }
}
