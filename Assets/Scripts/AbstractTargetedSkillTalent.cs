﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class AbstractTargetedSkillTalent : AbstractSkillTalent
{
    public int MinRange=1;
    public int MaxRange=1;

    public override bool Available(MapUnit Me, List<Vector2Int> path) {
        var baseAvailable = base.Available(Me, path);
        var HasATarget = PossibleTargets(Me).Any();
        var movementCheck = (path.Count() == 1 || !RequiresNoMovement);
        return baseAvailable && HasATarget && movementCheck;
    }
    
    public virtual bool ValidTargetableTileContents(MapUnit Me, Vector2Int v2i)
    {
        if (Me.CurrentPositions.Contains(v2i) && !TargetsSelf) return false;
        if (Me.CurrentPositions.Where(bt => bt.ManhattanDistance(v2i) <= MaxRange &&
            bt.ManhattanDistance(v2i) >= MinRange).Count() == 0) return false;

        if (Me.manager.directory.UnitAt(v2i) != null)
        {
            if (Me.manager.directory.UnitAt(v2i).data.Friendly == Me.data.Friendly)
            {
                return TargetsFriendly;
            } else
            {
                return TargetsHostile;
            }
        } else
        {
            return TargetsEmpty;
        }
    }
    
    public override TBSGameManager.InputHandler UseSkillHandler(MapUnit me, TBSGameManager.InputHandler previous)
    {
        return new SelectSkillTarget(me, this, PossibleTargets(me), previous);
    }

    public override List<Vector2Int> PossibleTargets(MapUnit Me)
    {
        List<Vector2Int> rtn = ValidTargetLocationsWithinRange(Me).Distinct().ToList();
        return rtn.Distinct().Where(v2i => ValidTargetableTileContents(Me, v2i)).ToList();
    }

    // This just shows tiles that could be targeted by this skill.
    // it doesnt check against tile contentens
    private List<Vector2Int> ValidTargetLocationsWithinRange(MapUnit Me)
    {
        var rtn = new List<Vector2Int>();
        for (int x = -MaxRange; x <= MaxRange; x += 1)
        {
            for (int y = -MaxRange; y <= MaxRange; y += 1)
            {
                foreach (var t in Me.CurrentPositions)
                {
                    Vector2Int offset = new Vector2Int(x, y);
                    if (offset.Manhattan() <= MaxRange &&
                        offset.Manhattan() >= MinRange)

                        rtn.Add(t + offset);
                }
            }
        }

        return rtn;
    }

    public abstract bool TargetsFriendly { get; }
    public abstract bool TargetsHostile { get; }
    public abstract bool TargetsEmpty { get; }
    public abstract bool TargetsSelf { get; }
}
