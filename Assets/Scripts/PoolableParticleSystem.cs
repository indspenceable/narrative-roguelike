﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolableParticleSystem : AbstractPoolable
{
    public ParticleSystem system;
    public SpriteRenderer s;
    public override void ExternalDeactivate()
    {
        var main = system.main;
        main.loop = false;
        s.enabled = false;
        base.ExternalDeactivate();
    }

    public override void OnActivatedByPool(AbstractObjectPool pool)
    {
        base.OnActivatedByPool(pool);
        var main = system.main;
        main.loop = true;
        system.Play();
        s.enabled = true;
    }
}
