﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractSelfSkillTalent : AbstractSkillTalent
{
    public override IEnumerator Use(MapUnit Me, Vector2Int _)
    {
        yield return Use(Me);
        Me.manager.directory.mapUnits.ForEach(u => u.RefreshStatusDecorators());
    }
    public abstract IEnumerator Use(MapUnit Me);

    public override TBSGameManager.InputHandler UseSkillHandler(MapUnit me, TBSGameManager.InputHandler previous)
    {
        return new CoroutineExecutorHandler(me.manager, ExecuteSkill(me));
    }
    public virtual IEnumerator ExecuteSkill(MapUnit me)
    {
        yield return Use(me);
        me.Spent = true;
        me.manager.directory.mapUnits.ForEach(u => u.RefreshStatusDecorators());
        me.manager.InstallDefaultHandler();
    }

    public override List<Vector2Int> PossibleTargets(MapUnit Me)
    {
        return Me.CurrentPositions;
    }
}
