﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="GameModes/Campaign")]
public class CampaignGameMode : GameModeBase
{
    //[System.NonSerialized]
    //[HideInInspector]
    public CampaignStatus campaign;

    public void ActivateWithData(TransitionEffect tEffect, CampaignStatus campaign)
    {
        this.campaign = campaign;
        Activate(tEffect);
    }

    [NaughtyAttributes.Button]
    public void ResetCampaignToNull()
    {
        campaign = null;
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }

}
