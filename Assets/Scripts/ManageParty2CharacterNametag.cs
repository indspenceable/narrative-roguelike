﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ManageParty2CharacterNametag : MonoBehaviour
{
    public Image img;
    public ManageParty2CharacterBar characterManager;
    private UnitData currentUnit;
    private bool currentlySelected;

    public ManageParty2DetailsPane detailsPane;
    public ManageParty2CharacterInventoryManager characterInventory;
    public TMPro.TextMeshProUGUI BigText;
    public TMPro.TextMeshProUGUI SmallText;

    internal void Setup(UnitData unitData)
    {
        BigText.gameObject.SetActive(false);
        SmallText.gameObject.SetActive(true);
        currentUnit = unitData;
        img.sprite = currentUnit.presentation.NametagSmall;
        img.SetNativeSize();
        currentlySelected = false;
        SmallText.text = unitData.CharacterName;
        BigText.text = unitData.CharacterName;
    }

    internal void Deselect()
    {
        
        Setup(currentUnit);
    }

    public void OnNametagClicked()
    {
        characterManager.DeselectPrevious();
        currentlySelected = true;
        img.sprite = currentUnit.presentation.NametagLarge;
        img.SetNativeSize();
        BigText.gameObject.SetActive(true);
        SmallText.gameObject.SetActive(false);
        detailsPane.FocusOn(currentUnit);
        characterInventory.Show(currentUnit);
    }
}
