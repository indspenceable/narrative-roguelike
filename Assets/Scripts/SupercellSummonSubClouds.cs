﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Supercell/Multisummon")]
public class SupercellSummonSubClouds : AbstractTargetedSkillTalent {
    public List<UnitDefinition> Summonables = new List<UnitDefinition>();
    public List<BuffInstance> InitialBuffs = new List<BuffInstance>();
    public AudioPool audioPool;
    public AudioClip clip;
    public AnimatorPool animatorPool;
    public RuntimeAnimatorController effectRAC;

    public override bool TargetsFriendly => false;
    public override bool TargetsHostile => false;
    public override bool TargetsEmpty => true;
    public override bool TargetsSelf => false;
    
    public int Radius = 7;

    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        List<KeyValuePair<Vector2Int, UnitData>> _Locations = new List<KeyValuePair<Vector2Int, UnitData>>();
        for (int x = -Radius; x <= Radius; x += 1)
        {
            for (int y = -Radius; y <= Radius; y += 1)
            {
                var Summonable = Summonables.Pick();
                Vector2Int pos = target + new Vector2Int(x, y);
                var mapTile = Me.manager.map.TileAt(pos);
                if ((mapTile != null) && mapTile.MovementAllowed(Summonable.Data.Mobility) && Me.manager.directory.UnitAt(pos) == null)
                    _Locations.Add(new KeyValuePair<Vector2Int, UnitData>(pos, Summonable.Data));
            }
            yield return null;
        }
        IEnumerable<KeyValuePair<Vector2Int, UnitData>> Locations = _Locations;
        Locations = Locations.Shuffled().Take(3);
        yield return new WaitForSeconds(0.25f);
        foreach (var kvPair in Locations)
        {
            var pos = kvPair.Key;
            var SummonableData = kvPair.Value;
            var mu = SummonSkillTalent.ExecuteSummon(Me, pos, SummonableData, InitialBuffs, new List<CharacterAnimationData>());
            this.audioPool.PlaySimple(clip);
            this.animatorPool.RequestWithAnimator(mu.CenterOfSprite(), this.effectRAC);
            yield return new WaitForSeconds(0.25f);
        }
        yield return new WaitForSeconds(0.25f);
    }

    public override bool ValidTargetableTileContents(MapUnit Me, Vector2Int v2i)
    {
        MapTile mapTile = Me.manager.map.TileAt(v2i);
        return base.ValidTargetableTileContents(Me, v2i) && ((mapTile != null) && mapTile.MovementAllowed(UnitData.MobilityType.LAND));
    }
}