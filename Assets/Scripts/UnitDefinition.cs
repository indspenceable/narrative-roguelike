﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Units/Definition")]
public class UnitDefinition : SerializableScriptableObject
{
    public UnitData Data;
    public List<CharacterAnimationData> AlternateCostumes = new List<CharacterAnimationData>();
    public bool SelectableCharacter;
}
