﻿using System;
using System.Collections.Generic;
using UnityEngine;
public class ManageParty2CharacterBar : MonoBehaviour
{
    public List<ManageParty2CharacterNametag> Nametags;


    internal void Setup(List<UnitData> team)
    {
        for (int i = 0; i < 4; i += 1)
        {
            Nametags[i].Setup(team[i]);
        }
    }

    internal void DeselectPrevious()
    {
        foreach (var tag in Nametags) tag.Deselect();
    }
}
