﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System;
using System.Linq;

[System.Serializable]
public class MetaProgressionData
{
    public MetaProgressionData(List<BaseBuilderPropPlacement> defaultProps)
    {
        this.baseBuilderProps = new List<BaseBuilderPropPlacement>(defaultProps);
    }
    public Dictionary<string, bool> bools = new Dictionary<string, bool>();
    public Dictionary<string, int> ints = new Dictionary<string, int>();
    public List<BaseBuilderPropPlacement> baseBuilderProps;
}
[CreateAssetMenu]
public class MetaProgression : ScriptableObject
{
    public Globals g;

    private MetaProgressionData data = null;
    public void OnGameStart()
    {
#if UNITY_EDITOR
        g.Editor_Only_RefreshAll();
#endif
        data = g.LoadMetaProgression();
    }

    [NaughtyAttributes.Button]
    void ListAllBools()
    {
        foreach( var d in data.bools)
        {
            Debug.Log($"{d.Key} -> {d.Value}");
        }
    }

    [NaughtyAttributes.Button]
    void SaveDefaultLayout()
    {
        g.DefaultBaseBuilderLayout = g.Duplicate(data.baseBuilderProps);
    }

    [NaughtyAttributes.Button]
    public void ResetProgression()
    {
        //PlayerPrefs.DeleteAll();
        g.SaveMetaProgression(new MetaProgressionData(g.DefaultBaseBuilderLayout));
        if (Application.isPlaying) 
            data = g.LoadMetaProgression();
    }

    public bool GetBool(string s, bool defaultValue=false)
    {
        if (data.bools == null) data.bools = new Dictionary<string, bool>();

        if (data.bools.ContainsKey(s))
        {
            return data.bools[s];
        }
        return defaultValue;
    }
    public void SetBool(string s, bool value)
    {
        if (data.bools.ContainsKey(s) && data.bools[s] == value) return;
        data.bools[s] = value;
        g.SaveMetaProgression(data);
    }
    public int GetInt(string s, int defaultValue = 0)
    {
        
        if (data.ints == null) data.ints = new Dictionary<string, int>();

        if (data.ints.ContainsKey(s))
        {
            return data.ints[s];
        }
        return defaultValue;
    }
    public void SetInt(string s, int value)
    {
        if (data.ints.ContainsKey(s) && data.ints[s] == value) return;
        data.ints[s] = value;
        g.SaveMetaProgression(data);
    }

    internal void SetIntByDelta(string key, int v)
    {
        SetInt(key, GetInt(key) + v);
    }

    public void SaveProp(BaseBuilderPropPlacement placement)
    {
        data.baseBuilderProps.Add(placement);

        g.SaveMetaProgression(data);
    }
    public void RemovePropPlacement(BaseBuilderPropPlacement pp)
    {
        data.baseBuilderProps.Remove(pp);


        g.SaveMetaProgression(data);
    }
    public List<BaseBuilderPropPlacement> LoadSavedProps()
    {
        if (data.baseBuilderProps == null) data.baseBuilderProps = new List<BaseBuilderPropPlacement>();
        return new List<BaseBuilderPropPlacement>(data.baseBuilderProps);
    }

    internal string CharacterGroupKey(params string[] characterNames)
    {
        var lst = characterNames.ToList();
        lst.Sort();
        return string.Join("|", lst);
    }


    [NaughtyAttributes.Button]
    public void UnlockAllProps()
    {
        foreach (var bbpd in g.All<BaseBuilderPropDefinition>())
        {
            SetBool(bbpd.UnlockFlag, true);
        }
    }
}