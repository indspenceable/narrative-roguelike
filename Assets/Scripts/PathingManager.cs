﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathingManager : MonoBehaviour
{
    public MapManager map;
    public UnitDirectory directory;
    public Dictionary<Vector2Int, List<Vector2Int>> ExploreAllPathsFromUnit(MapUnit u, int distance)
    {
        return ExploreAllPathsFromLocation(u.data.pos, distance, u.CheckOffsets, u.CanEnter);
    }
    
    public Dictionary<Vector2Int, List<Vector2Int>> ExploreAllPathsFromLocation(Vector2Int pos, int distance, Func<Vector2Int, bool> CheckOffsets, Func<Vector2Int, bool> CanEnter)
    {
        Queue<List<Vector2Int>> Paths = new Queue<List<Vector2Int>>();
        Paths.Enqueue(new List<Vector2Int>() { pos });

        Dictionary<Vector2Int, List<Vector2Int>> rtn = new Dictionary<Vector2Int, List<Vector2Int>>();
        rtn.Add(pos, new List<Vector2Int>() { pos });

        while (Paths.Count > 0) {
            var cp = Paths.Dequeue();
            var dest = cp[cp.Count - 1];
            var tile = map.TileAt(dest);
            foreach (var neighbor in tile.Neighbors.Shuffled()){
                if (map.TileAt(neighbor) == null) continue;
                
                if (!cp.Contains(neighbor) &&
                    !rtn.ContainsKey(neighbor) &&
                    distance > cp.Count - 1)
                {
                    if (CheckOffsets(neighbor)) { 
                        var newPath = cp.Append(neighbor).ToList();
                        Paths.Enqueue(newPath);
                        if (CanEnter(neighbor))
                        {
                            rtn.Add(neighbor, newPath);
                        }
                    }
                }
            }
        }
        return rtn;
    }
}
