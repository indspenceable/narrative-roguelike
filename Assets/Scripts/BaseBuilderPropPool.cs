﻿
using UnityEngine;

[CreateAssetMenu(menuName = "Object Pools/Base Builder Prop")]
public class BaseBuilderPropPool : ObjectPoolBase<BaseBuilderPropPoolable>
{
public BaseBuilderPropPoolable RequestProp(BaseBuilderPropPlacement PP, BaseBuilderPropManager bbpm)
    {
        var nn = Request((Vector2)PP.position);
        nn.backingPlacement = PP;
        nn.sr.sprite = PP.propDef.PropSprite;
        nn.bbpm = bbpm;
        return nn;
    }
}
