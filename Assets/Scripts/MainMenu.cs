﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : GameModeInstance
{
    public Button loadGameButton;
    public Globals globals;
    public MusicManager music;

    public override void PreActivate(object DeserializedData)
    {
        // we never serialize this state :)
        loadGameButton.interactable = globals.QuicksaveExists();
        music.FadeTo(music.config.MenuMusic);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
