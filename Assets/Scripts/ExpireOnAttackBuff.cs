﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Buffs/Expire On Attack")]
public class ExpireOnAttackBuff : BuffDefinition
{
    public override IEnumerator AfterUseAttack(MapUnit me)
    {
        var buff = me.currentBuffs.Find(b => b.buff = this);
        if (buff != null)
            buff.stacks = 0;
        yield return null;
    }
}

