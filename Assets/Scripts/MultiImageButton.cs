﻿using UnityEngine;
using UnityEngine.UI;

// From https://forum.unity.com/threads/tint-multiple-targets-with-single-button.279820/
public class MultiImageButton : Button
{
    protected override void DoStateTransition(SelectionState state, bool instant)
    {
        var targetColor =
            state == SelectionState.Disabled ? colors.disabledColor :
            state == SelectionState.Highlighted ? colors.highlightedColor :
            state == SelectionState.Normal ? colors.normalColor :
            state == SelectionState.Pressed ? colors.pressedColor :
            state == SelectionState.Selected ? colors.selectedColor : Color.white;

        foreach (var graphic in GetGraphicChildren())
        {
            graphic.CrossFadeColor(targetColor, instant ? 0f : colors.fadeDuration, true, true);
        }
    }

    Graphic[] graphics;
    private Graphic[] GetGraphicChildren()
    {
        //if (graphics == null)
        graphics = GetComponentsInChildren<Graphic>();
        return graphics;
    }
}