﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UnitStatusIndicator : MonoBehaviour
{
    private IEnumerable<Talent> CurrentlyDisplayedBuffs = new List<Talent>();
    private bool RequiresUpdate;
    Coroutine currentCoroutine;

    public Image buffDisplay;
    public float FadeTimeInOut = 0.5f;
    public float StayTime = 0.25f;
    public AnimationCurve curve = AnimationCurve.Linear(0, 0, 1, 1);

    // Start is called before the first frame update
    void Start()
    {
        currentCoroutine = null;
        RequiresUpdate = false;
        buffDisplay.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (currentCoroutine == null && CurrentlyDisplayedBuffs.Any())
        {
            currentCoroutine = StartCoroutine(DisplayList(CurrentlyDisplayedBuffs));
        }
    }

    private IEnumerator DisplayList(IEnumerable<Talent> cdb)
    {

        //Debug.Log("DISPLAYING LIST: ");
        buffDisplay.gameObject.SetActive(true);
        yield return null;
        foreach( var b in cdb.ToList())
        {
            buffDisplay.color = Color.clear;
            buffDisplay.sprite = b.sprite;
            if (b.sprite == null) Debug.LogError($"Trying to display a buff that has no sprite! {b.TalentName} / {b.name}", b);
            float dt = 0f;
            while (dt < FadeTimeInOut)
            {
                yield return null;
                float pct = dt / FadeTimeInOut;
                buffDisplay.color = Color.Lerp(Color.clear, Color.white, curve.Evaluate(pct));
                dt += Time.deltaTime;
            }
            dt = 0f;
            while (dt < StayTime)
            {
                yield return null;
                dt += Time.deltaTime;
            }
            dt = 0f;
            while (dt < FadeTimeInOut)
            {
                yield return null;
                float pct = dt / FadeTimeInOut;
                buffDisplay.color = Color.LerpUnclamped(Color.white, Color.clear, curve.Evaluate(pct));
                dt += Time.deltaTime;
            }
            yield return null;
            if (RequiresUpdate) break;
        }
        currentCoroutine = null;
        RequiresUpdate = false;
        buffDisplay.gameObject.SetActive(false);
    }

    internal void SetBuffs(IEnumerable<Talent> buffs)
    {
        if (buffs.SequenceEqual(CurrentlyDisplayedBuffs))
        {
            // DoNothing
        } else
        {
            CurrentlyDisplayedBuffs = buffs;
            RequiresUpdate = true;
        }
    }
}
