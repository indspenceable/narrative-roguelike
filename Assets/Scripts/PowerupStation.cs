﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class PowerupStation : GameModeInstance
{
    public CampaignGameMode PowerupStationMode;
    public List<Animator> CharacterAnimators;
    public GameObject UpgradeWindow;
    public List<IconButton> UpgradeButtons;

    public CampaignGameMode ManagePartyMode;
    public TransitionEffect tEffect;

    public Persona NPCPersona;
    public TextboxController textbox;
    public Localizer l;
    public Globals g;
    private string ZoneName;
    public RadialMenu menu;

    public AudioClip HealingSound;
    public MetaProgression progression;
    public Sprite menuIconBacking;
    public Sprite menuIconSelectedBacking;

    public TooltipManager tooltips;

    public List<BaseBuilderPropDefinition> currentRewards
    {
        get
        {
            return PowerupStationMode.campaign.currentRewards;
        }
        set
        {
            PowerupStationMode.campaign.currentRewards = value;
        }
    }

    public override void PreActivate(object DeserializedData)
    {
        ZoneName = PowerupStationMode.campaign.currentStage.Identifier;
        Instantiate(PowerupStationMode.campaign.currentStage.PowerupStationBG, transform);
        for (int i = 0; i < 4; i += 1)
        {
            CharacterAnimators[i].runtimeAnimatorController = PowerupStationMode.campaign.team[i].presentation.AnimatorController;
        }
        currentRewards = g.All<BaseBuilderPropDefinition>().Where(pd => !pd.AlwaysAvailable && !progression.GetBool(pd.UnlockFlag, false)).Shuffled().ToList();
        if (currentRewards.Count() > 3) currentRewards = currentRewards.Take(3).ToList();
    }
    bool firstDialogueFinished = false;
    public AnimatorPool prizeAnimator;
    public MusicManager music;
    public AnimationCurve hopcurve;
    public AudioPool audioPool;

    public override void PostActivate()
    {
        g.Quicksave(PowerupStationMode, new object());
        StartCoroutine(DisplayDialogue());
    }

    public IEnumerator DisplayDialogue() {
        var npc_name = l._($"restarea.{ZoneName}.npc_name");
        yield return textbox.DisplayCo(l._($"restarea.{ZoneName}.message1"), npc_name, NPCPersona.DefaultPortrait, NPCPersona);
        g.All<AudioPool>().First().PlaySimple(HealingSound);
        yield return textbox.DisplayCo(l._($"restarea.{ZoneName}.message2"), npc_name, NPCPersona.DefaultPortrait, NPCPersona);
        firstDialogueFinished = true;
        if (currentRewards.Count == 0)
        {
            yield return textbox.DisplayCo(l._($"restarea.nothing_left"), NPCPersona.Identifier, NPCPersona.DefaultPortrait, NPCPersona);
            GotoNextZone();
        }
        else
        {
            menu.Setup(Vector3.zero, PowerupStationMode.campaign.currentRewards.Select(reward =>
            {
                return new MenuBase.MenuOption(
                    reward.PropSprite,
                    reward.Identifier,
                    () => SelectReward(reward),
                    menuIconBacking,
                    menuIconSelectedBacking,
                    true,
                    "",
                    (b) => {
                        if (b)
                            this.tooltips.SetOverrideTarget(reward, true);
                        else
                            this.tooltips.ClearOverrideTarget();
                    });
            }).ToArray());
        }
    }

    private void SelectReward(BaseBuilderPropDefinition reward)
    {
        menu.Close();
        StartCoroutine(_SelectReward(reward));
    }
    private IEnumerator _SelectReward(BaseBuilderPropDefinition reward) {
        yield return Util.Hop(this.prizeAnimator, this.audioPool, this.music, reward.PropSprite, GameObject.Find("Friendly").transform.position, CharacterAnimators.Pick().transform.position, 1f, this.hopcurve);
        progression.SetBool(reward.UnlockFlag, true);
        yield return textbox.DisplayCo("Now, get back out there!", NPCPersona.Identifier, NPCPersona.DefaultPortrait, NPCPersona);
        GotoNextZone();
    }

    public void TalkToNPC()
    {
        if (!firstDialogueFinished) return;
        textbox.Display(l._($"restarea.{ZoneName}.message3"), l._($"restarea.{ZoneName}.npc_name"), NPCPersona.DefaultPortrait, NPCPersona);
    }

    public void SelectTeamMember(int i) {
        return;
        var CharacterAnimator = CharacterAnimators[i];
        CharacterAnimator.SetFloat("Facing_X", 0);
        CharacterAnimator.SetFloat("Facing_Y", -1);
        CharacterAnimator.SetTrigger("Attack");

        List<Talent> intrinsicTalents = PowerupStationMode.campaign.team[i].IntrinsicTalents;
        for (int j = 0; j < UpgradeButtons.Count; j += 1)
        {

            if (intrinsicTalents.Count > j && intrinsicTalents[j].Upgrade != null)
            {
                UpgradeButtons[j].icon.sprite = intrinsicTalents[j].sprite;
                UpgradeButtons[j].gameObject.SetActive(true);
                UpgradeButtons[j].button.onClick.RemoveAllListeners();
                // to fix a scoping issue
                var jj = j;
                UpgradeButtons[j].button.onClick.AddListener(() => {
                    SelectIntrinsicActionToUpgrade(i, jj);
                });
            }
            else
            {
                UpgradeButtons[j].gameObject.SetActive(false);
            }
        }

        // Show the window for their actions.
        UpgradeWindow.SetActive(true);
    }
    public void SelectIntrinsicActionToUpgrade(int teamMemberIndex, int IntrinsicIndex)
    {
        var currentTalent = PowerupStationMode.campaign.team[teamMemberIndex].IntrinsicTalents[IntrinsicIndex];
        PowerupStationMode.campaign.team[teamMemberIndex].IntrinsicTalents[IntrinsicIndex] = currentTalent.Upgrade;

        GotoNextZone();
    }

    private void GotoNextZone()
    {
        ManagePartyMode.ActivateWithData(tEffect, PowerupStationMode.campaign);
    }
}
