﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TBSModeDetailPane : MonoBehaviour
{
    public GameObject container;
    public TMPro.TextMeshProUGUI NameText;
    public TMPro.TextMeshProUGUI EntryText;
    public void Show(string MonsterName, string Entry)
    {
        this.NameText.text = MonsterName;
        this.EntryText.text = Entry;
        this.container.SetActive(true);
    }
    public void Hide()
    {
        this.container.SetActive(false);
    }
}
