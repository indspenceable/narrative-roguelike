﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
public class Poolable : AbstractPoolable { }
public class ConfiguredPoolable : AbstractPoolable
{
    public UnityEvent PoolActivated;
    public UnityEvent TriggerDeactivate;

    public override void ExternalDeactivate()
    {
        TriggerDeactivate.Invoke();
    }

    public override void OnActivatedByPool(AbstractObjectPool pool)
    {
        PoolActivated.Invoke();
        pool.Released(this);
    }
}
