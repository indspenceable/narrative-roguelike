﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowTextFirstTime : ActivateableBehaviour
{
    public MetaProgression progression;
    public TextboxController tt;
    public Localizer localization;
    public string key;
    public string progressionBool;

    public CampaignGameMode campaignMode;
    public bool TiedToCampaign;

    public override void OnPostActivate()
    {
        if (TiedToCampaign) OnPostActivateCamapaign();
        else OnPostActivateStandard();
    }
    void OnPostActivateStandard() {
        string mpKey = $"shown_{key}";
        if (!progression.GetBool(mpKey))
        {
            tt.Display(localization._(key), "", tt.defaultPersona.DefaultPortrait, tt.defaultPersona);
            progression.SetBool(mpKey, true);
        }
    }
    void OnPostActivateCamapaign()
    {
        string mpKey = $"shown_{key}";
        if (!campaignMode.campaign.GetFlag(mpKey))
        {
            tt.Display(localization._(key), "", tt.defaultPersona.DefaultPortrait, tt.defaultPersona);
            campaignMode.campaign.SetFlag(mpKey, true);
        }
    }
    public void ManualTrigger()
    {
        tt.Display(localization._(key), "", tt.defaultPersona.DefaultPortrait, tt.defaultPersona);
    }
}
