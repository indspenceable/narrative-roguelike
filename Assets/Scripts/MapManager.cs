﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static AttackTalent;
using static MapUnit;

public class MapManager : MonoBehaviour, IAttacker
{

    public TBSGameManager manager;
    public Transform BL { get { return manager.LevelInstance.BL; } }
    public Transform TR { get { return manager.LevelInstance.TR; } }

    TBSGameManager AttackTalent.IAttacker.manager => manager;

    bool AttackTalent.IAttacker.Spent { get => false; set { } }

    public LayerMask GeometryLayer;
    public LayerMask WaterLayer;
    public List<MapTile> Tiles = new List<MapTile>();

    public Globals globals;

    public MapTile TileAt(Vector2Int tilePos)
    {
        return Tiles.Find(tile => tile.pos == tilePos);
    }

    private void OnDrawGizmos()
    {
        foreach(var tile in Tiles)
        {
            Gizmos.color = Color.green;
            Vector2 point = tile.pos.CenterOfTile();

            Gizmos.color = tile.MovementAllowed(UnitData.MobilityType.LAND) ? Color.green : (tile.MovementAllowed(UnitData.MobilityType.WATER) ? Color.blue : Color.red);
            Gizmos.DrawWireSphere(point, tile.Radius);
        }
    }

    // TODO This should live in MapUnit
    public float DurationPerTile = 0.2f;

    public interface IPathWalker
    {
        Animator GetCharacterAnimator();
        Vector2Int Size();
        Transform GetTransform();
        void MoveTo(Vector2Int a, bool b);
        bool ResetAnimatorsAtEnd();
        void OnExitTileCallback(Vector2Int vector2Int);
    }
    public IEnumerator WarpOnPath(IPathWalker actor, List<Vector2Int> currentPath)
    {
        var animator = actor.GetCharacterAnimator();
        animator.SetFloat("Facing_X", 0);
        animator.SetFloat("Facing_Y", -1);
        animator.Play("WarpOut");
        yield return null;
        while (animator.GetBool("Busy")) yield return new WaitForEndOfFrame();
        actor.MoveTo(currentPath.Last(), true);
        yield return new WaitForSeconds(0.3f);
        animator.Play("WarpIn");
        yield return null;
        while (animator.GetBool("Busy")) yield return new WaitForEndOfFrame();
    }

    internal IEnumerator WalkOnPath(IPathWalker actor, List<Vector2Int> currentPath, float durationScale = 1f)
    {
        return WalkOnPathStatic(actor, currentPath, DurationPerTile/durationScale);
    }
    internal static IEnumerator WalkOnPathStatic(IPathWalker actor, List<Vector2Int> currentPath, float DurationPerTile)
    {
        var offset = (Vector2.one * actor.Size()) / 2f;
        float dt = 0;
        var animator = actor.GetCharacterAnimator();

        for (int i = 0; i < currentPath.Count-1; i += 1)
        {
            while (dt < DurationPerTile)
            {
                var direction = currentPath[i + 1] - (Vector2)currentPath[i];

                animator.SetFloat("Facing_X", direction.x);
                animator.SetFloat("Facing_Y", direction.y);
                animator.SetBool("Moving", true);

                actor.GetTransform().position = Vector2.Lerp(currentPath[i] + offset, currentPath[i + 1] + offset, dt / DurationPerTile);
                actor.OnExitTileCallback(currentPath[i]);
                yield return null;
                dt += Time.deltaTime;
            }
            dt -= DurationPerTile;
            actor.MoveTo(currentPath[i + 1], true);
        }

        if (actor.ResetAnimatorsAtEnd())
        {
            animator.SetFloat("Facing_X", 0);
            animator.SetFloat("Facing_Y", -1);
        }
        animator.SetBool("Moving", false);
    }

    internal string GetTooltipForLocation(Vector2Int vector2Int)
    {
        return string.Join("\n", TileAt(vector2Int).AllCurrentEffects().Select(buff => buff.TalentName + " - " + buff.Description));
    }

    internal void StartOfTurnVoid(Vector2Int pos, List<IEnumerator> Work)
    {
        var tile = TileAt(pos);
        if (tile != null)
        {
            foreach (var buffData in tile.currentBuffs)
            {
                var occupied = manager.directory.UnitAt(pos) != null;
                if (occupied ? buffData.buff.TicksDownOccupied : buffData.buff.TicksDownUnoccupied)
                    buffData.stacks -= 1;
            }

            tile.currentBuffs.RemoveAll(buff => buff.stacks <= 0);
            tile.UpdateDecorator();

            //foreach (var cb in tile.AllCurrentEffects())
            //{
            //    Work.Add(cb.StartOfTurnMap(this, this, pos, 0));
            //}
            //Debug.Log("Done");
        } else
        {
            Debug.LogError($"Trying to access a tile that doesnt exist at {pos}");

        }
    }
    [NaughtyAttributes.Button]
    public void BuildTiles()
    {
        Tiles.Clear();
        for (int i = LowerBoundX(); i < UpperBoundX(); i += 1)
        {
            for (int j = LowerBoundY(); j < UpperBoundY(); j += 1)
            {
                var pos = new Vector2Int(i, j);
                Tiles.Add(new MapTile(this, pos));
            }
        }

        foreach (var tile in Tiles)
        {
            var t = Tiles.Find(tile2 => tile2.pos == tile.pos);
            for (int dx = -1; dx <= 1; dx += 1)
            {
                for (int dy = -1; dy <= 1; dy += 1)
                {
                    if (Mathf.Abs(dx) + Mathf.Abs(dy) == 1)
                    {
                        t.Neighbors.Add(tile.pos + new Vector2Int(dx, dy));
                    }
                }
            }
        }

        foreach (var SpecialTile in manager.LevelInstance.SpecialTiles)
        {
            TileAt(TileLocationFromTransform(SpecialTile.transform)).Intrinsics.AddRange(SpecialTile.Qualities);
        }
    }

    //internal void PlaceTreasureChest(TreasureChestMapEffect treasureChestME)
    //{
        
    //}

    public void PlaceTraps(int TrapTileCount)
    {
        if (manager.LevelInstance.TrapTiles.Count == 0)
        {
            //Debug.LogError("No trap tiles to place!");
            return;
        }
        foreach (var t in Tiles.Where(_t => _t.MovementAllowed(UnitData.MobilityType.LAND)).Shuffled().Take(TrapTileCount))
        {
            t.AddMapEffect(new BuffInstance(manager.LevelInstance.TrapTiles.Pick(), 1));
        }
        
    }

    public static Vector2Int TileLocationFromTransform(Transform u)
    {
        return new Vector2Int(Mathf.RoundToInt(u.position.x - 0.5f), Mathf.RoundToInt(u.position.y - 0.5f));
    }


    public int UpperBoundY()
    {
        return Mathf.FloorToInt(TR.position.y);
    }

    public int LowerBoundY()
    {
        return Mathf.CeilToInt(BL.position.y);
    }

    public int UpperBoundX()
    {
        return Mathf.FloorToInt(TR.position.x);
    }

    public int LowerBoundX()
    {
        return Mathf.CeilToInt(BL.position.x);
    }

    int AttackTalent.IAttacker.Strength()
    {
        return 0;
    }

    public Vector2Int Pos()
    {
        return Vector2Int.down * 999;
    }

    void IAttacker.GetHealed(int appliedDamage)
    {
    }

    Vector2 IAttacker.CenterOfSprite()
    {
        return Pos();
    }

    List<Vector2Int> IAttacker.CurrentPositions => new List<Vector2Int>() { Pos() };

    IEnumerator IAttacker.AfterUseAttack()
    {
        return null;
    }
    IEnumerator IAttacker.BeforeUseAttack()
    {
        return null;
    }

    void IAttacker.ResetAnimator()
    {
    }

    bool IAttacker.Busy()
    {
        return false;
    }

    void IAttacker.DisplayAttack(Vector2 direction, string attackParameter)
    {
    }

    int IAttacker.BonusHeals => 0;

    internal void Deactivate()
    {
        foreach (var t in Tiles)
            t.CleanupEverything();
    }

    public void GoToSleep()
    {
        //throw new NotImplementedException();
        // Map manager never goes to sleep OOPS.
    }

    public bool ShouldWake()
    {
        return true;
    }

    public bool IsAwake()
    {
        return true;
    }
}
