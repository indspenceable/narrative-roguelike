﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName ="Object Pools/Bouncy Text")]
public class BouncyTextPool : ObjectPoolBase<BouncyText>
{
    public BouncyText RequestWithString(Camera c, Vector3 Position, string text, Color fg, Color bg)
    {
        var req = Request(Vector3.zero);
        req.Setup(c, Position, text, fg, bg);
        return req;
    }
}