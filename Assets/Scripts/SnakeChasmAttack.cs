﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Snake/Chasm Attack")]
public class SnakeChasmAttack : AbstractSelfSkillTalent
{
    public List<SnakeChasmMapStatus> PossibleChasms;
    public Talent invisibleChasms;
    public override IEnumerator Use(MapUnit Me)
    {
        int tries = 100;
        Me.DisplayAttack(Vector2Int.down, "Tail");
        yield return null;
        while (Me.CharacterAnimator.GetBool("Busy")) yield return null;
        Me.manager.cameraShake.Shake(CameraShake.Magnitude.ELEVEN);
        for (int i = 0; i < 3; i += 1)
        {
            tries -= 1;
            if (tries < 0) yield break;
            Vector2Int p = SelectRandomLandPoint(Me);
            var chasm = SelectValidChasm(Me.manager, p);
            if (chasm == null)
            {
                i -= 1;
            }
            else
            {
                Me.manager.map.TileAt(p).AddMapEffect(new BuffInstance(chasm, 3));
                var InvisibleOffsets = chasm.InvisibleOffsets.Select(io => io + p).ToList();
                foreach (Vector2Int p2 in InvisibleOffsets)
                {
                    Me.manager.map.TileAt(p2).AddMapEffect(new BuffInstance(invisibleChasms, 3));
                    MapUnit displacedUnit = Me.manager.directory.UnitAt(p2);
                    if (displacedUnit != null && displacedUnit != Me)
                    {
                        displacedUnit.MoveTo(Me.manager.FindClosestAvailableSpotTo(p2), true);
                    }
                }
            }
        }
        yield return null;
    }

    private Vector2Int SelectRandomLandPoint(MapUnit Me)
    {
        return Me.manager.map.Tiles.Where(t => t.MovementAllowed(UnitData.MobilityType.LAND)).Shuffled().First().pos;
    }

    private class ChasmPlacement
    {
        public Vector2Int pos;
        public SnakeChasmMapStatus def;

        public ChasmPlacement(Vector2Int pos, SnakeChasmMapStatus def)
        {
            this.pos = pos;
            this.def = def;
        }
    }
    private SnakeChasmMapStatus SelectValidChasm(TBSGameManager manager, Vector2Int p)
    {
        foreach(var c in PossibleChasms.Shuffled())
        {
            if (CheckChasm(manager, c, p)) return c;
        }
        return null;
    }

    private bool CheckChasm(TBSGameManager manager, SnakeChasmMapStatus c, Vector2Int p)
    {
        if (!manager.map.TileAt(p).MovementAllowed(UnitData.MobilityType.LAND)) return false;

        var InvisibleOffsets = c.InvisibleOffsets.Select(io => io + p).ToList();
        foreach (Vector2Int p2 in InvisibleOffsets)
        {
            MapTile mapTile = manager.map.TileAt(p2);
            if (mapTile == null) return false;
            if (!mapTile.MovementAllowed(UnitData.MobilityType.LAND)) return false;
            if (manager.map.TileAt(p2).currentBuffs.Any(bi => bi.buff is SnakeChasmMapStatus)) return false;
        }

        return true;
    }
}
