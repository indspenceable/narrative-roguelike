﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

[CreateAssetMenu(menuName = "Cutscenes/Credits")]
public class CreditsCutscene : AbstractCutsceneData
{
    [System.Serializable]
    public class CreditsZoneData
    {
        public StageData stage;
        public CutscenePropDefinition map;
        public Vector2Int pos;
    }
    public List<CreditsZoneData> zones;
    public List<UnitDefinition> Bosses;

    [NaughtyAttributes.ResizableTextArea] public string SetupScript;
    [Space]
    [NaughtyAttributes.ResizableTextArea] public string StartingScript;
    [NaughtyAttributes.ResizableTextArea] public string PerUnitMacro;
    [NaughtyAttributes.ResizableTextArea] public string StartZoneMacro;
    [NaughtyAttributes.ResizableTextArea] public string EndZoneMacro;

    public CutsceneData copyTarget;
    [NaughtyAttributes.Button]
    public void SaveToTarget()
    {
        if (copyTarget == null) { Debug.Log("No target to save to. Set the copyTarget."); }
        else
        {
            copyTarget.SetupScript = GetSetupScript();
            copyTarget.Script = GetScript();
            //UnityEditor.EditorUtility.SetDirty(copyTarget);
        }
    }
    
    public override string GetScript()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine(StartingScript);
        foreach (var zone in zones)
        {
            sb.AppendLine(ZoneBlock(zone, StartZoneMacro));
            foreach (var e in zone.stage.enemies)
            {
                sb.AppendLine(FillEnemyBlock(e));
            }
            sb.AppendLine(ZoneBlock(zone, EndZoneMacro));
        }
        return sb.ToString();
    }

    private string FillEnemyBlock(EnemyDefinition e)
    {
        var str = PerUnitMacro;
        str = str.Replace("$UNIT_IDENT", e.Data.presentation.Identifier);

        str = str.Replace("$UNIT_NAME", e.Data.CharacterName);
        return str;
    }
    private string ZoneBlock(CreditsZoneData z, string initial)
    {
        var str = initial;
        str = str.Replace("$ZONE_MAP", z.map.Identifier);
        str = str.Replace("$ZONE_X", z.pos.x.ToString());
        str = str.Replace("$ZONE_Y", z.pos.y.ToString());
        return str;
    }


    public override string GetSetupScript()
    {
        return SetupScript;
    }

    public override bool MainCharacter(string characterName)
    {
        return false;
    }

}
