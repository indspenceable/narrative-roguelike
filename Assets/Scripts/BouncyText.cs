﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BouncyText : AbstractPoolable
{
    public Canvas canvas;
    public List<TMPro.TextMeshProUGUI> TextDisplays;
    public TMPro.TextMeshProUGUI MainTextDisplay;
    public RectTransform Container;
    public AnimationCurve BounceCurve;
    public AnimationCurve FadeCurve;
    public float duration;
    

    public Coroutine Setup(Camera c, Vector3 Position, string text, Color fg, Color bg)
    {
        canvas.worldCamera = c;
        Container.position = Position;
        foreach (var TextDisplay in TextDisplays)
        {
            TextDisplay.text = text;
            var color = (TextDisplay == MainTextDisplay) ? fg : bg;
            TextDisplay.color = color;
            TextDisplay.enabled = true;
        }
        
        return StartCoroutine(Bounce(Position, fg, bg));
    }

    IEnumerator Bounce(Vector3 Position, Color fg, Color bg)
    {
        float dt = 0f;
        while (dt <= duration)
        {
            yield return null;
            dt += Time.unscaledDeltaTime;
            Container.position = Position + Vector3.up * BounceCurve.Evaluate(dt/duration);
        }
        dt = 0f;
        while (dt <= duration)
        {
            yield return null;
            dt += Time.unscaledDeltaTime;
            foreach (var TextDisplay in TextDisplays)
            {
                var color = (TextDisplay == MainTextDisplay) ? fg : bg;
                TextDisplay.color = Color.Lerp(color, Color.clear, FadeCurve.Evaluate(dt / duration));
                //TextDisplay.color = color;
            }
        }
        ExternalDeactivate();
    }
}
