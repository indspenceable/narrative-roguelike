﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Skills/Target And Self Attack")]
public class TargetAndSelfAttack : AttackTalent
{
    public OnHitEffect SelfAttack;
    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        yield return base.Use(Me, target);
        yield return ExecuteAttack(SelfAttack, Me, Me.data.pos, true);
    }
    public override bool TargetsSelf => true;
}
