﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/ApplyStatusToMapOnTileExit")]
public class CreateMapEffectTalentOnTileExit : AbstractTargetedSkillTalent
{
    public BuffInstance MapStatus;
    public override bool TargetsFriendly => false;
    public override bool TargetsHostile => false;
    public override bool TargetsEmpty => false;
    public override bool TargetsSelf => false;

    public override IEnumerator Use(MapUnit Me, Vector2Int target) => null;

    public override void OnExitTile(MapUnit Me, Vector2Int location)
    {
        Me.manager.map.TileAt(location).AddMapEffect(MapStatus);
    }
}
