﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static MapUnit;

[CreateAssetMenu(menuName = "Talents/Skills/Summon")]
public class SummonSkillTalent : AbstractTargetedSkillTalent
{

    public AudioClip clip;
    public AudioPool audioPool;
    public AnimatorPool animatorPool;
    public RuntimeAnimatorController effectRAC;

    public UnitDefinition Summonable;
    public List<BuffInstance> InitialBuffs = new List<BuffInstance>();
    public override bool TargetsEmpty => true;

    public override bool TargetsFriendly => false;

    public override bool TargetsHostile => false;

    public override bool TargetsSelf => false;

    public override bool ValidTargetableTileContents(MapUnit Me, Vector2Int v2i)
    {
        MapTile mapTile = Me.manager.map.TileAt(v2i);
        return base.ValidTargetableTileContents(Me, v2i) && ((mapTile != null) && mapTile.MovementAllowed(Summonable.Data.Mobility));
    }

    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        ExecuteSummon(Me, target, Summonable.Data, InitialBuffs, Summonable.AlternateCostumes);
        this.audioPool.PlaySimple(clip);
        this.animatorPool.RequestWithAnimator(target.CenterOfTile(), this.effectRAC);
        yield return new WaitForSeconds(0.25f);
    }
    public static MapUnit ExecuteSummon(MapUnit Me, Vector2Int target, UnitData SummonableData, List<BuffInstance> InitialBuffs, List<CharacterAnimationData> alternateCostumes) {
        Me.CharacterAnimator.SetTrigger("Attack");
        var mapUnit = Me.manager.directory.BuildMapUnit(Me.manager.globals.Duplicate(SummonableData), target, Me.data.Friendly, alternateCostumes);
        // Hackity hack to make sure every summoned unit has a background circle when used.
        if (mapUnit.data.presentation.IconBackingSmall == null) mapUnit.data.presentation.IconBackingSmall = Me.data.presentation.IconBackingSmall;

        mapUnit.Spent = true;
        foreach (var b in InitialBuffs)
        {
            mapUnit.ApplyStatus(Me.manager.globals.Duplicate(b));
        }
        return mapUnit;
    }
}
