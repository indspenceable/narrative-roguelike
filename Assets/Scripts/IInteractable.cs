﻿using UnityEngine;

public interface IInteractable
{
    string GetMenuText();
    Sprite InteractionSprite();
    void OnUseInteractable(MapUnit me, TBSGameManager.InputHandler prev);
    void UpdateInteractable();
    void OnDeactivate();
    bool Blocks();
    Vector2Int Pos();
    void AttachToCurrentLevel(TBSGameManager manager);
    bool ShowsUpInActionMenu();
}
