﻿using System.Collections;
using UnityEngine;

public class TownsfolkChatter : AbstractInteractable
{

    public Localizer l;
    public string textKey;
    public int Variations;
    public override IInteractable GetInteractable()
    {
        return new TownsfolkChatterRaw(l, textKey + Random.Range(1,Variations+1), MapManager.TileLocationFromTransform(transform));
    }
}

[System.Serializable]
public class TownsfolkChatterRaw : IInteractable
{
    private Localizer l;
    private string textKey;
    private Vector2Int _pos;
    [System.NonSerialized]
    private TBSGameManager manager;

    public TownsfolkChatterRaw(Localizer l, string textKey, Vector2Int _pos)
    {
        this.l = l;
        this.textKey = textKey;
        this._pos = _pos;
    }

    public bool Blocks()
    {
        return false;
    }

    public void OnDeactivate()
    {
        
    }

    public string GetMenuText()
    {
        return "Talk to townsfolk";
    }

    public Sprite InteractionSprite()
    {
        return manager.TalkSprite;
    }

    public void OnUseInteractable(MapUnit me, TBSGameManager.InputHandler prev)
    {
        me.StartCoroutine(OnUseInteractableCo(me, prev));
        manager.CurrentHandler = new NoOp();
    }
    private IEnumerator OnUseInteractableCo(MapUnit me, TBSGameManager.InputHandler prev) { 
        yield return this.manager.textbox.DisplayCo(l._(textKey), "Townsfolk", null, manager.textbox.defaultPersona);
        me.Spent = true;
        manager.InstallDefaultHandler();
    }

    public Vector2Int Pos()
    {
        return _pos;
    }

    public bool ShowsUpInActionMenu()
    {
        return true;
    }

    public void UpdateInteractable()
    {
    }

    public void AttachToCurrentLevel(TBSGameManager manager)
    {
        this.manager = manager;
    }
}