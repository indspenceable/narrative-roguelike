﻿using System;
using System.Linq;
using UnityEngine;

public class SerializableScriptableObject : ScriptableObject
{
    [NaughtyAttributes.ReadOnly]
    public string _HiddenIdentifier;
    public string Identifier;

    public virtual void OnValidate()
    {
        if (_HiddenIdentifier == null ||
            _HiddenIdentifier == "")
        {
            Debug.LogError("SSO with no identifier", this);
        }
        else
        {
            System.Collections.Generic.IEnumerable<SerializableScriptableObject> dups = Resources.FindObjectsOfTypeAll<SerializableScriptableObject>().Where(t => t._HiddenIdentifier == _HiddenIdentifier && t != this);
            if (dups.Any())
            {
                Debug.LogError("Duplicate identifiers for:");
                Debug.LogError(this, this);
                Debug.LogError(dups.First(), dups.First());
            }
        }
    }

    [NaughtyAttributes.Button]
    public void GenerateNewGUID()
    {
        _HiddenIdentifier = Guid.NewGuid().ToString();
    }
}
