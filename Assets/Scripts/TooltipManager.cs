﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public interface IHasTooltip
{
    string GetTooltip();
}

public class TooltipManager : MonoBehaviour
{
    public Camera cam;
    private IHasTooltip currentTarget;
    private Coroutine fadeCoroutine;
    public Image toolTipContainer;
    
    public TMPro.TextMeshProUGUI tooltipText;
    public AnimationCurve Easing = AnimationCurve.Linear(0, 0, 1, 1);
    [UnityEngine.Serialization.FormerlySerializedAs("duration")]
    public float SlowDuration = 2f;
    public float QuickDuration = 0.5f;

    public PointerEventData ped;
    public RectTransform bounds;

    public bool StaticLocation;
    public Vector2 StaticLocationPosition;

    public Globals g;

    // Start is called before the first frame update
    void Start()
    {

    }

    private IHasTooltip _overrideTarget = null;
    public bool LockOverride = false;
    public void SetOverrideTarget(IHasTooltip newTarget, bool overrideLock = false)
    {
        if (_overrideTarget == null)
        {
            _overrideTarget = newTarget;
            LockOverride = overrideLock;
        }
    }
    public void ClearOverrideTarget()
    {
        _overrideTarget = null;
        LockOverride = false;
    }

        // Update is called once per frame
        void LateUpdate()
    {
        Vector3 mousePosition = cam.ScreenToWorldPoint(Input.mousePosition);
        if (_overrideTarget != null)
        {
            if (StaticLocation)
            {
                SetTooltip(cam.ViewportToWorldPoint(StaticLocationPosition), _overrideTarget, QuickDuration);
            }
            else
            {
                SetTooltip(mousePosition, _overrideTarget, SlowDuration);
            }
            if (!LockOverride) {
                _overrideTarget = null;
            }
            return;
        }
        
        // Check the ray from the mouse position for all hits
        // on any layer that have IHasTooltip
        foreach (var hit in Physics2D.OverlapPointAll(mousePosition))
        {
            var comp = hit.GetComponent<IHasTooltip>();
            if (comp != null)
            {
                SetTooltip(mousePosition, comp, SlowDuration);
                g.SetMouseHighlight(true);
                return;
            }
        }
        currentTarget = null;
        if (fadeCoroutine != null)
            StopCoroutine(fadeCoroutine);
        toolTipContainer.gameObject.SetActive(false);
        //g.SetMouseHighlight(false);

        // on those, track the current one, and if it changes, update the tooltip state.
    }

    private void SetTooltip(Vector3 ttPos, IHasTooltip comp, float duration)
    {
        if (currentTarget != comp)
        {
            fadeCoroutine = StartCoroutine(FadeInOut(false, duration));
            tooltipText.text = comp.GetTooltip();
            currentTarget = comp;
        }

        toolTipContainer.rectTransform.position = new Vector2(
            Util.SmartFloor(ttPos.x, 16),
            Util.SmartFloor(ttPos.y, 16));
        bounds.ForceUpdateRectTransforms();
        toolTipContainer.rectTransform.ForceUpdateRectTransforms();

        Vector3 BoundsTopRight = new Vector3(
            bounds.rect.xMax / 16 + bounds.position.x,
            bounds.rect.yMax / 16 + bounds.position.y);
        Vector3 BoundsBottomLeft = new Vector3(
            bounds.rect.xMin / -16 + bounds.position.x,
            bounds.rect.yMin / -16 + bounds.position.y);
        
        Vector3 ttTopRight = new Vector3(
           toolTipContainer.rectTransform.rect.xMax / 16 + toolTipContainer.rectTransform.position.x,
           toolTipContainer.rectTransform.rect.yMax / 16 + toolTipContainer.rectTransform.position.y);
        Vector3 ttBottomLeft = new Vector3(
           toolTipContainer.rectTransform.rect.xMin / -16 + toolTipContainer.rectTransform.position.x,
           toolTipContainer.rectTransform.rect.yMin / -16 + toolTipContainer.rectTransform.position.y);
        //indicator.transform.position = ttTopRight;
        //indicator2.transform.position = ttBottomLeft;
        if (ttTopRight.x > BoundsTopRight.x) ttPos.x -= (ttTopRight.x - BoundsTopRight.x);
        //else if (ttBottomLeft.x < BoundsBottomLeft.x) ttPos.x += (ttBottomLeft.x - BoundsBottomLeft.x);
        if (ttTopRight.y > BoundsTopRight.y) ttPos.y -= (ttTopRight.y - BoundsTopRight.y);
        //else if (ttBottomLeft.y < BoundsBottomLeft.y) ttPos.y += (ttBottomLeft.y - BoundsBottomLeft.y);


        toolTipContainer.transform.position = new Vector2(
            Util.SmartFloor(ttPos.x, 16),
            Util.SmartFloor(ttPos.y, 16));
        return;
    }

    IEnumerator FadeInOut(bool FadeOut, float duration)
    {
        float dt = 0;
        toolTipContainer.gameObject.SetActive(true);
        while (dt <= duration)
        {
            dt += Time.deltaTime;
            if (FadeOut)
            {
                toolTipContainer.color = Color.Lerp(Color.white, Color.clear, Easing.Evaluate(dt / duration));
                tooltipText.color = Color.Lerp(Color.black, Color.clear, Easing.Evaluate(dt / duration));
            }
            else
            {
                toolTipContainer.color = Color.Lerp(Color.clear, Color.white, Easing.Evaluate(dt / duration));
                tooltipText.color = Color.Lerp(Color.clear, Color.black, Easing.Evaluate(dt / duration));
            }
            yield return null;
        }
        toolTipContainer.gameObject.SetActive(!FadeOut);
    }
}
