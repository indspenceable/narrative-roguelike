﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class AbstractDirectionalSkillTalent : AbstractTargetedSkillTalent
{
    private Vector2Int[] Directions = new Vector2Int[] { Vector2Int.left, Vector2Int.right, Vector2Int.up, Vector2Int.down };
    public override List<Vector2Int> PossibleTargets(MapUnit Me)
    {
        // TODO this should work with large targets!
        return Directions.Where(d => CanTargetDirection(Me, d)).Select(d=>Me.data.pos+d).ToList();
    }

    public abstract bool CanTargetDirection(MapUnit Actor, Vector2Int d);
}
