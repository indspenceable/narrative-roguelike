﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class Persona : SerializableScriptableObject
{
    public AudioClip[] SpeechClips;
    public int DefaultOffset = 0;
    [NaughtyAttributes.ReorderableList]
    public int[] Offsets = new int[] { 0 };
    [NaughtyAttributes.ShowAssetPreview]
    public Sprite ContainerSprite;
    public Color TextColor;
    public Sprite DefaultPortrait;
    [System.Serializable]
    public class PortraitPair
    {
        public string Key;
        [NaughtyAttributes.ShowAssetPreview]
        public Sprite Value;
    }
    public List<PortraitPair> _PortraitData = new List<PortraitPair>();
    public bool HasSpecialPortrait(string key)
    {
        return _PortraitData.Any(pp => pp.Key == key);
    }
    public Sprite GetPortrait(string key)
    {
        var result = _PortraitData.Find(pp => pp.Key == key);
        if (result == null) return DefaultPortrait;
        return result.Value;
    }
}
