﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Wait")]
public class WaitCommand : AbstractSelfSkillTalent
{
    public override bool Available(MapUnit Me, List<Vector2Int> path)
    {
        return true;
    }

    public override IEnumerator Use(MapUnit Me)
    {
        if (Me.data.Friendly && !Me.data.DoesNotAct) Me.manager.audioPool.Play(Me.manager.soundConfig.WaitSound,1f,1f);
        Me.ReduceCooldowns();
        Me.manager.hoverHint.focus = null;
        yield return null;
    }
}