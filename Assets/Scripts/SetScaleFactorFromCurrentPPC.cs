﻿using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class SetScaleFactorFromCurrentPPC : MonoBehaviour
{
    private PixelPerfectCamera ppc;
    public CanvasScaler scaler;
    private void Update()
    {
        Camera mc = Camera.main;
        if (ppc != null && ppc.gameObject.activeInHierarchy)
            ppc = null;
        if (ppc == null && mc != null)
            ppc = mc.GetComponent<PixelPerfectCamera>();
        if (ppc != null)
            scaler.scaleFactor = ppc.pixelRatio;
    }
}
