﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Snake")]
public class SnakeControllerStrategy : AIStrategy
{
    public override float ScorePlan(MapUnit currentUnit, AIActionPlan plan)
    {
        if (currentUnit.CurrentPositions.Contains(plan.path[plan.path.Count-1]) && plan.path.Count > 0) return 0;
        var rtn = base.ScorePlan(currentUnit, plan);
        return rtn;
    }
}
