﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Rotation Strategy")]
public class RotateAttacksStrategy : AIStrategyBase
{
    [NaughtyAttributes.ReorderableList]
    public List<AbstractSkillTalent> talents = new List<AbstractSkillTalent>();
    // So the way this one works is - put in 4 talents, and we'll shuffle them and blah blah blahs

    private List<string> GetData(MapUnit currentUnit)
    {
        object o;
        string key = GetType().ToString();
        if (!currentUnit.data.AIDataCache.TryGetValue(key, out o))
        {
            currentUnit.data.AIDataCache[key] = new List<string>();
            o = currentUnit.data.AIDataCache[key];
        }
        return o as List<string>;
    }


    public override List<ISkillBase> GetSkills(MapUnit currentUnit)
    {
        var storedData = GetData(currentUnit);
        //storedData.Clear();
        var OffCooldown = talents.Where(t => !storedData.Contains(t._HiddenIdentifier));
        //log += "Stored data is: " + string.Join(", ", storedData) + "\n";
        //log += string.Join(", ", OffCooldown.Select(oc => oc.SkillName())) + "\n";
        if (OffCooldown.Count() == 0)
        {
            storedData.Clear();
            OffCooldown = talents;
        }
        return OffCooldown.Cast<ISkillBase>().ToList();
    }

    public override IEnumerator SelectAndEnactPlan(MapUnit currentUnit)
    {
        AIActionPlan plan = SelectPlan(currentUnit);
        if (plan == null)
        {
            plan = new AIActionPlan(new List<Vector2Int>() { currentUnit.data.pos }, currentUnit.DefaultSkills[0], currentUnit.data.pos);
        }
        if (plan.skill != null)
            GetData(currentUnit).Add((plan.skill as AbstractSkillTalent)._HiddenIdentifier);
        yield return TakeUnitAction(currentUnit, plan, BossRoar);
    }

    public override float ScorePlan(MapUnit currentUnit, AIActionPlan plan)
    {
        return plan.skill == null ? -1 : 1;
    }
}
