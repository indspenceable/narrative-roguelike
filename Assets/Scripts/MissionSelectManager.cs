﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MissionSelectManager : GameModeInstance
{
    public CampaignGameMode msm;
    public TextboxController Textbox;
    public TransitionEffect te;
    public Globals g;
    //public RadialMenu menu;
    public CampaignGameMode ManagePartyGamemode;
    public CampaignGameMode TBSGameMode;
    public CutsceneMode cutscene;
    public MusicManager music;
    public AudioClip track;

    public List<BaseBuilderTeamMemberStation> PartyMembers;

    public List<ActivateableBehaviour> PACallbacks = new List<ActivateableBehaviour>();

    public int MissionCount = 5;
    public List<StageSelectButton> FirstMission;
    public List<StageSelectButton> LastMission;
    public List<StageSelectButton> MidMissions;
    public TMPro.TextMeshProUGUI MissionHint;
    public TMPro.TextMeshProUGUI MissionHintTitle;
    public Localizer localizer;

    public override void PreActivate(object DeserializedData) {
        if (DeserializedData != null)
        {
            msm.campaign = DeserializedData as CampaignStatus;
        }
        for (int i = 0; i < 4; i += 1) {
            //var pm = PartyMembers[i];
            msm.campaign.team[i].currentDamage = 0;

            var tm = msm.campaign.team[i];
            //pm.CharacterName = tm.CharacterName;
        }
        foreach (var ssb in FirstMission) {
            ssb.b.interactable = (msm.campaign.CompletedStages.Count == 0) && !msm.campaign.CompletedStages.Contains(ssb.stage);
        }
        foreach (var ssb in MidMissions)
        {
            ssb.b.interactable = (msm.campaign.CompletedStages.Count != 0) &&
                (msm.campaign.CompletedStages.Count != MissionCount - 1) &&
                !msm.campaign.CompletedStages.Contains(ssb.stage);
        }
        foreach (var ssb in LastMission)
        {
            ssb.b.interactable = (msm.campaign.CompletedStages.Count == MissionCount - 1) && !msm.campaign.CompletedStages.Contains(ssb.stage);
        }
        //LastMission.SetActive(msm.campaign.CompletedStages.Count == MissionCount - 1);
        //MidMissions.SetActive(!FirstMission.activeSelf && !LastMission.activeSelf);
        music.FadeTo(track);
        MissionHint.text = "";
        MissionHintTitle.text = "";
    }

    public override void PostDeactivate()
    {
        //menu.Close();
    }

    public void ShowBasebuilderHint()
    {
        MissionHintTitle.text = localizer._("stages.beach.name").StandardizeLineHeight();
        MissionHint.text = localizer._("stages.beach.hint").StandardizeLineHeight();
    }

    public void ShowHint(StageSelectButton button)
    {
        var stage = button.stage;
        if (button.b.interactable)
        {
            MissionHintTitle.text = localizer._($"stages.{stage.Identifier}.name").StandardizeLineHeight();
            MissionHint.text = localizer._($"stages.{stage.Identifier}.hint").StandardizeLineHeight();
        }
    }
    public void HideHint()
    {
        MissionHintTitle.text = "";
        MissionHint.text = "";
    }

    public void GoToGameMode(CampaignGameMode cgm)
    {
        cgm.ActivateWithData(te, msm.campaign);
    }

    public override void PostActivate()
    {
        g.Quicksave(msm, msm.campaign);
        foreach (var pa in PACallbacks) pa.OnPostActivate();
    }

    public void OnValidate()
    {
        PACallbacks.RemoveAll(m => m == null);
    }

    public void SelectMission(StageData stage)
    {
        msm.campaign.currentStage = stage;
        if (stage.HasPowerupStation)
            msm.campaign.PowerupStationLocation = Random.Range(1, stage.LevelsCount - 2);
        else
            msm.campaign.PowerupStationLocation = 9999;
        msm.campaign.CurrentStageProgress = 0;

        if (msm.campaign.CompletedStages.Count > 0)
            cutscene.ActivateWithData(te, msm.campaign, stage.IntroCutscene, () => ManagePartyGamemode.ActivateWithData(te, msm.campaign));
        else
            cutscene.ActivateWithData(te, msm.campaign, stage.IntroCutscene, () => TBSGameMode.ActivateWithData(te, msm.campaign));
    }
}
