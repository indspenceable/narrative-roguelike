﻿using System.Collections;
using UnityEngine;
using static AttackTalent;

[CreateAssetMenu(menuName = "Talents/Status/STSPoison")]
public class STSPoison : BuffDefinition
{
    public OnHitEffect hitEffect;
    public Globals g;
    public override IEnumerator StartOfTurn(IAttacker target, IAttacker Stage, Vector2Int pos, int stacks)
    {
        if (target != null)
        {
            //OnHitEffect ohe = g.Duplicate(hitEffect);
            hitEffect.Damage = stacks;
            yield return AttackTalent.ExecuteAttackAll(hitEffect, Stage, pos, false, new Color(0.5f, 0f, 1f));

        }
    }
}