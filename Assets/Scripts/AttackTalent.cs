﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Skills/Base")]
public class AttackTalent : AbstractTargetedSkillTalent
{
    public interface IAttacker
    {
        TBSGameManager manager { get; }
        bool Spent { get; set; }

        int Strength();
        Vector2Int Pos();
        void GetHealed(int appliedDamage);
        Vector2 CenterOfSprite();
        List<Vector2Int> CurrentPositions { get; }

        IEnumerator AfterUseAttack();
        IEnumerator BeforeUseAttack();
        void ResetAnimator();
        bool Busy();
        void DisplayAttack(Vector2 direction, string attackParameter);
        int BonusHeals { get; }

        void GoToSleep();
        bool ShouldWake();
        bool IsAwake();
    }


    public OnHitEffect onHit;

    public bool TargetEmptyTiles = false;
    public override bool TargetsEmpty => TargetEmptyTiles;
    public bool TargetFriendlyUnits = false;
    public override bool TargetsFriendly => TargetFriendlyUnits;
    public bool TargetHostileUnit = true;
    public override bool TargetsHostile => TargetHostileUnit;
    public bool CanHitSelf = false;
    public override bool TargetsSelf => CanHitSelf;

    public AnimatorPool animPool;


    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        yield return Me.BeforeUseAttack();
        yield return ExecuteAttackAll(onHit, Me, target, true);
    }

    public static IEnumerator ExecuteAttackAll(OnHitEffect currentAttack, IAttacker Me, Vector2Int CenteredAt, bool AnimateAttack, Color? color = null)
    {
        if (!color.HasValue) color = Color.red;
        List<Coroutine> cos = new List<Coroutine>();
        for (int dx = -currentAttack.Radius; dx <= currentAttack.Radius; dx += 1)
        {
            for (int dy = -currentAttack.Radius; dy <= currentAttack.Radius; dy += 1)
            {
                Vector2Int offset = new Vector2Int(dx, dy);
                if (offset.Manhattan() <= currentAttack.Radius)
                {
                    Vector2Int currentTarget = CenteredAt + offset;
                    IAttacker targetedUnit = Me.manager.directory.UnitAt(currentTarget);
                    if (targetedUnit != null && ((Me!=targetedUnit) || currentAttack.CanHitSelf))
                        cos.Add(Me.manager.StartCoroutine(ExecuteAttack(currentAttack, Me, currentTarget, AnimateAttack, color)));
                }
            }

        }
        foreach (var c in cos) yield return c;
        //Debug.Log("All done!");
    }

    static Vector2Int FindBestDirection(IAttacker facer, Vector2Int dest)
    {
        var myTile = facer.CurrentPositions.OrderBy(p => p.ManhattanDistance(dest)).First();
        return FindBestDirection(dest - myTile);
    }

    public static IEnumerator ExecuteAttack(OnHitEffect onHit, IAttacker Me, Vector2Int TargetedTile, bool AnimateAttack, Color? color = null)
    {
        if (!color.HasValue) color = Color.red;
        for (int HitNumber = 0; HitNumber < onHit.NumberOfHits; HitNumber += 1)
        {

            Vector2Int direction = FindBestDirection(Me, TargetedTile);
            Vector2 FacingDirection = FindBestDirection(TargetedTile.CenterOfTile() - Me.CenterOfSprite());

            if (onHit.OverrideKBDirection != Vector2Int.zero)
            {
                direction = onHit.OverrideKBDirection;
                FacingDirection = onHit.OverrideKBDirection;
            }

            MapUnit targetUnit = Me.manager.directory.UnitAt(TargetedTile);

            if (Me == (IAttacker)targetUnit) FacingDirection = Vector2Int.down;
            // Animate!
            if (AnimateAttack)
            {
                Me.DisplayAttack(FacingDirection, onHit.AttackAnimatorParameter);
                yield return null;
                while (Me.Busy()) yield return null;

                if (onHit.Projectile != null) {
                    var animPoolable = Me.manager.animatorPool.RequestWithAnimator(Me.CenterOfSprite(), onHit.Projectile);
                    //Debug.Log(animPoolable, animPoolable);
                    //Debug.Break();
                    animPoolable.anim.SetFloat("Facing_X", FacingDirection.x);
                    animPoolable.anim.SetFloat("Facing_Y", FacingDirection.y);
                    var dist = Me.CenterOfSprite() - TargetedTile.CenterOfTile();
                    yield return Me.manager.StartCoroutine(Util.MoveTransformToPosition(animPoolable.transform, Me.CenterOfSprite(), TargetedTile.CenterOfTile(), AnimationCurve.Linear(0, 0, 1, 1), 0.1f));
                    //Debug.Break();
                    animPoolable.ExternalDeactivate();
                }
            }

            if (onHit.TriggersShake != CameraShake.Magnitude.ZERO)
            {
                yield return Me.manager.cameraShake.ShakeCO(onHit.TriggersShake);
            }

            if (targetUnit == null) continue;

            if (!targetUnit.Dead())
            {
                if (Me == (IAttacker)targetUnit) FacingDirection = Vector2Int.up;
                if (targetUnit.ChangeFacingOnAttacks() && Me != (IAttacker)targetUnit)
                {
                    targetUnit.CharacterAnimator.SetFloat("Facing_X", -FacingDirection.x);
                    targetUnit.CharacterAnimator.SetFloat("Facing_Y", -FacingDirection.y);
                }
                targetUnit.CharacterAnimator.SetTrigger("GetHit");
                Me.manager.audioPool.Play(onHit.SFX, 1f, 1f);

                if (onHit.AnimatorController != null)
                {
                    Me.manager.animatorPool.RequestWithAnimator(TargetedTile.CenterOfTile(), onHit.AnimatorController);
                }
            }

            if (!targetUnit.Dead() && (IAttacker)targetUnit != Me && onHit.Knockback != 0 && targetUnit.CanKnockBack())
            {
                var currentDirection = direction;
               
                // can't knockback yourself!
                if (onHit.Knockback > 0)
                {
                    for (int i = 0; i < onHit.Knockback; i += 1)
                    {
                        if (targetUnit.CanEnter(targetUnit.data.pos + currentDirection))
                            yield return Util.MoveUnitToPosition(targetUnit, targetUnit.data.pos + currentDirection,
                                AnimationCurve.EaseInOut(0, 0, 1, 1), 0.2f);
                        else {
                            var AppliedDamage = targetUnit.TakeDamage(1, onHit.Nonlethal);
                            var text = Me.manager.bouncyTextPool.RequestWithString(Me.manager.cameraBlit.GetComponent<Camera>(), (targetUnit.data.pos).CenterOfTile(), AppliedDamage.ToString(), Color.red, Color.white);
                            targetUnit.HealthMeter.SetEngagedAndUpdate();
                            yield return new WaitForSeconds(1f);
                            MapUnit HitByCollision = targetUnit.manager.directory.UnitAt(targetUnit.data.pos + currentDirection);
                            if (HitByCollision)
                            {
                                AppliedDamage = HitByCollision.TakeDamage(1, onHit.Nonlethal);
                                text = Me.manager.bouncyTextPool.RequestWithString(Me.manager.cameraBlit.GetComponent<Camera>(), (HitByCollision.data.pos).CenterOfTile(), AppliedDamage.ToString(), Color.red, Color.white);
                                yield return new WaitForSeconds(1f);
                            }
                            targetUnit.HealthMeter.Engaged = false;
                        }
                    }
                }
                else
                {

                    if (targetUnit.CanEnter(Me.Pos() - currentDirection))
                    {
                        yield return Util.MoveUnitToPosition(targetUnit, Me.Pos() - currentDirection,
                            AnimationCurve.EaseInOut(0, 0, 1, 1), 0.2f);
                        for (int i = 1; i < -onHit.Knockback; i += 1)
                        {
                            if (targetUnit.CanEnter(targetUnit.data.pos - currentDirection))
                            {
                                yield return Util.MoveUnitToPosition(targetUnit, targetUnit.data.pos - currentDirection,
                                    AnimationCurve.EaseInOut(0, 0, 1, 1), 0.2f);
                            }
                            else
                            {
                                targetUnit.HealthMeter.SetEngagedAndUpdate();
                                var AppliedDamage = targetUnit.TakeDamage(1, onHit.Nonlethal);
                                var text = Me.manager.bouncyTextPool.RequestWithString(Me.manager.cameraBlit.GetComponent<Camera>(), (targetUnit.data.pos + currentDirection).CenterOfTile(), AppliedDamage.ToString(), Color.red, Color.white);
                                yield return new WaitForSeconds(1f);
                                targetUnit.HealthMeter.Engaged = false;
                            }
                        }
                    }
                }
            }
            yield return null;

            //Before damage, instakill attacks.
            if (!targetUnit.Dead() && onHit.InstantKill)
            {
                targetUnit.data.currentDamage = targetUnit.MaxHP();
            }

            //Before damage, reset cooldowns as needed.
            if (!targetUnit.Dead() && onHit.ResetCooldowns)
            {
                targetUnit.data.Cooldowns.Clear();
            }

            // Damage!
            if (!targetUnit.Dead() && onHit.Damage > 0)
            {
                int v = Me.Strength();
                int FinalDamage = Mathf.Max(CalculateDamage(onHit, targetUnit) + v, 0);
                var AppliedDamage = targetUnit.TakeDamage(FinalDamage, onHit.Nonlethal);
                MapTile tile = Me.manager.map.TileAt(targetUnit.data.pos);
                targetUnit.HealthMeter.SetEngagedAndUpdate();
                Me.manager.bouncyTextPool.RequestWithString(
                    Me.manager.cameraBlit.GetComponent<Camera>(), targetUnit.CenterOfSprite(), AppliedDamage.ToString(), color.Value, Color.white);
                if (onHit.Drain)
                {
                    Me.GetHealed(AppliedDamage);
                    Me.manager.audioPool.Play(onHit.DrainSFX, 1f, 1f);
                    Me.manager.bouncyTextPool.RequestWithString(
                        Me.manager.cameraBlit.GetComponent<Camera>(), Me.CenterOfSprite(), AppliedDamage.ToString(), Color.green, Color.white);
                }


                yield return new WaitForSeconds(1f);

                targetUnit.HealthMeter.Engaged = false;
                // Do spikes damage here
                if (!targetUnit.Dead() && targetUnit.SpikesAmt() > 0) 
                {
                    int v2 = targetUnit.SpikesAmt();
                    int FinalDamage2 = Mathf.Max(v2, 0);
                    var AppliedDamage2 = targetUnit.TakeDamage(FinalDamage2, onHit.Nonlethal);
                    targetUnit.HealthMeter.SetEngagedAndUpdate();
                    Me.manager.bouncyTextPool.RequestWithString(
                        Me.manager.cameraBlit.GetComponent<Camera>(), Me.CenterOfSprite(), AppliedDamage2.ToString(), Color.red, Color.white);

                    yield return new WaitForSeconds(1f);
                }
            }
            // Healing!
            if (!targetUnit.Dead() && onHit.Damage < 0)
            {
                int FinalHeal = Mathf.Max(-onHit.Damage + Me.BonusHeals, 0);
                targetUnit.GetHealed(FinalHeal);
                targetUnit.HealthMeter.SetEngagedAndUpdate();
                MapTile tile = Me.manager.map.TileAt(targetUnit.data.pos);
                var text = Me.manager.bouncyTextPool.RequestWithString(
                    Me.manager.cameraBlit.GetComponent<Camera>(), tile.pos.CenterOfTile(), FinalHeal.ToString(), Color.green, Color.white);
                yield return new WaitForSeconds(1f);
                targetUnit.HealthMeter.Engaged = false;
            }

            if (!targetUnit.Dead() && onHit.TargetStatus.stacks > 0)
            {
                targetUnit.ApplyStatus(onHit.TargetStatus);
            }

            // only do OnUseAttack on damaging attacks!
            if (onHit.TriggersOnAttackCallback)
            {
                Me.AfterUseAttack();
                if (!targetUnit.Dead())
                {
                    yield return targetUnit.OnGetHit();
                }
            }

            if (AnimateAttack)
            {
                Me.ResetAnimator();
                if (!targetUnit.Dead())
                {
                    targetUnit.ResetAnimator();
                }
            }
        }
    }

    private static int CalculateDamage(OnHitEffect onHit, MapUnit targetUnit)
    {
        return onHit.Damage + onHit.Weaknesses.Select(weak => Mathf.Min(targetUnit.GetStacksOfTempStatus(weak.status),weak.maxStacks) * weak.bonus).Append(0).Sum();
    }

    private static List<Vector2Int> Directions = new List<Vector2Int>() { Vector2Int.up, Vector2Int.down, Vector2Int.left, Vector2Int.right };
    private static Vector2Int FindBestDirection(Vector2 inputDirection)
    {
        return Directions.Shuffled().OrderByDescending(direction => Vector2.Dot(direction, inputDirection)).First();
    }
}
