﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class OnHitEffect
{
    public int NumberOfHits = 1;
    public int Damage;
    [Tooltip("ATM drain only applies on damage. Could do reverse drain for heals, though?")]
    public bool Drain = false;
    public int Knockback = 0;
    [UnityEngine.Serialization.FormerlySerializedAs("Status")]
    public BuffInstance TargetStatus;
    public RuntimeAnimatorController AnimatorController;

    public CameraShake.Magnitude TriggersShake = CameraShake.Magnitude.ONE;
    public bool TriggersOnAttackCallback = true;

    [Tooltip("Radius 0 is just the target tile. Each one from there is manhattan distance.")]
    public int Radius = 0;
    public AudioClip SFX;
    public AudioClip DrainSFX;
    public bool InstantKill = false;
    public bool Nonlethal = false;
    public bool ResetCooldowns = false;
    public bool CanHitSelf = true;
    public RuntimeAnimatorController Projectile;
    public string AttackAnimatorParameter = "Attack";
    public Vector2Int OverrideKBDirection;



    [System.Serializable]
    public class Vulnerability
    {
        public Talent status;
        public int bonus;
        public int maxStacks = 999;
    }
    public List<Vulnerability> Weaknesses;
}
