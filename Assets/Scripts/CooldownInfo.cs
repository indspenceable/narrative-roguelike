﻿[System.Serializable]
public class CooldownInfo
{
    public string CooldownName;
    public float TotalTime;
    public float RemainingTime;

    public CooldownInfo(string cooldown, float totalTime)
    {
        CooldownName = cooldown;
        TotalTime = totalTime;
        RemainingTime = totalTime;
    }

    public void Advance(float dt)
    {
        RemainingTime -= dt;
    }
}

[System.Serializable]
public class CooldownInfoInt
{
    public string CooldownName;
    public int RemainingTime;

    public CooldownInfoInt(string cooldown, int totalTime)
    {
        CooldownName = cooldown;
        RemainingTime = totalTime;
    }

    public void Advance()
    {
        RemainingTime -= 1;
    }
}
