﻿using UnityEngine;

public abstract class ActivateableBehaviour : MonoBehaviour
{
    public abstract void OnPostActivate();
}
