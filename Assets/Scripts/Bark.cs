﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Bark")]
public class Bark : AbstractSelfSkillTalent
{
    public int range;
    public override IEnumerator Use(MapUnit Me)
    {
        foreach(var u2 in Me.manager.directory.Query(u => u.data.pos.ManhattanDistance(Me.data.pos) <= range))
        {
            u2.WakeIfNeeded();
        }
        yield return null;
    }
}