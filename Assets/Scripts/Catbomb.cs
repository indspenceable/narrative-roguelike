﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Catbomb")]
public class Catbomb : AbstractTargetedSkillTalent {
    public OnHitEffect KillCat;
    public OnHitEffect Explode;

    public override bool TargetsFriendly => true;
    public override bool TargetsHostile => false;
    public override bool TargetsEmpty => false;
    public override bool TargetsSelf => false;

    public override bool ValidTargetableTileContents(MapUnit Me, Vector2Int v2i)
    {
        return base.ValidTargetableTileContents(Me, v2i) && Me.manager.directory.UnitAt(v2i) != null && Me.manager.directory.UnitAt(v2i).data.IsCat;
    }
    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        // ME is isgard
        var cat = Me.manager.directory.UnitAt(target);
        yield return AttackTalent.ExecuteAttackAll(KillCat, Me.manager.map, target, true);
        yield return AttackTalent.ExecuteAttackAll(Explode, Me.manager.map, target, true);
    }


}
