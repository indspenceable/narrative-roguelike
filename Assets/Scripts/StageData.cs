﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

[CreateAssetMenu()]
public class StageData : SerializableScriptableObject {
    public List<LevelData> Maps;
    public int LevelsCount;
    public List<EnemyDefinition> enemies = new List<EnemyDefinition>();
    public CutsceneData IntroCutscene;
    public CutsceneData BossCutscene;
    public CutsceneData StageCompleteCutscene;
    public GameObject PowerupStationBG;
    public TreasureChestDefinition TreasureChestDefinition;
    public bool IsNytmurCastle {
        get {
            return !HasPowerupStation;
        }
    }

    public bool HasPowerupStation
    {
        get
        {
            return PowerupStationBG != null;
        }
    }
}