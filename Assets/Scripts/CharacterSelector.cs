﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelector : MonoBehaviour
{
    public AssembleTeamManager manager;
    public List<UnitDefinition> PossibleCharacters;
    public List<CharacterSelector> Others;
    public UnitDefinition currentSelection;
    public int initialSelection;
    public List<Selectable> UI;

    public Image PortraitImage;
    public AnimationCurve MovementCurve;
    public Transform StandardAnchor;
    public Transform LowerAnchor;
    public Transform UpperAnchor;
    public TextboxController textbox;
    public Localizer localizer;
    public Globals g;

    private void Start()
    {
        Others.Remove(this);
        currentSelection = PossibleCharacters[initialSelection];
        PortraitImage.sprite = currentSelection.Data.presentation.BigSprite;
    }

    public void ShowCharacterDescription()
    {
        textbox.ExternalForceClose();
        textbox.Display(localizer._($"characters.{currentSelection.Data.Identifier}.description"), "", null, textbox.defaultPersona);
    }
    public void ShowCharacterJoinDialogue()
    {
        textbox.ExternalForceClose();
        CharacterAnimationData cad = g.FindById<CharacterAnimationData>(currentSelection.Data.Identifier);
        textbox.Display(localizer._($"characters.{currentSelection.Data.Identifier}.join_line"), currentSelection.Data.Identifier, cad.persona.DefaultPortrait, cad.persona);
    }

    public void DisableChanges()
    {
        foreach (var b in UI) b.interactable = false;
    }

    public void NextCharacter(int delta)
    {
        //DisableChanges();
        //var currentPossibles = PossibleCharacters.Where(pc => !manager.AssembledTeam.Contains(pc)).ToList();
        //int currentIndex = currentPossibles.IndexOf(currentSelection);
        //if (currentIndex == -1)
        //{
        //    currentIndex = 0;
        //}
        //var newUnit = currentPossibles[(currentIndex + delta + currentPossibles.Count) % currentPossibles.Count];


        //StartCoroutine(NextCharacterCO(newUnit));
    }
    IEnumerator NextCharacterCO(UnitDefinition newUnit)
    {
        currentSelection = newUnit;
        foreach (var b in UI) b.interactable = false;
        float duration = 0.55f;
        float dt = 0f;
        while (dt <= duration)
        {
            yield return null;
            dt += Time.deltaTime;
            var direction = LowerAnchor.position - StandardAnchor.position;

            PortraitImage.transform.position = StandardAnchor.position + direction * MovementCurve.Evaluate(dt / duration);
        }

        PortraitImage.sprite = newUnit.Data.presentation.BigSprite;

        dt = 0f;
        while (dt <= duration)
        {
            yield return null;
            dt += Time.deltaTime;
            PortraitImage.transform.position = Vector3.Lerp(UpperAnchor.position, StandardAnchor.position, MovementCurve.Evaluate(dt / duration));
            var direction = StandardAnchor.position - UpperAnchor.position;

            PortraitImage.transform.position = UpperAnchor.position + direction * MovementCurve.Evaluate(dt / duration);
        }

        PortraitImage.transform.position = StandardAnchor.position;
        foreach (var b in UI) b.interactable = true;
    }
}
