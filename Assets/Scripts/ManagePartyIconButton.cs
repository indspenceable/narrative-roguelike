﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ManagePartyIconButton :MonoBehaviour, IPointerClickHandler
{
    public Animator anim;
    public Image icon;
    public Image backing;
    public Talent talent;
    public System.Action UnloadCallback;

    internal void Setup(Sprite Backing, Talent talent, float delay, System.Action UnloadCallback)
    {
        anim.SetBool("Hidden", true);
        Invoke("_show", delay/10f);
        icon.sprite = talent.sprite;
        backing.sprite = Backing;
        this.talent = talent;
        this.UnloadCallback = UnloadCallback;
    }

    public void _show()
    {
        anim.SetBool("Hidden", false);
        anim.Play("Bounce");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        UnloadCallback();
    }
    
}