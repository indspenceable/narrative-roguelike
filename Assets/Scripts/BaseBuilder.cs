﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BaseBuilder : GameModeInstance
{

    public List<BaseBuilderPropDefinition> AvailableProps
    {
        get
        {
            return g.All<BaseBuilderPropDefinition>().Where(p => p.AvailableToPlace(propManager) && !p.IsVarient).ToList();
        }
    }
    public List<Image> PropIcons = new List<Image>();
    public int currentScrollOffset;
    private int CurrentRootProp;
    private BaseBuilderPropDefinition currentPropToPlace;
    public GhostProp GhostPropHint;
    public BaseBuilderPropManager propManager;

    public Camera cam;
    public CameraControlsBaseBuilder bbCameraController;
    public CampaignGameMode msm;
    public TransitionEffect te;
    public AudioClip audioTrack;
    public MusicManager mm;

    public override void PostActivate()
    {
        foreach (var pa in PACallbacks) pa.OnPostActivate();
    }

    //public List<BaseBuilderTeamMemberStation> PartyMembers;
    public void BackToSelectMission()
    {
        msm.ActivateWithData(te, msm.campaign);
    }

    [NaughtyAttributes.Button]
    public void ClearAllProps()
    {
        propManager.UnplaceAllProps();
    }

    [System.Serializable]
    public struct Area
    {
        public Transform BL;
        public Transform TR;
    }
    public Area[] AllAreas;
    public Area FullMap;
    public int currentAreaIndex;
    public Area currentArea => AllAreas[currentAreaIndex];

    public Globals g;

    public LayerMask GeometryLayer;
    public LayerMask WallLayerMask;
    public AudioClip ValidBleep;
    public AudioClip InvalidBleep;
    public AudioClip ClearBleep;
    public List<ActivateableBehaviour> PACallbacks = new List<ActivateableBehaviour>();
    public Sprite NytmurSprite;

    public override void PreActivate(object DeserializedData)
    {
        propManager.BuildPreexistingProps();
        ResetPropButtonIcons();
        bbCameraController.SetCurrentBounds(currentArea.BL, currentArea.TR);

        //for (int i = 0; i < 4; i += 1)
        //{
        //    var pm = PartyMembers[i];
        //    var tm = msm.campaign.team[i];
        //    pm.Setup(tm);
        //    pm.transform.position = SelectRandomSpot();
        //}

        mm.FadeTo(audioTrack);
    }

    public override void PostDeactivate()
    {
        base.PostDeactivate();
        propManager.DestroyAllExistingProps();
        GhostPropHint.Hide();
    }



    //Vector3 SelectRandomSpot()
    //{

    //    Func<Vector3> generate = () => {
    //        return new Vector3(
    //            Random.Range(FullMap.BL.position.x, FullMap.TR.position.x),
    //            Random.Range(FullMap.BL.position.y, FullMap.TR.position.y)
    //            );
    //    };
    //    var pt = generate();
    //    Vector2Int tile = PositionToTileAddress(pt);
    //    while (propManager.PoolablesAtLocation(tile).Count > 0 || GhostPropHint.CheckTileForProp(tile))
    //    {
    //        pt = generate();
    //        tile = PositionToTileAddress(pt);
    //    }
    //    return (Vector2)tile;
    //}

    public bool PropPlacementDisallowed(BaseBuilderPropDefinition cptp, Vector2Int vector2Int)
    {

        var pos = (Vector2)vector2Int;
        Debug.DrawRay((Vector3)(Vector2)pos + (Vector3.left + Vector3.up) * 0.15f, 0.3f * (Vector3.right + Vector3.down), Color.red, 0.25f);
        Debug.DrawRay((Vector3)(Vector2)pos + (Vector3.right + Vector3.up) * 0.15f, 0.3f * (Vector3.left + Vector3.down), Color.red, 0.25f);

        var pmPoolables = propManager.PoolablesAtLocation(vector2Int);
        var conflicts = pmPoolables.Any(bbp => propManager.CheckPropShouldRemoveExistingProp(cptp, bbp));
        //bool PropCheck = BaseBuilderPropManager.CheckPropClearsLayer(currentPropToPlace, conflics)
        return GeoDisallowsPlacement(cptp, vector2Int) || conflicts;

    }

    private bool GeoDisallowsPlacement(BaseBuilderPropDefinition cptp, Vector2Int vector2Int)
    {
        if (cptp.layer == BaseBuilderPropDefinition.LOCATION_CATEGORY.WALL)
        {
            return !Physics2D.OverlapCircle(vector2Int, 0.25f, WallLayerMask);
        }
        return Physics2D.OverlapCircle(vector2Int, 0.25f, GeometryLayer | WallLayerMask);
    }

    internal void HandleClick(int mouseButton)
    {
        if (mouseButton == 0)
        {
            if (currentPropToPlace != null)
            {
                Vector2Int mp = MousePositionToTileAddress();
                //Debug.Log("Placing prop at: " + mp);
                var PP = new BaseBuilderPropPlacement(currentPropToPlace, mp);
                if (!PP.AllOccupiedTiles().Any(t => GeoDisallowsPlacement(currentPropToPlace, t)))
                {
                    foreach (var p in PP.AllOccupiedTiles())
                    {
                        propManager.ClearPropAt(p, currentPropToPlace);
                    }
                    propManager.BuildPropPlacement(PP);
                    ResetPropButtonIcons();
                    if (!currentPropToPlace.AvailableToPlace(propManager))
                    {
                        GhostPropHint.Hide();
                        currentPropToPlace = null;
                    }
                    g.All<AudioPool>().First().PlaySimple(ValidBleep);
                } else
                {
                    g.All<AudioPool>().First().PlaySimple(InvalidBleep);
                }
            }
        } else if (mouseButton == 1)
        {
            var pos = MousePositionToTileAddress();
            if (propManager.ClearPropAt(pos, null))
                g.All<AudioPool>().First().PlaySimple(ClearBleep);
            ResetPropButtonIcons();
        }
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            AdjustCurrentPropVarient(-1);
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            AdjustCurrentPropVarient(1);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GhostPropHint.Hide();
            currentPropToPlace = null;
        }
        //var pos = MousePositionToTileAddress();
    }

    private void AdjustCurrentPropVarient(int i)
    {
        AvailableProps[CurrentRootProp].AdjustVarient(i);
        PrepareToPlacePropByRootIndex(CurrentRootProp);
        ResetPropButtonIcons();
        PreparePropPlacementHint();
    }

    public void ScrollBy(int delta)
    {
        currentScrollOffset = (currentScrollOffset + delta + AvailableProps.Count) % AvailableProps.Count;
        ResetPropButtonIcons();
    }
    public void ChangeCurrentArea(int delta)
    {
        currentAreaIndex = (currentAreaIndex - delta + AllAreas.Count()) % AllAreas.Count();
        bbCameraController.SetCurrentBounds(currentArea.BL, currentArea.TR);
        bbCameraController.AutoFocus();

        //currentPropToPlace = null;
        //GhostPropHint.Hide();
    }

    public Vector2Int MousePositionToTileAddress()
    {
        Vector3 mp = (Vector2)cam.ScreenToWorldPoint(Input.mousePosition);
        return PositionToTileAddress(mp);
    }
    public Vector2Int PositionToTileAddress(Vector3 mp) { 
        return new Vector2Int(Mathf.RoundToInt(mp.x), Mathf.RoundToInt(mp.y));
    }

    public void OnDrawGizmos()
    {
        foreach(var area in this.AllAreas)
        {
            Transform TR = area.TR;
            Transform BL = area.BL;
            DrawArea(TR, BL);
        }
    }

    private static void DrawArea(Transform TR, Transform BL)
    {
        var a = new Vector3(TR.position.x, TR.position.y);
        var b = new Vector3(BL.position.x, TR.position.y);
        var c = new Vector3(BL.position.x, BL.position.y);
        var d = new Vector3(TR.position.x, BL.position.y);

        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(a, b);
        Gizmos.DrawLine(c, b);
        Gizmos.DrawLine(c, d);
        Gizmos.DrawLine(a, d);
    }

    public void PrepareToPlacePropByButtonIndex(int ButtonIndex) {
        var rootIndex = (ButtonIndex + currentScrollOffset) % AvailableProps.Count;
        PrepareToPlacePropByRootIndex(rootIndex);
    }
    public void PrepareToPlacePropByRootIndex(int rootIndex) {
        this.CurrentRootProp = rootIndex;
        currentPropToPlace = AvailableProps[CurrentRootProp].GetCurrentVarient();
        PreparePropPlacementHint();
    }

    private void PreparePropPlacementHint()
    {
        GhostPropHint.Setup(currentPropToPlace);
    }
    
    public void ResetPropButtonIcons() {
        for (int i = 0; i < PropIcons.Count; i += 1)
        {
            PropIcons[i].sprite = AvailableProps[(i + currentScrollOffset) % AvailableProps.Count].GetCurrentVarient().PropSprite;
            PropIcons[i].SetNativeSize();
            var sizeDelta = PropIcons[i].rectTransform.sizeDelta;
            PropIcons[i].rectTransform.sizeDelta = sizeDelta.normalized * 16f;
        }
    }
}
