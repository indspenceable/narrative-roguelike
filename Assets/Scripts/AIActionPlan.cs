﻿using System.Collections.Generic;
using UnityEngine;

public class AIActionPlan
{
    public List<Vector2Int> path;
    public ISkillBase skill;
    public Vector2Int target;

    public AIActionPlan(List<Vector2Int> path, ISkillBase skill, Vector2Int target)
    {
        this.path = path;
        this.skill = skill;
        this.target = target;
    }
}
