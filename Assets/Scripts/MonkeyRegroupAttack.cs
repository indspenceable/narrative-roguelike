﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Lavamonkey/Regroup")]
public class MonkeyRegroupAttack : AbstractTargetedSkillTalent
{
    public override bool TargetsFriendly => true;
    public override bool TargetsHostile => true;
    public override bool TargetsEmpty => true;
    public override bool TargetsSelf => true;

    public BuffDefinition MonkeyBuff;
    public EnemyDefinition BigMonkey;
    public List<BuffInstance> InitialBuffs;

    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        List<MapUnit> monkeys = Me.manager.directory.Query(mu => mu.currentBuffs.Any(bi => bi.buff == MonkeyBuff)).ToList();
        int totalHP = monkeys.Select(m => m.MaxHP() - m.data.currentDamage).Sum();
        foreach(var monkey in monkeys)
        {
            Me.manager.directory.RemoveUnitWithoutAnimation(monkey);
        }
        var mapUnit = SummonSkillTalent.ExecuteSummon(Me, target, BigMonkey.Data, InitialBuffs, new List<CharacterAnimationData>());
        mapUnit.data.currentDamage = mapUnit.MaxHP() - totalHP;
        mapUnit.data.Cooldowns.Add("Boss", 3);
        mapUnit.CharacterAnimator.Play("LavaMonkeyAppear");
        yield return null;
    }
}
