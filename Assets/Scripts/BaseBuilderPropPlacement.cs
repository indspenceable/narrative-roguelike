﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseBuilderPropPlacement
{
    public BaseBuilderPropDefinition propDef;
    public Vector2Int position;

    public BaseBuilderPropPlacement(BaseBuilderPropDefinition propDef, Vector2Int position)
    {
        this.propDef = propDef;
        this.position = position;
    }

    public bool IsInPosition(Vector2Int p)
    {
        float px = p.x + propDef.StandardizedOffsetPosition.x;
        float py = p.y + propDef.StandardizedOffsetPosition.y;
        var rtn = (px >= position.x && px < position.x + propDef.Size.x) &&
            (py >= position.y && py < position.y + propDef.Size.y);
        return rtn;
    }
    public List<Vector2Int> AllOccupiedTiles()
    {
        var rtn = new List<Vector2Int>();
        for (int x = -propDef.Size.x; x <= propDef.Size.x; x += 1)
        {
            for (int y = -propDef.Size.y; y <= propDef.Size.y; y += 1)
            {
                Vector2Int currentPosition = new Vector2Int(position.x + x, position.y + y);
                if (IsInPosition(currentPosition))
                {
                    rtn.Add(currentPosition);
                }
            }
        }
        return rtn;
    }
}