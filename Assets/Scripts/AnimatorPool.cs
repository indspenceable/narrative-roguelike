﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Object Pools/Animator")]
public class AnimatorPool : ObjectPoolBase<AnimatorPoolable>
{
    internal AnimatorPoolable RequestWithAnimator(Vector3 position, RuntimeAnimatorController rac)
    {
        var rtn = Request(position);
        rtn.Setup(rac);
        return rtn;
    }
}