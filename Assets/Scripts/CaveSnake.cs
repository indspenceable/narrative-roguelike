﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CaveSnake : MapUnit
{
    public Animator SnakeSegmentPrefab;
    public List<Animator> SegmentRenderers = new List<Animator>();

    public SnakeData SDYo;

    [System.Serializable]
    public class SnakeData
    {

        public List<Vector2Int> SegmentLocations = new List<Vector2Int>();
        public Vector2Int Facing;
        public SnakeData(UnitData data, int segments)
        {
            for (int i = 0; i < segments; i += 1)
            {
                SegmentLocations.Add(data.pos);
            }
            SegmentLocations.Reverse();
            Facing = Vector2Int.left;
        }

    }
    public override void PostSetup()
    {
        if (data.SpecialUnitDataCache == null)
        {
            ResetSnakeData();
        }
        RefreshSegmentRenderers(GetSnakeData());
        var diff = GetSnakeData().Facing;
        this.CharacterAnimator.SetFloat("Facing_X", diff.x);
        this.CharacterAnimator.SetFloat("Facing_Y", diff.y);
        this.name = "SNAKE ENEMY";
    }

    [NaughtyAttributes.Button]
    public void ResetSnakeData()
    {
        int segments = 9;
        data.pos = data.pos + Vector2Int.right * segments;
        SnakeData newSnakeData = new SnakeData(data, segments);
        data.SpecialUnitDataCache = newSnakeData;
        RefreshSegmentRenderers(newSnakeData);
        SDYo = newSnakeData;
        for (int i = 0; i < segments; i += 1) MOVE_LEFT();
    }

    public SnakeData GetSnakeData()
    {
        if (data == null) return null;
        return data.SpecialUnitDataCache as SnakeData;
    }

    public override void MoveTo(Vector2Int pos, bool RefreshDecorators)
    {
        var OldPos = data.pos;
        base.MoveTo(pos, RefreshDecorators);

        SnakeData sd = GetSnakeData();
        if (sd != null && RefreshDecorators )
        {
            if (OldPos != pos)
            {
                sd.SegmentLocations.RemoveAt(0);
                sd.SegmentLocations.Add(pos);
            }
            RefreshSegmentRenderers(sd);
        }
    }

    [NaughtyAttributes.Button] public void MOVE_LEFT() { MoveTo(data.pos + Vector2Int.left, true); }
    [NaughtyAttributes.Button] public void MOVE_RIGHT() { MoveTo(data.pos + Vector2Int.right, true); }
    [NaughtyAttributes.Button] public void MOVE_UP() { MoveTo(data.pos + Vector2Int.up, true); }
    [NaughtyAttributes.Button] public void MOVE_DOWN() { MoveTo(data.pos + Vector2Int.down, true); }

    public override bool CanKnockBack()
    {
        return false;
    }
    public override bool ChangeFacingOnAttacks()
    {
        return false;
    }

    public override void ResetAnimator()
    {
        CharacterAnimator.SetBool("Moving", false);
    }


    public Color C1 = Color.black;
    public Color C2 = Color.white;

    private void RefreshSegmentRenderers(SnakeData sd)
    {
        var segments = sd.SegmentLocations;
        int DesiredSegmentCount = segments.Count;
        while (SegmentRenderers.Count > DesiredSegmentCount)
        {
            Destroy(SegmentRenderers[0].gameObject);
            SegmentRenderers.RemoveAt(0);
        }
        while (SegmentRenderers.Count < DesiredSegmentCount)
        {
            Animator item = Instantiate(SnakeSegmentPrefab);
            SegmentRenderers.Add(item);
            item.gameObject.SetActive(true);
        }

        for (int i = 0; i < DesiredSegmentCount; i += 1)
        {
            if (SnakeAI.CHECK(segments[i]) || i == DesiredSegmentCount-1) SegmentRenderers[i].GetComponent<SpriteRenderer>().color = C1;
            else SegmentRenderers[i].GetComponent<SpriteRenderer>().color = C2;

            SegmentRenderers[i].transform.position = segments[i] + Vector2.one / 2f;
            SegmentRenderers[i].gameObject.name = "Segment " + i;

            if (i > 0)
            {
                SegmentRenderers[i].SetFloat("OutDirection", SelectMovementHandler.DirectionBetween(segments[i], segments[i - 1]));
            }
            else
            {
                SegmentRenderers[i].SetFloat("OutDirection", -1);
            }
            if (i < DesiredSegmentCount - 2)
            {
                SegmentRenderers[i].SetFloat("InDirection", SelectMovementHandler.DirectionBetween(segments[i], segments[i + 1]));
            }
            else
            {
                var direction = SelectMovementHandler.DirectionBetween(segments.Last(), data.pos);
                SegmentRenderers[i].SetFloat("InDirection", direction);
            }

        }
    }
    public override List<Vector2Int> CurrentOffsets()
    {
        var rtn = new List<Vector2Int>();
        rtn.Add(Vector2Int.zero);

        if (GetSnakeData() != null)
            foreach (var sl in GetSnakeData().SegmentLocations)
            {
                rtn.Add(sl - data.pos);
            }
        return rtn;
    }
    internal Vector2Int Facing()
    {
        return CurrentOffsets()[1] - CurrentOffsets()[0];
    }
    public override void OnUnitDeath()
    {
        base.OnUnitDeath();
        GetSnakeData().SegmentLocations.Clear();
        RefreshSegmentRenderers(GetSnakeData());
    }

    public override bool CheckOffsets(Vector2Int neighbor)
    {
        var _o = data.pos;
        MapTile mapTile = manager.map.TileAt(neighbor + _o);
        bool blocked = mapTile != null && mapTile.Blocked(data.Mobility);
        MapUnit _u = manager.directory.UnitAt(neighbor + _o);
        return !blocked && (_u == null || (_u.data.Friendly == data.Friendly));
    }
}
