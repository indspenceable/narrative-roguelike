﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TextboxController : MonoBehaviour
{
    public GameObject Container;
    public Image backingImage;
    

    public Sprite DefaultContainerSprite;
    public Color DefaultTextColor;
    public Image portraitImage;
    public TMPro.TextMeshProUGUI VisibleTextBox;
    public TMPro.TextMeshProUGUI InvisibleTextBox;
    public Animator Prompt;
    public AnimationCurve Curve = AnimationCurve.Linear(0, 0, 1, 1);
    public AudioPool audioPool;

    public Image NameTagContainer;
    public TMPro.TextMeshProUGUI NameTagText;

    [System.Serializable]
    public class Command
    {
        public string CommandString;
        public UnityEvent Callback;
        public float Duration;
        public bool ActiveDuringSkip;
    }
    public List<Command> Commands;

    private static TextboxController _Instance;
    public Persona defaultPersona;

    public static TextboxController Instance
    {
        get
        {
            return _Instance;
        }
    }
    private void Start()
    {
        _Instance = this;
        Container.SetActive(false);
        VisibleTextBox.ForceMeshUpdate();
    }
    
    public void AddTextEffect(TextEffect item)
    {
        this.CurrentTextEffects.Add(item);
    }
    public void ClearTextEffects()
    {
        this.CurrentTextEffects.Clear();
    }

    void Process(int vertexIndex, int characterIndex, List<Vector3> vertices)
    {
        if (LetterEffects.Count >= characterIndex)
            foreach(var te in LetterEffects[characterIndex])
            {
                te.Process(vertexIndex, characterIndex, vertices);
            }
    }

    private void Update()
    {
        if (AnimationPaused) return;
        FullMeshUpdateWithEffects();
    }

    private void FullMeshUpdateWithEffects()
    {
    
        var vertices = new List<Vector3>();
        VisibleTextBox.ForceMeshUpdate();

        VisibleTextBox.mesh.GetVertices(vertices);
        for (int i = 0; i < VisibleTextBox.textInfo.characterCount; i += 1)
        {
            var charInfo = VisibleTextBox.textInfo.characterInfo[i];
            if (!charInfo.isVisible) continue;

            var vertexIndex = charInfo.vertexIndex;

            Process(vertexIndex, i, vertices);
        }
        VisibleTextBox.mesh.SetVertices(vertices);
        VisibleTextBox.UpdateGeometry(VisibleTextBox.mesh, 0);
    }

    public bool Showing()
    {
        return Container.activeSelf;
    }

    public void ExternalForceClose()
    {
        StopAllCoroutines();
        Container.SetActive(false);
        //Time.timeScale = 1f;
    }
    
    public void Display(string content, string displayName, Sprite portrait, Persona p)
    {
        StartCoroutine(OpenContainer(portrait, displayName, p, _Display(content + " ", p)));
        //Time.timeScale = 0f;
    }
    public IEnumerator DisplayCo(string content)
    {
        yield return DisplayCo(content, null, null, defaultPersona);
    }
    public IEnumerator DisplayCo(string content, string displayName, Sprite portrait, Persona p)
    {
        //Time.timeScale = 0f;
        yield return OpenContainer(portrait, displayName, p, _Display(content + " ", p));
    }

    public void Ask(string content, Sprite portrait, string displayName, Persona p, string Selection, string Blank, string Prompt1, System.Action Action1, string Prompt2, System.Action Action2)
    {
        StartCoroutine(OpenContainer(portrait, displayName, p, AskQuestion(content, p, Selection, Blank, Prompt1, Action1, Prompt2, Action2)));
        //Time.timeScale = 0f;
    }

    private IEnumerator OpenContainer(Sprite portrait, string displayName, Persona p, IEnumerator CB)
    {
        if (displayName != null && displayName != "")
        {
            NameTagContainer.gameObject.SetActive(true);
            NameTagText.text = displayName;
            NameTagText.color = p.TextColor;
        } else
        {
            NameTagContainer.gameObject.SetActive(false);
        }

        VisibleTextBox.text = "";
        portraitImage.sprite = (portrait == null ? p.DefaultPortrait : portrait);
        portraitImage.SetNativeSize();
        CurrentTextEffects.Clear();
        Prompt.SetBool("Active", false);
        if (p.ContainerSprite == null)
        {
            backingImage.sprite = DefaultContainerSprite;
            NameTagContainer.sprite = DefaultContainerSprite;
            VisibleTextBox.color = DefaultTextColor;
        }
        else
        {
            backingImage.sprite = p.ContainerSprite;
            NameTagContainer.sprite = p.ContainerSprite;
            VisibleTextBox.color = p.TextColor;
        }
        
        VisibleTextBox.ForceMeshUpdate();
        AnimationPaused = true;
        Container.transform.localScale = new Vector3(1f, 0f, 1f);
        Container.SetActive(true);
        float duration = 0.15f;
        float dt = 0f;
        AnimationPaused = true;
        while (dt < duration)
        {
            yield return null;
            dt += Time.deltaTime;
            Container.transform.localScale = new Vector3(1f, Curve.Evaluate(Mathf.Clamp01(dt / duration)), 1f);
        }
        AnimationPaused = false;
        yield return CB;
    }

    private IEnumerator _Display(string text, Persona p)
    {
        InvisibleTextBox.text = text;
        foreach (var command in Commands)
        {
            // TODO each command should cache their own invisible command string.
            InvisibleTextBox.text = Regex.Replace(InvisibleTextBox.text, $"<{command.CommandString}>", new string('\u200B', command.CommandString.Length));
        }
        InvisibleTextBox.text = InvisibleTextBox.text.StandardizeLineHeight();
        InvisibleTextBox.ForceMeshUpdate();

        if (InvisibleTextBox.isTextOverflowing)
        {
            int i = InvisibleTextBox.firstOverflowCharacterIndex;// - "<line-height=0%>".Length;
            int j = text.Substring(0, i).LastIndexOf(' ');
            yield return DisplayPage(text.Substring(0, j), text.Substring(j+1), p);
        }
        else
        {
            // just assume we have less than one page.
            yield return DisplayPage(text, "", p);
        }
    }

    private IEnumerator DisplayPage(string text, string RemainingText, Persona p)
    {

        yield return ShowPage(text, p);

        Prompt.SetBool("Active", true);
        while (!Press())
            yield return null;
        yield return new WaitForEndOfFrame();
        Prompt.SetBool("Active", false);

        if (RemainingText == "")
        {
            Container.SetActive(false);
            //Time.timeScale = 1f;
        }
        else
        {
            yield return _Display(RemainingText, p);
        }
    }

    private List<TextEffect> CurrentTextEffects = new List<TextEffect>();
    private List<List<TextEffect>> LetterEffects = new List<List<TextEffect>>();
    private bool AnimationPaused;

    

    private IEnumerator ShowPage(string text, Persona p)
    {
        VisibleTextBox.ForceMeshUpdate();

        LetterEffects = new List<List<TextEffect>>(text.Count());
        while (LetterEffects.Count <= text.Count()) LetterEffects.Add(new List<TextEffect>());

        var wfs = new WaitForSeconds(0.1f);
        int lettersSinceWait = 0;

        int currentLetter = 0;
        AudioPoolable previousAudio = null;
        bool skipped = false;

        for (int i = 0; i <= text.Length; i += 1)
        {
            string tagContents = null;
            if (text.Length != i && text[i] == '<')
            {
                i += 1;
                StringBuilder sb = new StringBuilder();
                while (text[i] != '>')
                {
                    sb.Append(text[i]);
                    i += 1;
                }
                tagContents = sb.ToString();
                i += 1;
                currentLetter -= 1;
            }
            // If the string starts with a tag, we revert currentLetter to -1. However, 
            // since it's a tag, it's a non-display letter, so we don't need to apply
            // vertex effects to it.
            
            if (currentLetter >= 0) 
                LetterEffects[currentLetter] = new List<TextEffect>(CurrentTextEffects);
            currentLetter += 1;
            string visiblePortion = text.Substring(0, i);
            foreach (var command in Commands)
            {
                visiblePortion = Regex.Replace(visiblePortion, $"<{command.CommandString}>", "");
            }


            string invisiblePortion = text.Substring(i);
            invisiblePortion = Regex.Replace(invisiblePortion, "<sprite=..>", "");

            string CompositeString = visiblePortion + "<color=#00000000>" + invisiblePortion + "</color>";
            VisibleTextBox.text = CompositeString.StandardizeLineHeight();

            FullMeshUpdateWithEffects();
            lettersSinceWait += 1;


            // TODO this shouldn't be by letter, it should be by time.
            if (lettersSinceWait >= (ShouldBeSpeedup() ? 5 : 2))
            {
                if (text.Length > i && char.IsLetter(text[i]))
                {
                    if (p.SpeechClips.Length > 0)
                    {
                        if (previousAudio != null && previousAudio.gameObject.activeInHierarchy && previousAudio.audioSource.time < 0.05f)
                        {
                            //Debug.Log("SKIPPING FOR NOW.");
                        }
                        else
                        {
                            //previousAudio.ExternalDeactivate();

                            AudioClip blip = p.SpeechClips.Pick();
                            //int tone = (new int[] { 0, 2, 4, 5, 7, 9, 11 }).Pick();
                            int tone = p.Offsets.Pick();
                            tone = 0;
                            previousAudio = this.audioPool.Play(blip, 1f, 1f + ((tone + p.DefaultOffset) / 12f));
                        }
                    }
                }
                if (!skipped)
                {
                    float dt = 0;
                    while (dt < 0.1f)
                    {
                        yield return null;
                        dt += Time.deltaTime;
                        if (Input.GetKey(KeyCode.Escape))
                        {
                            skipped = true;
                            break;
                        }
                    }
                }
                   
                lettersSinceWait = 0;
            }

            if (tagContents != null)
            {
                foreach (var command in Commands)
                {
                    if (command.CommandString == tagContents)
                    {
                        if (!skipped || command.ActiveDuringSkip)
                           command.Callback.Invoke();
                        if (!skipped)
                            yield return new WaitForSeconds(command.Duration);
                    }
                }
                i -= 1;
            }
        }
    }

    private IEnumerator AskQuestion(string text, Persona p, string Selection, string Blank, string Prompt1, System.Action Action1, string Prompt2, System.Action Action2)
    {
        yield return ShowPage(text, p);
        int index = 0;

        System.Action repaint = () =>
        {
            VisibleTextBox.text = text + "\n\n" +
                 (index == 0 ? Selection : Blank) + Prompt1 + "\n" +
                 (index == 1 ? Selection : Blank) + Prompt2;
            VisibleTextBox.text = VisibleTextBox.text.StandardizeLineHeight();
        };



        var eof = new WaitForEndOfFrame();
        repaint();
        while (true)
        {
            var PreviousInputs = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            yield return null;
            if (Press())
                break;

            System.Func<bool> Up = () => (Input.GetAxisRaw("Vertical") > 0.25f && (PreviousInputs.y < 0.25f));
            System.Func<bool> Down = () => (Input.GetAxisRaw("Vertical") < -0.25f && (PreviousInputs.y > -0.25f));

            if (Up() || Down())
            {
                index = (index + 1) % 2;
                repaint();
                yield return null;
            }

            //Debug.Log("Goodbye!.");
        }
        yield return eof;
        Container.SetActive(false);
        //Time.timeScale = 1f;
        if (index == 0)
        {
            Action1.Invoke();
        }
        else
        {
            if (Action2 != null)
                Action2.Invoke();
        }


    }

    private bool ShouldBeSpeedup()
    {
        return Input.GetButton("Submit") || Input.GetMouseButton(0);
    }
    private bool Press()
    {
        return Input.GetButtonDown("Submit") || Input.GetMouseButtonDown(0);
    }
}
