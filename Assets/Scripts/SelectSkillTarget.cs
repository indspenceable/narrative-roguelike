﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

internal class SelectSkillTarget : TBSGameManager.InputHandler
{
    private MapUnit me;
    private ISkillBase skill;
    private List<Vector2Int> possibleTargets;
    private TBSGameManager.InputHandler previous;
    private List<AbstractPoolable> higlights = new List<AbstractPoolable>();

    private MapUnit _hoveredUnitPrivate;
    public MapUnit currentlyHoveredUnit
    {
        get
        {
            return _hoveredUnitPrivate;
        }
        private set
        {
            if (_hoveredUnitPrivate != null) _hoveredUnitPrivate.HealthMeter.Displayed = false;
            _hoveredUnitPrivate = value;
            if (_hoveredUnitPrivate != null) _hoveredUnitPrivate.HealthMeter.Displayed = true;

            if (_hoveredUnitPrivate == null) me.manager.hoverHint.focus = me;
            else me.manager.hoverHint.focus = _hoveredUnitPrivate;
        }
    }

    public SelectSkillTarget(MapUnit me, ISkillBase skill, List<Vector2Int> possibleTargets, TBSGameManager.InputHandler previous)
    {
        this.me = me;
        this.skill = skill;
        this.possibleTargets = possibleTargets;
        this.previous = previous;
    }

    public void Click(int mouseButton)
    {
        if (mouseButton == 1) {
            Back();
            return;
        }
        var mp = me.manager.MousePositionToTileAddress();
        if (possibleTargets.Contains(mp))
        {
            //Do the attack!
            currentlyHoveredUnit = null;
            me.manager.hoverHint.focus = null;
            me.manager.CurrentHandler = new CoroutineExecutorHandler(me.manager, ExecuteSkill(mp));
        }
    }

    private IEnumerator ExecuteSkill(Vector2Int mp)
    {
        yield return skill.Use(me, mp);
        if (skill.Consumable())
        {
            var ts = skill as Talent;
            if (ts != null && me.data.inventory.Contains(ts))
                me.data.inventory.RemoveAt(me.data.inventory.IndexOf(ts));
        }
        
        me.SetCooldown(skill.CooldownIdentifier(), skill.Cooldown());
        me.Spent = true;
        me.manager.InstallDefaultHandler();
    }


    public void Install()
    {
        AudioPool ap = me.manager.audioPool;
        ap.PlaySimple(ap.sfx.StandardBeep);
        foreach (var p in possibleTargets)
        {
            var tile = me.manager.map.TileAt(p);
            if (tile != null)
            {
                higlights.Add(me.manager.TightHighlightsPool.Request(tile.pos.CenterOfTile()));
            }
        }
        me.HealthMeter.Displayed = true;
        me.manager.hoverHint.FocusMaybe(skill as Talent, true);
    }

    public void Uninstall()
    {
        
        higlights.ForEach(h => h.ExternalDeactivate());
        higlights.Clear();
        me.HealthMeter.Displayed = false;
        me.manager.hoverHint.FocusMaybe(null, false);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Back();
        }

        var mp = me.manager.MousePositionToTileAddress();
        var newHoveredUnit = me.manager.directory.UnitAt(mp);
        if (newHoveredUnit != null && newHoveredUnit != me)
        {
            this.currentlyHoveredUnit = newHoveredUnit;
        }
        else
        {
            this.currentlyHoveredUnit = null;
        }
    }

    private void Back()
    {
        me.manager.CurrentHandler = previous;
        me.manager.audioPool.PlaySimple(me.manager.soundConfig.Cancel);
    }
}