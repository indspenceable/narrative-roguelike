﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class PrizePanel:MonoBehaviour
{
    public Transform container;
    public IconButton buttonPrefab;
    private List<IconButton> Instances = new List<IconButton>();
    public TBSGameManager manager;

    public void Setup(List<Talent> prizes)
    {
        while (Instances.Count > 0)
        {
            Destroy(Instances[0].gameObject);
            Instances.RemoveAt(0);
        }
        for(int i =0; i < prizes.Count; i +=1)
        {
            IconButton currentInstance = Instantiate(buttonPrefab, container);
            Talent currentPrize = prizes[i];
            currentInstance.icon.sprite = currentPrize.sprite;
            currentInstance.button.onClick.RemoveAllListeners();
            currentInstance.button.onClick.AddListener(() => { manager.gm.campaign.inventory.Add(currentPrize); manager.AdvanceToNextMode(); });

            currentInstance.tt.TooltipContents = currentPrize.PrizePanelToolTip();
            Instances.Add(currentInstance);
            currentInstance.gameObject.SetActive(true);
        }
        container.gameObject.SetActive(true);
    }
    public void Hide()
    {
        container.gameObject.SetActive(false);
    }
}