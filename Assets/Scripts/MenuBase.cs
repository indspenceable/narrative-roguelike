﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public abstract class MenuBase : MonoBehaviour
{
    public Camera cam;
    public RectTransform container;
    public IconButton prefab;
    protected List<IconButton> Buttons = new List<IconButton>();
    Vector3 cachedLocation;

    public float duration = 10f;
    public AudioPool audioPool;

    [System.Serializable]
    public class MenuOption
    {
        public Sprite sprite;
        public UnityAction onClickAction;
        public bool enabled = true;
        public string tooltip;
        public Sprite backing;
        public Sprite SelectedBacking;
        public string BadgeContent;
        public Action<bool> HoverCallback;

        public MenuOption(Sprite sprite, string tooltip, UnityAction onClickAction, Sprite backing, Sprite SelectedBacking, bool enabled, string BadgeContent, Action<bool> HoverCallback)
        {
            this.sprite = sprite;
            this.tooltip = tooltip;
            this.onClickAction = onClickAction;
            this.backing = backing;
            this.SelectedBacking = SelectedBacking;
            this.enabled = enabled;
            this.BadgeContent = BadgeContent;
            this.HoverCallback = HoverCallback;
        }
    }
    public void Setup(Vector3 location,  params MenuOption[] options)
    {
        cachedLocation = location;
        cachedLocation = new Vector3(Util.SmartFloor(cachedLocation.x, 16), Util.SmartFloor(cachedLocation.y, 16));
        container.position = new Vector3(Util.SmartFloor(cachedLocation.x, 16), Util.SmartFloor(cachedLocation.y, 16));
        ClearAllButtons();
        foreach(var option in options)
        {
            var iconButton = Instantiate(prefab, container);
            var button = iconButton.button;
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(() => InvokeOption(option));
            iconButton.icon.sprite = option.sprite;
            button.interactable = option.enabled;

            button.gameObject.SetActive(true);

            if (option.BadgeContent != null)
            {
                
                iconButton.BadgeBack.sprite = option.backing;
                iconButton.Badge.text = option.BadgeContent;
                iconButton.BadgeBack.gameObject.SetActive(true);
            } else
            {
                iconButton.BadgeBack.gameObject.SetActive(false);
            }
            iconButton.SelectedIndicator.sprite = option.SelectedBacking;
            iconButton.OnHover = option.HoverCallback;
            iconButton.Backing.sprite = option.backing;

            iconButton.tt.TooltipContents = option.tooltip;
            this.Buttons.Add(iconButton);
        }
        //this.OnEscape = OnEscape;
        container.gameObject.SetActive(true);
        StartCoroutine(AnimateMenu());
    }

    private static void InvokeOption(MenuOption option)
    {
        option.onClickAction.Invoke();
    }

    public void Close()
    {
        //Debug.Log("CLOSING");
        container.gameObject.SetActive(false);
    }

    public IEnumerator AnimateMenu()
    {
        float dt = 0f;
        while (dt <= duration)
        {
            yield return null;
            dt += Time.deltaTime;
            for (int i = 0; i < Buttons.Count; i += 1)
            {
                Buttons[i].transform.position = container.position + ButtonPositionOffset(i, Mathf.Clamp01(dt / duration));
            }
        }
    }

    public abstract Vector3 ButtonPositionOffset(int i, float v);

    private void ClearAllButtons()
    {
        foreach(var b in Buttons)
        {
            Destroy(b.gameObject);
        }
        Buttons.Clear();
    }

    private void LateUpdate()
    {
        //container.position = cachedLocation;
    }
}
