﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HasTooltip : MonoBehaviour, IHasTooltip, IPointerEnterHandler, IPointerExitHandler
{
    public string TooltipContents;
    public TooltipManager tooltips;

    public string GetTooltip()
    {
        return TooltipContents;
    }

    [SerializeField]
    bool Hovered = false;
    public void OnPointerEnter(PointerEventData eventData)
    {
        Hovered = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Hovered = false;
    }
    private void Update()
    {
        if (tooltips != null && Hovered)
            tooltips.SetOverrideTarget(this);
    }
}
