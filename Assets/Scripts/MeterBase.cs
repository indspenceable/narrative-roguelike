﻿using System;
using UnityEngine;

public abstract class MeterBase : MonoBehaviour
{
    public bool Engaged;
    public bool Displayed;

    public abstract void SetEngagedAndUpdate();
    public abstract void SetContainerLocalPosition(Vector3 vector3);
}
