﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PartyConvoy
{
    [System.Serializable]
    private class ConvoyItem
    {
        public Talent item;
        public int count;

        public ConvoyItem(Talent item, int count)
        {
            this.item = item;
            this.count = count;
        }
    }
    [SerializeField]
    private List<ConvoyItem> Items;

    public PartyConvoy(List<Talent> inventory)
    {
        this.Items = new List<ConvoyItem>();
        foreach (var i in inventory) Add(i);
    }

    public int Count()
    {
        return Items.Count;
    }

    public Talent Get(int i)
    {
        if (i < Items.Count) { return Items[i].item; }
        return null;
    }
    public int GetCount(int i)
    {
        if (i < Items.Count) { return Items[i].count; }
        return 0;
    }
    public int GetCount(Talent i)
    {
        int index = Items.FindIndex(item => item.item == i);
        if (index < 0) return 0;
        return GetCount(index);
    }
    public Talent Remove(int i)
    {
        if (i < Items.Count)
        {
            var item = Items[i];
            if (item.count == 1)
            {
                Items.Remove(item);
            }else
            {
                item.count -= 1;
            }
            return item.item;
        }
        return null;
    }

    internal void Add(Talent item)
    {
        var match = Items.Find(i => i.item == item);
        if (match != null)
        {
            match.count += 1;
        } else
        {
            Items.Add(new ConvoyItem(item, 1));
        }
    }

    internal void RemoveItem(Talent value)
    {
        Remove(Items.FindIndex(ci => ci.item == value));
    }
}