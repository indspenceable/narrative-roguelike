﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BaseBuilderPropManager : MonoBehaviour
{
    public BaseBuilderPropPool pool;
    public List<BaseBuilderPropPoolable> poolables;


    public MetaProgression meta;
    public void BuildPropPlacement(BaseBuilderPropPlacement PP)
    {
        BuildPropPlacementWithoutSave(PP);
        meta.SaveProp(PP);
    }
    public void BuildPropPlacementWithoutSave(BaseBuilderPropPlacement PP)
    {
        var newInstance = pool.RequestProp(PP, this);
        poolables.Add(newInstance);
    }

    public void DestroyAllExistingProps()
    {
        foreach (var p in poolables)
        {
            p.ExternalDeactivate();
        }
        poolables.Clear();
    }

    private void OnDestroy()
    {
        DestroyAllExistingProps();
    }

    internal bool ClearPropAt(Vector2Int pos, BaseBuilderPropDefinition currentPropToPlace)
    {
        //foreach (var p in PoolablesAtLocation(pos))
        var tempPoolables = PoolablesAtLocation(pos);
        if (!tempPoolables.Any()) return false;
        tempPoolables.Sort((a, b) => b.backingPlacement.propDef.layer.CompareTo(a.backingPlacement.propDef.layer));
        var p = tempPoolables.First();
        if (CheckPropShouldRemoveExistingProp(currentPropToPlace, p))
        {
            var tiles = p.backingPlacement.AllOccupiedTiles();
            meta.RemovePropPlacement(p.backingPlacement);
            p.ExternalDeactivate();
            poolables.Remove(p);

            foreach (var t in tiles)
            {
                foreach (var p2 in PoolablesAtLocation(t))
                {
                    p2.RequiresRefresh = true;
                }
            }
        }
        return true;
    }

    public bool CheckPropShouldRemoveExistingProp(BaseBuilderPropDefinition newProp, BaseBuilderPropPoolable existingProp)
    {
        if (newProp == null) return true;
        // if they're on the same layer, then it needs to clear
        if (existingProp.backingPlacement.propDef.layer == newProp.layer) return true;
        switch (newProp.layer)
        {
            // Not a floor, so can slide under any other layer
            case BaseBuilderPropDefinition.LOCATION_CATEGORY.FLOOR: return false;
            // Tables can be built over floors, but clear other layers that exist.
            case BaseBuilderPropDefinition.LOCATION_CATEGORY.TABLE: return existingProp.backingPlacement.propDef.layer != BaseBuilderPropDefinition.LOCATION_CATEGORY.FLOOR;
            // Ornaments dont clear anything but other ornaments.
            case BaseBuilderPropDefinition.LOCATION_CATEGORY.ORNAMENT: return false;
            // Objects, clear tables and oranments.
            case BaseBuilderPropDefinition.LOCATION_CATEGORY.OBJECT:
                return
existingProp.backingPlacement.propDef.layer == BaseBuilderPropDefinition.LOCATION_CATEGORY.ORNAMENT ||
existingProp.backingPlacement.propDef.layer == BaseBuilderPropDefinition.LOCATION_CATEGORY.TABLE;
        }
        return true;
    }

    public void UnplaceAllProps()
    {
        while (meta.LoadSavedProps().Count > 0)
        {
            ClearPropAt(meta.LoadSavedProps()[0].position, null);
        }
    }

    internal bool PropPlaced(BaseBuilderPropDefinition prop)
    {
        return poolables.Any(poolable => poolable.backingPlacement.propDef == prop);
    }

    public List<BaseBuilderPropPoolable> PoolablesAtLocation(Vector2Int Pos)
    {
        return poolables.Where(poolable => poolable.backingPlacement.IsInPosition(Pos)).ToList();
    }

    public void BuildPreexistingProps()
    {
        foreach (var prop in meta.LoadSavedProps())
        {
            BuildPropPlacementWithoutSave(prop);
        }
    }
}