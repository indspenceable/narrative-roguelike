﻿using UnityEngine;

public class Coordinator : MonoBehaviour
{
    public MetaProgression mp;
    public MusicManager mm;
    public Globals g;
    public Localizer localizer;
    public Material TransitionMaterial;

    private void Start()
    {
        mp.OnGameStart();
        mm.OnGameStart(this);
        localizer.Load();

#if UNITY_EDITOR
        Invoke("FixGlobals", 0.1f);
    }

    void FixGlobals()
    {
        g.Editor_Only_RefreshAll();
#endif
    }
   
}