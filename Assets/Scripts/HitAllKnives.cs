﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Hit All Knives")]
public class HitAllKnives : AttackTalent
{
    public bool ClearKnivesAfterwards = false;
    public Talent KnifeMapEffect;
    public override List<Vector2Int> PossibleTargets(MapUnit Me)
    {
        if (Me.data.KnifeLocations.Distinct().Where(v2i => ValidTargetableTileContents(Me, v2i)).Any() || (Me.data.KnifeLocations.Count > 0 && ClearKnivesAfterwards))
        {
            return Me.CurrentPositions;
        } else
        {
            return new List<Vector2Int>();
        }
    }

    public override TBSGameManager.InputHandler UseSkillHandler(MapUnit me, TBSGameManager.InputHandler previous)
    {
        return new CoroutineExecutorHandler(me.manager, ExecuteSkill(me));
    }

    public virtual IEnumerator ExecuteSkill(MapUnit Me)
    {
        foreach(var kl in Me.data.KnifeLocations.Distinct().Where(v2i => ValidTargetableTileContents(Me, v2i)).ToList())
        {
            yield return AttackTalent.ExecuteAttackAll(onHit, Me, kl, true);
        }
        Me.Spent = true;
        Me.manager.directory.mapUnits.ForEach(u => u.RefreshStatusDecorators());
        Me.manager.InstallDefaultHandler();
        if (ClearKnivesAfterwards)
        {
            foreach(var kl in Me.data.KnifeLocations)
            {
                MapTile mapTile = Me.manager.map.TileAt(kl);
                mapTile.ClearMapEffect(KnifeMapEffect);
            }
            Me.data.KnifeLocations.Clear();
        }
    }
}