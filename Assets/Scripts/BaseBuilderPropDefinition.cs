﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Basebuilder Prop")]
public class BaseBuilderPropDefinition : SerializableScriptableObject, IHasTooltip
{
    public string PropName;
    public string PropDescription;
    [NaughtyAttributes.ShowAssetPreview]
    public Sprite PropSprite;
    public Vector2Int Size = Vector2Int.one;
    public Vector2 CustomSpriteOffsetPosition = Vector2.zero;
    public bool OverrideStandardOffsetPosition = false;

    public bool AlwaysAvailable;
    public string UnlockFlag;
    public int CurrentVarient = -1;
    public List<BaseBuilderPropDefinition> Varients = new List<BaseBuilderPropDefinition>();
    public bool IsVarient;
    public int TableHeight = 0;

    public enum LOCATION_CATEGORY
    {
        OBJECT,
        TABLE,
        FLOOR,
        ORNAMENT,
        WALL
    }
    public LOCATION_CATEGORY layer;

    public Vector2 SpriteOffsetPositionAuto
    {
        get
        {
            var standardOffset = new Vector2(Size.x % 2 == 0 ? 0.5f : 0f, Size.y % 2 == 0 ? 0.5f : 0f);
            if (OverrideStandardOffsetPosition)
            {
                return standardOffset + CustomSpriteOffsetPosition;
            }
            else
            {
                return standardOffset;
            }
        }
    }


    internal bool AvailableToPlace(BaseBuilderPropManager propManager)
    {
        if (AlwaysAvailable) return true;
        if (!propManager.meta.GetBool(UnlockFlag, false)) return false;
        if (propManager.PropPlaced(this)) return false;
        return true;
    }

    public Vector2 StandardizedOffsetPosition
    {
        get
        {
            return (Size - Vector2.one) / 2;
        }
    }

    internal BaseBuilderPropDefinition GetCurrentVarient()
    {
        if (CurrentVarient == -1) return this;
        return Varients[CurrentVarient];
    }

    public void AdjustVarient(int v)
    {
        CurrentVarient += v;
        if (CurrentVarient < -1) CurrentVarient += (Varients.Count + 1);
        if (CurrentVarient >= Varients.Count) CurrentVarient -= (Varients.Count + 1);
    }

    public override void OnValidate()
    {
        base.OnValidate();
#if UNITY_EDITOR

        if (UnlockFlag == "" || UnlockFlag == null)
            UnlockFlag = name;
        foreach (var varient in Varients)
        {
            if (!varient.IsVarient)
            {
                varient.IsVarient = true;
                UnityEditor.EditorUtility.SetDirty(varient);
            }
        }
#endif
    }

    public string GetTooltip()
    {
        return this.PropName;
    }
}