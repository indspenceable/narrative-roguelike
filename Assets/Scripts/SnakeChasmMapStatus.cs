﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Snake/Chasm Map Status")]
public class SnakeChasmMapStatus : BuffDefinition
{
    public List<Vector2Int> InvisibleOffsets;
}