﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TBSGameManager : GameModeInstance
{
    public interface InputHandler
    {
        void Install();
        void Uninstall();
        void Click(int mouseButton);
        void Update();
    }

    internal Vector2Int FindClosestAvailableSpotTo(Vector2Int pos)
    {
        int dist = 0;
        while(true)
        {
            for (int x = pos.x-dist; x <= pos.x + dist; x += 1) {
                for (int y = pos.y - dist; y <= pos.y + dist; y += 1)
                {
                    Vector2Int currentPosition = new Vector2Int(x, y);
                    var mt = map.TileAt(currentPosition);
                    if (mt != null && mt.MovementAllowed(UnitData.MobilityType.LAND) && directory.UnitAt(currentPosition) == null)
                    {
                        return currentPosition;
                    }
                }
            }
            dist += 1;
        }
    }

    internal bool MenuOpen()
    {
        return Settings.activated;
    }

    public Camera cam;
    public CampaignGameMode gm;
    public StatelessGameMode mainMenu;
    public CampaignGameMode PowerupStationMode;
    public CutsceneMode cgm;
    public CutsceneData OnLose;

    public UnitDirectory directory;
    public MapManager map;
    public RadialMenu menu;
    public PathingManager pathing;
    public TBSModeDetailPane detailPane;

    //public AIStrategy AICoordinator;
    public CameraControlsTBS cc;
    public CampaignGameMode msm;
    public CampaignGameMode ManagePartyMode;
    public Globals globals;
    public MusicManager music;
    public BouncyTextPool bouncyTextPool;
    public AudioPool audioPool;
    public SoundConfig soundConfig { get { return music.config; } }
    public LevelData LevelInstance;
    public Localizer localizer;

    public TBSUnitHoverHint hoverHint;
    public AbstractSkillTalent CancelTalent;
    public ToggleableUIElement Settings;

    public Vector2Int MousePositionToTileAddress()
    {
        Vector3 mp = (Vector2)cam.ScreenToWorldPoint(Input.mousePosition);
        return new Vector2Int(Mathf.FloorToInt(mp.x), Mathf.FloorToInt(mp.y));
    }

    public MetaProgression progression;

    public BetweenTurnOverlayManager betweenTurnManager;
    [NaughtyAttributes.ReadOnly]
    [SerializeField]
    private int LevelIndex;

    bool TriggerStartOfMapActivities = true;
    
    private InputHandler _Default;
    public TransitionEffect tEffect;
    public TextboxController textbox;
    public PrizePanel prize;

    public UIMouseTracker MouseInput;

    internal bool MousePassthru()
    {
        return MouseInput.Hovered;
    }

    public AnimatorPool animatorPool;
    public RuntimeAnimatorController DeathRAC;
    public RuntimeAnimatorController PathRAC;
    public RuntimeAnimatorController AlarmRAC;

    private InputHandler _CurrentHandler;
    public Sprite UndoIcon;
    public PersistantObjectPool TightHighlightsPool;
    public Transform TileIndicator;
    public TooltipManager ActionTitleTooltip;
    public CameraShake cameraShake;
    public bool UIDisabled;

    [System.Serializable]
    public struct UIAnchor
    {
        public Transform content;
        public Transform inactivePosition;
        public Transform activePosition;
    }
    [Space]
    public List<UIAnchor> UIElements = new List<UIAnchor>();


    internal bool IsPlayerTurn;
    public InteractableManager interactables;




    public GameObject AdvanceToNextModeButton;
    public Transform NextModeAnchorStart;
    public Transform NextModeAnchorEnd;

    public int TurnNumber;



    private Coroutine UIToggleAnimationCoroutine = null;
    public bool UICurrentlyDisplayed;

    public InputHandler CurrentHandler
    {
        get
        {
            if (_CurrentHandler != null)
                return _CurrentHandler;
            return new NoOp();
        }
        set
        {
            if (_CurrentHandler != null)
            {
                _CurrentHandler.Uninstall();
            }
            _CurrentHandler = value;
            if (_CurrentHandler != null)
            {
                _CurrentHandler.Install();
            }
        }
    }

    internal void SetUIDisplayed(bool v, bool instant = false)
    {
        if (UICurrentlyDisplayed == v) return;
        if (UIToggleAnimationCoroutine != null)
            StopCoroutine(UIToggleAnimationCoroutine);

        var collector = new Util.MultiCoroutineCollector(this);
        foreach (var ui in this.UIElements)
        {
            var sp = v ? ui.inactivePosition.localPosition : ui.activePosition.localPosition;
            var ep = v ? ui.activePosition.localPosition : ui.inactivePosition.localPosition;
            float duration = instant ? 0 : 0.25f;
            collector.Add(Util.MoveTransformToLocalPosition(ui.content, sp, ep, AnimationCurve.EaseInOut(0, 0, 1, 1), duration));
        }
        UIToggleAnimationCoroutine = StartCoroutine(collector.Collect(() => UIToggleAnimationCoroutine = null));
        UICurrentlyDisplayed = v;
    }

    [System.Serializable]
    public class SD
    {
        public CampaignStatus campaign;
        public List<UnitData> OtherUnits;
        public Dictionary<Vector2Int, UnitData> Graveyard;
        public int LevelPrefabIndex;
        public List<MapTile> Tiles;
        public readonly List<IInteractable> interactableList;
        public int TurnNumber;

        public SD(CampaignStatus campaign, List<UnitData> OtherUnits, Dictionary<Vector2Int, UnitData> Graveyard, int LevelPrefabIndex, List<MapTile> Tiles, List<IInteractable> interactableList, int turnNumber)
        {
            this.campaign = campaign;
            this.OtherUnits = OtherUnits;
            this.Graveyard = Graveyard;
            this.LevelPrefabIndex = LevelPrefabIndex;
            this.Tiles = Tiles;
            this.interactableList = interactableList;
            this.TurnNumber = turnNumber;
        }
    }

    public SD SerializeCurrentState()
    {
        return new SD(gm.campaign,
            directory.Query(u => !gm.campaign.team.Contains(u.data)).Select(u => u.data).ToList(),
            directory.Graveyard,
            LevelIndex,
            map.Tiles,
            interactables.interactableList,
            TurnNumber);
    }
    

    public override void PreActivate(object SerializedData)
    {

        if (SerializedData != null)
        {
            // We're loading a saved game.
            var sd = (SD)SerializedData;
            this.LevelIndex = sd.LevelPrefabIndex;
            this.gm.campaign = sd.campaign;
            this.LevelInstance = Instantiate(gm.campaign.currentStage.Maps[LevelIndex]);
            map.Tiles = sd.Tiles;
            map.Tiles.ForEach(t => t.manager = map);
            directory.SpawnInUnitsRaw(gm.campaign.team);
            directory.SpawnInUnitsRaw(sd.OtherUnits);
            directory.Graveyard = sd.Graveyard;
            interactables.interactableList = sd.interactableList;
            interactables.InitializeInteractables();
            this.TurnNumber = sd.TurnNumber;
            interactables.UpdateInteractables();
            RemoveTheDeadInstant();
            foreach (var _u in directory.Query(u => u))
            {
                _u.Spent = _u.data.Spent;
            }
            TriggerStartOfMapActivities = false;

            SetUIDisplayed(true);
            SetUIDisplayed(false);
            //EndTurnButtonContainer.position = EndTurnOnScreenPos.position;
        }
        else
        {
            // We're not loading a saved game, so we're actually starting the map in this branch!
            StageData StageToSpawnEnemiesFrom = gm.campaign.currentStage;

            TriggerStartOfMapActivities = true;
            this.LevelIndex = SelectMap(gm.campaign.CurrentStageProgress);
            this.LevelInstance = Instantiate(gm.campaign.currentStage.Maps[LevelIndex]);

            map.BuildTiles();
            map.PlaceTraps((gm.campaign.CurrentStageProgress / 4) + 5);
            interactables.BuildInteractables(StageToSpawnEnemiesFrom);
            interactables.InitializeInteractables();
            interactables.UpdateInteractables();
            directory.SpawnInPlayerTeamUnitsInInitialSpawnLocations(gm.campaign.team);
            directory.SpawnInInitialEnemyUnits(StageToSpawnEnemiesFrom, (gm.campaign.CurrentStageProgress/2)+(gm.campaign.CompletedStages.Count) + 3);
            this.TurnNumber = 0;
            foreach (var u in directory.Query(u => true))
            {
                u.Spent = false;
                u.data.KnifeLocations.Clear();
                u.data.CurrentBuffs.Clear();
            }

            SetUIDisplayed(true);
            SetUIDisplayed(false);
        }
        detailPane.Hide();
        AdvanceToNextModeButton.SetActive(false);
        gameEnded = false;

        CurrentHandler = new NoOp();
        this.cc.transform.position = directory.Query(u => u.data.Friendly).First().transform.position;
        betweenTurnManager.HideEverything();
        foreach(var tile in map.Tiles)
        {
            tile.UpdateDecorator();
        }
        this.music.FadeTo(LevelInstance.Music);
        directory.mapUnits.ForEach(u => u.RefreshStatusDecorators());

        var boss = directory.mapUnits.Find(mu => mu.data.Strategy != null && mu.data.Strategy.BossRoar != null);
        if (boss != null) audioPool.PlaySimple(boss.data.Strategy.BossRoar);
    }


    

    private int SelectMap(int mapsCompleted)
    {
        return mapsCompleted;
        //return UnityEngine.Random.Range(0, levelPrefabs.Count() - 1);
    }

    private void SetDefaultHandler(InputHandler handle)
    {
        _Default = handle;
        InstallDefaultHandler();
    }
    public void InstallDefaultHandler()
    {
        if (directory.Query(u => u.Dead()).Any())
            CurrentHandler = new CoroutineExecutorHandler(this, RemoveTheDeadLoud());
        else
            CurrentHandler = _Default;
    }

    private void RemoveTheDeadInstant()
    {
        foreach (var u in directory.Query(u => u.Dead()).ToList())
        {
            u.OnUnitDeath();
            directory.RemoveUnitWithoutAnimation(u);
        }

        directory.mapUnits.ForEach(u => u.RefreshStatusDecorators());
    }

    public IEnumerator RemoveTheDeadLoud()
    {
        foreach (var u in directory.Query(u => u.Dead()).ToList())
        {

            if (u.data.HasDeathQuote)
            {
                yield return u.manager.textbox.DisplayCo(localizer._($"deathquotes.{u.data.CharacterName}.{UnityEngine.Random.Range(1,2)}"), u.data.CharacterName, u.data.presentation.persona.GetPortrait("frown"), u.data.presentation.persona);
                yield return new WaitForSeconds(0.25f);
            }
            audioPool.Play(soundConfig.OnDeathSFX, 1f, 1f);
            animatorPool.RequestWithAnimator(u.CenterOfSprite(), DeathRAC);
            u.OnUnitDeath();
            directory.RemoveUnitWithoutAnimation(u);
        }
        directory.mapUnits.ForEach(u => u.RefreshStatusDecorators());

        InstallDefaultHandler();
    }

    public override void PostActivate()
    {
        // Ok this is where the real stuff happens.
        if (TriggerStartOfMapActivities)
        {

            IsPlayerTurn = true;
            CurrentHandler = new CoroutineExecutorHandler(this, TriggerStartOfMapCallbacksThenStartMap());
        }
        else
        {

            IsPlayerTurn = true;
            SetDefaultHandler(new CallbackExecutorHandler(CompletePlayerAction));
        }
    }

    IEnumerator TriggerStartOfMapCallbacksThenStartMap()
    {
        foreach(var u in directory.mapUnits)
        {
            yield return u.StartOfMapCo();
        }
        
        yield return TakePlayerTurn();
    }

    public override void PostDeactivate()
    {
        directory.Deactivate();
        map.Deactivate();
        interactables.Deactivate();
        Destroy(LevelInstance.gameObject);
    }

    IEnumerator TakePlayerTurn()
    {
        cc.ControlsDisabled = true;
        var plTeam = directory.Query(u => u.data.Friendly).Where(u => !u.Dead());
        if (!plTeam.Any())
        {
            //this.globals.ClearQuicksave();
            yield return this.textbox.DisplayCo("You lose. Better luck next time!");
            globals.WipeQuicksaveData();
            mainMenu.ActivateWithoutData(tEffect);
            yield break;
        }

        var leader = plTeam.First();
        yield return cc.FocusOn(leader);
        yield return new WaitForSeconds(0.5f);
        audioPool.PlaySimple(audioPool.sfx.PlayerTurnStart);
        yield return betweenTurnManager.Display("Oneironauts, go!");
        TurnNumber += 1;
        cc.ControlsDisabled = false;

        var SOT = Time.deltaTime;

        interactables.UpdateInteractables();

        foreach (var u in directory.Query(u => u.data.Friendly).ToList())
        {

            yield return u.StartOfTurnCo();
        }
        
        List<IEnumerator> coroutinesToDo = new List<IEnumerator>();
        for (int x = map.LowerBoundX(); x < map.UpperBoundX(); x += 1)
        {
            for (int y = map.LowerBoundY(); y < map.UpperBoundY(); y += 1)
            {
                var pos = new Vector2Int(x, y);
                var u = directory.UnitAt(pos);
                map.StartOfTurnVoid(pos, coroutinesToDo);
            }
        }
        foreach (var attp in coroutinesToDo)
        {
            if (attp == null) continue;
            for (int i = 0; i < 3; i += 1)
            {
                attp.MoveNext();
            }
            yield return attp;
        }

        //StartCoroutine(Util.MoveTransformToPosition(EndTurnButtonContainer, EndTurnOffScreenPos.position, EndTurnOnScreenPos.position, AnimationCurve.EaseInOut(0, 0, 1, 1), 0.25f));
        //Debug.Log($"All Start of Turn things. {Time.unscaledTime - SOT}");

        if (!progression.GetBool("hints.tbs"))
        {
            yield return textbox.DisplayCo(localizer._("hints.tbs"));
            progression.SetBool("hints.tbs", true);
        }

        SetDefaultHandler(new CallbackExecutorHandler(CompletePlayerAction));
        IsPlayerTurn = true;
    }

    IEnumerator TakeEnemyTurn()
    {
        IsPlayerTurn = false;
        //StartCoroutine(Util.MoveTransformToPosition(EndTurnButtonContainer, EndTurnOnScreenPos.position, EndTurnOffScreenPos.position, AnimationCurve.EaseInOut(0, 0, 1, 1), 0.25f));

        cc.ControlsDisabled = true;
        hoverHint.focus = null;
        hoverHint.focusTile = null;
        yield return new WaitForSeconds(0.5f);
        audioPool.PlaySimple(audioPool.sfx.EnemyTurnStart);
        yield return betweenTurnManager.Display("Dream Denizens");

        List<MapUnit> AllEnemies = directory.Query(u => !u.data.Friendly).ToList();
        foreach (var e in AllEnemies)
        {
            //yield return cc.FocusOn(e);
            yield return e.StartOfTurnCo();
        }
        SetDefaultHandler(new CallbackExecutorHandler(CompleteEnemyAction));
    }

    void CompletePlayerAction()
    {
        CurrentHandler = new CoroutineExecutorHandler(this, CompletePlayerActionCo());
    }
    IEnumerator CompletePlayerActionCo()
    {
        var friendly = directory.Query(u => u.data.Friendly);
        var unfriendly = directory.Query(u => !u.data.Friendly && !u.data.DoesNotAct);
        var unspent = friendly.Where(u => u.ShouldAct());

        foreach (var e in friendly)
        {
            //Debug.Log(e.gameObject);
            yield return e.RemoveExpiredAndInactiveBuffs();
        }

        if (!unfriendly.Any())
        {
            friendly.ToList().ForEach(u => u.ClearCooldowns());
            CurrentHandler = new CoroutineExecutorHandler(this, ShowYouWinScreen());
        } else if (!unspent.Any())
        {
            foreach (var u in directory.Query(u => u.data.Friendly))
            {
                u.Spent = false;
            }

            CurrentHandler = new CoroutineExecutorHandler(this, TakeEnemyTurn());
        }
        else
        {
            //if (true)
            //{
            //    //Debug.Break();
            //    var formatter = this.globals.BuildFormatter();
            //    System.IO.FileStream file = System.IO.File.Create(Application.persistentDataPath + "/Testing");
            //    formatter.Serialize(file, this.SerializeCurrentState().interactableList[0]);
            //}


            this.globals.Quicksave(gm, this.SerializeCurrentState());
            CurrentHandler = new SelectUnitHandler(this);
        }
    }


    private IEnumerator ShowYouWinScreen()
    {
        cc.ControlsDisabled = true;
        this.UIDisabled = true;
        music.FadeToNone();
        yield return new WaitForSeconds(1f);
        music.FadeTo(soundConfig.WinMusic);
        yield return betweenTurnManager.BeginShowing("Victory!");
        yield return new WaitForSeconds(1f);
        AdvanceToNextModeButton.SetActive(true);
        yield return Util.MoveTransformToPosition(AdvanceToNextModeButton.transform, NextModeAnchorStart.position, NextModeAnchorEnd.position, AnimationCurve.EaseInOut(0, 0, 1, 1), 2f);
    }
    //private IEnumerator SetupPrizeWindowAfterWin()
    //{
    //    StartCoroutine(Util.MoveTransformToPosition(EndTurnButton, EndTurnOnScreenPos.position, EndTurnOffScreenPos.position, AnimationCurve.EaseInOut(0, 0, 1, 1), 0.25f));

    //    music.FadeToInstant(null);
    //    music.FadeTo(soundConfig.WinMusic);
    //    yield return this.textbox.DisplayCo($"You win! Select your prize.");
    //    prize.Setup(map.globals.All<Talent>().Where(t => t.CanBeAwardedAsPrize).Shuffled().Take(3).ToList());
    //}

    private void LoseLevel()
    {
        globals.WipeQuicksaveData();
        cgm.ActivateWithData(tEffect, msm.campaign, OnLose, () =>
        {
            mainMenu.ActivateWithoutData(tEffect);
        });
    }

    bool gameEnded = false;
    public Sprite TalkSprite;
    public AbstractCutsceneData creditsCutscene;

    public void AdvanceToNextMode()
    {
        AdvanceToNextMode(false);
    }
    public void AdvanceToNextMode(bool SkipPreparations)
    {
        if (!gameEnded)
        {
            gameEnded = true;
            StartCoroutine(AdvancetoNextModeCO(SkipPreparations));
        }

    }
    private IEnumerator AdvancetoNextModeCO(bool SkipPreparations) {
        yield return Util.MoveTransformToPosition(AdvanceToNextModeButton.transform, NextModeAnchorEnd.position, NextModeAnchorStart.position, AnimationCurve.EaseInOut(0, 0, 1, 1), 0.75f);
        yield return betweenTurnManager.FinishShowing();

        gm.campaign.CompleteMap();

        prize.Hide();

        

        if (gm.campaign.CurrentStageProgress == gm.campaign.currentStage.LevelsCount)
        {
            // Just cleared the boss. Go back to Mission Select.
            // but first, remember that we've beat this stage.
            gm.campaign.CompleteCurrentStage();


            // When you beat a boss, bump up everyone's supports!
            IncreaseAllSupports();

            if (gm.campaign.currentStage.IsNytmurCastle)
            {
                // You just won!
                OnRunVictory();
                globals.WipeQuicksaveData();

                cgm.ActivateWithData(tEffect, gm.campaign, gm.campaign.currentStage.StageCompleteCutscene, () =>
                {
                    cgm.ActivateWithData(tEffect, gm.campaign, this.creditsCutscene, () =>
                    {
                        mainMenu.ActivateWithoutData(tEffect);
                    });
                });
            }
            else
            {
                cgm.ActivateWithData(tEffect, gm.campaign, gm.campaign.currentStage.StageCompleteCutscene, () => msm.ActivateWithData(tEffect, gm.campaign));
            }
        }
        else
        {
           
            if (gm.campaign.CurrentStageProgress == gm.campaign.PowerupStationLocation)
            {
                PowerupStationMode.ActivateWithData(tEffect, gm.campaign);
            } else {
                // else, manage party, then go to the next room.
                if (SkipPreparations)
                {
                    gm.ActivateWithData(tEffect, gm.campaign);
                } else
                {
                    ManagePartyMode.ActivateWithData(tEffect, gm.campaign);
                }
            }
        }
    }

    private void IncreaseAllSupports()
    {
        for (int i = 0; i < 4; i += 1)
        {
            for (int j = i + 1; j < 4; j += 1)
            {
                string key = progression.CharacterGroupKey(gm.campaign.team[i].CharacterName, gm.campaign.team[j].CharacterName);
                progression.SetIntByDelta(key, 1);
            }
        }
    }

    private void OnRunVictory()
    {
        IncreaseAllSupports();
        // TODO other stuff on run win?
    }

    void CompleteEnemyAction()
    {
        CurrentHandler = new CoroutineExecutorHandler(this, CompleteEnemyActionCo());
    }
    IEnumerator CompleteEnemyActionCo()
    {
        if (!directory.Query(u=> u.data.Friendly && !u.Dead()).Any())
        {
            LoseLevel();
            yield break;
        }

        List<MapUnit> Unfriendly = directory.Query(u => !u.data.Friendly).ToList();
        foreach (var e in Unfriendly)
        {
            yield return e.RemoveExpiredAndInactiveBuffs();
        }

        while (directory.Query(u => !u.data.Friendly && u.data.Unaware && u.ShouldWake()).Any())
        {
            var e = directory.Query(u => !u.data.Friendly && u.data.Unaware && u.ShouldWake()).First();
            yield return this.cc.FocusOn(e);
            e.Wake();
            yield return new WaitForSeconds(1f);
        }

        if (!directory.Query(u => !u.data.Friendly && u.ShouldAct()).Any())
        {
            foreach (var eu in directory.Query(u => !u.data.Friendly))
            {
                eu.Spent = false;
                yield return eu.EndOfTurnCo();
            }
            yield return TakePlayerTurn();
        }
        else
        {
            var currentUnit = directory.Query(u => !u.data.Friendly && u.ShouldAct()).First();

            // TAKE ACTION HERE.
            if (currentUnit.data.Strategy != null)
                yield return currentUnit.data.Strategy.SelectAndEnactPlan(currentUnit);
            else
            {
                Debug.LogError($"{currentUnit.data.CharacterName} has no strategy!");
                yield return AIStrategyBase.TakeUnitAction(currentUnit, new AIActionPlan(new List<Vector2Int>() { currentUnit.data.pos }, null, Vector2Int.zero), null);
            }
        }
    }

    private void Update()
    { 
        // Pop open the 
        CurrentHandler.Update();
    }

    // DEBUG HELPERS v

    [NaughtyAttributes.Button]
    void CheckAllDescriptions()
    {
        var list = new List<Talent>();
        list.AddRange(globals.All<Talent>().Where(t => t.CanBeAwardedAsPrize));
        list.AddRange(globals.All<UnitDefinition>().Where(ud => ud.SelectableCharacter).SelectMany(ud => ud.Data.IntrinsicTalents));

        foreach (var i in list)
        {
            if (i.FlavorText == null || i.FlavorText == "" || i.FlavorText == " ")
            {
                Debug.Log(i, i);
            }
        }
    }

    [NaughtyAttributes.Button]
    void IterateAllItems()
    {
        StartCoroutine(_IterateAllItems());
    }
    IEnumerator _IterateAllItems() {
        foreach (var i in globals.All<Talent>().Where(t => t.CanBeAwardedAsPrize))
        {
            hoverHint.focusTalent = i;
            yield return new WaitForSeconds(1);
        }
    }

    [NaughtyAttributes.Button]
    void IterateAllBuffs()
    {
        StartCoroutine(_IterateAllBuffs());
    }
    IEnumerator _IterateAllBuffs()
    {
        WaitForEndOfFrame eof = new WaitForEndOfFrame();
        WaitUntil keydown = new WaitUntil(() => Input.GetKeyDown(KeyCode.P));
        foreach (var i in globals.All<Talent>().Where(t => t.ShownAsBuff))
        {
            Debug.Log(i, i);
            hoverHint.focusTalent = i;
            yield return keydown;
            yield return eof;
        }
        hoverHint.focusTalent = null;
    }

    [NaughtyAttributes.Button]
    void IterateAllInnates()
    {
        StartCoroutine(_IterateAllInnates());
    }
    IEnumerator _IterateAllInnates()
    {
        WaitForEndOfFrame eof = new WaitForEndOfFrame();
        WaitUntil keydown = new WaitUntil(() => Input.GetKeyDown(KeyCode.P));
        IEnumerable<UnitDefinition> a = globals.All<UnitDefinition>().Where(u => u.SelectableCharacter);
        IEnumerable<Talent> b = a.SelectMany(u => u.Data.IntrinsicTalents);
        //IEnumerable<Talent> c = b.Where(t => t.ShownAsBuff);
        foreach (var i in b)
        {
            Debug.Log(i, i);
            //map.TileAt(new Vector2Int(3, 3)).AddMapEffect(new BuffInstance(i, 3));
            hoverHint.focusTalent = i;
            yield return keydown;
            yield return eof;
            //map.TileAt(new Vector2Int(3, 3)).ClearMapEffect(i);
        }
        hoverHint.focusTalent = null;
    }

    [NaughtyAttributes.Button]
    void IterateAllTiles()
    {
        StartCoroutine(_IterateAllTiles());
    }
    IEnumerator _IterateAllTiles()
    {
        WaitForEndOfFrame eof = new WaitForEndOfFrame();
        WaitUntil keydown = new WaitUntil(() => Input.GetKeyDown(KeyCode.P));
        List<Talent> a = new List<Talent>();

        a.AddRange(globals.All<CreateMapEffectTalent>().Select(cme => cme.Status.buff));
        a.AddRange(globals.All<CreateMapEffectTalentOnTileExit>().Select(cme => cme.MapStatus.buff));
        // TODO - add all the traps for all the stages.
        foreach(var stage in globals.All<StageData>())
        {
            foreach(var map in stage.Maps)
            {
                a.AddRange(map.TrapTiles);
            }
        }
        a.AddRange(globals.All<KnifeThrow>().Select(kt => kt.KnifeMapEffect.buff));
        //a.AddRange(globals.All<SnakeChasmAttack>());
        foreach (var i in a.Where(z => z != null).Distinct())
        {
            Debug.Log(i, i);
            map.TileAt(new Vector2Int(0, -3)).AddMapEffect(new BuffInstance(i, 3));
            hoverHint.focusTalent = i;
            yield return keydown;
            yield return eof;
            map.TileAt(new Vector2Int(0, -3)).ClearMapEffect(i);
        }
        hoverHint.focusTalent = null;
    }

    [NaughtyAttributes.Button]
    public void SpendAll()
    {
        foreach (var u_ in directory.Query(u => u.data.Friendly && u.ShouldAct()))
        {
            u_.Spent = true;
        }
        InstallDefaultHandler();
    }
    [NaughtyAttributes.Button]
    public void KillBaddies()
    {
        foreach (var u_ in directory.Query(u => !u.data.Friendly))
        {
            u_.TakeDamage(9999999, false);
        }
        InstallDefaultHandler();
    }
    [NaughtyAttributes.Button]
    public void SkipBackToMapSelection()
    {
        var prizes = globals.All<Talent>().Where(t => t.CanBeAwardedAsPrize).Shuffled().ToList();
        var chars = directory.Query(u => u.data.Friendly).ToList();
        for (int i = 0; i < chars.Count; i += 1)
        {
            chars[i].data.inventory.Add(prizes[i]);
        }
        gm.campaign.CompleteCurrentStage();
        msm.ActivateWithData(tEffect, gm.campaign);
    }
}


