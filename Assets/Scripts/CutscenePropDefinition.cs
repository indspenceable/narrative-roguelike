﻿using UnityEngine;

[CreateAssetMenu]
public class CutscenePropDefinition : SerializableScriptableObject
{
    public GameObject prefab;
    public string Message;
}
