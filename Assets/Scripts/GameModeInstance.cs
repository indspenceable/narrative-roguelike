﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameModeInstance : MonoBehaviour
{
    public SimpleBlit cameraBlit;
    public virtual IEnumerator Activate(TransitionEffect tEffect, object data)
    {
        PreActivate(data);
        this.gameObject.SetActive(true);
        cameraBlit.TransitionMaterial.SetTexture("_TransitionTex", tEffect.In);
        cameraBlit.TransitionMaterial.SetFloat("_Sepia", 0);
        float dt = 0f;
        float duration = tEffect.InDuration;
        cameraBlit.TransitionMaterial.SetFloat("_Cutoff", 1f);
        while (dt < duration)
        {
            yield return null;
            dt += Time.deltaTime;
            cameraBlit.TransitionMaterial.SetFloat("_Cutoff", 1f - (dt / duration));
        }
        PostActivate();
    }
    public virtual IEnumerator Deactivate(TransitionEffect tEffect)
    {
        tEffect.PreDeactivate();
        PreDeactivate();
        cameraBlit.TransitionMaterial.SetTexture("_TransitionTex", tEffect.Out);
        float dt = 0f;
        float duration = tEffect.OutDuration;
        cameraBlit.TransitionMaterial.SetFloat("_Cutoff", 0f);
        while (dt < duration)
        {
            yield return null;
            dt += Time.deltaTime;
            cameraBlit.TransitionMaterial.SetFloat("_Cutoff", (dt / duration));
        }
        this.gameObject.SetActive(false);
        PostDeactivate();
    }

    public virtual void PreActivate(object DeserializedData) { }
    public virtual void PostActivate() { }
    public virtual void PreDeactivate() { }
    public virtual void PostDeactivate() { }
}