﻿using UnityEngine;

[CreateAssetMenu]
public class MapUnitPrefabDefinition : SerializableScriptableObject
{
    public MapUnit prefab;
}