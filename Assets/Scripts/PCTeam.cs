﻿using System;
using System.Collections.Generic;

[System.Serializable]
public class CampaignStatus
{
    public StageData currentStage;
    public int CurrentStageProgress;
    public int PowerupStationLocation;
    public List<StageData> CompletedStages;
    public List<UnitData> team;
    public PartyConvoy inventory;
    public Dictionary<string, bool> flags = new Dictionary<string, bool>();
    public List<BaseBuilderPropDefinition> currentRewards;

    public CampaignStatus(List<UnitData> team, List<Talent> Inventory)
    {
        this.team = team;
        inventory = new PartyConvoy(Inventory);
        currentStage = null;
        CurrentStageProgress = 0;
        CompletedStages = new List<StageData>();
        this.currentRewards = new List<BaseBuilderPropDefinition>();
        this.inventory = new PartyConvoy(Inventory);
    }


    public void CompleteMap()
    {
        CurrentStageProgress += 1;
    }
    public void CompleteCurrentStage() {
        CurrentStageProgress = 0;
        CompletedStages.Add(currentStage);
    }
    
    public bool GetFlag(string s, bool defaultValue = false)
    {
        if (flags == null) flags = new Dictionary<string, bool>();

        if (flags.ContainsKey(s))
        {
            return flags[s];
        }
        return defaultValue;
    }
    public void SetFlag(string s, bool value)
    {
        if (flags.ContainsKey(s) && flags[s] == value) return;
        flags[s] = value;
    }
}
