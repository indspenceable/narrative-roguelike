﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(menuName = "GameModes/Base")]
public abstract class GameModeBase : SerializableScriptableObject
{
    public bool ReInstantiate = false;
    public GameModeInstance InstancePrefab;
    private GameModeInstance _InstantiatedPrefab;
    private static GameModeBase CurrentGameMode;

    private static Coordinator coordinator;
    public static bool TransitionInProgress;
    public Coordinator GetCoordinator()
    {
        if (coordinator == null) coordinator = FindObjectOfType<Coordinator>();
        return coordinator;
    }

    protected void Activate(TransitionEffect tEffect)
    {
        if (!TransitionInProgress)
            GetCoordinator().StartCoroutine(ActivateCO(tEffect));
    }

    public void ActivateFromQuicksave(TransitionEffect tEffect, object o)
    {
        if (!TransitionInProgress && o != null)
            GetCoordinator().StartCoroutine(LoadCO(tEffect, o));
    }

    protected IEnumerator ActivateCO(TransitionEffect tEffect)
    {
        TransitionInProgress = true;
        if (CurrentGameMode != null)
        {
            yield return CurrentGameMode.Deactivate(tEffect);
        }
        CurrentGameMode = this;
        if (_InstantiatedPrefab == null)
        {
            _InstantiatedPrefab = Instantiate(InstancePrefab);
        }
        yield return _InstantiatedPrefab.Activate(tEffect, null);
        TransitionInProgress = false;
    }
    protected IEnumerator LoadCO(TransitionEffect tEffect, object o)
    {
        TransitionInProgress = true;
        if (CurrentGameMode != null)
        {
            yield return CurrentGameMode.Deactivate(tEffect);
        }
        CurrentGameMode = this;
        if (_InstantiatedPrefab == null)
        {
            _InstantiatedPrefab = Instantiate(InstancePrefab);
        }
        yield return _InstantiatedPrefab.Activate(tEffect, o);
        TransitionInProgress = false;
    }

    protected IEnumerator Deactivate(TransitionEffect tEffect)
    {
        yield return _InstantiatedPrefab.Deactivate(tEffect);
        if (ReInstantiate || true) // Hack - just always reinstantiate stuff, cause menus are hard.
        {
            Destroy(_InstantiatedPrefab.gameObject);
            _InstantiatedPrefab = null;
        }
    }
}
