﻿using UnityEngine;

[CreateAssetMenu(menuName = "TransitionEffect/Audio")]
public class TransitionEffectWithSFX : TransitionEffect
{
    public AudioPool pool;
    public AudioClip sfx;
    public override void PreDeactivate()
    {
        pool.PlaySimple(sfx);
    }
}