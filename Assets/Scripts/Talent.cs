﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static AttackTalent;

public abstract class Talent : SerializableScriptableObject, IHasTooltip
{
    public enum BuffStackingMethod
    {
        NO_STACKING,
        EXTEND_DURATION,
        GREATER_DURATION,
        IGNORE_NEW
    }
    public BuffStackingMethod StackingMethod;

    [NaughtyAttributes.ShowAssetPreview]
    public Sprite sprite;
    public string TalentName;
    public bool ShownAsBuff = false;
    public bool CanBeAwardedAsPrize;
    [NaughtyAttributes.ResizableTextArea]
    public string Description;
    [NaughtyAttributes.ResizableTextArea]
    public string FlavorText;
    public PersistantObjectPool MapDecorator;
    //TODO get rid of this
    public Talent Upgrade;

    [UnityEngine.Serialization.FormerlySerializedAs("TicksDown")]
    public bool TicksDownOccupied = true;
    public bool TicksDownUnoccupied = false;
    public bool BlocksMovement;

    public virtual int Modifier(string name, MapUnit u)
    {
        return 0;
    }

    public virtual IEnumerator StartOfTurn(IAttacker mapUnit, IAttacker Stage, Vector2Int pos, int stacks)
    {
        return null;
    }

    public virtual IEnumerator EndOfTurn(IAttacker mapUnit, Vector2Int pos, int stacks)
    {
        return null;
    }

    public virtual IEnumerator OnBuffExpired(MapUnit mapUnit, IAttacker Stage)
    {
        return null;
    }

    public virtual IEnumerator OnGetHit(MapUnit Me)
    {
        return null;
    }

    public virtual IEnumerator BeforeAttack(MapUnit mapUnit)
    {
        return null;
    }

    public virtual IEnumerator AfterUseAttack(MapUnit mapUnit)
    {
        return null;
    }
    public virtual void OnUnitDied(MapUnit ME)
    {

    }


    public string PrizePanelToolTip()
    {
        return $"{TalentName}\n\n{Description}\n\n<i>{FlavorText}</i>";
    }

    public virtual IEnumerator StartOfMap(IAttacker mapUnit)
    {
        return null;
    }

    public virtual void OnExitTile(MapUnit mapUnit, Vector2Int v2i) { }

    public string GetTooltip()
    {
        return TalentName;
    }
}
