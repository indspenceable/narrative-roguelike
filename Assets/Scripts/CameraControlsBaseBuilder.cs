﻿using System;
using UnityEngine;

public class CameraControlsBaseBuilder : CameraControlsBase, IHasBounds
{
    public Transform currentBL;
    public Transform currentTR;
    public void SetCurrentBounds(Transform newbl, Transform newtr)
    {
        currentBL = newbl;
        currentTR = newtr;

    }

    public void AutoFocus()
    {
        CurrentPosition = Vector3.Lerp(currentTR.position, currentBL.position, 0.5f);
    }

    public Transform GetBL()
    {
        return BL;
    }

    public Transform GetTR()
    {
        return TR;
    }

    public override IHasBounds GetBounds => this;
    public override Transform BL => currentBL;
    public override Transform TR => currentTR;
}
