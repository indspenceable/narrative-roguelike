﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class DebugAnimatorBuilderHelper : MonoBehaviour
{

    //[NaughtyAttributes.ShowAssetPreview]
    [NaughtyAttributes.ReadOnly]
    public List<Sprite> WALKER_SPRITES = new List<Sprite>();
    public List<Sprite> spritesForAnimations  = new List<Sprite>();
    public RuntimeAnimatorController WalkerBase;
    public RuntimeAnimatorController WarperBase;
    [TextArea]
    public string ActorName;
    private string WALKER_NAME;
    public Globals g;

#if UNITY_EDITOR
    public void BuildStandardMovementUnit()
    {
        var animClip = new AnimationClip();

        if (AssetDatabase.IsValidFolder($"Assets/Generated/Animations/{WALKER_NAME}"))
        {
            //FileUtil.DeleteFileOrDirectory("Assets/Generated/Animations");
        }
        else
        {
            AssetDatabase.CreateFolder("Assets/Generated/Animations", WALKER_NAME);
        }

        var idle_d = CreateAnimationClip(0,0,0,0,0,0,11,13,11);
        var idle_u = CreateAnimationClip(1,1,1,1,1,1,12,14,12);
        var idle_l = CreateAnimationClip(15,15,15,15,15,15,26,28,26);
        var idle_r = CreateAnimationClip(16,16,16,16,16,16,27,29,27);

        var walk_d = CreateAnimationClip(2, 3);
        var walk_u = CreateAnimationClip(17, 18);
        var walk_l = CreateAnimationClip(15, 19);
        var walk_r = CreateAnimationClip(16, 4);

        var attack_d = CreateAnimationClip(5, 6);
        var attack_u = CreateAnimationClip(20, 21);
        var attack_l = CreateAnimationClip(22, 23);
        var attack_r = CreateAnimationClip(7, 8);

        var get_hit_d = CreateAnimationClip(9);
        var get_hit_u = CreateAnimationClip(10);
        var get_hit_l = CreateAnimationClip(24);
        var get_hit_r = CreateAnimationClip(25);

        AssetDatabase.CreateAsset(idle_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_D_Generated.anim");
        AssetDatabase.CreateAsset(idle_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_U_Generated.anim");
        AssetDatabase.CreateAsset(idle_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_L_Generated.anim");
        AssetDatabase.CreateAsset(idle_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_R_Generated.anim");
        AssetDatabase.CreateAsset(walk_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Walk_D_Generated.anim");
        AssetDatabase.CreateAsset(walk_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Walk_U_Generated.anim");
        AssetDatabase.CreateAsset(walk_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Walk_L_Generated.anim");
        AssetDatabase.CreateAsset(walk_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Walk_R_Generated.anim");
        AssetDatabase.CreateAsset(attack_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_D_Generated.anim");
        AssetDatabase.CreateAsset(attack_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_U_Generated.anim");
        AssetDatabase.CreateAsset(attack_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_L_Generated.anim");
        AssetDatabase.CreateAsset(attack_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_R_Generated.anim");
        AssetDatabase.CreateAsset(get_hit_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_D_Generated.anim");
        AssetDatabase.CreateAsset(get_hit_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_U_Generated.anim");
        AssetDatabase.CreateAsset(get_hit_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_L_Generated.anim");
        AssetDatabase.CreateAsset(get_hit_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_R_Generated.anim");

        AnimatorOverrideController overrideController = new AnimatorOverrideController(WalkerBase);
        var ExistingOverrides = new List<KeyValuePair<AnimationClip, AnimationClip>>();
        overrideController.GetOverrides(ExistingOverrides);
        Func<string, AnimationClip> F = (s) => {
            return WalkerBase.animationClips.First(c =>
            {
                //Debug.Log("Animation clip is named: " + c.name);
                return c.name == s;
            });
        };
        var overrides = new List<KeyValuePair<AnimationClip, AnimationClip>>
        {
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_D"), walk_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_U"), walk_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_L"), walk_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_R"), walk_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_D"), idle_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_U"), idle_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_L"), idle_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_R"), idle_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_D"), attack_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_U"), attack_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_L"), attack_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_R"), attack_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_D"), get_hit_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_U"), get_hit_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_L"), get_hit_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_R"), get_hit_r),
        };
        overrideController.ApplyOverrides(overrides);

        AssetDatabase.CreateAsset(overrideController, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Overrides.overrideController");
        CharacterAnimationData MapPresentation = ScriptableObject.CreateInstance<CharacterAnimationData>();
        MapPresentation.AnimatorController = overrideController;
        MapPresentation.Identifier = $"Generated_{WALKER_NAME}";
        AssetDatabase.CreateAsset(MapPresentation, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Map_Presentation.asset");

        g.Editor_Only_RefreshAll();
    }


    public void BuildEyeballMovement()
    {
        var animClip = new AnimationClip();

        if (AssetDatabase.IsValidFolder($"Assets/Generated/Animations/{WALKER_NAME}"))
        {
            //FileUtil.DeleteFileOrDirectory("Assets/Generated/Animations");
        }
        else
        {
            AssetDatabase.CreateFolder("Assets/Generated/Animations", WALKER_NAME);
        }

        var idle_d = CreateAnimationClip(0, 0, 0, 0, 0, 0, 14, 16, 14);
        var idle_u = CreateAnimationClip(1, 1, 1, 1, 1, 1, 15, 17, 15);
        var idle_l = CreateAnimationClip(18, 18, 18, 18, 18, 18, 32, 34, 32);
        var idle_r = CreateAnimationClip(19, 19, 19, 19, 19, 19, 33, 35, 33);

        var walk_d = CreateAnimationClip(2, 3);
        var walk_u = CreateAnimationClip(20, 21);
        var walk_l = CreateAnimationClip(22, 23);
        var walk_r = CreateAnimationClip(4, 5);

        var attack_d = CreateAnimationClip(6, 7, 8);
        var attack_u = CreateAnimationClip(24, 25, 26);
        var attack_l = CreateAnimationClip(27, 28, 29);
        var attack_r = CreateAnimationClip(9, 10, 11);

        var get_hit_d = CreateAnimationClip(12);
        var get_hit_u = CreateAnimationClip(13);
        var get_hit_l = CreateAnimationClip(30);
        var get_hit_r = CreateAnimationClip(31);

        AssetDatabase.CreateAsset(idle_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_D_Generated.anim");
        AssetDatabase.CreateAsset(idle_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_U_Generated.anim");
        AssetDatabase.CreateAsset(idle_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_L_Generated.anim");
        AssetDatabase.CreateAsset(idle_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_R_Generated.anim");
        AssetDatabase.CreateAsset(walk_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Walk_D_Generated.anim");
        AssetDatabase.CreateAsset(walk_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Walk_U_Generated.anim");
        AssetDatabase.CreateAsset(walk_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Walk_L_Generated.anim");
        AssetDatabase.CreateAsset(walk_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Walk_R_Generated.anim");
        AssetDatabase.CreateAsset(attack_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_D_Generated.anim");
        AssetDatabase.CreateAsset(attack_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_U_Generated.anim");
        AssetDatabase.CreateAsset(attack_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_L_Generated.anim");
        AssetDatabase.CreateAsset(attack_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_R_Generated.anim");
        AssetDatabase.CreateAsset(get_hit_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_D_Generated.anim");
        AssetDatabase.CreateAsset(get_hit_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_U_Generated.anim");
        AssetDatabase.CreateAsset(get_hit_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_L_Generated.anim");
        AssetDatabase.CreateAsset(get_hit_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_R_Generated.anim");

        AnimatorOverrideController overrideController = new AnimatorOverrideController(WalkerBase);
        var ExistingOverrides = new List<KeyValuePair<AnimationClip, AnimationClip>>();
        overrideController.GetOverrides(ExistingOverrides);
        Func<string, AnimationClip> F = (s) => {
            return WalkerBase.animationClips.First(c =>
            {
                //Debug.Log("Animation clip is named: " + c.name);
                return c.name == s;
            });
        };
        var overrides = new List<KeyValuePair<AnimationClip, AnimationClip>>
        {
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_D"), walk_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_U"), walk_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_L"), walk_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_R"), walk_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_D"), idle_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_U"), idle_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_L"), idle_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_R"), idle_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_D"), attack_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_U"), attack_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_L"), attack_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_R"), attack_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_D"), get_hit_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_U"), get_hit_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_L"), get_hit_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_R"), get_hit_r),
        };
        overrideController.ApplyOverrides(overrides);

        AssetDatabase.CreateAsset(overrideController, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Overrides.overrideController");
        CharacterAnimationData MapPresentation = ScriptableObject.CreateInstance<CharacterAnimationData>();
        MapPresentation.AnimatorController = overrideController;
        MapPresentation.Identifier = $"Generated_{WALKER_NAME}";
        AssetDatabase.CreateAsset(MapPresentation, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Map_Presentation.asset");

        g.Editor_Only_RefreshAll();
    }

    public void BuildNytmurMovement()
    {
        var animClip = new AnimationClip();

        if (AssetDatabase.IsValidFolder($"Assets/Generated/Animations/{WALKER_NAME}"))
        {
            //FileUtil.DeleteFileOrDirectory("Assets/Generated/Animations");
        }
        else
        {
            AssetDatabase.CreateFolder("Assets/Generated/Animations", WALKER_NAME);
        }

        var idle_d = CreateAnimationClip(0);
        var idle_u = CreateAnimationClip(1);
        var idle_l = CreateAnimationClip(13);
        var idle_r = CreateAnimationClip(14);

        var walk_d = CreateAnimationClip(2, 3);
        var walk_u = CreateAnimationClip(15, 16);
        var walk_l = CreateAnimationClip(13, 17);
        var walk_r = CreateAnimationClip(14, 4);

        var attack_d = CreateAnimationClip(5, 6, 7);
        var attack_u = CreateAnimationClip(18, 19, 20);
        var attack_l = CreateAnimationClip(21, 22, 23);
        var attack_r = CreateAnimationClip(8,9,10);

        var get_hit_d = CreateAnimationClip(11);
        var get_hit_u = CreateAnimationClip(12);
        var get_hit_l = CreateAnimationClip(24);
        var get_hit_r = CreateAnimationClip(25);

        AssetDatabase.CreateAsset(idle_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_D_Generated.anim");
        AssetDatabase.CreateAsset(idle_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_U_Generated.anim");
        AssetDatabase.CreateAsset(idle_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_L_Generated.anim");
        AssetDatabase.CreateAsset(idle_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_R_Generated.anim");
        AssetDatabase.CreateAsset(walk_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Walk_D_Generated.anim");
        AssetDatabase.CreateAsset(walk_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Walk_U_Generated.anim");
        AssetDatabase.CreateAsset(walk_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Walk_L_Generated.anim");
        AssetDatabase.CreateAsset(walk_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Walk_R_Generated.anim");
        AssetDatabase.CreateAsset(attack_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_D_Generated.anim");
        AssetDatabase.CreateAsset(attack_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_U_Generated.anim");
        AssetDatabase.CreateAsset(attack_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_L_Generated.anim");
        AssetDatabase.CreateAsset(attack_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_R_Generated.anim");
        AssetDatabase.CreateAsset(get_hit_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_D_Generated.anim");
        AssetDatabase.CreateAsset(get_hit_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_U_Generated.anim");
        AssetDatabase.CreateAsset(get_hit_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_L_Generated.anim");
        AssetDatabase.CreateAsset(get_hit_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_R_Generated.anim");

        AnimatorOverrideController overrideController = new AnimatorOverrideController(WalkerBase);
        var ExistingOverrides = new List<KeyValuePair<AnimationClip, AnimationClip>>();
        overrideController.GetOverrides(ExistingOverrides);
        Func<string, AnimationClip> F = (s) => {
            return WalkerBase.animationClips.First(c =>
            {
                //Debug.Log("Animation clip is named: " + c.name);
                return c.name == s;
            });
        };
        var overrides = new List<KeyValuePair<AnimationClip, AnimationClip>>
        {
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_D"), walk_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_U"), walk_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_L"), walk_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_R"), walk_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_D"), idle_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_U"), idle_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_L"), idle_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_R"), idle_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_D"), attack_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_U"), attack_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_L"), attack_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_R"), attack_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_D"), get_hit_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_U"), get_hit_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_L"), get_hit_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_R"), get_hit_r),
        };
        overrideController.ApplyOverrides(overrides);

        AssetDatabase.CreateAsset(overrideController, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Overrides.overrideController");
        CharacterAnimationData MapPresentation = ScriptableObject.CreateInstance<CharacterAnimationData>();
        MapPresentation.AnimatorController = overrideController;
        MapPresentation.Identifier = $"Generated_{WALKER_NAME}";
        AssetDatabase.CreateAsset(MapPresentation, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Map_Presentation.asset");

        g.Editor_Only_RefreshAll();
    }

    [NaughtyAttributes.Button]
    public void BulkImportStandard()
    {
        _BulkImport(BuildStandardMovementUnit, 30);
    }
    [NaughtyAttributes.Button]
    public void EyeballBulkImport()
    {
        _BulkImport(BuildEyeballMovement, 36);
    }

    [NaughtyAttributes.Button]
    public void NytmurBulkImport()
    {
        _BulkImport(BuildNytmurMovement, 26);
    }
    public void _BulkImport(System.Action act, int count)
    {
        List<Sprite> ls = spritesForAnimations.ToList();
        Debug.Log($"List of sprites has:{ls.Count}");
        var ActorNames = ActorName.Split('|').ToList();
        try
        {
            if (ActorNames.Count()*count == ls.Count())
            {
                for (int i = 0; i < ActorNames.Count(); i += 1)
                {
                    WALKER_NAME = ActorNames[i];
                    WALKER_SPRITES.Clear();
                    WALKER_SPRITES.AddRange(ls.GetRange(count * i, count));
                    act();
                }
            }
            else
            {
                Debug.Log($"Expecting {ActorNames.Count()} actors to have a total of {ActorNames.Count() * count} sprites, instead have: {spritesForAnimations.Count()}");
            }
        } finally
        {
            WALKER_SPRITES = ls;
        }
    }

    public void BuildWarperAnimations()
    {
        var animClip = new AnimationClip();
        UnityEditor.AssetDatabase.CreateFolder("Assets/Generated/Animations", WALKER_NAME);

        var idle_d = CreateAnimationClip(0);
        UnityEditor.AssetDatabase.CreateAsset(idle_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_D.anim");
        var idle_u = CreateAnimationClip(1);
        UnityEditor.AssetDatabase.CreateAsset(idle_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_U.anim");
        var idle_l = CreateAnimationClip(16);
        UnityEditor.AssetDatabase.CreateAsset(idle_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_L.anim");
        var idle_r = CreateAnimationClip(17);
        UnityEditor.AssetDatabase.CreateAsset(idle_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Idle_R.anim");

        //var walk_d = CreateAnimationClip(2, 3);
        //UnityEditor.AssetDatabase.CreateAsset(walk_d, $"Assets/Generated/Animations/{ActorName}/{ActorName}_Walk_D.anim");
        //var walk_u = CreateAnimationClip(13, 14);
        //UnityEditor.AssetDatabase.CreateAsset(walk_u, $"Assets/Generated/Animations/{ActorName}/{ActorName}_Walk_U.anim");
        //var walk_l = CreateAnimationClip(15, 11);
        //UnityEditor.AssetDatabase.CreateAsset(walk_l, $"Assets/Generated/Animations/{ActorName}/{ActorName}_Walk_L.anim");
        //var walk_r = CreateAnimationClip(4, 12);
        //UnityEditor.AssetDatabase.CreateAsset(walk_r, $"Assets/Generated/Animations/{ActorName}/{ActorName}_Walk_R.anim");

        var diveDown = CreateAnimationClip(11, 10, 9, 8);
        UnityEditor.AssetDatabase.CreateAsset(diveDown, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Dive_Down.anim");
        var surface = CreateAnimationClip(14,15,16,17);
        UnityEditor.AssetDatabase.CreateAsset(surface, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Surface.anim");
        var hidden = CreateAnimationClip(3);
        UnityEditor.AssetDatabase.CreateAsset(hidden, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Hidden.anim");

        var attack_d = CreateAnimationClip(12, 13);
        UnityEditor.AssetDatabase.CreateAsset(attack_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_D.anim");
        var attack_u = CreateAnimationClip(18, 19);
        UnityEditor.AssetDatabase.CreateAsset(attack_u, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_U.anim");
        var attack_l = CreateAnimationClip(20, 21);
        UnityEditor.AssetDatabase.CreateAsset(attack_l, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_L.anim");
        var attack_r = CreateAnimationClip(14, 15);
        UnityEditor.AssetDatabase.CreateAsset(attack_r, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Attack_R.anim");

        var get_hit_d = CreateAnimationClip(16, 17);
        UnityEditor.AssetDatabase.CreateAsset(get_hit_d, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Get_Hit_D.anim");
        //var get_hit_u = CreateAnimationClip(10);
        //UnityEditor.AssetDatabase.CreateAsset(get_hit_u, $"Assets/Generated/Animations/{ActorName}/{ActorName}_Get_Hit_U.anim");
        //var get_hit_l = CreateAnimationClip(20);
        //UnityEditor.AssetDatabase.CreateAsset(get_hit_l, $"Assets/Generated/Animations/{ActorName}/{ActorName}_Get_Hit_L.anim");
        //var get_hit_r = CreateAnimationClip(21);
        //UnityEditor.AssetDatabase.CreateAsset(get_hit_r, $"Assets/Generated/Animations/{ActorName}/{ActorName}_Get_Hit_R.anim");


        AnimatorOverrideController overrideController = new AnimatorOverrideController(WarperBase);
        var ExistingOverrides = new List<KeyValuePair<AnimationClip, AnimationClip>>();
        overrideController.GetOverrides(ExistingOverrides);
        System.Func<string, AnimationClip> F = (s) => {
            return WarperBase.animationClips.First(c =>
            {
                Debug.Log("Animation clip is named: " + c.name);
                return c.name == s;
            });
        };
        var overrides = new List<KeyValuePair<AnimationClip, AnimationClip>>
        {
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_D"), idle_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_U"), idle_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_L"), idle_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_R"), idle_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_D"), attack_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_U"), attack_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_L"), attack_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Attack_R"), attack_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_D"), get_hit_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_U"), get_hit_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_L"), get_hit_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("GetHit_R"), get_hit_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Invisible"), hidden),
            new KeyValuePair<AnimationClip, AnimationClip>(F("WarpIn"), surface),
            new KeyValuePair<AnimationClip, AnimationClip>(F("WarpOut"), diveDown),
        };
        overrideController.ApplyOverrides(overrides);

        AssetDatabase.CreateAsset(overrideController, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Overrides.overrideController");
        CharacterAnimationData MapPresentation = ScriptableObject.CreateInstance<CharacterAnimationData>();
        MapPresentation.AnimatorController = overrideController;
        MapPresentation.Identifier = $"Generated_{WALKER_NAME}";
        AssetDatabase.CreateAsset(MapPresentation, $"Assets/Generated/Animations/{WALKER_NAME}/{WALKER_NAME}_Map_Presentation.asset");

        g.Editor_Only_RefreshAll();
    }

    [NaughtyAttributes.Button]
    public void BuildFlashbackChar12()
    {
        if (AssetDatabase.IsValidFolder($"Assets/Generated/Humans/{ActorName}"))
        {
            //FileUtil.DeleteFileOrDirectory("Assets/Generated/Animations");
        }
        else
        {
            AssetDatabase.CreateFolder("Assets/Generated/Humans", ActorName);
        }

        var idle_d = CreateAnimationClip(0);
        var idle_u = CreateAnimationClip(1);
        var idle_l = CreateAnimationClip(6);
        var idle_r = CreateAnimationClip(7);
        var walk_d = CreateAnimationClip(2, 3);
        var walk_u = CreateAnimationClip(8,9);
        var walk_l = CreateAnimationClip(10, 11);
        var walk_r = CreateAnimationClip(4, 5);
        AssetDatabase.CreateAsset(idle_d, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Idle_D_Generated.anim");
        AssetDatabase.CreateAsset(idle_u, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Idle_U_Generated.anim");
        AssetDatabase.CreateAsset(idle_l, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Idle_L_Generated.anim");
        AssetDatabase.CreateAsset(idle_r, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Idle_R_Generated.anim");
        AssetDatabase.CreateAsset(walk_d, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Walk_D_Generated.anim");
        AssetDatabase.CreateAsset(walk_u, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Walk_U_Generated.anim");
        AssetDatabase.CreateAsset(walk_l, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Walk_L_Generated.anim");
        AssetDatabase.CreateAsset(walk_r, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Walk_R_Generated.anim");
        AnimatorOverrideController overrideController = new AnimatorOverrideController(WalkerBase);
        var ExistingOverrides = new List<KeyValuePair<AnimationClip, AnimationClip>>();
        overrideController.GetOverrides(ExistingOverrides);
        Func<string, AnimationClip> F = (s) => {
            return WalkerBase.animationClips.First(c =>
            {
                //Debug.Log("Animation clip is named: " + c.name);
                return c.name == s;
            });
        };
        var overrides = new List<KeyValuePair<AnimationClip, AnimationClip>>
        {
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_D"), walk_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_U"), walk_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_L"), walk_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_R"), walk_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_D"), idle_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_U"), idle_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_L"), idle_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_R"), idle_r),
        };
        overrideController.ApplyOverrides(overrides);

        AssetDatabase.CreateAsset(overrideController, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Overrides.overrideController");
        CharacterAnimationData MapPresentation = ScriptableObject.CreateInstance<CharacterAnimationData>();
        MapPresentation.AnimatorController = overrideController;
        MapPresentation.Identifier = $"Generated_{ActorName}";
        AssetDatabase.CreateAsset(MapPresentation, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Map_Presentation.asset");

        g.Editor_Only_RefreshAll();
    }
    [NaughtyAttributes.Button]
    public void BuildFlashbackChar10()
    {
        if (AssetDatabase.IsValidFolder($"Assets/Generated/Humans/{ActorName}"))
        {
            //FileUtil.DeleteFileOrDirectory("Assets/Generated/Animations");

        }
        else
        {
            Debug.Log("yo?");
            Debug.Log(AssetDatabase.CreateFolder("Assets/Generated/Humans", ActorName));
        }

        var idle_d = CreateAnimationClip(0);
        var idle_u = CreateAnimationClip(1);
        var idle_l = CreateAnimationClip(5);
        var idle_r = CreateAnimationClip(6);
        var walk_d = CreateAnimationClip(2, 3);
        var walk_u = CreateAnimationClip(7, 8);
        var walk_l = CreateAnimationClip(9, 5);
        var walk_r = CreateAnimationClip(4, 6);
        AssetDatabase.CreateAsset(idle_d, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Idle_D_Generated.anim");
        AssetDatabase.CreateAsset(idle_u, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Idle_U_Generated.anim");
        AssetDatabase.CreateAsset(idle_l, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Idle_L_Generated.anim");
        AssetDatabase.CreateAsset(idle_r, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Idle_R_Generated.anim");
        AssetDatabase.CreateAsset(walk_d, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Walk_D_Generated.anim");
        AssetDatabase.CreateAsset(walk_u, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Walk_U_Generated.anim");
        AssetDatabase.CreateAsset(walk_l, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Walk_L_Generated.anim");
        AssetDatabase.CreateAsset(walk_r, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Walk_R_Generated.anim");
        AnimatorOverrideController overrideController = new AnimatorOverrideController(WalkerBase);
        var ExistingOverrides = new List<KeyValuePair<AnimationClip, AnimationClip>>();
        overrideController.GetOverrides(ExistingOverrides);
        Func<string, AnimationClip> F = (s) => {
            return WalkerBase.animationClips.First(c =>
            {
                //Debug.Log("Animation clip is named: " + c.name);
                return c.name == s;
            });
        };
        var overrides = new List<KeyValuePair<AnimationClip, AnimationClip>>
        {
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_D"), walk_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_U"), walk_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_L"), walk_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Walk_R"), walk_r),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_D"), idle_d),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_U"), idle_u),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_L"), idle_l),
            new KeyValuePair<AnimationClip, AnimationClip>(F("Idle_R"), idle_r),
        };
        overrideController.ApplyOverrides(overrides);

        AssetDatabase.CreateAsset(overrideController, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Overrides.overrideController");
        CharacterAnimationData MapPresentation = ScriptableObject.CreateInstance<CharacterAnimationData>();
        MapPresentation.AnimatorController = overrideController;
        MapPresentation.Identifier = $"Generated_{ActorName}";
        AssetDatabase.CreateAsset(MapPresentation, $"Assets/Generated/Humans/{ActorName}/{ActorName}_Map_Presentation.asset");

        g.Editor_Only_RefreshAll();
    }

    private AnimationClip CreateAnimationClip(params int[] frames)
    {
        Sprite[] spriteFrames = frames.Select(i => WALKER_SPRITES[i]).ToArray();
        var animClip = new AnimationClip();
        animClip.frameRate = 4f;
        // First you need to create e Editor Curve Binding
        EditorCurveBinding curveBinding = new EditorCurveBinding();
        // I want to change the sprites of the sprite renderer, so I put the typeof(SpriteRenderer) as the binding type.
        curveBinding.type = typeof(SpriteRenderer);
        // Regular path to the gameobject that will be changed (empty string means root)
        curveBinding.path = "Sprite";
        // This is the property name to change the sprite of a sprite renderer
        curveBinding.propertyName = "m_Sprite";
        // An array to hold the object keyframes
        ObjectReferenceKeyframe[] keyFrames = new ObjectReferenceKeyframe[frames.Length];

        for (int i = 0; i < keyFrames.Count(); i += 1)
        {
            keyFrames[i] = new ObjectReferenceKeyframe();
            // set the time
            keyFrames[i].time = i/4f;
            // set reference for the sprite you want
            keyFrames[i].value = spriteFrames[i];
        }
        AnimationUtility.SetObjectReferenceCurve(animClip, curveBinding, keyFrames);

        var tSettings = AnimationUtility.GetAnimationClipSettings(animClip);
        tSettings.loopTime = true;
        AnimationUtility.SetAnimationClipSettings(animClip, tSettings);

        return animClip;
    }

    public List<Sprite> BBSpritesDebug;
    [NaughtyAttributes.Button]
    public void AutoGenerateBasebuilderDefs()
    {
        //UnityEditor.AssetDatabase.CreateFolder("Assets/Generated/Props");
        int i = 0;
        foreach (var bbSprite in BBSpritesDebug) {
            var propDef = ScriptableObject.CreateInstance<BaseBuilderPropDefinition>();
            propDef.PropSprite = bbSprite;
            propDef.Size = new Vector2Int(Mathf.CeilToInt(bbSprite.rect.size.x/16), Mathf.CeilToInt(bbSprite.rect.size.y/16));
            //propDef.OffsetPosition = ((Vector2)propDef.Size / 2f);
            AssetDatabase.CreateAsset(propDef, $"Assets/Generated/Props/reward_prop_{i}.asset");
            i += 1;
        }

        g.Editor_Only_RefreshAll();
    }
    [NaughtyAttributes.Button]
    public void FixUpAllMapSprites()
    {
        foreach (var cad in g.All<CharacterAnimationData>().Where(cad => cad.MapSprite == null))
        {
            Debug.Log($"looking at {cad.name}");
            AnimatorOverrideController aoc = cad.AnimatorController as AnimatorOverrideController;
            if (aoc != null)
            {
                List<KeyValuePair<AnimationClip, AnimationClip>> overrides = new List<KeyValuePair<AnimationClip, AnimationClip>>();
                aoc.GetOverrides(overrides);
                AnimationClip idle_d = overrides.Find(kvp => kvp.Key.name == "Idle_D").Value;
                Debug.Log(idle_d);
                EditorCurveBinding[] bindings = AnimationUtility.GetObjectReferenceCurveBindings(idle_d);
                EditorCurveBinding spriteBinding = bindings.First(binding => binding.path == "Sprite");
                var keyFrames = AnimationUtility.GetObjectReferenceCurve(idle_d, spriteBinding);
                //anim.Evaluate(0f);
                var sprite = keyFrames[0].value as Sprite;

                cad.MapSprite = sprite;
            }
        }
    }

    [NaughtyAttributes.Button]
    public void OnlyBuffsAreVisibleByDefault()
    {
        foreach(var t in g.All<Talent>())
        {
            t.ShownAsBuff = t is BuffDefinition;
        }
    }

    [NaughtyAttributes.Button]
    public void FixTalentDescs()
    {
        foreach (var t in g.All<Talent>())
        {
            if (t.Description == "" || t.Description == null)
                t.Description = " ";
        }
    }

#endif
}
