﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractObjectPool : SerializableScriptableObject
{
    public int CurrentCapacity
    {
        get;
        protected set;
    }
    public int DesiredCapacity;
    public int IncreasedCapacityPerFrame;

    public abstract AbstractPoolable RequestNew(Vector3 position);

    public abstract void Setup(ObjectPoolManager objectPoolManager);
    public virtual bool RequiresUpdate() => true;
    public virtual void OnUpdate() { }

    public abstract void Released(object g);
}

public class ObjectPoolBase<T> : AbstractObjectPool where T : AbstractPoolable
{
    [NaughtyAttributes.ReadOnly]
    public ObjectPoolManager opm;
    public T prefab;

    //public HashSet<Poolable> ActiveInstances;
    private Queue<T> InactiveInstances;

    public override void Setup(ObjectPoolManager objectPoolManager)
    {
        this.opm = objectPoolManager;
        InactiveInstances = new Queue<T>();
        CurrentCapacity = 0;
    }
    public override AbstractPoolable RequestNew(Vector3 position)
    {
        if (prefab == null) Debug.LogError(this);
        var rtn = Instantiate(prefab, position, Quaternion.identity, opm.transform);
        //rtn.gameObject.SetActive(false);
        rtn.OnActivatedByPool(this);
        CurrentCapacity += 1;
        return rtn;
    }
    protected T Request(Vector3 position)
    {
        T instance = null;
        while (instance == null && InactiveInstances != null && InactiveInstances.Count > 0)
        {
            instance = InactiveInstances.Dequeue();
        }
        if (instance == null)
        {
            return RequestNew(position) as T;
        }
        instance.transform.position = position;
        instance.OnActivatedByPool(this);
        return instance;
    }

    public override void Released(object g)
    {
        if (g is T)
        {
            var t = g as T;
            if (!InactiveInstances.Contains(t))
                InactiveInstances.Enqueue(t);
        }
    }
}
