﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Units/AnimationData")]
public class CharacterAnimationData : SerializableScriptableObject
{
    [NaughtyAttributes.ShowAssetPreview]
    public Sprite BigSprite;
    [NaughtyAttributes.ShowAssetPreview]
    public Sprite MapSprite;
    public RuntimeAnimatorController AnimatorController;
    public bool Teleporter;
    public Persona persona;
    [NaughtyAttributes.ShowAssetPreview]
    public Sprite NametagSmall;
    [NaughtyAttributes.ShowAssetPreview]
    public Sprite NametagLarge;

    [NaughtyAttributes.ShowAssetPreview]
    public Sprite IconBackingSmall;
    [NaughtyAttributes.ShowAssetPreview]
    public Sprite IconBackingSmallSelected;
    [NaughtyAttributes.ShowAssetPreview]
    public Sprite IconBackingLarge;
    //[NaughtyAttributes.ShowAssetPreview]
    public Sprite[] StickyNotes;
    
    public HeartSprites heartSprites;
    public CharacterAnimationData GhostPresentation;

    [NaughtyAttributes.ShowAssetPreview]
    public Sprite StarSprite;

    [NaughtyAttributes.ShowAssetPreview]
    public Sprite NametagSticky;

    public int Complexity;
}
