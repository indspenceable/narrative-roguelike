﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Buffs/TeamBased")]
public class TeamBasedBuff : Talent
{
    [System.Serializable]
    public class KVPair
    {
        public string Stat;
        public int modifier;

        public KVPair(string k, int v)
        {
            Stat = k;
            modifier = v;
        }
    }
    public List<KVPair> ModifiersFriendly = new List<KVPair>();
    public List<KVPair> ModifiersEnemy = new List<KVPair>();

    public override int Modifier(string name, MapUnit u)
    {
        var Modifiers = u.data.Friendly ? ModifiersFriendly : ModifiersEnemy;
        var mods = Modifiers.Where(m => m.Stat == name).Select(m => m.modifier);
        if (mods.Any())
            return mods.Sum();
        return base.Modifier(name, u);
    }
}
