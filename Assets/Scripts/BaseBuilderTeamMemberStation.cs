﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class BaseBuilderTeamMemberStation : MonoBehaviour, IPointerClickHandler
{
    public CampaignGameMode bbMode;
    public Animator characterAnimator;
    public RadialMenu menu;
    public SpriteRenderer sr;
    public UnitDefinition myUnit;
    public Globals g;
    public MetaProgression mp;
    public TransitionEffect te;
    public CutsceneMode cutsceneMode;
    public BaseBuilder bb;
    public TextboxController textbox;
    public BaseBuilderUIMouseTracker tracker;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (textbox.Showing()) return;
        Vector3 wp = eventData.pointerCurrentRaycast.worldPosition;
        if (GetMenuOptionsForChar(myUnit.Data).Any())
        {
            ShowMenuToSelectCharacter(new Vector3(Mathf.Round(wp.x), Mathf.Round(wp.y)), myUnit.Data, bbMode.campaign.team);
        }
        else
        {
            textbox.Display("I need to spend more time with the folks on your current team before we'll have anything to say to eachother. Select me on future runs!",
                myUnit.Data.CharacterName,
                myUnit.Data.presentation.persona.DefaultPortrait,
                myUnit.Data.presentation.persona);
        }
    }

    public void Start()
    {
        characterAnimator.runtimeAnimatorController = myUnit.Data.presentation.AnimatorController;
    }

    public void ShowMenuToSelectCharacter(Vector3 pos, UnitData character, List<UnitData> team)
    {

        var menuOptions = GetMenuOptionsForChar(character).Select(character2 =>
        {
            return new MenuBase.MenuOption(
                character2.Data.presentation.MapSprite,
                character2.Identifier,
                //() => cutsceneMode.ActivateWithData(te, msm.campaign, support, () => msm.ActivateWithData(te, msm.campaign)),
                () => ShowMenuToSelectCutscene(pos, character, character2.Identifier),
                character.presentation.IconBackingSmall,
                character.presentation.IconBackingSmallSelected,
                true,
                null,
                (b) => { });
        });

        var nytmurCutscene = UnlockedNytmurCutscene(character);
        if (nytmurCutscene != null)
        {
            menuOptions = menuOptions.Append(new MenuBase.MenuOption(
                bb.NytmurSprite,
                "Nytmur",
                //() => cutsceneMode.ActivateWithData(te, msm.campaign, support, () => msm.ActivateWithData(te, msm.campaign)),
                () => cutsceneMode.ActivateWithData(te, bbMode.campaign, nytmurCutscene, () => bbMode.ActivateWithData(te, bbMode.campaign)),
                character.presentation.IconBackingSmall,
                character.presentation.IconBackingSmallSelected,
                true,
                null,
                (b) => { }));
        }

        menu.Setup(pos, menuOptions.ToArray());
    }

    private CutsceneData UnlockedNytmurCutscene(UnitData character)
    {
        var key = mp.CharacterGroupKey(character.CharacterName, "Nytmur");
        return g.All<SupportConversation>().Where(su => su.MainCharacter(character.CharacterName) && su.IsWithNytmur && mp.GetBool(key)).FirstOrDefault();
    }

    private IEnumerable<UnitDefinition> GetMenuOptionsForChar(UnitData character)
    {
        return g.All<UnitDefinition>().Where(ud => ud.SelectableCharacter && ud.Identifier != character.Identifier).Where(character2 =>
        {
            List<SupportConversation> supports = SupportsContainingBoth(character, character2.Identifier);
            return supports.Any();
        });
    }

    private void ShowMenuToSelectCutscene(Vector3 pos, UnitData character, string character2Name)
    {
        var supports = SupportsContainingBoth(character, character2Name);
        menu.Setup(pos, 
            supports.Where(s =>
                s.MainCharacter(character.CharacterName) &&
                s.MainCharacter(character2Name) &&
                (mp.GetInt(mp.CharacterGroupKey(s.RequiredCharacters.ToArray())) >= s.RequiredSupportLevel)
            ).Select(
                s => new MenuBase.MenuOption(null,
                    s.name,
                    () => cutsceneMode.ActivateWithData(te, bbMode.campaign, s, () => bbMode.ActivateWithData(te, bbMode.campaign)),
                character.presentation.IconBackingSmall,
                character.presentation.IconBackingSmallSelected,
                    true,
                    null,
                    (b) => { }
                )
        ).ToArray());
    }
    

    private List<SupportConversation> SupportsContainingBoth(UnitData character, string character2)
    {
        return g.All<SupportConversation>().Where(s => s.MainCharacter(character.Identifier) &&
            s.MainCharacter(character2) &&
            mp.GetInt(mp.CharacterGroupKey(s.RequiredCharacters.ToArray())) >= s.RequiredSupportLevel).ToList();
    }

    private bool ConfirmDisplaySupport(UnitData character, SupportConversation s, List<string> currentMembers)
    {
        bool a = s.RequiredCharacters.Contains(character.CharacterName);
        bool b = s.RequiredCharacters.Any(c_ => currentMembers.Contains(c_));
        bool c = mp.GetInt(mp.CharacterGroupKey(s.RequiredCharacters.ToArray())) >= s.RequiredSupportLevel;
        return a && b;
    }
}
