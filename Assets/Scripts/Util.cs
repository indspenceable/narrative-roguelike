﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using System;

public static class Util
{
    public class MultiCoroutineCollector
    {
        private List<Coroutine> cos = new List<Coroutine>();
        private MonoBehaviour runner;

        public MultiCoroutineCollector(MonoBehaviour coroutineRunner)
        {
            this.runner = coroutineRunner;
        }

        public void Add(IEnumerator co)
        {
            cos.Add(runner.StartCoroutine(co));
        }
        public IEnumerator Collect(System.Action cb = null)
        {
            foreach (var co in cos)
            {
                yield return co;
            }
            if (cb != null) cb();
        }
    }

    public static Vector2 CenterOfTile(this Vector2Int v2i)
    {
        return v2i + new Vector2(0.5f, 0.5f);
    }

    public static string StandardizeLineHeight(this string s)
    {
        return "<line-height=0%>" + s + "</line-height>";
    }

// in Vector2Int
public static int ManhattanDistance(this Vector2Int a, Vector2Int b)
    {
        checked
        {
            return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
        }
    }
    public static int Manhattan(this Vector2Int v2i)
    {
        return v2i.ManhattanDistance(Vector2Int.zero);
    }
    public static float Distance(this Vector2Int a, Vector2Int b)
    {
        checked
        {
            return Vector2.Distance(a, b);
        }
    }
    public static bool Adjacent(this Vector2Int a, Vector2Int b)
    {
        checked
        {
            return ManhattanDistance(a, b) == 1;
        }
    }
    // in Vector3Int
    public static int ManhattanDistance(this Vector3Int a, Vector3Int b)
    {
        checked
        {
            return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y) + Mathf.Abs(a.z - b.z);
        }
    }



    /// <summary>
    /// Shuffles the element order of the specified list.
    /// </summary>
    public static void Shuffle<T>(this IList<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }

    public static IList<T> Shuffled<T>(this IEnumerable<T> ts)
    {
        var dup = new List<T>(ts);
        dup.Shuffle();
        return dup;
    }

    public static IList<T> Reversed<T>(this IEnumerable<T> ts)
    {
        var dup = new List<T>(ts);
        dup.Reverse();
        return dup;
    }

    public static T Pick<T>(this IEnumerable<T> ts)
    {
        return ts.Shuffled()[0];
    }

    public static T WeightedPick<T>(this IEnumerable<T> ts, System.Func<T, int> weight)
    {
        var pairs = ts.Select(o => new KeyValuePair<T, int>(o, weight(o))).Where(kvp => kvp.Value > 0).ToList();
        if (!pairs.Any()) Debug.LogError("#WeightedPick on a collection with no weighted elements");
        float pct = UnityEngine.Random.Range(0f, 1f);
        float total = pairs.Select(p => p.Value).Sum();
        int index = 0;
        int SumValue = 0;


        while (true)
        {
            SumValue += pairs[index].Value;
            if (SumValue / total > pct) return pairs[index].Key;
            index += 1;
        }
        //return pairs[pairs.Count-1].Key;
    }

    public static T AggregateOrDefault<T>(this IEnumerable<T> ts, System.Func<T, T, T> ag, T DefaultValue)
    {
        if (ts.Count() > 0)
            return ts.Aggregate(ag);
        return DefaultValue;
    }

    public static IEnumerator Hop(AnimatorPool pool, AudioPool audioPool, MusicManager music, Sprite prize, Vector3 sp, Vector3 ep, float duration, AnimationCurve curve)
    {
        AnimatorPoolable ap = pool.RequestWithAnimator(sp, null);
        ap.sr.sprite = prize;
        ap.transform.localScale = Vector3.zero;

        yield return Util.Ease(v => music.CrossfadeCurrentVolumePercent = v, 1f, 0.5f, AnimationCurve.EaseInOut(0, 0, 1, 1), 0.75f);
        //manager.music.CrossfadeCurrentVolumePercent = 0.5f;
        audioPool.PlaySimple(audioPool.sfx.OpenChestSFX);

        float dt = 0f;
        while (dt < duration)
        {
            yield return null;
            dt += Time.deltaTime;
            float pct = (dt / duration);
            ap.transform.position = Vector3.Lerp(sp, ep, pct) + Vector3.up * curve.Evaluate(pct);
            ap.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, curve.Evaluate(pct));
        }

        yield return Util.Ease(v => music.CrossfadeCurrentVolumePercent = v, 0.5f, 1f, AnimationCurve.EaseInOut(0, 0, 1, 1), 0.75f);
        //manager.music.CrossfadeCurrentVolumePercent = 1f;
        yield return null;

        ap.transform.localScale = Vector3.one;
        ap.ExternalDeactivate();
    }
    public static IEnumerator MoveUnitToPosition(MapUnit targetUnit, Vector2Int destination, AnimationCurve curve, float duration) {
        // offset from our tile center -> our sprite center, is then applied to both the center of our tile and the center of the end tile
        var offset = targetUnit.CenterOfSprite()-targetUnit.data.pos.CenterOfTile();

        yield return MoveTransformToPosition(targetUnit.transform, targetUnit.data.pos.CenterOfTile() + offset, destination.CenterOfTile() + offset, curve, duration);

        targetUnit.MoveTo(destination, true);
    }
    private static void DoNothing() { }
    public static IEnumerator MoveTransformToPosition(Transform targetUnit, Vector3 sp, Vector3 ep, AnimationCurve curve, float duration, System.Action cb = null)
    {
        
        float dt = 0f;
        while (dt <= duration)
        {
            yield return null;
            dt += Time.deltaTime;
            float position = 0;
            if (duration != 0) {
                position = curve.Evaluate(dt / duration);
            }
            targetUnit.transform.position = Vector3.Lerp(sp, ep, position);
        }
        if (cb != null) cb();
    }

    public static IEnumerator Ease(System.Action<float> cb, float startValue, float endValue, AnimationCurve animationCurve, float duration)
    {
        float dt = 0f;
        while (dt < duration)
        {
            yield return null;
            dt += Time.deltaTime;
            cb(Mathf.Lerp(startValue, endValue, animationCurve.Evaluate(dt / duration)));
        }
    }

    public static IEnumerator MoveTransformToLocalPosition(Transform targetUnit, Vector3 sp, Vector3 ep, AnimationCurve curve, float duration, System.Action cb = null)
    {
        float dt = 0f;
        while (dt <= duration)
        {
            yield return null;
            dt += Time.deltaTime;
            targetUnit.transform.localPosition = Vector3.Lerp(sp, ep, curve.Evaluate(dt / duration));
        }

        if (cb != null) cb();
    }

    public static float SmartFloor(float v, int ToNearest)
    {
        return ((int)(v * ToNearest)) / (float)ToNearest;
    }
    public static float SmartRounded(float v, int ToNearest)
    {
        return (Mathf.RoundToInt(v * ToNearest)) / (float)ToNearest;
    }
    public static Vector3 Rounded(this Vector3 vec)
    {
        return new Vector3(SmartFloor(vec.x, 16), SmartFloor(vec.y, 16));
    }
}
