﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DebugAnimatedTileMaker : MonoBehaviour
{
    public UnityEngine.Tilemaps.AnimatedTile tile;
    public List<Sprite> sprites;
    public int IdleFrames;
    public int PeekFrames;


    [NaughtyAttributes.Button]
    public void SetSprites()
    {
        List<Sprite> main = new List<Sprite>();
        for (int i = 0; i < IdleFrames; i += 1)
        {
            main.Add(sprites[0]);
        }
        main.AddRange(sprites);
        IList<Sprite> reversed = sprites.Reversed();
        for (int i = 0; i < PeekFrames; i += 1)
        {
            main.Add(reversed[0]);
        }
        main.AddRange(reversed);
        tile.m_AnimatedSprites = main .ToArray();
        //UnityEditor.AssetDatabase.
    }
}