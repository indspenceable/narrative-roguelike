﻿using UnityEngine;

public class ManageParty2 : GameModeInstance
{
    public CampaignGameMode gm;
    public MusicManager music;
    public ManageParty2CharacterBar CharacterBar;
    public ManageParty2DetailsPane Details;
    public ManageParty2CharacterInventoryManager CharacterInventory;
    public ManageParty2SharedInventoryManager SharedInventory;
    public Globals globals;

    public TransitionEffect tEffect;
    public CampaignGameMode TBSBattleMode;
    public CutsceneMode cutscene;

    public override void PostActivate()
    {
       this.globals.Quicksave(gm, gm.campaign);
    }

    public override void PostDeactivate()
    {
    }

    public override void PreActivate(object DeserializedData)
    {
        if (DeserializedData != null)
            gm.campaign = DeserializedData as CampaignStatus;

        // Heal em up
        // also select quotes
        foreach (var c in gm.campaign.team)
        {
            c.currentDamage = Mathf.Min(c.currentDamage, c.MaxHP - 1);
            c.currentQuote = c.SelectQuoteKey();
        }

        CharacterBar.Setup(gm.campaign.team);
        CharacterInventory.Clear();
        Details.Clear();
        SharedInventory.Setup(gm.campaign.inventory, gm.campaign.team);
        music.FadeTo(music.config.BetweenRoomDowntime);
    }

    public override void PreDeactivate()
    {
        var a = 3;
    }
    public void NextRoom()
    {
        if ((gm.campaign.currentStage.IsNytmurCastle && gm.campaign.currentStage.LevelsCount == gm.campaign.CurrentStageProgress + 2) ||
           (!gm.campaign.currentStage.IsNytmurCastle && gm.campaign.currentStage.LevelsCount == gm.campaign.CurrentStageProgress + 1))
        {
            cutscene.ActivateWithData(tEffect, gm.campaign, gm.campaign.currentStage.BossCutscene, () => TBSBattleMode.ActivateWithData(tEffect, gm.campaign));
        }
        else
        {
            TBSBattleMode.ActivateWithData(tEffect, gm.campaign);
        }
    }
}
