﻿using System;
using System.Linq;
using System.Text;
using UnityEngine;

public class SelectUnitHandler : TBSGameManager.InputHandler
{
    public TBSGameManager manager;

    private MapUnit _HoveredUnit;
    private MapUnit HoveredUnit
    {
        set
        {
            if (_HoveredUnit == value) return;

            if (_HoveredUnit != null) _HoveredUnit.HealthMeter.Displayed = false;
            _HoveredUnit = value;
            if (_HoveredUnit != null) _HoveredUnit.HealthMeter.Displayed = true;
            manager.hoverHint.focus = value;
        }
    }

    public SelectUnitHandler(TBSGameManager manager)
    {
        this.manager = manager;
    }

    public void Click(int mouseButton)
    {
        if (FirstTick) return;
        if (manager.MenuOpen()) return;
        if (mouseButton == 0)
        {
            MapUnit u = manager.directory.UnitAt(manager.MousePositionToTileAddress());
            if (u != null)
            {
                // select u!
                if (u.data.Friendly && u.ShouldAct())
                {
                    manager.CurrentHandler = new SelectMovementHandler(manager, u, this);

                    manager.audioPool.PlaySimple(manager.soundConfig.SelectUnit);
                }
            }
        }
        else if (mouseButton == 1)
        {
            // nothing happens when you right click
        }
    }
    
    bool FirstTick = false;
    public void Install()
    {
        //manager.TileIndicator.gameObject.SetActive(true);
        //Cursor.visible = false;
        FirstTick = true;
        manager.SetUIDisplayed(true);
    }

    public void Uninstall()
    {
        //manager.TileIndicator.gameObject.SetActive(false);
        //Cursor.visible = true;
        HoveredUnit = null;
        FirstTick = true;
        manager.SetUIDisplayed(false);
    }

    private class TrapTooltip : IHasTooltip
    {
        public string tt;
        public TrapTooltip(string tt) { this.tt = tt; }
        public string GetTooltip()
        {
            return tt;
        }
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) manager.Settings.Toggle();
        if (manager.MenuOpen()) return;
        var MouseTile = manager.MousePositionToTileAddress();
        HoveredUnit = manager.directory.UnitAt(MouseTile);
        manager.hoverHint.focusTile = manager.map.TileAt(MouseTile);
        FirstTick = false;

        manager.SetUIDisplayed((this._HoveredUnit == null || manager.hoverHint.MOUSE_ON_RIGHT_SIDE_OF_SCREEN()) && !this.manager.UIDisabled);
    }
}