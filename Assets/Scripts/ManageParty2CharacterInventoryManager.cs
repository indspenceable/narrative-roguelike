﻿using System.Collections.Generic;
using UnityEngine;

public class ManageParty2CharacterInventoryManager : MonoBehaviour
{
    public List<ManageParty2CharacterInventoryButton> Buttons;

    internal void Clear()
    {
        foreach (var b in Buttons) b.Hide();
    }

    internal void Show(UnitData u)
    {
        Clear();
        CharacterAnimationData cad = u.presentation;
        var i = 0;
        foreach (var t in u.IntrinsicTalents)
        {
            Buttons[i].ShowIntrinsic(cad.IconBackingLarge, t);
            Buttons[i].ShowAnimation(i / 25f);
            i += 1;
        }
        //foreach (var t in u.inventory)
        //{
        //    var _t = t;
        //    Buttons[i].ShowItem(cad, t, () => { u.inventory.Remove(_t); Show(u); });
        //    Buttons[i].ShowAnimation(i / 25f);
        //    i += 1;
        //}
    }
}
