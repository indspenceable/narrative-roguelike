﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UnitDirectory : MonoBehaviour
{
    public TBSGameManager manager;
    public MapUnitPrefabDefinition DefaultUnitPrefab;
    public List<MapUnit> mapUnits = new List<MapUnit>();
    public Dictionary<Vector2Int, UnitData> Graveyard = new Dictionary<Vector2Int, UnitData>();
    public Globals globals;
    public BuffInstance enemyInitialStatus;

    internal void SpawnInUnitsRaw(List<UnitData> units)
    {
        foreach (var unit in units)
        {
            BuildMapUnit(unit, unit.pos, unit.Friendly);
        }
    }
    internal void SpawnInPlayerTeamUnitsInInitialSpawnLocations(List<UnitData> units)
    {
        //var vs = new List<Vector2Int>() { Vector2Int.up, Vector2Int.right, Vector2Int.up + Vector2Int.right, Vector2Int.zero };
        int i = 0;
        foreach (var unit in units)
        {
            BuildMapUnit(unit, manager.FindClosestAvailableSpotTo(MapManager.TileLocationFromTransform(manager.LevelInstance.PlayerSpawnLocations[i].transform)), true);
            i += 1;
        }
    }

    public void AddToGraveyard(UnitData d)
    {
        Graveyard[d.pos] = d;
    }
    internal bool DeadUnitAt(Vector2Int v2i)
    {
        return Graveyard.ContainsKey(v2i);
    }
    internal UnitData RaiseUnitAt(Vector2Int v2i)
    {
        UnitData rtn = Graveyard[v2i];
        Graveyard.Remove(v2i);
        return rtn;
    }
    
    public void SpawnInInitialEnemyUnits(StageData currentStage, int count) {
        //Explore from the first player spawn location to anchor to only accessible tiles.
        var startingPos = MapManager.TileLocationFromTransform(manager.LevelInstance.PlayerSpawnLocations[0].transform);
        Func<Vector2Int, bool> cb = (v2i) => manager.map.TileAt(v2i).MovementAllowed(UnitData.MobilityType.LAND);
        Dictionary<Vector2Int, List<Vector2Int>> explo = manager.pathing.ExploreAllPathsFromLocation(startingPos, 50, cb, cb);    

            foreach (var spawner in manager.LevelInstance.PredefinedEnemySpawns)
            {
                Vector2Int SpawnPosition = MapManager.TileLocationFromTransform(spawner.transform);
                UnitDefinition UnitDef = spawner.Definition;
                var mu = BuildMapUnit(globals.Duplicate(UnitDef.Data), SpawnPosition, false, UnitDef.AlternateCostumes);
                mu.data.Unaware = mu.data.StartsUnaware;
            }

            for (int i = 0; i < count; i += 1)
            {
                EnemyDefinition eDef = SelectUnitToSpawn(currentStage);
                Vector2Int? SpawnPosition = SelectSpawnPosition(explo, eDef.Data.Mobility);
                if (SpawnPosition.HasValue)
                {
                    var mu = BuildMapUnit(globals.Duplicate(eDef.Data), SpawnPosition.Value, false, eDef.AlternateCostumes);
                    //foreach(BuffInstance initialBuff in eDef.InitialBuffs)
                    //{
                    //    mu.ApplyStatus(initialBuff);
                    //}
                    mu.data.Unaware = mu.data.StartsUnaware;
            } else
                {
                    Debug.LogError("Trying to spawn an enemy but theres no room. For mobility " + eDef.Data.Mobility);
                }
            }
       
    }

    private Vector2Int? SelectSpawnPosition(Dictionary<Vector2Int, List<Vector2Int>> exploredPaths, UnitData.MobilityType mobility)
    {
        if (mobility == UnitData.MobilityType.FLIGHT) mobility = UnitData.MobilityType.LAND;
        IEnumerable<MapTile> enumerable = manager.map.Tiles.Where(rtn =>
        {
            return UnitAt(rtn.pos) == null &&
            manager.LevelInstance.PlayerSpawnLocations.Select(sp => MapManager.TileLocationFromTransform(sp.transform).ManhattanDistance(rtn.pos)).Min() > 4 &&
            rtn.MovementAllowed(mobility) &&
            ((mobility == UnitData.MobilityType.LAND) ? exploredPaths.ContainsKey(rtn.pos) : true);
        });
        if (enumerable.Any())
            return enumerable.Pick().pos;
        return null;
    }

    private EnemyDefinition SelectUnitToSpawn(StageData currentStage)
    {
        return currentStage.enemies.WeightedPick(e => e.weight * (e.DebugDisabled ? 0 : 1));
    }

    public MapUnit BuildMapUnit(UnitData unit, Vector2Int pos, bool Friendly, List<CharacterAnimationData> alternateCostumes = null)
    {
        if (unit.UnitPrefab == null) unit.UnitPrefab = DefaultUnitPrefab;
        var mu = Instantiate(unit.UnitPrefab.prefab);

        if (alternateCostumes != null)
        {
            unit.presentation = alternateCostumes.Append(unit.presentation).Pick();
        }
        mapUnits.Add(mu);
        mu.Setup(unit, manager);
        mu.MoveTo(pos, true);
        mu.PostSetup();

        mu.data.Friendly = Friendly;
        return mu;
    }

    public MapUnit UnitAt(Vector2Int pos)
    {
        return mapUnits.Find(u => u.IsAt(pos));
    }

    internal IEnumerable<MapUnit> Query(Func<MapUnit, bool> conditions)
    {
        return mapUnits.Where(conditions);
    }

    internal void RemoveUnitWithoutAnimation(MapUnit u)
    {
        if (!u.data.Friendly && !u.data.CannotRaise)
        {
            Graveyard[u.data.pos] = u.data;
        }
        mapUnits.Remove(u);
        Destroy(u.gameObject);
    }

    public void Deactivate()
    {
        foreach (var u in mapUnits.ToList())
            RemoveUnitWithoutAnimation(u);
    }
}
