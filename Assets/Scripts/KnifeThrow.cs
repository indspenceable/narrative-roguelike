﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Skills/Knife Throw")]
public class KnifeThrow : AttackTalent
{
    public int KnifeCount;
    public BuffInstance KnifeMapEffect;

    public override bool TargetsEmpty => false;
    public override bool TargetsFriendly => false;
    public override bool TargetsHostile => true;
    public override bool TargetsSelf => false;

    public override bool ValidTargetableTileContents(MapUnit Me, Vector2Int v2i)
    {
        return base.ValidTargetableTileContents(Me, v2i) && Me.data.KnifeLocations.Count < KnifeCount;
    }

    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        Me.data.KnifeLocations.Add(target);
        Me.manager.map.TileAt(target).AddMapEffect(KnifeMapEffect);
        yield return base.Use(Me, target);
    }
}
