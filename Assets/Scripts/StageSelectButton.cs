﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageSelectButton : MonoBehaviour
{
    public StageData stage;
    public MissionSelectManager msm;
    public CampaignGameMode gm;
    public Button b;
    public bool AlwaysAvailable = false;

    public Sprite WhenUnavailable;
    public Sprite WhenCompleted;
    public Image CompletedCheck;
    public Color ClearColor = new Color(0.8f, 0.8f, 0.8f);

    // Update is called once per frame
    void LateUpdate()
    {
        //b.interactable = (AlwaysAvailable || !gm.campaign.CompletedStages.Contains(stage));
        bool completed = gm.campaign.CompletedStages.Contains(stage);
        var ss = b.spriteState;
        ss.disabledSprite = completed ? WhenCompleted : WhenUnavailable;
        b.spriteState = ss;
        CompletedCheck.gameObject.SetActive(completed);
        b.image.SetNativeSize();
        b.image.color = completed ? ClearColor : Color.white;
    }

    public void Click()
    {
        msm.SelectMission(stage);
    }
}
