﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class IconButton : MonoBehaviour
{
    public Image icon;
    public Button button;
    public Image BadgeBack;
    public TMPro.TextMeshProUGUI Badge;
    public HasTooltip tt;
    public Image SelectedIndicator;
    public Image Backing;
    public EventTrigger trigger;
    public AudioPool audioPool;
    public Action<bool> OnHover = null;

    private bool OldInteractable = false;
    private void OnEnable()
    {
        ForceUpdate();
    }
    internal void Update()
    {
        if (button.interactable != OldInteractable) 
            ForceUpdate();
    }

    public void TriggerHoverCallbacks(bool b)
    {
        if (OnHover == null) return;
        OnHover(b);
    }

    private void ForceUpdate()
    {
        OldInteractable = button.interactable;
        EventTrigger.Entry pointerDownTrigger = trigger.triggers.Find(t => t.eventID == EventTriggerType.PointerDown);
        if (pointerDownTrigger == null)
        {
            pointerDownTrigger = new UnityEngine.EventSystems.EventTrigger.Entry();
            pointerDownTrigger.eventID = UnityEngine.EventSystems.EventTriggerType.PointerDown;
            trigger.triggers.Add(pointerDownTrigger);
        }
        pointerDownTrigger.callback.RemoveAllListeners();
        if (!button.interactable)
            pointerDownTrigger.callback.AddListener(bed => audioPool.PlaySimple(audioPool.sfx.Thud));
    }
}
