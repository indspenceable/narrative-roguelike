﻿using System.Collections;
using UnityEngine;
using static MapUnit;

[CreateAssetMenu(menuName = "Talents/Special/Apply Status To Map")]
public class CreateMapEffectTalent : AbstractTargetedSkillTalent
{
    public bool TargetEmptyTiles = true;
    public override bool TargetsEmpty => TargetEmptyTiles;

    public bool TargetFriendlyUnits = true;
    public override bool TargetsFriendly => TargetFriendlyUnits;
    public bool TargetHostileUnit = true;
    public override bool TargetsHostile => TargetHostileUnit;
    public override bool TargetsSelf => TargetsFriendly;

    public UnitData.MobilityType MobilityFilter = UnitData.MobilityType.FLIGHT;
    public BuffInstance Status;
    public AudioClip OnUseAudio;

    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        for (int x = -Radius; x <= Radius; x += 1)
        {
            for (int y = -Radius; y <= Radius; y += 1)
            {
                if (Mathf.Abs(x) + Mathf.Abs(y) <= Radius)
                {
                    MapTile currentMapTile = Me.manager.map.TileAt(target + new Vector2Int(x, y));
                    if (currentMapTile != null && currentMapTile.MovementAllowed(MobilityFilter))
                    {
                        Me.manager.animatorPool.RequestWithAnimator(currentMapTile.pos.CenterOfTile(), rac);
                        currentMapTile.AddMapEffect(Status);
                    }
                }
            }
        }
        Me.manager.audioPool.PlaySimple(OnUseAudio);
        yield return null;
    }

    public int Radius = 0;
    public RuntimeAnimatorController rac;
}
