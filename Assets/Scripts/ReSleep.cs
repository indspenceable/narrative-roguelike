﻿using System.Collections;
using UnityEngine;
using static AttackTalent;

[CreateAssetMenu(menuName = "Talents/Special/ReSleep")]
public class ReSleep : Talent
{
    public AudioClip sfx;

    public override IEnumerator StartOfTurn(IAttacker mapUnit, IAttacker Stage, Vector2Int pos, int stacks)
    {
        if (!mapUnit.ShouldWake() && mapUnit.IsAwake())
        {
            mapUnit.manager.audioPool.PlaySimple(sfx);
            mapUnit.GoToSleep();
            yield return null;
        }
    }
}