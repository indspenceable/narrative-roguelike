﻿using System.Collections;
using System.Linq;
using UnityEngine;
[CreateAssetMenu(menuName = "AI/Standard Strategy")]
public class AIStrategy : AIStrategyBase
{

    public int IsAttackScore;
    public int TargetLureMultiplier;
    public int DistanceFromNearestPlayerUnitScore;
    public int DistanceFromNearestOtherEnemyScore;
    public int ScorePerAttackPower;
    public int ScorePerMovement;
    public int HasPlanScore = 30;

    public override float ScorePlan(MapUnit currentUnit, AIActionPlan plan)
    {
        int score = 0;
        var tu = currentUnit.manager.directory.UnitAt(plan.target);

        if (plan.skill != null && !(plan.skill is WaitCommand))
            score += HasPlanScore;
        else
            score -= HasPlanScore * 10000;
        if (plan.skill is AttackTalent)
        {
            score += IsAttackScore;
            score += (plan.skill as AttackTalent).onHit.Damage * ScorePerAttackPower;
        }
        if (tu != null)
            score += tu.Lure() * TargetLureMultiplier;
        var nearestPlayerUnit = currentUnit.manager.directory.
            Query(u => u != currentUnit && u.data.Friendly == true).
            OrderBy(u => u.data.pos.ManhattanDistance(currentUnit.data.pos)).
            First();
        var distanceToNearestPlayerUnit = nearestPlayerUnit.data.pos.ManhattanDistance(currentUnit.data.pos);
        score += DistanceFromNearestPlayerUnitScore * distanceToNearestPlayerUnit;
        var nearestEnemyUnit = currentUnit.manager.directory.
            Query(u => u != currentUnit && u.data.Friendly == true).
            OrderBy(u => u.data.pos.ManhattanDistance(currentUnit.data.pos)).
            First();
        var distanceToNearestEnemyUnit = nearestEnemyUnit.data.pos.ManhattanDistance(currentUnit.data.pos);
        score += DistanceFromNearestOtherEnemyScore * distanceToNearestEnemyUnit;

        score += ScorePerMovement * (plan.path.Count - 1);
        return score;
    }
}