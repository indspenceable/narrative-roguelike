﻿using System;
using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Object Pools/Persistant")]
public class PersistantObjectPool : ObjectPoolBase<AbstractPoolable>
{
    internal AbstractPoolable Request(Vector2 vector2)
    {
        return base.Request(vector2);
    }
}
