﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Trigger Nightmare 2")]
public class TriggerNightmarePart2 : Talent
{
    public MetaProgression mp;
    public CutsceneMode cutscenes;
    public StatelessGameMode MainMenu;
    public CutsceneData LoopCutscene;
    public CutsceneData NoLoopCutscene;
    public TransitionEffect te;

    public override void OnUnitDied(MapUnit Me)
    {
        Me.manager.directory.RemoveUnitWithoutAnimation(Me);

        foreach (var friend in Me.manager.directory.Query(u => u.data.Friendly == Me.data.Friendly))
        {
            //friend.data.currentDamage += friend.MaxHP();
        }
        Debug.Log("OK, loading up nightmare 2...");

        // Remaining nightmare tings

        var friends = Me.manager.directory.Query(unit => unit.data.Friendly && unit.data.HasDeathQuote);
        var friendsWithNoNytmurSupport = friends.Where(friend => !mp.GetBool(mp.CharacterGroupKey(friend.data.CharacterName, "Nytmur")));
        if (friendsWithNoNytmurSupport.Any())
        {
            var friend = friendsWithNoNytmurSupport.Pick();
            Debug.Log("SETTING BOOL FOR " + friend);
            Debug.Log(mp.CharacterGroupKey(friend.data.CharacterName, "Nytmur"));
            mp.SetBool(mp.CharacterGroupKey(friend.data.CharacterName, "Nytmur"), true);

            Me.manager.CurrentHandler = new NoOp();
            Me.globals.WipeQuicksaveData();
            cutscenes.ActivateWithData(te, Me.manager.gm.campaign, LoopCutscene, () => MainMenu.ActivateWithoutData(te));
        } else
        {
            var campaign = Me.manager.gm.campaign;
            campaign.CurrentStageProgress += 1;
            cutscenes.ActivateWithData(te, Me.manager.gm.campaign, NoLoopCutscene, () =>
                Me.manager.gm.ActivateWithData(te, campaign)
            );
        }
    }
}