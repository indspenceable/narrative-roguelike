﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class HeartSprites :SerializableScriptableObject
{
    public List<Sprite> Sprites;
}