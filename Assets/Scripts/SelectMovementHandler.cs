﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static TBSGameManager;

public class SelectMovementHandler : InputHandler
{
    private TBSGameManager manager;
    private MapUnit actor;
    private InputHandler previous;
    private static List<AnimatorPoolable> higlights = new List<AnimatorPoolable>();
    private static List<AbstractPoolable> PossibleRangeHighlights = new List<AbstractPoolable>();

    private Dictionary<Vector2Int, List<Vector2Int>> cachedExploration = new Dictionary<Vector2Int, List<Vector2Int>>();
    private List<Vector2Int> currentPath = new List<Vector2Int>();

    public SelectMovementHandler(TBSGameManager manager, MapUnit actor, InputHandler previous)
    {
        this.manager = manager;
        this.actor = actor;
        this.previous = previous;
    }

    public void Click(int mouseButton)
    {
        if (mouseButton == 0)
        {
            var mp = manager.MousePositionToTileAddress();
            var tile = manager.map.TileAt(mp);
            var unit = manager.directory.UnitAt(mp);

            if (tile != null && cachedExploration.ContainsKey(mp))
            {
                //actor.MoveTo(mp);
                SelectActionForUnitHandler nextHandler = new SelectActionForUnitHandler(actor, currentPath, manager, this);
                manager.CurrentHandler =
                    new CoroutineExecutorHandler(manager, SelectPath(nextHandler));

                manager.audioPool.PlaySimple(manager.soundConfig.StandardBeep);
            }
        }
    }

    IEnumerator SelectPath(SelectActionForUnitHandler nextHandler)
    {
        //yield return manager.cc.FocusOn(actor);
        yield return manager.map.WalkOnPath(actor, currentPath);
        yield return manager.cc.FocusOn(actor);
        manager.CurrentHandler = nextHandler;
    }

    public void Install()
    {
        manager.hoverHint.focus = actor;
        manager.hoverHint.focusTile = manager.map.TileAt(actor.data.pos);
        ClearPathHighlights();
        cachedExploration = manager.pathing.ExploreAllPathsFromUnit(actor, actor.Movement());
        foreach (var v in cachedExploration.Keys)
        {
            // TODO FIX ANIM
            PossibleRangeHighlights.Add(manager.TightHighlightsPool.Request(v.CenterOfTile()));
        }
        currentPath = new List<Vector2Int>() { actor.data.pos };
        //manager.StartCoroutine(manager.cc.FocusOn(actor));

        actor.HealthMeter.Displayed = true;
        //manager.TileIndicator.gameObject.SetActive(true);
        //Cursor.visible = false;
    }

    public void Uninstall()
    {
        PossibleRangeHighlights.ForEach(h => h.ExternalDeactivate());
        PossibleRangeHighlights.Clear();
        ClearPathHighlights();
        ClearDangerZone();

        actor.HealthMeter.Displayed = false;
        //manager.TileIndicator.gameObject.SetActive(false);
        //Cursor.visible = true;
    }

    public static int DirectionBetween(Vector2Int a, Vector2Int b)
    {
        Vector2Int direction = (b - a);
        if (direction == Vector2Int.up) return 0;
        if (direction == Vector2Int.right) return 1;
        if (direction == Vector2Int.down) return 2;
        if (direction == Vector2Int.left) return 3;
        return -1;
    }

    private void HighlightPath()
    {
        System.Func<int, int> Reverse = (int i) =>
        {
            return (i + 2) % 4;
        };

        var _highlights = new List<AnimatorPoolable>();
        foreach (var p in currentPath)
        {
            AnimatorPoolable poolable = manager.animatorPool.RequestWithAnimator(p.CenterOfTile(), manager.PathRAC);
            poolable.Setup(manager.PathRAC);
            poolable.sr.sortingLayerName = "Default";
            _highlights.Add(poolable);
        }
        // Always?
        if (currentPath.Count == 1)
        {
            _highlights[0].anim.SetFloat("InDirection", -1);
            _highlights[0].anim.SetFloat("OutDirection", -1);
        }
        else
        {
            // OK do the first and last, then interate over the rest;
            _highlights[0].anim.SetFloat("InDirection", -1);
            _highlights[0].anim.SetFloat("OutDirection", DirectionBetween(currentPath[0], currentPath[1]));

            var li = currentPath.Count-1;
            _highlights[li].anim.SetFloat("InDirection", DirectionBetween(currentPath[li], currentPath[li - 1]));
            _highlights[li].anim.SetFloat("OutDirection", -1);

            for (int i = 1; i < currentPath.Count - 1; i += 1)
            {
                _highlights[i].anim.SetFloat("InDirection", DirectionBetween(currentPath[i], currentPath[i - 1]));
                _highlights[i].anim.SetFloat("OutDirection", DirectionBetween(currentPath[i], currentPath[i + 1]));
            }
        }

        higlights.AddRange(_highlights);
    }

    private void ClearPathHighlights()
    {

        higlights.ForEach(h => h.ExternalDeactivate());
        higlights.Clear();
    }

    Vector2Int currentCachedPointedLocation;
    public List<AnimatorPoolable> DangerZoneHighlights = null;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(1))
        {
            manager.CurrentHandler = previous;
            manager.audioPool.PlaySimple(manager.soundConfig.Cancel);
            return;
        }
        if (Input.GetKey(KeyCode.Tab))
        {
            if (DangerZoneHighlights == null)
                HighlightDangerZone();
        } else
        {
            if (DangerZoneHighlights != null)
                ClearDangerZone();
        }
        var mousePosition = manager.MousePositionToTileAddress();
        if (cachedExploration.ContainsKey(mousePosition))
        {
            if (currentCachedPointedLocation != mousePosition)
            {
                if (currentPath[currentPath.Count - 1].Adjacent(mousePosition))
                {
                    if (currentPath.Contains(mousePosition))
                    {
                        while (currentPath[currentPath.Count - 1] != mousePosition)
                            currentPath.RemoveAt(currentPath.Count - 1);
                    }
                    else
                    {
                        currentPath.Add(mousePosition);
                    }

                    if (currentPath.Count - 1 > actor.Movement())
                        currentPath = cachedExploration[mousePosition].ToList();
                }
                else
                {
                    currentPath = cachedExploration[mousePosition].ToList();
                }
                ClearPathHighlights();
                HighlightPath();
                currentCachedPointedLocation = currentPath[currentPath.Count - 1];
            }
        }
        else
        {
            ClearPathHighlights();
            currentPath.Clear();
            currentPath.Add(actor.data.pos);
            currentCachedPointedLocation = Vector2Int.one * 999;
        }


        manager.hoverHint.focus = manager.directory.UnitAt(mousePosition);
        manager.hoverHint.focusTile = manager.map.TileAt(mousePosition);
        //manager.TileIndicator.position = manager.MousePositionToTileAddress().CenterOfTile();
    }

    private void ClearDangerZone()
    {
        if (DangerZoneHighlights == null) return;
        foreach(var a in DangerZoneHighlights)
        {
            a.ExternalDeactivate();
        }
        DangerZoneHighlights = null;
    }
    private void HighlightDangerZone()
    {
        DangerZoneHighlights = new List<AnimatorPoolable>();
        foreach (var u in manager.directory.Query(u2 => !u2.data.Friendly && u2.data.Unaware))
        {
            var n = actor.Noise();
            for (int x = -n; x <= n; x += 1)
            {
                for (int y = -n; y <= n; y += 1)
                {
                    Vector2Int offset = new Vector2Int(x, y);
                    if (offset.ManhattanDistance(Vector2Int.zero) <= n)
                    {
                        DangerZoneHighlights.Add(
                            manager.animatorPool.RequestWithAnimator(
                                (u.data.pos + offset).CenterOfTile(),
                                manager.AlarmRAC));
                    }
                }
            }
        }
    }
}
