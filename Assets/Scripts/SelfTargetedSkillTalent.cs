﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static AttackTalent;
using static MapUnit;

[CreateAssetMenu(menuName = "Talents/Skills/Self Targeted")]
public class SelfTargetedSkillTalent : AbstractSelfSkillTalent
{
    public OnHitEffect onHit;

    public override IEnumerator Use(MapUnit Me)
    {
        yield return Me.BeforeUseAttack();
        yield return ExecuteAttackAll(onHit, Me, Me.data.pos, true);
    }
}
