﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISkillBase : IHasTooltip
{
    Sprite Icon();
    string CooldownIdentifier();
    int Cooldown();
    bool Available(MapUnit Me, List<Vector2Int> path);
    List<Vector2Int> PossibleTargets(MapUnit Me);
    TBSGameManager.InputHandler UseSkillHandler(MapUnit me, TBSGameManager.InputHandler previous);
    IEnumerator Use(MapUnit Me, Vector2Int target);
    string SkillName();
    int UsesPerRoom();
    bool Consumable();
    string Description();
}
