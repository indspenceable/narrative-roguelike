﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class TreasureChest : IInteractable, TBSGameManager.InputHandler
{
    [System.NonSerialized]
    private AnimatorPoolable display;
    [System.NonSerialized]
    private TBSGameManager manager;
    private Vector2Int pos;
    public Vector2Int Pos()
    {
        return pos;
    }

    private TreasureChestDefinition rac;
    private List<Talent> PrizeList;
    private bool Opened
    {
        get
        {
            return PrizeList == null || PrizeList.Count == 0 || manager.TurnNumber >= 7;
        }
    }

    public TreasureChest(Vector2Int pos, TreasureChestDefinition rac, List<Talent> prize)
    {
        this.pos = pos;
        this.rac = rac;
        this.PrizeList = prize;
    }

    public void AttachToCurrentLevel(TBSGameManager manager)
    {
        this.manager = manager;
    }

    public string GetMenuText()
    {
        return "Open treasure chest";
    }

    public Sprite InteractionSprite()
    {
        return rac.UseIcon;
    }
    public void UpdateInteractable()
    {
        if (display == null)
        {
            display = manager.animatorPool.RequestWithAnimator(pos.CenterOfTile(), rac.rac);
            display.sr.sortingLayerName = "Default";
        }

        if (!display.anim.GetBool("Open")  && manager.TurnNumber >= 7)
        {
            manager.audioPool.PlaySimple(manager.audioPool.sfx.SadChestSound);
        }
        display.anim.SetBool("Open", Opened);
        
    }

    public void OnDeactivate()
    {
        if (display != null)
        {
            display.ExternalDeactivate();
            display = null;
        }
    }

    public bool Blocks()
    {
        return true;
    }

    public void TakePrize(MapUnit me, Talent prize)
    {
        manager.StartCoroutine(TakePrizeCO(me, prize));
        PrizeList = null;
        manager.CurrentHandler = new NoOp();
    }
    
    private IEnumerator TakePrizeCO(MapUnit me, Talent prize) {
        if (me.data.inventory.Count < 4)
        {
            me.data.inventory.Add(prize);
        } else
        {
            me.manager.gm.campaign.inventory.Add(prize);
        }



        yield return Util.Hop(me.manager.animatorPool, me.audioPool, me.manager.music, prize.sprite, pos.CenterOfTile(), me.CenterOfSprite(), 1, rac.Curve);
        me.Spent = true;
        me.manager.InstallDefaultHandler();
    }
    [System.NonSerialized]
    private MapUnit actor;
    [System.NonSerialized]
    private TBSGameManager.InputHandler newPrevious;

    
    public void OnUseInteractable(MapUnit actor, TBSGameManager.InputHandler newPrevious)
    {
        this.actor = actor;
        this.newPrevious = newPrevious;
        manager.CurrentHandler = this;
    }

    public void Install()
    {
        display.anim.SetBool("Open", true);

        manager.menu.Setup(pos.CenterOfTile(), PrizeList.Select(prize => new MenuBase.MenuOption(
            prize.sprite,
            prize.TalentName,
            () => { TakePrize(actor, prize); },
            actor.data.presentation.IconBackingSmall,
            actor.data.presentation.IconBackingSmallSelected,
            true,
            null,
            (b) => actor.manager.hoverHint.FocusMaybe(prize, b)
            )).ToArray());
    }

    public void Uninstall()
    {
        manager.menu.Close();
        UpdateInteractable();
        actor.manager.hoverHint.focus = null;
        actor.manager.hoverHint.focusTile = null;
        actor.manager.hoverHint.focusTalent = null;
    }

    public void Click(int mouseButton)
    {
        if (mouseButton == 1)
        {
            display.anim.SetBool("Open", false);
            manager.CurrentHandler = newPrevious;
        }
    }

    public void Update()
    {

    }

    public bool ShowsUpInActionMenu()
    {
        return !Opened;
    }
}
