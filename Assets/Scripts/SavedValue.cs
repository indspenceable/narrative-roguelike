﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavedValue : MonoBehaviour
{
    public float DefaultValue;
    public string Key;
    public UnityEngine.UI.Slider slider;
    private bool HasBeenActivated = false;

    public void Awake()
    {
        slider.value = GetValue();
        HasBeenActivated = true;
        ResetValue();
    }
    public float GetValue()
    {
        return PlayerPrefs.GetFloat(Key, DefaultValue);
    }
    public void SetValue(float f)
    {
        if (!HasBeenActivated) return;
        PlayerPrefs.SetFloat(Key, f);
    }
    public void ResetValue()
    {
        SetValue(GetValue());
    }
}
