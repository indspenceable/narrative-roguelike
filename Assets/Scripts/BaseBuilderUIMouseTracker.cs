﻿using UnityEngine;
using UnityEngine.EventSystems;

public class BaseBuilderUIMouseTracker : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    public BaseBuilder bb;
    public GhostProp gp;
    public bool Fallthru;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
            bb.HandleClick(0);
        if (eventData.button == PointerEventData.InputButton.Right)
            bb.HandleClick(1);
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        Fallthru = true;
        gp.shown = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Fallthru = false;
        gp.shown = false;
    }

    public bool Dragging = false;
    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
            Dragging = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Dragging = false;
    }
}
