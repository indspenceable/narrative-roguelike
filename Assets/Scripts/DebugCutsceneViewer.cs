﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugCutsceneViewer : MonoBehaviour
{
    public Button b;
    public Globals g;
    public List<Button> instances = new List<Button>();
    public CutsceneMode cutscenes;
    public TransitionEffect teffect;
    public CampaignGameMode manager;
    public Transform Container;

    public void Show()
    {
        foreach (var i in instances) Destroy(i.gameObject);
        instances.Clear();

        foreach (AbstractCutsceneData cd in g.All<AbstractCutsceneData>())
        {
            var inst = Instantiate(b, Container);
            inst.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = cd.name;
            inst.onClick.RemoveAllListeners();
            inst.onClick.AddListener(() => { cutscenes.ActivateWithData(teffect, manager.campaign, cd, () => manager.ActivateWithData(teffect, manager.campaign)); });
            instances.Add(inst);
        }

        gameObject.SetActive(true);
    }
}
