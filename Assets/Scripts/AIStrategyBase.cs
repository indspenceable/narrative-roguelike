﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static AIStrategy;

public abstract class AIStrategyBase : SerializableScriptableObject {
    public float WalkSpeed = 1f;
    public bool ChangeFacingOnAttacks = true;
    // For bosses only!
    public AudioClip BossRoar;

    public virtual IEnumerator SelectAndEnactPlan(MapUnit cu)
    {
        yield return TakeUnitAction(cu, SelectPlan(cu), BossRoar);
    }

    public virtual AIActionPlan SelectPlan(MapUnit currentUnit)
    {
        TBSGameManager manager = currentUnit.manager;
        manager.cc.ControlsDisabled = true;
        Dictionary<Vector2Int, List<Vector2Int>> cachedExploration = Explore(currentUnit, manager);
        List<AIActionPlan> PossiblePlans = new List<AIActionPlan>();
        Vector2Int initialPosition = currentUnit.data.pos;
        var Keys = cachedExploration.Keys;
        foreach (var k in Keys)
        {
            currentUnit.MoveTo(k, false);
            List<ISkillBase> rawSkills = GetSkills(currentUnit);
            var skills = rawSkills.Where(skill => skill.Available(currentUnit, cachedExploration[k])).ToList();
            foreach (var skill in skills)
            {
                if (currentUnit.CurrentCooldown(skill.CooldownIdentifier()) == 0 && skill.Available(currentUnit, cachedExploration[k]))
                {
                    foreach (var target in skill.PossibleTargets(currentUnit))
                    {
                        PossiblePlans.Add(new AIActionPlan(cachedExploration[k], skill, target));
                    }
                }
            }
            PossiblePlans.Add(new AIActionPlan(cachedExploration[k], null, Vector2Int.zero));
        }
        var Plan = PossiblePlans.Shuffled().OrderByDescending(plan => ScorePlan(currentUnit, plan)).FirstOrDefault();

        currentUnit.MoveTo(initialPosition, false);
        if (Plan == null) Plan = new AIActionPlan(new List<Vector2Int>() { initialPosition }, null, Vector2Int.zero);
        return Plan;
    }

    public virtual Dictionary<Vector2Int, List<Vector2Int>> Explore(MapUnit currentUnit, TBSGameManager manager)
    {
        return manager.pathing.ExploreAllPathsFromUnit(currentUnit, currentUnit.Movement());
    }

    public virtual List<ISkillBase> GetSkills(MapUnit currentUnit)
    {
        return currentUnit.GetSkills();
    }

    public static IEnumerator TakeUnitAction(MapUnit currentUnit, AIActionPlan plan, AudioClip roar)
    {
        if (plan == null) Debug.LogError("Unable to act on a null plan!");
        TBSGameManager manager = currentUnit.manager;
        yield return manager.cc.FocusOn(currentUnit);
        yield return currentUnit.AnimateWalkOnPath(plan.path, 1f);
        yield return manager.cc.FocusOn(currentUnit);
        if (roar != null)
        {
            currentUnit.manager.audioPool.PlaySimple(roar);
            yield return new WaitForSeconds(0.25f);
        }
        if (plan.skill != null)
        {
            TooltipManager actionTitleTooltip = currentUnit.manager.ActionTitleTooltip;
            var sl = actionTitleTooltip.StaticLocation;
            actionTitleTooltip.StaticLocation = true;
            actionTitleTooltip.SetOverrideTarget(plan.skill, true);
            //actionTitleTooltip.gameObject.SetActive(true);
            yield return plan.skill.Use(currentUnit, plan.target);
            actionTitleTooltip.ClearOverrideTarget();
            actionTitleTooltip.StaticLocation = sl;

            currentUnit.SetCooldown(plan.skill.CooldownIdentifier(), plan.skill.Cooldown());
        }
        if (currentUnit != null)
            currentUnit.Spent = true;
        manager.cc.ControlsDisabled = false;
        manager.InstallDefaultHandler();
    }

    public abstract float ScorePlan(MapUnit currentUnit, AIActionPlan plan);
    
}
