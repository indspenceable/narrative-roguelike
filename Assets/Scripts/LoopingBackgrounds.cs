﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopingBackgrounds : MonoBehaviour
{
    private List<Transform> instances = new List<Transform>();
    public Vector3 ValidPositionsSize;
    public Vector3 ValidPositionsBottomLeft;
    public Vector3 movement;
    public Vector2Int bgSize;
    public GameObject prefab;
    private Dictionary<Transform, Vector3> positions = new Dictionary<Transform, Vector3>();

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(ValidPositionsBottomLeft + ValidPositionsSize / 2f, ValidPositionsSize);
    }

    public void Setup()
    {
        foreach (var i in instances) Destroy(i.gameObject);
        instances.Clear();
        positions.Clear();

        movement = new Vector3(Random.Range(-0.15f, 0.15f), Random.Range(-0.15f, 0.15f));

        for (int x = 0; x < bgSize.x; x += 1)
        {
            for (int y = 0; y < bgSize.y; y += 1)
            {
                Transform currentInstance = Instantiate(prefab, Vector3.Scale(ValidPositionsSize, new Vector3(x, y)), Quaternion.identity, transform).transform;
                instances.Add(currentInstance);
                positions[currentInstance] = currentInstance.position;
            }
        }
        Update();
        foreach (var i in instances)
        {
            i.position = i.position.Rounded();
        }
    }
    private void Update()
    {
        foreach(var i in instances)
        {
            positions[i] += movement * Time.deltaTime;

            if (positions[i].x > (ValidPositionsBottomLeft + ValidPositionsSize).x)
            {
                positions[i] -= new Vector3(ValidPositionsSize.x/2f, 0);
            }
            if (positions[i].x < (ValidPositionsBottomLeft).x)
            {
                positions[i] += new Vector3(ValidPositionsSize.x/2f, 0);
            }

            if (positions[i].y > (ValidPositionsBottomLeft + ValidPositionsSize).y)
            {
                positions[i] -= new Vector3(0, ValidPositionsSize.y/2f);
            }
            if (positions[i].y < (ValidPositionsBottomLeft).y)
            {
                positions[i] += new Vector3(0, ValidPositionsSize.y/2f);
            }

            i.position = positions[i].Rounded();
        }

    }
}
