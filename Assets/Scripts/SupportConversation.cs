﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "Cutscenes/Support Convo")]
public class SupportConversation : CutsceneData {
    [NaughtyAttributes.ReorderableList]
    public List<string> RequiredCharacters;
    public string TooltipText;
    public Sprite icon;
    public int RequiredSupportLevel = 1;
    internal Sprite sprite;
    public bool IsWithNytmur;

    public bool AddNightmareBG = false;

    public override bool MainCharacter(string characterName)
    {
        return RequiredCharacters.Contains(characterName);
    }


    public override void OnValidate()
    {
        base.OnValidate();
        if (AddNightmareBG)
        {
            this.SetupScript += "\nMUSIC nytmurtheme";
            AddNightmareBG = false;

        }
    }

#if UnityEditor
    public string PREPEND_STRING = "";
    [NaughtyAttributes.Button]
    void PREPEND_ALL()
    {
        Script = string.Join("\n", Script.Split('\n').Select(l => PREPEND_STRING + l));
        UnityEditor.EditorUtility.SetDirty(this);
    }
#endif


}


