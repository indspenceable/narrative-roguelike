﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class AssembleTeamCharacterSelectButton:MonoBehaviour {
    public Button b;
    public UnitDefinition character;
    public Animator anim;
    public AssembleTeamManager manager;
    public bool CharacterSelected;
    public Image Indicator;
    public Sprite Unselected;
    public AssembleTeamCharacterDetailsPane pane;
    public Localizer localizer;

    void Start()
    {
        if (character == null)
        {
            gameObject.SetActive(false);
            return;
        }
        anim.runtimeAnimatorController = character.Data.presentation.AnimatorController;
        //Indicator.sprite = Selected;
    }

    public void StartHover() {
        pane.StartHover(character);
    }
    public void EndHover() {
        pane.EndHover();
    }


    private void Update()
    {
        // Can we interact with this button? as lnog as there's room, or we've already selected this character
        var chars = manager.SelectedCharacters();
        b.interactable = (chars.Count() < 4 || chars.Contains(this));

        Indicator.enabled = CharacterSelected;
    }
    public void Toggle()
    {
        CharacterSelected = !CharacterSelected;
        if (CharacterSelected)
        {
            StartCoroutine(ShowText());
            anim.Play(CharacterSelected ? "Attack" : "Idle");
        }
    }

    private IEnumerator ShowText()
    {
        UnitData cd = character.Data;
        CharacterAnimationData presentataion = cd.presentation;
        pane.StartFocus(character);

        Persona persona = presentataion.persona;
        var Portrait = persona.DefaultPortrait;
        string LocalizedContent = localizer._($"characters.{cd.Identifier}.join_line");

        bool sw = LocalizedContent.StartsWith("<");
        if (sw)
        {
            var portraitMatch = Regex.Match(LocalizedContent, @"^\<([a-zA-Z]*)\>(.*)");
            string portraitNameString = portraitMatch.Groups[1].Value;
            if (portraitMatch.Groups.Count > 1 && persona.HasSpecialPortrait(portraitNameString))
            {
                Portrait = persona.GetPortrait(portraitNameString);
                LocalizedContent = portraitMatch.Groups[2].Value.Trim();
            }
        }

        yield return manager.textbox.DisplayCo(LocalizedContent, cd.Identifier, Portrait, persona);
        pane.EndFocus();
    }
}
