﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class CharacterStatsPane : MonoBehaviour
{
    public GameObject container;
    public GameObject SkillContainer;
    public TMPro.TextMeshProUGUI SkillDescription;
    //public Image portrait;
    public TMPro.TextMeshProUGUI Name;
    public TMPro.TextMeshProUGUI HP;
    public List<IconButton> buttons;

    public List<ManagePartyIconButton> intrinsics;
    public Sprite DefaultBacking;
    public ManagePartyGameModeInstance manager;


    private void Start()
    {
        foreach (var i in intrinsics)
            i.anim.SetBool("Hidden", true);
        container.SetActive(false);
        SkillContainer.SetActive(false);
    }

    private static Action NullCallback = () => { };


    internal void Setup(UnitData data)
    {
        Name.text = data.CharacterName;
        HP.text = $"HP: {data.FormatUnalteredHP()}";
        //portrait.sprite = data.presentation.portrait;
        //for (int i = 0; i < 4; i += 1)
        //{
        //    if (data.inventory.Count > i)
        //    {
        //        buttons[i].button.interactable = true;
        //        buttons[i].icon.sprite = data.inventory[i].sprite;
        //        buttons[i].icon.enabled = true;
        //    }
        //    else
        //    {
        //        buttons[i].icon.enabled = false;
        //        buttons[i].button.interactable = false;
        //    }
        //}
        float delay = 0f;
        for (int i = 0; i < intrinsics.Count; i += 1)
        {
            delay += Random.Range(0f, 0.25f);
            if (data.IntrinsicTalents.Count <= i)
            {
                intrinsics[i].anim.SetBool("Hidden", true);
            }
            else
            {
                intrinsics[i].Setup(data.presentation.IconBackingLarge, data.IntrinsicTalents[i], delay, NullCallback);

            }
        }
        int SpentSlots = data.IntrinsicTalents.Count;
        for (int InventoryIndex = 0; InventoryIndex < data.inventory.Count; InventoryIndex += 1)
        {
            int currentButtonSlot = InventoryIndex + SpentSlots;
            delay += Random.Range(0f, 0.25f);
            if (data.IntrinsicTalents.Count <= InventoryIndex)
            {
                intrinsics[currentButtonSlot].anim.SetBool("Hidden", true);
            }
            else
            {
                // Quirk of lambdas. ugh.
                int currentIndex = InventoryIndex;
                intrinsics[currentButtonSlot].Setup(data.presentation.StickyNotes.Pick(), data.inventory[InventoryIndex], delay, () => manager.SendItemToConvoy(currentIndex));

            }
        }
        container.gameObject.SetActive(true);
    }

    public void ShowSkillDetailPane(ManagePartyIconButton button)
    {
        var talent = button.talent;
        if (talent != null)
            ShowTalentDetail(talent);
    }
    public void ShowItemDetailPane(int index)
    {
        var talent = manager.gm.campaign.inventory.Get(index+manager.currentInventoryOffset);
        if (talent != null)
            ShowTalentDetail(talent);
    }

    private void ShowTalentDetail(Talent talent)
    {
        SkillDescription.text = $"{talent.TalentName}\n{talent.Description}\n\n\"{talent.FlavorText}\"";
        SkillContainer.gameObject.SetActive(true);
    }

    public void HideSkillDetailPane()
    {
        SkillDescription.text = "";
        SkillContainer.gameObject.SetActive(false);
    }

    private void Update()
    {

    }
}