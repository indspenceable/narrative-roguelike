﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class TextEffect : ScriptableObject
{
    internal abstract void Process(int vertexIndex, int characterIndex, List<Vector3> vertices);
}
