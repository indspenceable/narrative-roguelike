﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Skills/Hit Random+Valid In Range")]
public class HitRandomInRangeAttack : AttackTalent
{
    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        return ExecuteAttack(onHit, Me, PossibleTargets(Me).Pick(), true);
    }
}