﻿using System;
using UnityEngine;

[CreateAssetMenu(menuName = "Object Pools/HeartPool")]
public class HeartMeterPool : ObjectPoolBase<HeartMeterPoolable>
{
    internal HeartMeterPoolable RequestHeart(Vector3 zero)
    {
        return Request(zero);
    }
}