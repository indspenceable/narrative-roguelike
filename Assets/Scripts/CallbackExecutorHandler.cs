﻿using System;
using System.Collections;


public class CoroutineExecutorHandler : TBSGameManager.InputHandler
{
    TBSGameManager manager; IEnumerator enumerator;

    public CoroutineExecutorHandler(TBSGameManager manager, IEnumerator enumerator)
    {
        this.manager = manager;
        this.enumerator = enumerator;
    }

    public void Click(int mouseButton)
    {
        
    }

    public void Install()
    {
        manager.StartCoroutine(enumerator); 
    }

    public void Uninstall()
    {
        
    }

    public void Update()
    {
       
    }
}
public class CallbackExecutorHandler : TBSGameManager.InputHandler
{
    private Action callback;

    public CallbackExecutorHandler(Action callback)
    {
        this.callback = callback;
    }

    public void Click(int mouseButton)
    {
    }

    public void Install()
    {
        callback();
    }

    public void Uninstall() {
    }

    public void Update()
    {
    }
}