﻿using System.Collections;
using UnityEngine;
using static AttackTalent;

[CreateAssetMenu(menuName = "Talents/Buffs/Trigger On Start Of Map")]
public class TriggerAttackOnStartOfMap : Talent
{
    public OnHitEffect onHit;
    public override IEnumerator StartOfMap(IAttacker mapUnit)
    {
        yield return ExecuteAttackAll(onHit, mapUnit, mapUnit.Pos(), true);
    }
}