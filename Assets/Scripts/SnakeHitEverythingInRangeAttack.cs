﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Snake/Hit Everything In Range")]
public class SnakeHitEverythingInRangeAttack : HitEverythingInRangeAttack
{
    public override List<Vector2Int> PossibleTargets(MapUnit Me)
    {
        List<Vector2Int> rtn = ValidTargetLocationsWithinRange(Me).Distinct().ToList();
        return rtn.Distinct().Where(v2i => ValidTargetableTileContents(Me, v2i)).ToList();
    }
    private List<Vector2Int> ValidTargetLocationsWithinRange(MapUnit Me)
    {
        var rtn = new List<Vector2Int>();
        for (int x = -MaxRange; x <= MaxRange; x += 1)
        {
            for (int y = -MaxRange; y <= MaxRange; y += 1)
            {
                var t = Me.data.pos;
                //{
                    Vector2Int offset = new Vector2Int(x, y);
                    if (offset.Manhattan() <= MaxRange &&
                        offset.Manhattan() >= MinRange)

                        rtn.Add(t + offset);
                //}
            }
        }

        return rtn;
    }
}