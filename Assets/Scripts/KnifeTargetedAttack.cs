﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Skills/KnifeTargeted")]
public class KnifeTargetedAttack : AttackTalent
{
    public override List<Vector2Int> PossibleTargets(MapUnit Me)
    {
        return Me.data.KnifeLocations.Distinct().Where(v2i => ValidTargetableTileContents(Me, v2i)).ToList();
    }
    public override bool ValidTargetableTileContents(MapUnit Me, Vector2Int v2i)
    {
        return base.ValidTargetableTileContents(Me, v2i) && Me.data.KnifeLocations.Contains(v2i);
    }
}
