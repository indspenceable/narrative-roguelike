﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public partial class MapTile
{
    [NonSerialized]
    public MapManager manager;
    public Vector2Int pos;
    public List<Vector2Int> Neighbors = new List<Vector2Int>();
    public List<BuffInstance> currentBuffs = new List<BuffInstance>();
    public List<Talent> Intrinsics = new List<Talent>();

    public MapTile(MapManager manager, Vector2Int pos)
    {
        this.manager = manager;
        this.pos = pos;
        this.currentBuffs = new List<BuffInstance>();
    }


    public bool Blocked(UnitData.MobilityType movementType)
    {
        return !MovementAllowed(movementType);
    }
    public bool MovementAllowed(UnitData.MobilityType movementType) {
        if (movementType == UnitData.MobilityType.LAND) 
            return !Walled() && !Water() && !SpecialBlock() && !BuffBlocks();
        if (movementType == UnitData.MobilityType.WATER)
            return !Walled() && Water() && !SpecialBlock() && !BuffBlocks();
        else return !SpecialBlock();
    }
    public float Radius { get { return 0.25f; } }
    public bool Walled() => Physics2D.OverlapCircle(pos.CenterOfTile(), Radius, manager.GeometryLayer);
    public bool Water() => Physics2D.OverlapCircle(pos.CenterOfTile(), Radius, manager.WaterLayer);
    public bool SpecialBlock()
    {
        var inter = manager.manager.interactables.InteractionsAt(pos);
        if (inter != null) return inter.Blocks();
        return false;
    }
    public bool BuffBlocks()
    {
        if (currentBuffs.Any(cb => cb.buff.BlocksMovement)) return true;
        return false;
    }
    public void AddMapEffect(BuffInstance newInstance)
    {
        var existingStacks = currentBuffs.Find(b => b.buff == newInstance.buff);
        if (existingStacks == null)
        {
            currentBuffs.Add(manager.globals.Duplicate(newInstance));
        }
        else
        {
            switch (newInstance.buff.StackingMethod)
            {
                case Talent.BuffStackingMethod.NO_STACKING:
                    currentBuffs.Add(manager.globals.Duplicate(newInstance));
                    break;
                case Talent.BuffStackingMethod.EXTEND_DURATION:
                    existingStacks.stacks += newInstance.stacks;
                    break;
                case Talent.BuffStackingMethod.GREATER_DURATION:
                    existingStacks.stacks = Mathf.Max(existingStacks.stacks, newInstance.stacks);
                    break;
                case Talent.BuffStackingMethod.IGNORE_NEW:
                    break;
            }
        }

        UpdateDecorator();
    }

    internal void ClearMapEffect(Talent knifeMapEffect)
    {
        currentBuffs.RemoveAll(bi => bi.buff == knifeMapEffect);
        UpdateDecorator();
    }

    public IEnumerable<Talent> AllCurrentEffects()
    {
        return Intrinsics.Concat(currentBuffs.Select(cb => cb.buff)).Where(cb => cb != null);
    }

    //public IEnumerable<KeyValuePair<Talent, int?>> AllCurrentEffectsWithStacks()
    //{
    //    return Intrinsics.Concat(currentBuffs.Where(cb => cb.buff != null).Select(cb => new KeyValuePair<Talent, int?>(cb.buff, cb.stacks));
    //}

    [NonSerialized]
    private Dictionary<Talent, AbstractPoolable> _poolables = new Dictionary<Talent, AbstractPoolable>();

    public void UpdateDecorator()
    {
        if (_poolables == null)
            _poolables = new Dictionary<Talent, AbstractPoolable>();

        var currentTalents = AllCurrentEffects();
        foreach (var e in currentTalents)
        {
            if (e.MapDecorator != null && !_poolables.ContainsKey(e))
                _poolables[e] = e.MapDecorator.Request(pos.CenterOfTile());
        }
        foreach(var k in _poolables.Keys.Where(_k => !currentTalents.Contains(_k)).ToList())
        {
            _poolables[k].ExternalDeactivate();
            _poolables.Remove(k);
        }
    }

    public void CleanupEverything()
    {
        foreach (var p in _poolables.Values) p.ExternalDeactivate();
        _poolables.Clear();
    }
}