﻿using System;

[System.Serializable]
public class BuffInstance
{
    public Talent buff;
    public int stacks;

    public BuffInstance(Talent buff, int stacks)
    {
        this.buff = buff;
        this.stacks = stacks;
    }

    internal int? ShownMapStacks(Talent buff)
    {
        if (buff.TicksDownOccupied || buff.TicksDownOccupied) return this.stacks;
        return null;
    }
}
