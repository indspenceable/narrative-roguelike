﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Text Effects/FreeMove")]
public class FreeMoveText : TextEffect
{
    public float HorizontalMovementDistanceScaler = 1f;
    public float VerticalMovementDistanceScaler = 1f;
    public float LetterScaler = 1f;
    public float TimeScaler = 10f;
    public AnimationCurve HorizontalLetterMovementCurve = AnimationCurve.Linear(0, 0, 1, 1);
    public AnimationCurve VerticalLetterMovementCurve = AnimationCurve.Linear(0, 0, 1, 1);

    internal override void Process(int vertexIndex, int characterIndex, List<Vector3> vertices)
    {
        var t = (characterIndex * LetterScaler) + (Time.unscaledTime * TimeScaler);
        Vector3 VerticalMovement = Vector3.up * VerticalLetterMovementCurve.Evaluate(t - (int)t) * VerticalMovementDistanceScaler;
        Vector3 HorizontalMovement = Vector3.right * HorizontalLetterMovementCurve.Evaluate(t - (int)t) * HorizontalMovementDistanceScaler;
        vertices[vertexIndex] += VerticalMovement + HorizontalMovement;
        vertices[vertexIndex + 1] += VerticalMovement + HorizontalMovement;
        vertices[vertexIndex + 2] += VerticalMovement + HorizontalMovement;
        vertices[vertexIndex + 3] += VerticalMovement + HorizontalMovement;

        vertices[vertexIndex] = vertices[vertexIndex].Rounded();
        vertices[vertexIndex+1] = vertices[vertexIndex+1].Rounded();
        vertices[vertexIndex+2] = vertices[vertexIndex+2].Rounded();
        vertices[vertexIndex+3] = vertices[vertexIndex+3].Rounded();
    }
}