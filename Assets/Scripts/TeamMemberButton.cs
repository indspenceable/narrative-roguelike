﻿using UnityEngine;
using UnityEngine.UI;

public class TeamMemberButton : MonoBehaviour
{
    public UnitDefinition c;
    public Selectable s;
    public Image CharacterImage;
    public Image FrameImage;
}