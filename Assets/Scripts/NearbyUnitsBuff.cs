﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Buffs/Nearby Units Based")]
public class NearbyUnitsBuff : BuffDefinition
{
    public bool FriendlyUnits;
    public bool EnemyUnits;
    public int MaxManhattanDistance = 1;
    public int DefaultCount = -1;

    public RuntimeAnimatorController EffectOnMe;
    public AudioClip SFXForEachTarget;
    public AudioClip SFXOnMe;

    public override int Modifier(string name, MapUnit u)
    {
        return base.Modifier(name, u) * Mathf.Clamp(CountNearbyUnits(u) + DefaultCount, MinCount, MaxCount);
    }
    private Vector2Int[] Directions = new Vector2Int[] { Vector2Int.left, Vector2Int.right, Vector2Int.up, Vector2Int.down };
    public int MinCount = 0;
    public int MaxCount = 100;


    public List<MapUnit> GatherUnits(MapUnit u)
    {
        List<MapUnit> rtn = new List<MapUnit>();
        for (int x = -MaxManhattanDistance; x <= MaxManhattanDistance; x += 1)
        {
            for (int y = -MaxManhattanDistance; y <= MaxManhattanDistance; y += 1)
            {
                var xy = new Vector2Int(x, y);
                if (xy.Manhattan() <= MaxManhattanDistance)
                {
                    var o = u.manager.directory.UnitAt(u.data.pos + xy);
                    if (o != null)
                    {
                        bool targetsThisUnit = o.data.Friendly == u.data.Friendly ? FriendlyUnits : EnemyUnits;
                        if (targetsThisUnit && !rtn.Contains(o) && o != u)
                        {
                            rtn.Add(o);
                        }
                    }
                }
            }
        }
        return rtn;
    }

    public int CountNearbyUnits(MapUnit u)
    {
        return GatherUnits(u).Count();

    }

    public override IEnumerator BeforeAttack(MapUnit me)
    {
        if (EffectOnMe != null || this.SFXOnMe != null)
        {
            List<MapUnit> nearbyUnits = GatherUnits(me);
            if (nearbyUnits.Any())
            {
                foreach (var u2 in nearbyUnits)
                {
                    if (EffectOnMe != null)
                    {
                        me.manager.bouncyTextPool.RequestWithString(me.manager.cameraBlit.GetComponent<Camera>(), u2.CenterOfSprite(), "!!", Color.red, Color.white);
                    }
                    if (this.SFXForEachTarget != null)
                    {
                        me.audioPool.PlaySimple(SFXForEachTarget);
                    }
                    yield return new WaitForSeconds(0.25f);
                }
                yield return new WaitForSeconds(0.25f);
                me.manager.animatorPool.RequestWithAnimator(me.CenterOfSprite(), EffectOnMe);
                me.manager.audioPool.PlaySimple(SFXOnMe);
                yield return new WaitForSeconds(0.25f);
            }
        }
    }

    //private int CountForUnit(MapUnit Me, MapUnit target)
    //{
    //    if (target == null) return 0;
    //    if (target == Me) return 0;
    //    if (target.data.Friendly == Me.data.Friendly)
    //    {
    //        return CountPerFriendly;
    //    } else
    //    {
    //        return CountPerHostile;
    //    }
    //}
}
