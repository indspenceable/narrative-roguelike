﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
public class InteractableManager : MonoBehaviour
{
    public TBSGameManager manager;
    public MapManager map;
    public UnitDirectory units;
    public List<IInteractable> interactableList = new List<IInteractable>();
    //public int interactableCount;


    public TreasureChestDefinition TreasureChestAnimations;

    internal void BuildInteractables(StageData currentStage)
    {
        interactableList.Clear();
        for (int i = 0; i < 1; i += 1) {
            var location = SelectAvailableLocation();
            interactableList.Add(new TreasureChest(location,
                currentStage.TreasureChestDefinition, 
                manager.globals.All<Talent>().Where(t => t.CanBeAwardedAsPrize).Shuffled().Take(3).ToList()
            ));
        }
        interactableList.AddRange(manager.LevelInstance.Interactables.Select(ii => ii.GetInteractable()));
    }

    public void InitializeInteractables()
    {
        foreach(var i in interactableList)
        {
            i.AttachToCurrentLevel(manager);
        }
    }

    internal IInteractable InteractionsAt(Vector2Int a)
    {
        return interactableList.FirstOrDefault(ii => ii. Pos() == a);
    }
    internal IInteractable AvailableInteractionsAt(Vector2Int a)
    {
        return interactableList.FirstOrDefault(ii => ii.Pos() == a && ii.ShowsUpInActionMenu());
    }

    public void UpdateInteractables()
    {
        foreach(var ii in interactableList)
        {
            ii.UpdateInteractable();
        }
    }

    private Vector2Int SelectAvailableLocation()
    {
        int tries = 0;
        while (tries < 100)
        {
            var mt = map.Tiles.Pick();
            if (mt.MovementAllowed(UnitData.MobilityType.LAND) && units.UnitAt(mt.pos) == null)
            {
                return mt.pos;
            }
            tries += 1;
        }
        return Vector2Int.one;
    }

    internal void Deactivate()
    {
        foreach(var i in interactableList)
        {
            i.OnDeactivate();
        }
    }
}
