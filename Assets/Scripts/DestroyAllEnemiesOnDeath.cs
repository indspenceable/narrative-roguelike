﻿
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Boss Destroy Friends on Death")]
public class DestroyAllEnemiesOnDeath : Talent
{
    public override void OnUnitDied(MapUnit ME)
    {
        foreach (var friend in ME.manager.directory.Query(u => u.data.Friendly == ME.data.Friendly))
        {
            Debug.Log("Killing allied unit due to DestroyEnemiesOnDeathTalent");
            friend.data.currentDamage += friend.MaxHP();
        }
    }
}
