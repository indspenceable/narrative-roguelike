﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class CutsceneDisplayInstance : GameModeInstance
{
    public CutsceneMode mode;
    public TextboxController textbox;
    public Globals g;
    public Persona DefaultPersona;
    public MusicManager music;
    public Localizer localizer;

    public AudioPool audioPool;
    public AnimatorPool animatorPool;
    public Image FadeOverlay;
    public Canvas UIParent;

    private Dictionary<string, Animator> Characters = new Dictionary<string, Animator>();

    private Dictionary<string, GameObject> Props = new Dictionary<string, GameObject>();
    public AnimationCurve JumpArc;

    private List<AbstractPoolable> AudioToClean = new List<AbstractPoolable>();
    void MarkForCleanup(AbstractPoolable poolable)
    {
        AudioToClean.Add(poolable);
    }

    public abstract class Instruction
    {
        public abstract IEnumerator Execute(CutsceneDisplayInstance cds);
    }
    public InstructionRegistry registry;
    public Animator UnitPrefab;

    public class InstructionRegistry
    {
        private Dictionary<string, Func<CutsceneDisplayInstance, string, Instruction>> registry;
        public InstructionRegistry()
        {
            registry = new Dictionary<string, Func<CutsceneDisplayInstance, string, Instruction>>();
            registry["SAY"] = SayInstruction.Build;
            registry["SAYRAW"] = SayInstruction.Raw;
            registry["SAY_RAW"] = SayInstruction.Raw;
            registry["WAIT"] = WaitInstruction.Build;
            registry["CHAR"] = SpawnCharacterInstruction.Build;
            registry["DECHAR"] = RemoveCharacterInstruction.Build;
            registry["SPAWN"] = SpawnPropInstruction.Build;
            registry["SPAWN_UI"] = SpawnPropInstruction.BuildUI;
            registry["PLAY"] = PlayAnimationInstruction.Build;
            registry["MOVE"] = MoveInstruction.Build;
            registry["MOVEF"] = MoveInstruction.MaintainFacing;

            registry["MOVE_PROP"] = MovePropInstruction.Build;

            registry["FACE"] = SetFacingInstruction.Build;
            registry["TELE"] = MoveInstantInstruction.Build;
            registry["SOUND"] = PlaySoundInstruction.Build;
            registry["LOOP"] = PlaySoundInstruction.Loop;
            registry["MUSIC"] = PlayMusicInstruction.Build;
            registry["MUSIC_ONCE"] = PlayMusicInstruction.BuildOnce;
            registry["EFFECT"] = PlayEffectInstruction.Build;
            registry["CLEAR"] = ClearPropInstruction.Build;
            registry["FADE"] = FadeInstruction.Build;
            registry["FADEW"] = FadeInstruction.BuildFadeWhite;
            registry["SEPIA"] = SepiaInstruction.Build;
            registry["SHUFFLE"] = ReshuffleRandomCharacterOrderInstruction.Build;
            registry["SHAKE"] = ShakeInstruction.Build;
            registry["MOVEC"] = MoveCameraInstruction.Build;

            registry["CREDIT_TEXT"] = CreditText.Build;
            //registry["SET_SPEED"] = SetTimescaleInstruction.Build;
        }

        internal bool ContainsKey(string v)
        {
            return registry.ContainsKey(v);
        }

        public Func<CutsceneDisplayInstance, string, Instruction> Get(string v)
        {
            return registry[v];
        }
    }
    private class SayInstruction : Instruction
    {
        private Persona persona;
        private string content;
        public bool Localized;
        private Sprite Portrait;
        private string Identifier;

        public SayInstruction(CutsceneDisplayInstance cds, string line, bool Localized)
        {
            var split = line.Split(new char[] { ':' });
            var ContentToParse = line.Substring(split[0].Length + 1);
            Identifier = split[0];
            Portrait = null;

            var match = Regex.Match(Identifier, @"(\w*)\< ?(\w*)\>");
            if (match.Groups.Count > 1)
            {
                Identifier = match.Groups[1].Value;
            }


            var persona = cds.g.FindById<Persona>(Identifier, true);
            if (persona == null)
            {
                //Debug.LogError("Unable to find persona with identifier: [" + Identifier + "].");
                persona = cds.g.FindById<Persona>("Narrator");
            }
            this.persona = persona;
            this.content = ContentToParse.Trim();
            this.Localized = Localized;

            string LocalizedContent = Localized ? cds.localizer._(ContentToParse) : ContentToParse;

            if (match.Groups.Count > 1)
            {
                Portrait = persona.GetPortrait(match.Groups[2].Value);
            } else
            {
                bool sw = LocalizedContent.StartsWith("<");
                if (sw)
                {
                    var portraitMatch = Regex.Match(LocalizedContent, @"^\<([a-zA-Z]*)\>(.*)");
                    string portraitNameString = portraitMatch.Groups[1].Value;
                    if (portraitMatch.Groups.Count > 1 && persona.HasSpecialPortrait(portraitNameString))
                    {
                        Portrait = persona.GetPortrait(portraitNameString);
                        this.Localized = false;
                        content = portraitMatch.Groups[2].Value.Trim();
                    }
                }
            }
        }

        public static Instruction Build(CutsceneDisplayInstance cds, string s)
        {
            return new SayInstruction(cds, s, true);
        }
        public static Instruction Raw(CutsceneDisplayInstance cds, string s)
        {
            return new SayInstruction(cds, s, false);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            string s = Localized ? cds.localizer._(content) : content;
            if (persona == null) persona = cds.textbox.defaultPersona;
            yield return cds.textbox.DisplayCo(s, Identifier, Portrait, persona);
        }
    }

    public override void PreActivate(object DeserializedData)
    {
        base.PreActivate(DeserializedData);
        registry = new InstructionRegistry();

        //Debug.Log("Preactivate - calling w/ SetupScript");
        this.setupComplete = false;
        mode.GetCoordinator().StartCoroutine(ExecuteCutscene(mode.cd.GetSetupScript(), mode.cd, () => this.setupComplete = true));
    }

    public override void PostActivate()
    {
        //Debug.Log("postactivate - calling w/ Script");
        StartCoroutine(PlayMainScene());
    }
    public IEnumerator PlayMainScene()
    {
        while (!setupComplete) yield return null;
        yield return ExecuteCutscene(mode.cd.GetScript(), mode.cd, mode.cb);
    }

    public override void PostDeactivate()
    {
        base.PostDeactivate();
        foreach (var key in Characters.Keys.ToList())
        {
            Destroy(Characters[key].gameObject);
        }
        Characters.Clear();
        foreach (var kv in Props)
            Destroy(kv.Value);
        Props.Clear();
        foreach (var tc in AudioToClean)
        {
            tc.ExternalDeactivate();
        }
        AudioToClean.Clear();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StopAllCoroutines();
            mode.cb();
        }
    }
    public interface ICutsceneTeamInfo
    {
        bool MainCharacter(string characterName);
    }

    private Dictionary<string, string> DynamicStrings = null;
    public CameraShake cameraShake;
    public Transform camerContainer;
    private bool setupComplete;
    public Transform MovableTextBubbleTransform;
    public TMPro.TextMeshProUGUI MovableTextBubbleText;
    public Transform BubbleOffScreenPos;
    public Transform BubbleOnScreenPos;

    private IEnumerator ExecuteCutscene(string l, ICutsceneTeamInfo info, System.Action cb)
    {
        DynamicStrings = null;
        if (l.Length > 0)
            foreach (var rawline in l.Split(new char[] { '\n' }))
            {

                var line = rawline.Trim();
                if (line == "") continue;
                if (line.StartsWith("#")) continue;


                // If a line is in parens, then its a narrator line.
                var NarratorMatch = Regex.Match(line, @"^\((.*)\)");
                //if (NarratorMatch.Success) line = $"SAY_RAW Narrator:{NarratorMatch.Groups[0].Value}";
                if (NarratorMatch.Success) continue;
                // if a line is in brackets, its a narrator line
                NarratorMatch = Regex.Match(line, @"^\[(.*)\]");
                if (NarratorMatch.Success) line = $"SAY_RAW Narrator:{NarratorMatch.Groups[0].Value}";

                var split = line.Split(new char[] { ' ' }, 2).Where(l_ => l_ != "").ToList();
                if (split.Count < 2)
                {
                    Debug.Log("No Command.");
                    continue;
                }
                // if the line ends in *, its run in the bg.
                bool bg = false;
                if (split[0].EndsWith("*"))
                {
                    bg = true;
                    split[0] = split[0].Substring(0, split[0].Length - 1);
                }


                // Sub in any variables to this line.
                split[1] = Regex.Replace(split[1], @"\$(\w+)", new MatchEvaluator(match =>
                {
                    var key = match.Groups[0].Value;
                    if (DynamicStrings == null) SetupDynamicStrings(info);
                    if (DynamicStrings.ContainsKey(key))
                    {
                        return DynamicStrings[key];
                    }
                    return "";
                }));


                if (!registry.ContainsKey(split[0])) Debug.LogError($"Unable To find Command <{split[0]}> in line <{line}>");
                else
                {
                    //Debug.Log("EXECUTING LINE: " + line);
                    var ienum = registry.Get(split[0])(this, split[1]).Execute(this);
                    if (bg)
                    {
                        StartCoroutine(ienum);
                        yield return null;
                    }
                    else
                    {
                        yield return ienum;
                    }
                }
            }

        yield return null;
        cb();
    }

    private void SetupDynamicStrings(ICutsceneTeamInfo info)
    {
        var shuffled = mode.campaign.team.Shuffled();
        var ocs = g.All<UnitDefinition>().Where(ud => ud.SelectableCharacter && !shuffled.Any(uda => ud.Data.Identifier == uda.Identifier)).ToList();
        var friends = mode.campaign.team.Where(c => !info.MainCharacter(c.CharacterName)).Shuffled();
        //Dictionary<string, string>
        DynamicStrings = new Dictionary<string, string>()
        {
            { "$p1", mode.campaign.team[0].Identifier },
            { "$p2", mode.campaign.team[1].Identifier },
            { "$p3", mode.campaign.team[2].Identifier },
            { "$p4", mode.campaign.team[3].Identifier },
            { "$r1", shuffled[0].Identifier },
            { "$r2", shuffled[1].Identifier },
            { "$r3", shuffled[2].Identifier },
            { "$r4", shuffled[3].Identifier },
            { "$o1", ocs[0].Identifier },
            { "$o2", ocs[1].Identifier },
            { "$o3", ocs[2].Identifier },
            { "$o4", ocs[3].Identifier },
            { "$o5", ocs[4].Identifier },
            { "$o6", ocs[5].Identifier },
            { "$food_judge", ocs.Where(c =>c.Data.CharacterName != "Kion").First().Identifier },
            { "$f1", friends[0].Identifier },
            { "$f2", friends[1].Identifier },
        };
    }

    private void SpawnCharacter(string characterName, string x, string y, string variableName)
    {
        if (!g.All<CharacterAnimationData>().Any(cad => cad.Identifier == characterName))
        {
            Debug.LogError($"Unable to spawn a character with ident: {characterName}");
        }
        Characters[variableName] = Instantiate(UnitPrefab, new Vector3(float.Parse(x) + 0.5f, float.Parse(y) + 0.5f), Quaternion.identity, transform);
        RuntimeAnimatorController currentAnimatorController = g.All<CharacterAnimationData>().First(cad => cad.Identifier == characterName).AnimatorController;
        Characters[variableName].runtimeAnimatorController = currentAnimatorController;
        Characters[variableName].gameObject.SetActive(true);
    }
    private void DespawnCharacter(string variableName)
    {
        if (!Characters.ContainsKey(variableName))
        {
            Debug.LogError($"Attempting to remove a non-existing character! {variableName}");
        }
        Destroy(Characters[variableName].gameObject);
        Characters.Remove(variableName);
    }

    private class WaitInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;

        public WaitInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            this.content = content;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new WaitInstruction(cdi, content);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            yield return new WaitForSeconds(float.Parse(content));
        }
    }
    private class SpawnPropInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;
        private bool IsUI;

        public SpawnPropInstruction(CutsceneDisplayInstance cdi, string content, bool IsUI)
        {
            this.cdi = cdi;
            this.content = content;
            this.IsUI = IsUI;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new SpawnPropInstruction(cdi, content, false);
        }

        internal static Instruction BuildUI(CutsceneDisplayInstance cdi, string content)
        {
            return new SpawnPropInstruction(cdi, content, true);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            var PropName = split[0];
            var x = split[1];
            var y = split[2];
            var VariableName = split[3];
            cds.SpawnProp(PropName, x, y, VariableName, IsUI);
            yield return null;
        }
    }
    private class ClearPropInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;

        public ClearPropInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            this.content = content;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new ClearPropInstruction(cdi, content);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            var VariableName = split[0];
            cds.ClearProp(VariableName);
            yield return null;
        }
    }
    private class ShakeInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;

        public ShakeInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            this.content = content;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new ShakeInstruction(cdi, content);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            var duration = int.Parse(split[0]);
            yield return cds.cameraShake.ShakeCO((CameraShake.Magnitude)duration);
        }
    }

    private class FadeInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;
        private Color color;

        public FadeInstruction(CutsceneDisplayInstance cdi, string content, Color c)
        {
            this.cdi = cdi;
            this.content = content;
            this.color = c;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new FadeInstruction(cdi, content, Color.black);
        }
        internal static Instruction BuildFadeWhite(CutsceneDisplayInstance cdi, string content)
        {
            return new FadeInstruction(cdi, content, Color.white);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            var PCT = float.Parse(split[0]);
            var Duration = float.Parse(split[1]);
            float dt = 0f;
            var curve = AnimationCurve.EaseInOut(0, 1f - PCT, 1, PCT);

            while (dt <= Duration)
            {
                yield return null;
                dt += Time.deltaTime;
                cdi.FadeOverlay.color = Color.Lerp(this.color, Color.clear, curve.Evaluate(dt / (Duration + 0.00001f)));
            }
            yield return null;
        }
    }

    public class FadeWhiteInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;

        public virtual Color GetColor()
        {
            return Color.white;
        }

        public FadeWhiteInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            this.content = content;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new FadeWhiteInstruction(cdi, content);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            var PCT = float.Parse(split[0]);
            var Duration = float.Parse(split[1]);
            float dt = 0f;
            var curve = AnimationCurve.EaseInOut(0, 1f - PCT, 1, PCT);

            while (dt <= Duration)
            {
                yield return null;
                dt += Time.deltaTime;
                cdi.FadeOverlay.color = Color.Lerp(GetColor(), Color.clear, curve.Evaluate(dt / (Duration + 0.00001f)));
            }
            yield return null;
        }
    }

    private class SepiaInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;

        public SepiaInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            this.content = content;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new SepiaInstruction(cdi, content);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            cds.cameraBlit.TransitionMaterial.SetFloat("_Sepia", float.Parse(content));
            if (false) yield return null;
        }
    }

    private void SpawnProp(string propName, string x, string y, string VariableName, bool isUI)
    {
        var pd = g.FindById<CutscenePropDefinition>(propName);

        Transform parent = transform;
        if (isUI) parent = UIParent.transform;

        var prop = Instantiate(pd.prefab, new Vector3(float.Parse(x), float.Parse(y)), Quaternion.identity, parent);
        
        if (pd.Message != null && pd.Message != "") prop.SendMessage(pd.Message);
        Props.Add(VariableName, prop);
    }
    public void ClearProp(string VariableName)
    {
        Destroy(Props[VariableName]);
        Props.Remove(VariableName);
    }

    private class SpawnCharacterInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;

        public SpawnCharacterInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            this.content = content;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new SpawnCharacterInstruction(cdi, content);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            var characterName = split[0];
            var x = split[1];
            var y = split[2];
            var variableName = split[3];
            cds.SpawnCharacter(characterName, x, y, variableName);
            yield return null;
        }
    }
    private class RemoveCharacterInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;

        public RemoveCharacterInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            this.content = content;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new RemoveCharacterInstruction(cdi, content);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            var variableName = split[0];
            cds.DespawnCharacter(variableName);
            yield return null;
        }
    }
    private class PlayAnimationInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;

        public PlayAnimationInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            this.content = content;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new PlayAnimationInstruction(cdi, content);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            var characterName = split[0];
            var animationName = split[1];
            cds.Characters[characterName].Play(animationName);
            //cds.Characters[characterName].SetFloat("Facing_Y", 1);
            yield return new WaitForSeconds(0.5f);
            //cds.Characters[characterName].SetFloat("Facing_Y", -1);
        }
    }
    private class MoveInstantInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;

        public MoveInstantInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            this.content = content;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new MoveInstantInstruction(cdi, content);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            var characterName = split[0];
            var deltaX = int.Parse(split[1]);
            var deltaY = int.Parse(split[2]);
            float dt = 0;
            float duration = 0.5f;

            var sp = cds.Characters[characterName].transform.position;
            var ep = cds.Characters[characterName].transform.position + new Vector3(deltaX, deltaY);

            while (dt < duration)
            {
                yield return null;
                dt += Time.deltaTime;
                var jumpHeight = cds.JumpArc.Evaluate(dt / duration);
                cds.Characters[characterName].transform.position = Vector3.Lerp(sp, ep, dt / duration) + jumpHeight * Vector3.up;
            }
            yield return null;
        }
    }
    private class PlaySoundInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;
        public bool Looping;

        public PlaySoundInstruction(CutsceneDisplayInstance cdi, string content, bool Looping)
        {
            this.cdi = cdi;
            this.content = content;
            this.Looping = Looping;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new PlaySoundInstruction(cdi, content, false);
        }
        internal static Instruction Loop(CutsceneDisplayInstance cdi, string content)
        {
            return new PlaySoundInstruction(cdi, content, true);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var rtn = cdi.audioPool.Play(cdi.g.FindById<AudioRef>(content).clip, cdi.audioPool.PlaySimplePct, 1f);
            rtn.audioSource.loop = Looping;
            if (Looping) cdi.MarkForCleanup(rtn);
            yield return null;
        }
    }

    private class PlayMusicInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;
        private bool loops;

        public PlayMusicInstruction(CutsceneDisplayInstance cdi, string content, bool loop)
        {
            this.cdi = cdi;
            this.content = content;
            this.loops = loop;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new PlayMusicInstruction(cdi, content, true);
        }
        internal static Instruction BuildOnce(CutsceneDisplayInstance cdi, string content)
        {
            return new PlayMusicInstruction(cdi, content, false);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            cds.music.FadeTo(cdi.g.FindById<AudioRef>(content).clip);
            cds.music.audioSource.loop = this.loops;
            yield return null;
        }
    }
    private class PlayEffectInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;

        public PlayEffectInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            this.content = content;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new PlayEffectInstruction(cdi, content);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            var EffectName = split[0];
            var X = split[1];
            var Y = split[2];
            var poolable = cdi.animatorPool.RequestWithAnimator(new Vector3(float.Parse(X), float.Parse(Y)), cds.g.FindById<EffectDefinition>(EffectName).controller);
            do
            {
                yield return null;
            } while (poolable.isActiveAndEnabled);
        }
    }
    private class SetFacingInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string CharacterName;
        private string direction;

        public SetFacingInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            CharacterName = split[0];
            direction = split[1];
        }
        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new SetFacingInstruction(cdi, content);
        }
        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            float x = 0;
            float y = 0;
            switch (direction.ToLower())
            {
                case "left":
                    x = -1; break;
                case "up":
                    y = 1; break;
                case "down":
                    y = -1; break;
                case "right":
                    x = 1; break;
            }
            yield return null;
            var ca = cds.Characters[CharacterName];
            ca.SetFloat("Facing_X", x);
            ca.SetFloat("Facing_Y", y);
        }
    }

    private class ReshuffleRandomCharacterOrderInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;

        public ReshuffleRandomCharacterOrderInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
        }
        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new ReshuffleRandomCharacterOrderInstruction(cdi, content);
        }
        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            yield return null;
            cds.DynamicStrings = null;
        }

    }
    private class MoveCameraInstruction : Instruction {
        private CutsceneDisplayInstance cdi;
        private Vector2 offset = new Vector2Int();
        private float duration;
        public MoveCameraInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            var x = split[0];
            var y = split[1];
            offset = new Vector2(float.Parse(x), float.Parse(y));
            this.duration = float.Parse(split[2]);
        }
        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new MoveCameraInstruction(cdi, content);
        }
        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            Transform cmainTransform = cds.camerContainer;
            yield return Util.MoveTransformToPosition(cmainTransform, cmainTransform.position, cmainTransform.position + (Vector3)offset, AnimationCurve.Linear(0,0,1,1), duration);
        }

    }
    private class CreditText : Instruction
    {
        private CutsceneDisplayInstance cdi;
        private string content;
        private float duration;

        public CreditText(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            this.content = content;
            this.duration = 0.5f;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new CreditText(cdi, content);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            Transform cmainTransform = cds.MovableTextBubbleTransform;
            cds.MovableTextBubbleText.text = split[0];
            var csf = cmainTransform.GetComponent<HorizontalLayoutGroup>();
            csf.enabled = false;
            yield return null;
            csf.enabled = true;
            yield return Util.MoveTransformToPosition(cmainTransform, cds.BubbleOffScreenPos.position, cds.BubbleOnScreenPos.position, AnimationCurve.EaseInOut(0, 0, 1, 1), duration);
            yield return new WaitForSeconds(float.Parse(split[1]));
            yield return Util.MoveTransformToPosition(cmainTransform, cds.BubbleOnScreenPos.position, cds.BubbleOffScreenPos.position, AnimationCurve.EaseInOut(0, 0, 1, 1), duration);

        }
    }
    private class MoveInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        string CharacterName;
        string PathString;
        float durationPerTile = 0.5f;
        public bool ResetFacing;

        public MoveInstruction(CutsceneDisplayInstance cdi, string content, bool ResetFacing)
        {
            this.cdi = cdi;
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            CharacterName = split[0];
            PathString = split[1];
            this.ResetFacing = ResetFacing;
            if (split.Count > 2)
                durationPerTile = float.Parse(split[2]);
        }

        private class PathWalker : MapManager.IPathWalker
        {
            private Animator animator;
            private bool ResetAnimators;

            public PathWalker(Animator a, bool ResetAnimators)
            {
                this.animator = a;
                this.ResetAnimators = ResetAnimators;
            }
            public Animator GetCharacterAnimator()
            {
                return animator;
            }

            public Transform GetTransform()
            {
                return animator.transform;
            }

            public void MoveTo(Vector2Int a, bool b)
            {
                GetTransform().position = (a + Vector2.one / 2f);
            }

            public void OnExitTileCallback(Vector2Int tile) { }

            public bool ResetAnimatorsAtEnd()
            {
                return ResetAnimators;
            }

            public Vector2Int Size()
            {
                return Vector2Int.one;
            }
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new MoveInstruction(cdi, content, true);
        }
        internal static Instruction MaintainFacing(CutsceneDisplayInstance cdi, string content)
        {
            return new MoveInstruction(cdi, content, false);
        }

        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            Dictionary<char, Vector2Int> DirectionalDirectory = new Dictionary<char, Vector2Int>()
            {
                { 'n', Vector2Int.up },
                { 's', Vector2Int.down },
                { 'e', Vector2Int.right },
                { 'w', Vector2Int.left },
                { '.', Vector2Int.zero }
            };
            if (!cds.Characters.ContainsKey(CharacterName)) Debug.LogError("Unable to find a character named: " + CharacterName);
            var ca = cds.Characters[CharacterName];
            var pos = ca.transform.position;

            List<Vector2Int> positions = new List<Vector2Int>() { new Vector2Int(Mathf.RoundToInt(pos.x - 0.5f), Mathf.RoundToInt(pos.y - 0.5f)) };
            for (int i = 0; i < PathString.Length; i += 1)
            {
                //Debug.Log("---" + PathString[i]);
                positions.Add(positions[positions.Count - 1] + DirectionalDirectory[PathString[i]]);
            }
            var ienum = MapManager.WalkOnPathStatic(new PathWalker(ca, this.ResetFacing), positions, durationPerTile);
            yield return ienum;

        }
    }

    private class MovePropInstruction : Instruction
    {
        private CutsceneDisplayInstance cdi;
        string content;

        public MovePropInstruction(CutsceneDisplayInstance cdi, string content)
        {
            this.cdi = cdi;
            this.content = content;
        }

        internal static Instruction Build(CutsceneDisplayInstance cdi, string content)
        {
            return new MovePropInstruction(cdi, content);
        }
        public override IEnumerator Execute(CutsceneDisplayInstance cds)
        {
            var split = content.Split(',').Select(s => s.Trim()).ToList();
            var propName = split[0];
            var offsetX = float.Parse(split[1]);
            var offsetY = float.Parse(split[2]);
            var duration = float.Parse(split[3]);

            float dt = 0f;
            GameObject prop = cds.Props[propName];
            Vector3 sp = prop.transform.position;
            Vector3 ep = sp + new Vector3(offsetX, offsetY);
            while (dt <= duration)
            {
                yield return null;
                dt += Time.deltaTime;
                float pct = (duration == 0) ? 1 : dt / duration;
                prop.transform.position = Vector3.Lerp(sp, ep, pct).Rounded();
            }
        }
    }
}

