﻿using UnityEngine;

[CreateAssetMenu]
public class AudioRef : SerializableScriptableObject
{
    public AudioClip clip;
}