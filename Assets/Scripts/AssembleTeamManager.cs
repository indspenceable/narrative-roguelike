﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AssembleTeamManager : GameModeInstance
{
    [Header("Intro")]
    public Image IntroBacking;
    public TMPro.TextMeshProUGUI IntroTextDisplay;
    public float IntroDurationPerString;
    public AnimationCurve ColorCurve;
    public List<string> IntroMessages = new List<string>();

    [Header("Unsorted")]
    public CampaignGameMode msm;
    public TransitionEffect te;
    public TextboxController textbox;
    public Globals g;
    public Button EmbarkButton;
    public TMPro.TextMeshProUGUI EmbarkText;
    public CutsceneMode cutsceneMode;
    public CutsceneData introCutsceneData;

    public AudioClip WavesAudio;
    public MusicManager music;
    public AudioClip MusicTrack;

    //public List<UnitDefinition> AllCharacters = new List<UnitDefinition>();
    public List<AssembleTeamCharacterSelectButton> CharacterButtons = new List<AssembleTeamCharacterSelectButton>();
    
    public List<Talent> StartingInventory;
    public Localizer localizer;
    public AssembleTeamCharacterDetailsPane pane;


    [UnityEngine.Serialization.FormerlySerializedAs("stars")]
    public Image[] CharacterIcons;
    public Sprite UnselectedCharacterIcon;

    public override void PreActivate(object _Data)
    {
        music.FadeTo(WavesAudio);
        pane.HIDE();
        IntroBacking.gameObject.SetActive(true);
        IntroBacking.raycastTarget = true;
        IntroBacking.color = Color.black;
        IntroTextDisplay.text = "";
        IntroTextDisplay.color = Color.black;
    }
    public override void PostActivate()
    {
        StartCoroutine(DisplayIntroText());
    }

    IEnumerator DisplayIntroText()
    {
        //// DEBUG
        //IntroBacking.gameObject.SetActive(false);
        //yield break;

        Persona persona = textbox.defaultPersona;
        
        yield return new WaitForSeconds(2f);
        foreach (var str in IntroMessages)
        {
            IntroTextDisplay.text = localizer._(str);
            IntroTextDisplay.color = Color.black;

            float dt = 0;
            while (dt < IntroDurationPerString)
            {
                yield return null;
                dt += Time.deltaTime;
                IntroTextDisplay.color = Color.Lerp(Color.black, Color.white, ColorCurve.Evaluate(dt / IntroDurationPerString));
            }
        }        
        IntroTextDisplay.gameObject.SetActive(false);
        float dt2 = 0f;
        yield return new WaitForSeconds(2f);
        IntroBacking.raycastTarget = false;
        music.FadeTo(MusicTrack);
        while (dt2 < IntroDurationPerString)
        {
            yield return null;
            dt2 += Time.deltaTime;
            IntroBacking.color = Color.Lerp(Color.black, Color.clear, dt2 / IntroDurationPerString);
        }
        IntroBacking.gameObject.SetActive(false);
        
    }


    public IEnumerable<AssembleTeamCharacterSelectButton> SelectedCharacters()
    {
        return CharacterButtons.Where(cb => cb.CharacterSelected);
    }

    private void Update()
    {
        var sc = SelectedCharacters().ToList();
        int count = sc.Count();
        EmbarkButton.interactable = (count == 4);
        for (int i = 0; i < 4; i += 1)
        {
            if (count > i)
            {
                CharacterIcons[i].sprite = sc[i].character.Data.presentation.MapSprite;
            } else
            {
                CharacterIcons[i].sprite = UnselectedCharacterIcon;
            }
        }
        //EmbarkText.text = EmbarkButton.interactable ? "Embark!" : "Select your team";
    }

    public void StartGame()
    {
        if (SelectedCharacters().Count ()!= 4) return;
        EmbarkButton.interactable = false;
        var campaign = new CampaignStatus(SelectedCharacters().Select(def => g.Duplicate(def.character.Data)).ToList(), new List<Talent>());
        //campaign.team.ForEach(ud => ud.PC = true);
        foreach (var i in StartingInventory)
            campaign.inventory.Add(i);
        cutsceneMode.ActivateWithData(te, campaign, introCutsceneData, () => msm.ActivateWithData(te, campaign));
    }
}
