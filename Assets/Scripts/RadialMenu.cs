﻿using System;
using UnityEngine;

public class RadialMenu : MenuBase
{
    public float Distance = 1.2f;
    public float ThetaOffset = 0f;

    public int DistanceThreshold = 9;
    public float DistanceAdder = 0.1f;
    
    public override Vector3 ButtonPositionOffset(int i, float pct)
    {
        int total = Buttons.Count;

        var RotationPCT = (i / (float)Buttons.Count);
        float theta = (RotationPCT) * (2f*Mathf.PI);

        if (Buttons.Count == 1)
        {
            theta = Mathf.PI / 2f;
        }
        else if (Buttons.Count == 2)
        {
            if (i == 0)
            {
                theta = Mathf.PI * 3 / 4f;
            }
            else
            {
                theta = Mathf.PI * 1 / 4f;
            }
        }

        theta += ThetaOffset;

        var RealDistance = total > DistanceThreshold ? Distance + (total - DistanceThreshold) * DistanceAdder : Distance;
        Vector3 pos = new Vector3(Mathf.Cos(theta * pct), Mathf.Sin(theta * pct)) * RealDistance * pct;

        return new Vector3(Util.SmartFloor(pos.x, 16), Util.SmartFloor(pos.y, 16));
        //return pos;
    }
}