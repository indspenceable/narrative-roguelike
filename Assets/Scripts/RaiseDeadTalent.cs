﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Skills/Raise")]
public class RaiseDeadTalent : AbstractTargetedSkillTalent
{
    public AudioClip clip;
    public AudioPool audioPool;
    public AnimatorPool animatorPool;
    public RuntimeAnimatorController effectRAC;
    public List<UnitDefinition> PossibleSummons = new List<UnitDefinition>();
    public List<BuffInstance> InitialBuffs = new List<BuffInstance>();
    public Globals g;
    public CharacterAnimationData GhostPresentation;

    public override bool TargetsEmpty => true;
    public override bool TargetsFriendly => false;
    public override bool TargetsHostile => false;
    public override bool TargetsSelf => false;

    public override bool ValidTargetableTileContents(MapUnit Me, Vector2Int v2i)
    {
        return base.ValidTargetableTileContents(Me, v2i) && Me.manager.directory.DeadUnitAt(v2i);
    }

    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        // Fetch + Remove the target from graveyard;
        // Summon copied from SummonSkillTalent;
        UnitData fetchedData = Me.manager.directory.RaiseUnitAt(target);
        Me.CharacterAnimator.SetTrigger("Attack");
        //CharacterAnimationData ghostPresentation = fetchedData.presentation.GhostPresentation;
        //if (ghostPresentation == null) ghostPresentation = fetchedData.presentation;
        var mapUnit = Me.manager.directory.BuildMapUnit(PossibleSummons.Pick().Data, target, Me.data.Friendly, new List<CharacterAnimationData>() {});
        mapUnit.Spent = true;
        foreach (var b in InitialBuffs)
        { 
            mapUnit.ApplyStatus(g.Duplicate(b));
        }
        this.audioPool.PlaySimple(clip);
        this.animatorPool.RequestWithAnimator(target.CenterOfTile(), this.effectRAC);
        yield return null;
    }
}
