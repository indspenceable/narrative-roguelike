﻿using UnityEngine;

public abstract class AbstractInteractable : MonoBehaviour
{
    protected TBSGameManager manager;



    public abstract IInteractable GetInteractable();
}
