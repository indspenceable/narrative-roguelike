﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Units/Enemy")]
public class EnemyDefinition : UnitDefinition
{
    public bool DebugDisabled;
    public int weight = 100;
    public bool Boss;
}


