﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManageParty2DetailsPane : MonoBehaviour
{
    private UnitData currentUnitFocus;
    private Talent currentHover;
    public TMPro.TextMeshProUGUI Name;
    public List<ManageParty2CharacterInventoryButton> InventoryButtons;

    public Image icon;
    public TMPro.TextMeshProUGUI hpText;
    public TMPro.TextMeshProUGUI Description;
    public TMPro.TextMeshProUGUI FlavorText;
    public Localizer localizer;
    public CampaignGameMode gm;
    public ManageParty2SharedInventoryManager shared;

    public bool TakeItem(UnitData giver, Talent item)
    {
        if (giver == currentUnitFocus)
        {
            return false;
        }
        if (currentUnitFocus != null && currentUnitFocus.inventory.Count < 4)
        {
            currentUnitFocus.inventory.Add(item);
            ItemsShown = false;
            return true;
        }
        return false;
    }

    internal void Clear()
    {
        currentUnitFocus = null;
    }

    internal void FocusOn(UnitData u)
    {
        this.currentUnitFocus = u;
        ItemsShown = false;
    }

    internal void HoverOn(Talent hoverTalent)
    {
        this.currentHover = hoverTalent;
    }


    private bool ItemsShown = false;

    public void LateUpdate()
    {
        if (currentHover != null)
        {
            Name.text = currentHover.TalentName;
            hpText.text = "";
            icon.sprite = currentHover.sprite;
            Description.text = currentHover.Description.StandardizeLineHeight();
            FlavorText.text = $"\"{currentHover.FlavorText}\"".StandardizeLineHeight();
        } else if (currentUnitFocus != null)
        {
            Name.text = currentUnitFocus.CharacterName;
            hpText.text = "hp: " + currentUnitFocus.FormatUnalteredHP().StandardizeLineHeight();
            icon.sprite = currentUnitFocus.presentation.MapSprite;
            Description.text = localizer._($"characters.{currentUnitFocus.CharacterName}.description").StandardizeLineHeight();
            FlavorText.text = $"\"{localizer._(currentUnitFocus.currentQuote)}\"".StandardizeLineHeight();
        } else
        {
            Name.text = "";
            icon.sprite = null;
            Description.text = "";
            FlavorText.text = "";
            foreach (var btn in InventoryButtons) btn.Hide();
            ItemsShown = false;
        }


        if (!ItemsShown && currentUnitFocus != null)
        {
            ShowItems();
        }
    }

    private void ShowItems()
    {
        for (int i = 0; i < 4; i += 1)
        {
            if (currentUnitFocus.inventory.Count > i)
            {
                Talent currentItem = currentUnitFocus.inventory[i];
                InventoryButtons[i].ShowItem(currentUnitFocus.presentation.StickyNotes.Pick(), currentItem, 1, () =>
                {
                    gm.campaign.inventory.Add(currentItem);
                    currentUnitFocus.inventory.Remove(currentItem);
                    ItemsShown = false;
                    currentHover = null;
                    shared.Setup(gm.campaign.inventory, gm.campaign.team);
                });
                InventoryButtons[i].ShowAnimation(i / 25f);
            }
            else
            {
                InventoryButtons[i].Hide();
            }

        }
        ItemsShown = true;
    }
}
