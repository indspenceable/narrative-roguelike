﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainParticleManager : MonoBehaviour
{
    [System.Serializable]
    public class LMPSPair
    {
        public LayerMask Mask;
        public ParticleSystem System;
    }

    public List<LMPSPair> Systems;

    [ExecuteInEditMode]
    void OnParticleTrigger()
    {
        ParticleSystem ps = GetComponent<ParticleSystem>();
        List<ParticleSystem.Particle> inside = new List<ParticleSystem.Particle>();
        int numEnter = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Inside, inside);
        for (int i = 0; i < numEnter; i +=1) 
        {
            ParticleSystem.Particle p = inside[i];


            Systems.Find(lmps =>
            {
                if (Physics2D.OverlapPoint(p.position + Vector3.down*0.25f, lmps.Mask))
                {
                    var shape = lmps.System.shape;
                    shape.position = p.position;
                    lmps.System.Emit(1);
                    p.startLifetime = 0f;
                    p.remainingLifetime = 0f;
                    return true;
                }
                return false;
            });
            
            inside[i] = p;
        }

        ps.SetTriggerParticles(ParticleSystemTriggerEventType.Inside, inside);
    }
}
