﻿using UnityEngine;

[CreateAssetMenu]
public class EffectDefinition : SerializableScriptableObject
{
    public RuntimeAnimatorController controller;
}
