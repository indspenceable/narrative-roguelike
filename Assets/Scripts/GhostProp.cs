﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GhostProp :MonoBehaviour
{
    public SpriteRenderer sr;
    public BaseBuilder bb;

    private BaseBuilderPropDefinition currentPropToPlace;
    public PersistantObjectPool pool;
    public List<AbstractPoolable> poolables;

    private bool _shown = false;
    public bool shown
    {
        set
        {
            if (_shown == value) return;
            _shown = value;
            if (value)
            {
                Setup(currentPropToPlace);
            } else
            {
                Hide();
            }
        }
        get
        {
            return _shown;
        }
    }

    public void Hide()
    {
        foreach (var p in poolables) p.ExternalDeactivate();
        gameObject.SetActive(false);
    }


    internal void Setup(BaseBuilderPropDefinition currentPropToPlace)
    {
        this.currentPropToPlace = currentPropToPlace;
        // after we update that we're showing a different sprite, shortcircuit if we're not supposed to show it directly.
        if (!_shown || currentPropToPlace == null) return;
        sr.sprite = currentPropToPlace.PropSprite;
        sr.transform.localPosition = currentPropToPlace.SpriteOffsetPositionAuto;
        sr.color = new Color(1, 1, 1, 0.75f);

        foreach (var p in poolables) p.ExternalDeactivate();
        poolables.Clear();
        for (int x = 0; x < currentPropToPlace.Size.x; x += 1)
        {
            for (int y = 0; y < currentPropToPlace.Size.y; y += 1) {
                var poolable = pool.Request(Vector2.zero);
                poolables.Add(poolable);
            }
        }
        
        gameObject.SetActive(true);
    }

    private void LateUpdate()
    {
        transform.position = (Vector2)(bb.MousePositionToTileAddress());
        int a = 0;
        for (int x = 0; x < currentPropToPlace.Size.x; x += 1)
        {
            for (int y = 0; y < currentPropToPlace.Size.y; y += 1)
            {
                if (shown)
                {
                    var poolable = poolables[a];
                    a += 1;
                    Vector3 poolablePosition = new Vector2(
                        Mathf.Ceil(transform.position.x + x - currentPropToPlace.StandardizedOffsetPosition.x),
                        Mathf.Ceil(transform.position.y + y - currentPropToPlace.StandardizedOffsetPosition.y)
                    );
                    poolable.transform.position = (Vector2)poolablePosition;

                    var pos = new Vector2Int(Mathf.RoundToInt(poolablePosition.x), Mathf.RoundToInt(poolablePosition.y));
                    bool disalowed = bb.PropPlacementDisallowed(currentPropToPlace, pos);
                    //val = val || bb.propManager.LocationBlocked(pos);
                    poolable.GetComponent<Animator>().SetBool("Overlap", disalowed);
                } else
                {
                    Hide();
                }
            }
        }
    }

    
}
