﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;

[CreateAssetMenu]
public class Globals : ScriptableObject
{
    public string VersionNumber = "0.0.1";
    public List<SerializableScriptableObject> SOs = new List<SerializableScriptableObject>();
    [Space]
    public Texture2D DefaultMouseTex;
    public Texture2D HighlightedMouseTex;


    [NaughtyAttributes.ReadOnly]
    public List<BaseBuilderPropPlacement> DefaultBaseBuilderLayout;


    private class Vector2SerializationSurrogate : ISerializationSurrogate
    {
        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            var v2i = (Vector2Int)obj;
            info.AddValue("x", v2i.x);
            info.AddValue("y", v2i.y);
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            Vector2Int v2i = new Vector2Int((int)info.GetValue("x", typeof(int)), (int)info.GetValue("y", typeof(int)));
            return v2i;
        }
    }

    private class ScriptableObjectSerializationSurrogate : ISerializationSurrogate
    {
        public Globals globals;

        public ScriptableObjectSerializationSurrogate(Globals globals)
        {
            this.globals = globals;
        }

        public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
        {
            var SO = (SerializableScriptableObject)obj;
            info.AddValue("identifier", SO._HiddenIdentifier);
        }

        public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
        {
            string identifier = info.GetValue("identifier", typeof(string)) as string;
            SerializableScriptableObject rtn = globals.SOs.Find(talent => talent._HiddenIdentifier == identifier);
            if (rtn == null || identifier == "" || identifier == null)
            {
                Debug.LogError("Was unable to deserialize scriptable object with identifier: " + identifier);
            }
            return rtn;
        }
    }


    void AddSurrogates(SurrogateSelector selector, List<Type> forTypes, ISerializationSurrogate surrogate)
    {
        foreach (var type in forTypes)
        {
            selector.AddSurrogate(type, new StreamingContext(StreamingContextStates.All), surrogate);
        }
    }

    public BinaryFormatter BuildFormatter()
    {
        var rtn = new BinaryFormatter();
        SurrogateSelector surrogateSelector = new SurrogateSelector();
        surrogateSelector.AddSurrogate(typeof(Vector2Int),
            new StreamingContext(StreamingContextStates.All),
            new Vector2SerializationSurrogate());
        AddSurrogates(surrogateSelector,
            SOs.Select(t => t.GetType()).
                Append(typeof(Talent)).
                Append(typeof(AIStrategyBase)).Distinct().ToList(),
            new ScriptableObjectSerializationSurrogate(this));
        rtn.SurrogateSelector = surrogateSelector;
        return rtn;
    }

    [System.Serializable]
    public class SerializedData
    {
        public string Version;
        public GameModeBase gm;
        public object o;

        public SerializedData(string Version, GameModeBase gm, object o)
        {
            this.Version = Version;
            this.gm = gm;
            this.o = o;
        }
    }

    private string QuicksaveFilePath()
    {
        return Application.persistentDataPath + "/quicksave.remcycles";
    }

    public bool QuicksaveExists()
    {
        return File.Exists(QuicksaveFilePath());
    }

    internal void WipeQuicksaveData()
    {
        File.Delete(QuicksaveFilePath());
    }

    private Dictionary<Type, object> cachedLists;
    public IEnumerable<T> All<T>() where T : SerializableScriptableObject
    {
        if (cachedLists == null) cachedLists = new Dictionary<Type, object>();
        var key = typeof(T);
        if (!cachedLists.ContainsKey(key))
        {
            cachedLists[key] = this.SOs.Where(so => so is T).Cast<T>().ToList();
        }
        return (List<T>)cachedLists[key];
    }

    public void Quicksave(GameModeBase gm, object o)
    {
        if (o == null) Debug.Log("Unable to save null object.");

        BinaryFormatter binFormatter = BuildFormatter();
        FileStream filet = File.Create(QuicksaveFilePath() + "_test"); //you can call it anything you want
        binFormatter.Serialize(filet, this.Duplicate(new SerializedData(VersionNumber, gm, o)));
        filet.Close();
        FileStream file = File.Create(QuicksaveFilePath()); //you can call it anything you want
        binFormatter.Serialize(file, this.Duplicate(new SerializedData(VersionNumber, gm, o)));
        file.Close();
    }

    public void LoadQuicksave(TransitionEffect te)
    {
        BinaryFormatter binFormatter = BuildFormatter();
        FileStream file = File.Open(QuicksaveFilePath(), FileMode.Open);
        Action Todo = null;
        try {
            var data = (SerializedData)binFormatter.Deserialize(file);
            if (data.Version == VersionNumber)
            {
                Todo = ()=>data.gm.ActivateFromQuicksave(te, data.o);
            }
        } catch (Exception)
        {

        } finally
        {
            file.Close();
        }

        if (Todo != null)
        {
            Todo();
        } else {
            var ap = All<AudioPool>().First();
            ap.PlaySimple(ap.sfx.Thud);
        }
    }

    public string MetaSaveFilePath()
    {
        return Application.persistentDataPath + "/persistent_data.remcycles";
    }

    public void SaveMetaProgression(MetaProgressionData prog)
    {
        BinaryFormatter binFormatter = BuildFormatter();
        FileStream file = File.Create(MetaSaveFilePath()); //you can call it anything you want
        binFormatter.Serialize(file, prog);
        file.Close();
    }

    public MetaProgressionData LoadMetaProgression()
    {
        MetaProgressionData rtn;
        if (File.Exists(MetaSaveFilePath()))
        {
            BinaryFormatter binFormatter = BuildFormatter();
            FileStream file = File.Open(MetaSaveFilePath(), FileMode.Open);
            rtn = (MetaProgressionData)binFormatter.Deserialize(file);
            file.Close();
        }
        else
        {
            rtn = new MetaProgressionData(DefaultBaseBuilderLayout);
        }
        return rtn;
    }


    public bool TT;
    public void SetMouseHighlight(bool t)
    {
        TT = t;
        Cursor.SetCursor(t ? HighlightedMouseTex : DefaultMouseTex, Vector2.zero, CursorMode.Auto);
    }

#if UNITY_EDITOR
    [NaughtyAttributes.Button]
    public void Editor_Only_RefreshAll()
    {
        SOs = AssembleAllInstancesInProject<SerializableScriptableObject>().ToList();
        SOs.ForEach(so => UnityEditor.EditorUtility.SetDirty(so));
        UnityEditor.EditorUtility.SetDirty(this);
        this.cachedLists = null;
    }

    public static T[] AssembleAllInstancesInProject<T>() where T : ScriptableObject
    {
        string[] guids = UnityEditor.AssetDatabase.FindAssets("t:" + typeof(T).Name);  //FindAssets uses tags check documentation for more info
        T[] a = new T[guids.Length];
        for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
        {
            string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(path);
        }

        return a;

    }

    public static Sprite[] GetAllSprites()
    {
        string[] guids = UnityEditor.AssetDatabase.FindAssets("t:Sprite");  //FindAssets uses tags check documentation for more info
        Sprite[] a = new Sprite[guids.Length];
        for (int i = 0; i < guids.Length; i++)         //probably could get optimized 
        {
            string path = UnityEditor.AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = UnityEditor.AssetDatabase.LoadAssetAtPath<Sprite>(path);
        }

        return a;

    }
#endif

    public T Duplicate<T>(T obj)
    {
        Stream stream = new MemoryStream();
        BinaryFormatter binFormatter = BuildFormatter();
        binFormatter.Serialize(stream, obj);
        stream.Seek(0, SeekOrigin.Begin);
        return (T)binFormatter.Deserialize(stream);
    }

    public T FindById<T>(string Identifier, bool allowNull = false) where T : SerializableScriptableObject
    {
        var matches = All<T>().Where(o => o.Identifier == Identifier);
        if (!matches.Any())
        {
            if (allowNull) return null;
            Debug.LogError($"Unable to find: {typeof(T)} with ID: {Identifier}");
        }
        return matches.First();
    }


    [NaughtyAttributes.Button]
    public void ValidateDataPlease()
    {
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
        foreach(var ed in All<EnemyDefinition>())
        {
            if (ed.Data.Strategy == null) Debug.Log($"{ed.name} is missing an AI Strategy.", ed);
            if (ed.Data.presentation == null) Debug.Log($"{ed.name} is missing a presentation.", ed);
        }

        CutsceneDisplayInstance.InstructionRegistry ir = new CutsceneDisplayInstance.InstructionRegistry();
        foreach(var support in All<SupportConversation>())
        {
            var split = support.GetScript().Split(new char[] { '\n' });
            if(support.RequiredCharacters.Count == 0)
            {
                Debug.Log($"NO required characters for {support.name}", support);
            }
            foreach(var line in split)
            {
                if (line.StartsWith("SAY_RAW"))
                {
                    var match = Regex.Match(line, @"SAY_RAW (\w*)( ?\<(\w*)\>)?:");
                    if (!match.Success) {
                        Debug.Log($"erorr with line: {line}", support);
                        continue;
                    } 


                    var name = match.Groups[1];
                    Persona persona = null;
                    try
                    {
                        persona = FindById<Persona>(name.Value);
                    } catch(Exception)
                    {

                    }
                    if (persona == null)
                    {
                        Debug.Log($"Unable to find persona with id: [{name}]");
                    }
                    else
                    {
                        var expression = match.Groups[2];
                        if (expression.Success)
                        {
                            var rawExpression = expression.Value.Substring(1, expression.Value.Length - 2);
                            if (!persona.HasSpecialPortrait(rawExpression))
                            {
                                Debug.Log($"Missing Portrait: {rawExpression} for {name}",  persona);
                                Debug.Log($"^^^                               for support", support);
                            }
                        }
                    }
                }
            }
        }
    }
}
