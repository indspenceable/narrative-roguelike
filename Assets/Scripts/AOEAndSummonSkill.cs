﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/AOEAndSummon")]
public class AOEAndSummonSkill : SummonSkillTalent
{
    public OnHitEffect onHit;
    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        yield return base.Use(Me, target);
        MapUnit summoned = Me.manager.directory.UnitAt(target);
        yield return AttackTalent.ExecuteAttackAll(onHit, summoned, target, true);
    }
}