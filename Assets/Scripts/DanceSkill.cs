﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Skills/Dance")]
public class DanceSkill : AbstractTargetedSkillTalent
{
    public AudioClip clip;
    public override bool TargetsEmpty => false;

    public override bool TargetsFriendly => true;

    public override bool TargetsHostile => false;

    public override bool TargetsSelf => false;

    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        Me.CharacterAnimator.SetTrigger("Attack");
        Me.manager.audioPool.PlaySimple(clip);
        MapUnit targetUnit = Me.manager.directory.UnitAt(target);
        targetUnit.Spent = false;
        yield return new WaitForSeconds(0.25f);
        targetUnit.CharacterAnimator.SetTrigger("Attack");
    }

    public override bool ValidTargetableTileContents(MapUnit Me, Vector2Int v2i)
    {
        return base.ValidTargetableTileContents(Me, v2i) && Me.manager.directory.UnitAt(v2i).Spent == true;
    }
}
