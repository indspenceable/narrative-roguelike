﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UnitData
{
    public CharacterAnimationData presentation;
    public string Identifier;

    public string CharacterName;
    [NaughtyAttributes.ResizableTextArea]
    public string Description;
    public int CurrentLevel;
    public Vector2Int pos;
    [NaughtyAttributes.ReorderableList]
    public List<Talent> IntrinsicTalents;

    [NaughtyAttributes.ReorderableList]
    public List<Talent> inventory;

    public bool Spent;
    public bool Friendly;
    public int currentDamage;
    public string FormatUnalteredHP()
    {
        return $"{MaxHP - currentDamage}/{MaxHP}";
    }

    public bool DoesNotAct;
    public bool Unaware;
    public bool StartsUnaware = true;
    public string UnawareAnimatorName = "Unaware";
    public bool HasDeathQuote;

    public int MaxHP = 30;
    public int Movement = 5;

    internal string SelectQuoteKey()
    {
        List<string> rtnKeys = new List<string>()
        {
           "quotes.Generic.1",
            "quotes.Generic.2",
            "quotes.Generic.3",
            "quotes.Generic.4",
            "quotes.Generic.5",
            "quotes.Generic.6"
        };
        if (currentDamage < MaxHP / 3f)
        {
            rtnKeys.Add("quotes.Healthy.1");
            rtnKeys.Add("quotes.Healthy.2");
        }
        if (currentDamage > 2f * MaxHP / 3f)
        {
            rtnKeys.Add("quotes.Unhealthy.1");
            rtnKeys.Add("quotes.Unhealthy.2");
        }
        rtnKeys.Add($"quotes.{Identifier}");
        return rtnKeys.Pick();
    }

    public Dictionary<string, int> SkillUsesPerRoom = new Dictionary<string, int>();
    public Dictionary<string, int> Cooldowns = new Dictionary<string, int>();



    public int Strength;
    public int Armor;
    public int BaseNoise = 3;
    public int BaseAlertness = 0;

    // Only for enemy units
    public AIStrategyBase Strategy;

    // TODO revisit these and see if they can be consolidated. maybe the whole thing is just one data cache.
    public Dictionary<string, object> AIDataCache = new Dictionary<string, object>();
    public MapUnitPrefabDefinition UnitPrefab;
    // This should be used to serialize a unit-specific script.
    public object SpecialUnitDataCache;
    public enum MobilityType
    {
        LAND,
        WATER,
        FLIGHT
    };
    public MobilityType Mobility;
    public bool CannotRaise = false;
    public List<Vector2Int> KnifeLocations = new List<Vector2Int>();
    public List<BuffInstance> CurrentBuffs;
    public string currentQuote;
    public int Complexity = 1;
    public bool IsCat;
}