﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.Events;

public class DynamicMenu :MonoBehaviour
{
    public Button prefab;
    public List<Button> instances = new List<Button>();
    private Action onCancel;

    public class OptionData
    {
        public string Text;
        public UnityAction onPress;

        public OptionData(string name, UnityAction onPress)
        {
            this.Text = name;
            this.onPress = onPress;
        }
    }

    private void ClearAllButtons()
    {
        while (instances.Count > 0)
        {
            Destroy(instances[0].gameObject);
            instances.RemoveAt(0);
        }
    }

    public bool IsOpen()
    {
        return gameObject.activeSelf;
    }
    public void Close()
    {
        ClearAllButtons();
        gameObject.SetActive(false);
    }

    public void Setup(Vector3 location, System.Action onCancel, params OptionData[] options)
    {
        ClearAllButtons();
        gameObject.SetActive(true);
        this.onCancel = onCancel;
        float initialTheta = 0f;
        float thetaPerIteration = 360 / (options.Length+2);
        for (int i= 0; i < options.Length; i += 1)
        {
            var currentTheta = initialTheta + thetaPerIteration * i;

            var inst = Instantiate(prefab, location +
                //new Vector3(Mathf.Cos(currentTheta), Mathf.Sin(currentTheta)) * 1.5f, 
                Vector3.down * (i * 1f),
                Quaternion.identity, transform);
            inst.onClick.RemoveAllListeners();
            inst.onClick.AddListener(options[i].onPress);
            inst.gameObject.SetActive(true);
            inst.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = options[i].Text;
            inst.GetComponent<HasTooltip>().TooltipContents = options[i].Text;
            instances.Add(inst);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Escape! cancelling out.");
            this.onCancel();
        }
    }
}