﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BetweenTurnOverlayManager : MonoBehaviour
{
    public RectTransform TopImage;
    public RectTransform BottomImage;
    public TMPro.TextMeshProUGUI TextDisplay;
    public RectTransform TextPosition;
    [Space]
    public AnimationCurve ImageMovementCurve;
    public AnimationCurve TextMovementCurve;
    public float duration;
    [Space]
    public RectTransform TopInactiveAnchor;
    public RectTransform TopActiveAnchor;
    public RectTransform BottomInactiveAnchor;
    public RectTransform BottomActiveAnchor;
    public RectTransform LeftTextAnchor;
    public RectTransform RightTextAnchor;
    public RectTransform ActiveTextAnchor;

    private bool IsDisplaying = false;

    public IEnumerator Display(string displayText, float WaitDuration = 1f)
    {
        yield return BeginShowing(displayText);
        yield return new WaitForSeconds(WaitDuration);
        yield return FinishShowing();
    }
    public IEnumerator BeginShowing(string displayText)
    {
        IsDisplaying = true;
        TextDisplay.transform.position = RightTextAnchor.position;
        TextDisplay.text = displayText;

        float dt = 0;
        while (dt <= duration)
        {
            yield return null;
            dt += Time.deltaTime;
            TopImage.position = Vector3.LerpUnclamped(TopInactiveAnchor.position, TopActiveAnchor.position, ImageMovementCurve.Evaluate(dt / duration));
            BottomImage.position = Vector3.LerpUnclamped(BottomInactiveAnchor.position, BottomActiveAnchor.position, ImageMovementCurve.Evaluate(dt / duration));
            TextPosition.position = Vector3.LerpUnclamped(RightTextAnchor.position, ActiveTextAnchor.position, ImageMovementCurve.Evaluate(dt / duration));
        }
    }
    public IEnumerator FinishShowing() {
        float dt = 0f;
        while (dt <= duration)
        {
            yield return null;
            dt += Time.deltaTime;
            TopImage.position = Vector3.LerpUnclamped(TopActiveAnchor.position, TopInactiveAnchor.position, ImageMovementCurve.Evaluate(dt / duration));
            BottomImage.position = Vector3.LerpUnclamped(BottomActiveAnchor.position, BottomInactiveAnchor.position, ImageMovementCurve.Evaluate(dt / duration));
            TextPosition.position = Vector3.LerpUnclamped(ActiveTextAnchor.position, LeftTextAnchor.position, ImageMovementCurve.Evaluate(dt / duration));
        }
        IsDisplaying = false;
    }

    internal void HideEverything()
    {
        TextPosition.position = RightTextAnchor.position;
        TopImage.position = TopInactiveAnchor.position;
        BottomImage.position = BottomInactiveAnchor.position;
        IsDisplaying = false;
    }

    public List<TextEffect> TextEffects;

    private void Update()
    {
        if (!IsDisplaying) return;
        var vertices = new List<Vector3>();
        TextDisplay.ForceMeshUpdate();

        TextDisplay.mesh.GetVertices(vertices);
        for (int i = 0; i < TextDisplay.textInfo.characterCount; i += 1)
        {
            var charInfo = TextDisplay.textInfo.characterInfo[i];
            if (!charInfo.isVisible) continue;

            var vertexIndex = charInfo.vertexIndex;
            
            foreach (var te in TextEffects)
            {
                te.Process(vertexIndex, i, vertices);
            }
        }
        TextDisplay.mesh.SetVertices(vertices);
        TextDisplay.UpdateGeometry(TextDisplay.mesh, 0);
    }
}
