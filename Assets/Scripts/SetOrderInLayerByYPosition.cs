﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetOrderInLayerByYPosition : MonoBehaviour
{
    public SpriteRenderer sr;
    private void LateUpdate()
    {
        this.sr.sortingOrder = Mathf.FloorToInt(-transform.position.y*1000);
    }
}
