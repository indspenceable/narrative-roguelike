﻿using UnityEngine;

public abstract class AbstractCutsceneData : SerializableScriptableObject, CutsceneDisplayInstance.ICutsceneTeamInfo {
    public abstract string GetSetupScript();
    public abstract string GetScript();

    public abstract bool MainCharacter(string characterName);
}
[CreateAssetMenu(menuName = "Cutscenes/Standard")]
public class CutsceneData : AbstractCutsceneData
{
    [NaughtyAttributes.ResizableTextArea]
    public string SetupScript;
    [NaughtyAttributes.ResizableTextArea]
    public string Script;

    public override string GetSetupScript()
    {
        return SetupScript;
    }

    public override string GetScript()
    {
        return Script;
    }


    public override bool MainCharacter(string characterName)
    {
        return false;
    }

    [NaughtyAttributes.Button]
    public override void OnValidate()
    {
        bool b = ReplaceChars("…", "...");
        b = ReplaceChars("’",  "'") || b;
        b = ReplaceChars("‘",  "'") || b;
        b = ReplaceChars("“", "\"") || b;
        b = ReplaceChars("”", "\"") || b;
        b = ReplaceChars("é",  "e") || b;
        b = ReplaceChars("\r", "") || b;
        //b = ReplaceChars("SAY_RAW Danny", "SAY_RAW Narrator") || b;
#if UNITY_EDITOR
        if (b)
        {
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif

        base.OnValidate();
    }

    private bool ReplaceChars(string ToReplace, string ReplaceWith)
    {
        //Debug.Log("Checking: " + ToReplace);
        bool exists = (SetupScript.IndexOf(ToReplace) != -1) || (Script.IndexOf(ToReplace) != -1);
        if (exists)
        {
            Debug.Log("replacing: " + ToReplace);
            SetupScript = SetupScript.Replace(ToReplace, ReplaceWith);
            Script = Script.Replace(ToReplace, ReplaceWith);
            return true;
        }
        return false;
    }
}
