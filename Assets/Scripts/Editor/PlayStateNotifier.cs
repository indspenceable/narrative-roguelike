﻿using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public static class PlayStateNotifier
{

    static PlayStateNotifier()
    {
        EditorApplication.playModeStateChanged += ModeChanged;
        
    }

    static void ModeChanged(PlayModeStateChange playModeState)
    {
        if (playModeState == PlayModeStateChange.EnteredEditMode)
        {
            Object.FindObjectOfType<Coordinator>().TransitionMaterial.SetFloat("_Cutoff", 0f);
        }
    }
}