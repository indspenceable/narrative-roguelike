﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorPoolable : AbstractPoolable
{
    public SpriteRenderer sr;
    public Animator anim;

    public override void OnActivatedByPool(AbstractObjectPool pool)
    {
        transform.rotation = Quaternion.identity;
        sr.sprite = null;
        anim.runtimeAnimatorController = null;
        base.OnActivatedByPool(pool);

    }
    public void Setup(RuntimeAnimatorController cont)
    {
        gameObject.SetActive(true);
        sr.color = Color.white;
        anim.runtimeAnimatorController = cont;
        anim.SetBool("Active", true);
        sr.sortingLayerName = "ui";
    }
    private void Update()
    {
        if (anim.runtimeAnimatorController != null && !anim.GetBool("Active"))
        {
            ExternalDeactivate();
        }
    }
}
