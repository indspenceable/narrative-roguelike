﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="GameModes/Stateless")]
public class StatelessGameMode : GameModeBase
{
    public void ActivateWithoutData(TransitionEffect tEffect)
    {
       Activate(tEffect);
    }
}
