﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartMeter : MeterBase
{
    public MapUnit u;

    private int MaxValue = 12;

    [SerializeField]
    [NaughtyAttributes.ReadOnly]
    private int TargetValue = 0;
    [SerializeField]
    [NaughtyAttributes.ReadOnly]
    private float DisplayedValue = 0f;
    private bool _CurrentlyDisplayed = false;

    public HeartMeterPool heartPool;
    private List<HeartMeterPoolable> hearts = new List<HeartMeterPoolable>();

    public float TimeToFill = 1f;
    private float _vel = 0f;
    public HeartSprites defaultHearts;


    public Transform Container;

    private void Update()
    {
        if (Displayed || Engaged || Input.GetKey(KeyCode.LeftAlt))
        {
            ForceUpdate();
        }
        else
        {
            _CurrentlyDisplayed = false;
            EnsureCorrectHeartCount(0);
        }
    }

    private void ForceUpdate()
    {
        MaxValue = u.MaxHP();
        EnsureCorrectHeartCount(Mathf.CeilToInt(MaxValue / 4f));
        TargetValue = u.MaxHP() - u.data.currentDamage;
        if (!_CurrentlyDisplayed)
        {
            DisplayedValue = Mathf.Max(TargetValue, 0);
            _vel = 0f;
        }
        _CurrentlyDisplayed = true;

        if (Mathf.Abs(DisplayedValue - TargetValue) > 0.2f)
        {
            DisplayedValue = Mathf.SmoothDamp(DisplayedValue, TargetValue, ref _vel, TimeToFill);
        }
        else
        {
            DisplayedValue = TargetValue;
        }
        UpdateHeartAnimationFrames();
    }

    private void UpdateHeartAnimationFrames()
    {
        var hs = u.data.presentation.heartSprites;
        if (hs == null) hs = defaultHearts;
        var hp = Mathf.RoundToInt(DisplayedValue);
        for (int i = 0; i < hearts.Count; i += 1)
        {
            hearts[i].img.sprite = hs.Sprites[Mathf.Clamp(hp, 0, 4)];
            hp -= 4;
        }
    }
    
    public override void SetEngagedAndUpdate()
    {
        Engaged = true;
        ForceUpdate();
    }

    private void EnsureCorrectHeartCount(int correctNumber)
    {
        while (hearts.Count < correctNumber)
        {
            HeartMeterPoolable heart = heartPool.RequestHeart(Vector3.zero) as HeartMeterPoolable;
            heart.transform.SetParent(Container.transform,false);
            hearts.Add(heart);
        }
        while (hearts.Count > correctNumber)
        {
            hearts[0].ExternalDeactivate();
            hearts.RemoveAt(0);
        }
        for (int i = 0; i < hearts.Count; i += 1)
        {
            if (i == 0 || i == hearts.Count-1)
            {
                hearts[i].img.transform.localPosition = new Vector3(0, -1 / 8f);
            } else
            {
                hearts[i].img.transform.localPosition = Vector3.zero;
            }
        }
    }

    public override void SetContainerLocalPosition(Vector3 pos)
    {
        Container.localPosition = pos;
    }
}
