﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public abstract class AbstractSkillTalent : Talent, ISkillBase
{
    public bool RequiresNoMovement;
    public int UsesPerRoom = 0;
    public int cooldown = 0;
    [Tooltip("This allows multiple skills to share the same cooldown.")]
    public string OverrideCooldownName;

    public virtual bool Available(MapUnit Me, List<Vector2Int> path)
    {
        return CheckRoomUses(Me);
    }

    private bool CheckRoomUses(MapUnit Me)
    {
        return UsesPerRoom == 0 || Me.SkillUses(CooldownIdentifier()) < UsesPerRoom;
    }

    public Sprite Icon()
    {
        return sprite;
    }

    public string SkillIdentifier()
    {
        return this.GetType().ToString() + TalentName;
    }
    public string CooldownIdentifier() { 
        return (OverrideCooldownName == "" ? SkillIdentifier() : OverrideCooldownName);
    }

    public int Cooldown()
    {
        return cooldown;
    }
    public abstract TBSGameManager.InputHandler UseSkillHandler(MapUnit me, TBSGameManager.InputHandler previous);

    public string SkillName()
    {
        return TalentName;
    }

    public bool _Consumable;
    public bool Consumable() => _Consumable;

    public abstract List<Vector2Int> PossibleTargets(MapUnit Me);

    public abstract IEnumerator Use(MapUnit Me, Vector2Int target);

    

    int ISkillBase.UsesPerRoom()
    {
        return UsesPerRoom;
    }

    public string GetTooltip()
    {
        return SkillName();
    }

    string ISkillBase.Description()
    {
        return Description;
    }
}
