﻿using System.Collections;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Snake/Ensnare Attack")]
public class EnsnarePartyMember : AttackTalent
{
    public override IEnumerator Use(MapUnit Me, Vector2Int target)
    {
        yield return base.Use(Me, target);
        yield return new WaitForSeconds(1f);
        Me.manager.directory.UnitAt(target).MoveTo(Me.data.pos, true);
    }
}
