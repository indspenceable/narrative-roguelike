﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Skills/Multisummon")]
public class SummonInRadius : SummonSkillTalent
{
    public bool _TargetEmpty = true;
    public override bool TargetsEmpty => _TargetEmpty;

    public bool _TargetsFriendly = true;
    public override bool TargetsFriendly => _TargetsFriendly;

    public bool _TargetsHostile = true;
    public override bool TargetsHostile => _TargetsHostile;

    public bool _TargetsSelf = true;
    public override bool TargetsSelf => _TargetsSelf;

    public int Radius = 1;
    public int SummonCount = -1;
    
    public override IEnumerator Use(MapUnit Me, Vector2Int v2i)
    {
        List<Vector2Int> _Locations = new List<Vector2Int>();
        for (int x = -Radius; x <= Radius; x += 1)
        {
            for (int y = -Radius; y <= Radius; y += 1)
            {
                Vector2Int pos = v2i + new Vector2Int(x, y);
                var mapTile = Me.manager.map.TileAt(pos);
                if ((mapTile != null) && mapTile.MovementAllowed(Summonable.Data.Mobility) && Me.manager.directory.UnitAt(pos) == null)
                    _Locations.Add(pos);
            }
        }
        IEnumerable<Vector2Int> Locations = _Locations;
        if (SummonCount > -1)
        {
            Locations = Locations.Shuffled().Take(SummonCount);
        }
        foreach(var pos in Locations)
        {
            yield return base.Use(Me, pos);
        }
    }
}
