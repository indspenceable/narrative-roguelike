﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "Talents/Special/Snake/Flood Attack")]
public class FloodAttack : AbstractSelfSkillTalent {
    public OnHitEffect onHit;
    public RuntimeAnimatorController WaveAnim;

    public override IEnumerator Use(MapUnit Me)
    {
        yield return Me.BeforeUseAttack();
        // show the wave effect
        var poolable = Me.manager.animatorPool.RequestWithAnimator(Camera.main.transform.position + Vector3.forward * 10, WaveAnim);
        poolable.sr.flipX = true;
        yield return new WaitUntil(() => !poolable.anim.GetBool("Active"));
        poolable.sr.flipX = false;
        //yield return AttackTalent.ExecuteAttackAll(onHit, Me, Me.data.pos, true);
        foreach(var u in Me.manager.directory.mapUnits)
        {
            if (u != Me && !u.data.CurrentBuffs.Any(bi => bi.buff is Ensnared))
            {
                yield return AttackTalent.ExecuteAttack(onHit, Me, u.data.pos, false);
            }
        }
    }
}
