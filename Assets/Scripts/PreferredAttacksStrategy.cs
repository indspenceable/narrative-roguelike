﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Preferred Moves")] 
public class PreferredAttacksStrategy: AIStrategy
{
    public int Bonus = 50;
    public List<Talent> PreferredAttacks;
    public override float ScorePlan(MapUnit currentUnit, AIActionPlan plan)
    {
        if (PreferredAttacks.Cast<ISkillBase>().Contains(plan.skill))
        {
            return base.ScorePlan(currentUnit, plan) + Bonus;
        } else
        {
            return base.ScorePlan(currentUnit, plan);
        }
    }
}
