﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TBSUnitHoverHintRow : MonoBehaviour
{
    public Image tileImageDisplay;
    public TMPro.TextMeshProUGUI tileNameTextArea;
    public TMPro.TextMeshProUGUI talentDescArea;
    public TMPro.TextMeshProUGUI stacksTextArea;

    public void displayTalent(Talent buff, int? stacks)
    {
        tileImageDisplay.sprite = buff.sprite;
        tileNameTextArea.text = buff.TalentName;
        talentDescArea.text = buff.Description;
        if (stacks.HasValue)
        {
            stacksTextArea.text = "x" + stacks.Value;
        } else
        {
            stacksTextArea.text = "";
        }
    }
}
