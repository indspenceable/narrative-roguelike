﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TBSUnitHoverHint : MonoBehaviour
{
    public MapUnit focus;
    public MapTile focusTile;
    public Talent focusTalent;

    public List<PanelInstance> panels;

    [System.Serializable]
    public class PanelInstance
    {
        public ToggleableUIElement ui;
        public Transform Main;
        public GameObject UnitContainer;
        public Transform TileContainer;
        public Image unitImageDisplay;
        public TMPro.TextMeshProUGUI unitNameTextArea;
        public TMPro.TextMeshProUGUI unitHealthTextArea;
        public List<TBSUnitHoverHintRow> rows = new List<TBSUnitHoverHintRow>();
    }
    public TBSUnitHoverHintRow rowPrefab;

    private MapUnit shownFocus = null;
    private MapTile shownTile = null;

    private Talent shownTalent;
    // Update is called once per frame
    void Update()
    {
        bool ShouldShow = ShouldDisplay();

        if (ShouldShow)
        {
            if (focusTalent == null)
            {
                ShowUnitAndTile(focus, focusTile);
            }
            else
            {
                ShowTalent(focusTalent);
            }
            int index = MOUSE_ON_RIGHT_SIDE_OF_SCREEN() ? 0 : 1;
            for (int i = 0; i < panels.Count; i += 1)
            {
                panels[i].ui.SetActive(index == i);
            }
        }
        else
        {
            for (int i = 0; i < panels.Count; i += 1)
            {
                panels[i].ui.SetActive(false);
            }
        }

        

    }

    public bool MOUSE_ON_RIGHT_SIDE_OF_SCREEN()
    {
        return ((Input.mousePosition.x / Screen.width) > 0.5f);
    }

    internal void FocusMaybe(Talent p, bool b)
    {
        if (b) focusTalent = p;
        else focusTalent = null;
    }

    private void ShowTalent(Talent newTalent)
    {
        if (shownTalent == newTalent) return;
        shownFocus = null;
        shownTile = null;
        shownTalent = newTalent;
        foreach (var panel in panels)
        {
            panel.UnitContainer.SetActive(false);
        }
        P_ShowBuffs(newTalent);
    }

    private void LateUpdate() {
        //HintMain.transform.position = Input.mousePosition.x / Screen.width > 0.45f ?
        //    lSide.position : rSide.position;
    }

    private void ShowUnitAndTile(MapUnit newFocus, MapTile newFocusTile)
    {
        if (newFocus == shownFocus && shownTile == newFocusTile) return;
        shownFocus = newFocus;
        shownTile = newFocusTile;
        shownTalent = null;


        List<KeyValuePair<Talent, int?>> BuffsToDisplay = new List<KeyValuePair<Talent, int?>>();
        Func<Talent, KeyValuePair<Talent, int?>> ToPair = (t) => new KeyValuePair<Talent, int?>(t, null);

        // Only do this if we're showing a unit! otherwise, deactivate the unit stuff
        if (newFocus == null)
        {
            foreach (var panel in panels)
                panel.UnitContainer.SetActive(false);
        }
        else
        {
            foreach (var panel in panels) { 
                panel.UnitContainer.SetActive(true);
                panel.unitImageDisplay.sprite = newFocus.data.presentation.MapSprite;
                panel.unitImageDisplay.SetNativeSize();
                panel.unitNameTextArea.text = newFocus.data.CharacterName;
                panel.unitHealthTextArea.text = "hp: " + (newFocus.MaxHP() - newFocus.data.currentDamage) + "/" + newFocus.MaxHP();
            }
            BuffsToDisplay.AddRange(newFocus.data.IntrinsicTalents.Where(i => i != null && i.ShownAsBuff).Select(ToPair));
            BuffsToDisplay.AddRange(newFocus.data.inventory.Where(i => i.ShownAsBuff).Select(ToPair));
            BuffsToDisplay.AddRange(newFocus.currentBuffs.Select(b => new KeyValuePair<Talent, int?>(b.buff, b.stacks)));
        }

        BuffsToDisplay.AddRange(newFocusTile.Intrinsics.Select(ToPair));
        BuffsToDisplay.AddRange(newFocusTile.currentBuffs.Select(b => new KeyValuePair<Talent, int?>(b.buff, b.ShownMapStacks(b.buff))));
        P_ShowBuffsWithStacks( BuffsToDisplay.ToArray());
    }
    private void P_ShowBuffs(params Talent[] BuffsToDisplay)
    {
        P_ShowBuffsWithStacks(BuffsToDisplay.Select(btd => new KeyValuePair<Talent, int?>(btd, null)).ToArray());
    }
    private void P_ShowBuffsWithStacks(params KeyValuePair<Talent, int?>[] BuffsToDisplay)
    {
        foreach (var panel in panels)
        {
            foreach (var row in panel.rows)
            {
                Destroy(row.gameObject);
            }
            panel.rows.Clear();

            foreach (var buff in BuffsToDisplay)
            {
                var newRow = Instantiate(rowPrefab, panel.Main);
                newRow.displayTalent(buff.Key, buff.Value);
                newRow.gameObject.SetActive(true);
                panel.rows.Add(newRow);
            }
        }
    }

    private bool ShouldDisplay()
    {
        return focus != null || (focusTile != null && focusTile.AllCurrentEffects().Any()) || (focusTalent != null);
    }
}
